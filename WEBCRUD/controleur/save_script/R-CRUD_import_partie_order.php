<?php
try{
$bdd = new PDO('mysql:host=localhost;dbname=prestashop;charset=utf8', 'root', '');
}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}

	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	   //																																			  ///////
	  //-----------------------------------------------------------// PARTIE PRESTASHOP //-----------------------------------------------------------///////
	 //																																				///////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
				//connection de l'api au webservice
				include("connection.php");
				
					//on recupere le fichier xml
					$get_xml_orders = simplexml_load_file("orders.xml");	
					$get_xml_carts = simplexml_load_file("carts.xml");	
	
	//-----------------------------------------------// CUSTOMER //---------------------------------//
	foreach ($get_xml_orders->children()->children() as $ressources_cart)
	{
		try {
				$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); //connection
				$xml_cart = $webService->get(array('url' => PS_SHOP_PATH.'/api/carts?schema=synopsis'));//on recupere un shemas blanc xml
							
				$opt_customers['resource'] = 'customers';
				$xml_customers = $webService->get($opt_customers);
				$resources_customer_id = $xml_customers->children()->children()->id;

				$opt_addresses['resource'] = 'addresses';
				$xml_address = $webService->get($opt_addresses);
				$resources_address_id = $xml_address->children()->children()->id;

				$opt_currencies['resource'] = 'currencies';
				$xml_currency = $webService->get($opt_currencies);
				$resources_currency_id = $xml_currency->children()->children()->id;	

				$opt_languages['resource'] = 'languages';
				$xml_lang = $webService->get($opt_languages);
				$resources_lang_id = $xml_lang->children()->children()->id;

				$opt_carriers['resource'] = 'carriers';
				$xml_carrier = $webService->get($opt_carriers);
				$resources_carrier_id = $xml_carrier->children()->children()->id;

				$opt_products['resource'] = 'products';
				$xml_product = $webService->get($opt_products);
				$resources_product_id = $xml_product->children()->children()->id;	
				$resources_product_quantity = $xml_product->children()->children()->quantity;	

				while($xml_customers->children()->children()->outstanding_allow_amount > 0)
				{
						
						$xml_cart->cart->id_customer = $resources_customer_id;
						$xml_cart->cart->id_address_delivery = $resources_address_id;
						$xml_cart->cart->id_address_invoice = $resources_address_id;
						$xml_cart->cart->id_currency = $resources_currency_id;
						$xml_cart->cart->id_lang = $resources_lang_id;
						$xml_cart->cart->id_carrier = $resources_carrier_id;
						
						$xml_cart->cart->associations->cart_rows->cart_row->id_product = $resources_product_id;
						$xml_cart->cart->associations->cart_rows->cart_row->quantity = $resources_product_quantity;
						
						
						$opt_orders = array('resource' => 'carts');
						$opt_orders['postXml'] = $xml_cart->asXML();

						// var_dump($opt);
						
						//envoie le xml a prestashop
						$xml_orders = $webService->add($opt_orders);

						$monid_orders = $xml_orders->customer->id;//récupere l'id inséré dans prestashop par auto increment
						$bdd->query('UPDATE `prestashop`.`ps_order` SET `id_order` = '.$id_orders.' WHERE `ps_order`.`id_order` = '.$monid_orders.'');//envoie la nouvelle id				
				}	
			}
			catch (PrestaShopWebserviceException $e)
			{
			  // Here we are dealing with errors
			  $trace = $e->getTrace();
			  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
			  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
			  else echo 'Other error<br />'.$e->getMessage();
			}
	}

	
	// foreach ($get_xml_orders->children()->children() as $ressources_orders)
	// {
		// try {
				// $webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); //connection
				// $xml_orders = $webService->get(array('url' => PS_SHOP_PATH.'/api/orders?schema=synopsis'));//on recupere un shemas blanc xml


				// données obligatoires dans mon xml	
				// $id_orders 					         					= $ressources_orders->id;
				// $id_address_delivery_orders  					        = $ressources_orders->id_address_delivery;
				// $id_address_invoice_orders  					        = $ressources_orders->id_address_invoice;
				// $id_cart_orders  					         			= $ressources_orders->id_cart;
				// $id_currency_orders  					         		= $ressources_orders->id_currency;
				// $id_lang_orders 		 			         			= $ressources_orders->id_lang;
				// $id_customer_orders 					         		= $ressources_orders->id_customer;
				// $id_carrier_orders 					         			= $ressources_orders->id_carrier;
				// $current_state_orders 					         		= $ressources_orders->current_state;
				// $module_orders 					         				= $ressources_orders->module;
				// $invoice_number_orders 					         		= $ressources_orders->invoice_number;
				// $invoice_date_orders 					         		= $ressources_orders->invoice_date;
				// $delivery_number_orders 					         	= $ressources_orders->delivery_number;
				// $delivery_date_orders 					         		= $ressources_orders->delivery_date;
				// $valid_orders 					         				= $ressources_orders->valid;
				// $date_add_orders 					         			= $ressources_orders->date_add;
				// $date_upd_orders 					         			= $ressources_orders->date_upd;
				// $shipping_number_orders 					         	= $ressources_orders->shipping_number;
				// $id_shop_group_orders 					         		= $ressources_orders->id_shop_group;
				// $id_shop_orders 					         			= $ressources_orders->id_shop;
				// $secure_key_orders 					         			= $ressources_orders->secure_key;
				// $payment_orders 					         			= $ressources_orders->payment;
				// $recyclable_orders 					         			= $ressources_orders->recyclable;
				// $gift_orders 					         				= $ressources_orders->gift;
				// $gift_message_orders 					         		= $ressources_orders->gift_message;
				// $mobile_theme_orders 					         		= $ressources_orders->mobile_theme;
				// $total_discounts_orders 					         	= $ressources_orders->total_discounts;
				// $total_discounts_tax_incl_orders 					    = $ressources_orders->total_discounts_tax_incl;
				// $total_discounts_tax_excl_orders 					    = $ressources_orders->total_discounts_tax_excl;
				// $total_paid_orders 					         			= $ressources_orders->total_paid;
				// $total_paid_tax_incl_orders 					        = $ressources_orders->total_paid_tax_incl;
				// $total_paid_tax_excl_orders 					        = $ressources_orders->total_paid_tax_excl;
				// $total_paid_real_orders 					         	= $ressources_orders->total_paid_real;
				// $total_products_orders 					         		= $ressources_orders->total_products;
				// $total_products_wt_orders 					         	= $ressources_orders->total_products_wt;
				// $total_shipping_orders 					         		= $ressources_orders->total_shipping;
				// $total_shipping_tax_incl_orders 					    = $ressources_orders->total_shipping_tax_incl;
				// $total_shipping_tax_excl_orders 					    = $ressources_orders->total_shipping_tax_excl;
				// $carrier_tax_rate_orders 					         	= $ressources_orders->carrier_tax_rate;
				// $total_wrapping_orders 					         		= $ressources_orders->total_wrapping;
				// $total_wrapping_tax_incl_orders 					    = $ressources_orders->total_wrapping_tax_incl;
				// $total_wrapping_tax_excl_orders 					    = $ressources_orders->total_wrapping_tax_excl;
				// $round_mode_orders 					         			= $ressources_orders->round_mode;
				// $round_type_orders 					         			= $ressources_orders->round_type;
				// $conversion_rate_orders 					         	= $ressources_orders->conversion_rate;
				// $reference_orders 					         			= $ressources_orders->reference;

				
				// $xml_orders->order->id_address_delivery  				       	= $id_address_delivery_orders;
				// $xml_orders->order->id_address_invoice  				       	= $id_address_invoice_orders;
				// $xml_orders->order->id_cart  				        	   		= $id_cart_orders;
				// $xml_orders->order->id_currency  				        	   	= $id_currency_orders;
				// $xml_orders->order->id_lang  				        	   		= $id_lang_orders;
				// $xml_orders->order->id_customer  				        	   	= $id_customer_orders;
				// $xml_orders->order->id_carrier  				        	   	= $id_carrier_orders;
				// $xml_orders->order->current_state  				        	   	= $current_state_orders;
				// $xml_orders->order->module  				        	   		= $module_orders;
				// $xml_orders->order->invoice_number  				        	= $invoice_number_orders;
				// $xml_orders->order->invoice_date  				        	   	= $invoice_date_orders;
				// $xml_orders->order->delivery_number  				        	= $delivery_number_orders;
				// $xml_orders->order->delivery_date  				        	   	= $delivery_date_orders;
				// $xml_orders->order->valid  				        	   			= $valid_orders;
				// $xml_orders->order->date_add  				        	   		= $date_add_orders;
				// $xml_orders->order->date_upd  				        	   		= $date_upd_orders;
				// $xml_orders->order->shipping_number  				        	= $shipping_number_orders;
				// $xml_orders->order->id_shop_group  				        	   	= $id_shop_group_orders;
				// $xml_orders->order->id_shop  				        	   		= $id_shop_orders;
				// $xml_orders->order->secure_key  				        	   	= $secure_key_orders;
				// $xml_orders->order->payment  				        	   		= $payment_orders;
				// $xml_orders->order->recyclable  				        	   	= $recyclable_orders;
				// $xml_orders->order->gift  				        	   			= $gift_orders;
				// $xml_orders->order->gift_message  				        	   	= $gift_message_orders;
				// $xml_orders->order->mobile_theme  				        	   	= $mobile_theme_orders;
				// $xml_orders->order->total_discounts  				        	= $total_discounts_orders;
				// $xml_orders->order->total_discounts_tax_incl  				    = $total_discounts_tax_incl_orders;
				// $xml_orders->order->total_discounts_tax_excl  				    = $total_discounts_tax_excl_orders;
				// $xml_orders->order->total_paid  				        	   	= $total_paid_orders;
				// $xml_orders->order->total_paid_tax_incl  				        = $total_paid_tax_incl_orders;
				// $xml_orders->order->total_paid_tax_excl  				        = $total_paid_tax_excl_orders;
				// $xml_orders->order->total_paid_real  				        	= $total_paid_real_orders;
				// $xml_orders->order->total_products  				        	= $total_products_orders;
				// $xml_orders->order->total_products_wt  				        	= $total_products_wt_orders;
				// $xml_orders->order->total_shipping  				        	= $total_shipping_orders;
				// $xml_orders->order->total_shipping_tax_incl  				    = $total_shipping_tax_incl_orders;
				// $xml_orders->order->total_shipping_tax_excl  				    = $total_shipping_tax_excl_orders;
				// $xml_orders->order->carrier_tax_rate  				        	= $carrier_tax_rate_orders;
				// $xml_orders->order->total_wrapping  				        	= $total_wrapping_orders;
				// $xml_orders->order->total_wrapping_tax_incl  				    = $total_wrapping_tax_incl_orders;
				// $xml_orders->order->total_wrapping_tax_excl  				   	= $total_wrapping_tax_excl_orders;
				// $xml_orders->order->round_mode  				        	   	= $round_mode_orders;
				// $xml_orders->order->round_type  				        	   	= $round_type_orders;
				// $xml_orders->order->conversion_rate  				        	= $conversion_rate_orders;
				// $xml_orders->order->reference  				        	   		= $reference_orders;
				
				// foreach ($ressources_orders->associations->order_rows->children() as $ressources_orders_assos)
				// {				
				
					// $id_assos_orders 					         			= $ressources_orders_assos->id;
					// $product_id_assos_orders 					         	= $ressources_orders_assos->product_id;
					// $product_attribute_id_assos_orders  					= $ressources_orders_assos->product_attribute_id;
					// $product_quantity_assos_orders 					        = $ressources_orders_assos->product_quantity;
					// $product_name_assos_orders 					         	= $ressources_orders_assos->product_name;
					// $product_reference_assos_orders 					    = $ressources_orders_assos->product_reference;
					// $product_ean13_assos_orders 					        = $ressources_orders_assos->product_ean13;
					// $product_upc_assos_orders 					         	= $ressources_orders_assos->product_upc;
					// $product_price_assos_orders 					        = $ressources_orders_assos->product_price;
					// $unit_price_tax_incl_assos_orders 					    = $ressources_orders_assos->unit_price_tax_incl;
					// $unit_price_tax_excl_assos_orders 					    = $ressources_orders_assos->unit_price_tax_excl;
					
					// $xml_orders->order->associations->order_rows->order_row->id 					= $id_assos_orders;
					// $xml_orders->order->associations->order_rows->order_row->product_id 			= $product_id_assos_orders;
					// $xml_orders->order->associations->order_rows->order_row->product_attribute_id 	= $product_attribute_id_assos_orders;
					// $xml_orders->order->associations->order_rows->order_row->product_quantity 		= $product_quantity_assos_orders;
					// $xml_orders->order->associations->order_rows->order_row->product_name 			= $product_name_assos_orders;
					// $xml_orders->order->associations->order_rows->order_row->product_reference 		= $product_reference_assos_orders;
					// $xml_orders->order->associations->order_rows->order_row->product_ean13 			= $product_ean13_assos_orders;
					// $xml_orders->order->associations->order_rows->order_row->product_upc 			= $product_upc_assos_orders;
					// $xml_orders->order->associations->order_rows->order_row->product_price 			= $product_price_assos_orders;
					// $xml_orders->order->associations->order_rows->order_row->unit_price_tax_incl 	= $unit_price_tax_incl_assos_orders;
					// $xml_orders->order->associations->order_rows->order_row->unit_price_tax_excl 	= $unit_price_tax_excl_assos_orders;
				// }
				
				// $opt_orders = array('resource' => 'orders');
				// $opt_orders['postXml'] = $xml_orders->asXML();

				// var_dump($opt);
				
				// envoie le xml a prestashop
				// $xml_orders = $webService->add($opt_orders);

				// $monid_orders = $xml_orders->customer->id;//récupere l'id inséré dans prestashop par auto increment
				// $bdd->query('UPDATE `prestashop`.`ps_order` SET `id_order` = '.$id_orders.' WHERE `ps_order`.`id_order` = '.$monid_orders.'');//envoie la nouvelle id				
			// }	
			// catch (PrestaShopWebserviceException $e)
			// {
			  // Here we are dealing with errors
			  // $trace = $e->getTrace();
			  // if ($trace[0]['args'][0] == 404) echo 'Bad ID';
			  // else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
			  // else echo 'Other error<br />'.$e->getMessage();
			// }
	// }
		?>