<?php
/*
 * Module de paiement Paybox par l'�quipe de Magavenue
 * contact@magavenue.com
 * http://www.magavenue.com/blog/
 */
logTxt("Validation auto");

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
ob_get_clean();
include(dirname(__FILE__).'/paybox.php');

$paybox = new Paybox();
$httpDirect = $paybox->httpDirect();

$erreurPaybox = true;
$signatureValide = false;

$erreurPayment ='';
$link = new Link();

if(_PS_VERSION_ >= '1.5')
	Context::getContext()->link = $link;

//Verification des adresses ip du serveur paybox (seulement pour http direct)
if($httpDirect) {
	logTxt("Verification serveur");
	$verification = verification_ip_appelant();
}

if (($httpDirect AND $verification) OR (!$httpDirect)) {
    logTxt("processus de traitement de la requete");
    $pbx_retour = '';
//recuperation des variables envoyes par Paybox
    $vars = $_GET;
    foreach ($vars as $key => $val)
    {
    		if($key == 'controller')
				continue;
			
            if ($key == 'pbx_sign')
                    $pbx_sign = $val;
            else
                    $pbx_retour .= '&' . $key . '=' . $val;
    }
    $pbx_retour = substr($pbx_retour, 1);

    //verification signature :
    if(PbxVerSign($pbx_sign, $pbx_retour, 'clefpublique.pem')!= 1){
            $signatureValide = false;
            logTxt("Erreur lors de la verification de la signature");
            $erreurPayment = 'Erreur lors de la verification de la signature';
    }else {
            $signatureValide = true;
            logTxt("Verification signature valide.");
    }

    //verification du code derreur
    $erreurRequetePaybox = $vars['pbx_error'];
    logTxt("Retour Paybox code erreur :".$erreurRequetePaybox);
    switch($erreurRequetePaybox)
    {
            case '00000':
                    $erreurPaybox = false;
                    logTxt("Code derreur valide");
                    break;
            default:
                    logTxt("Code derreur different de 00000");
                    $erreurPayment ='Code Paybox invalide :'.$erreurRequetePaybox;
                    break;
    }
	
	if($vars['pbx_auth'] == 'XXXXXX' && (int)Configuration::get('PAYBOX_TESTMOD') != 1)
	{
		$erreurPaybox = true;
        logTxt("Retour de test en production");
	}

	if(isset($_GET['ETAT_PBX']) && $_GET['ETAT_PBX'] == 'PBX_RECONDUCTION_ABT')
	{
        logTxt("Reconduction ".$vars['ref']." ".$total);
		exit;
	}


    //si le numero de commande est present, passons au traitement de la commande
    if(isset($vars['ref']))
    {
        $cart = new Cart($vars['ref']);
		if(_PS_VERSION_ >= '1.5'){
				Context::getContext()->id_currency = $cart->id_currency;
				Context::getContext()->cart = $cart;
				Context::getContext()->currency = new Currency((int)$cart->id_currency); 
				Context::getContext()->customer = new Customer((int)$cart->id_customer);
		}
		
        logTxt("Ref Commande : ".$vars['ref']);

        //si le code derreur est bon et que la signature est bonne alors nous pouvons valider la commande
        if(!$erreurPaybox AND $signatureValide)
        {
            logTxt("Validation de la commande");
            $total = floatval(number_format($vars['pbx_amount'], 2, '.', ''))/100;

            /*
             * Verification de l'existence de la commande. Au cas o� celle-ci existe (par exemple dans le cas d'une commande annule, on change son statut en paiement accepte
             */
            $result = Db::getInstance()->getRow('
                    SELECT `id_order`
                    FROM `'._DB_PREFIX_.'orders`
                    WHERE `id_cart` = '.intval($vars['ref']).'');

            if (!$result OR empty($result) OR !key_exists('id_order', $result)){//la commande nexiste pas, on peut la valider
                if(_PS_VERSION_ < '1.4')
                    $paybox->validateOrder($vars['ref'], _PS_OS_PAYMENT_, $cart->getOrderTotal(), $paybox->displayName, 'Abonnement '.$vars['pbx_abonnement'].'<br />Pays : '.$vars['pbx_country'].' - IP : '.$vars['pbx_ip'].' - auto : '.$vars['pbx_auth'].' - trans : '.$vars['pbx_trans'].'<br />');
                else
                    $paybox->validateOrder((int)$vars['ref'], _PS_OS_PAYMENT_, $cart->getOrderTotal(), $paybox->displayName, 'Abonnement '.$vars['pbx_abonnement'].'<br />Pays : '.$vars['pbx_country'].' - IP : '.$vars['pbx_ip'].' - auto : '.$vars['pbx_auth'].' - trans : '.$vars['pbx_trans'].'<br /> Total paiement :'.$total, array(), NULL, false, $cart->secure_key);
            }else { //la commande existe il faut changer son statut.

                   	$order = new Order((int)$result['id_order']);
					$order->current_state = _PS_OS_PAYMENT_;
					$order->save();
                    $history = new OrderHistory();
                    $history->id_order = $result['id_order'];
                    $history->changeIdOrderState(_PS_OS_PAYMENT_, $order);
                    $history->addWithemail(true, '');
				
					if(_PS_VERSION_ >= '1.5')
						$order->addOrderPayment($cart->getOrderTotal(), null, $vars['pbx_trans']);

                    /* Modification de la commande */
                    $result = Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'orders` SET `total_paid_real` = "'.$cart->getOrderTotal().'" WHERE `id_order` = '.(int)$result['id_order']);
            }

            if(!$httpDirect) // si il nya pas la validation de commande auto, cest donc le client qui vient de valider sa commande, on le redirige donc vers son historique.
                    Tools::redirectLink(__PS_BASE_URI__.'history.php');
        } else {
            logTxt("Annulation de la commande");
            $paybox->validateOrder(intval($vars['ref']), _PS_OS_ERROR_, 0, $paybox->displayName, $erreurPayment.'<br />');
            if(!$httpDirect) // si il nya pas la validation de commande auto, cest donc le client qui vient de valider sa commande, on le redirige donc vers son historique.
                    Tools::redirectLink(__PS_BASE_URI__.'history.php');
        }
    }
}

function verification_ip_appelant() {
	$unauthorized_server = true;
	//les differentes ips autorises sont en dur pour eviter les modifications par base de donnee
	$authorized_ips = array('195.101.99.76','194.2.122.158','62.39.109.166','194.50.38.6','195.25.7.166','88.190.32.108');

	foreach ($authorized_ips as $authorized_ip) {
		if ($_SERVER['REMOTE_ADDR'] == $authorized_ip) {
			logTxt("serveur autorise");
			return true;
			break;
		}
	}
	if($unauthorized_server){
        logTxt("Serveur non autorise :".$_SERVER['REMOTE_ADDR']);
	}
}

function LoadKey( $keyfile, $pub=true, $pass='' ) {         // chargement de la cl� (publique par d�faut)

    $fp = $filedata = $key = FALSE;                         // initialisation variables
    $fsize =  filesize( $keyfile );                         // taille du fichier
    if( !$fsize ) return FALSE;                             // si erreur on quitte de suite
    $fp = fopen( $keyfile, 'r' );                           // ouverture fichier
    if( !$fp ) return FALSE;                                // si erreur ouverture on quitte
    $filedata = fread( $fp, $fsize );                       // lecture contenu fichier
    fclose( $fp );                                          // fermeture fichier
    if( !$filedata ) return FALSE;                          // si erreur lecture, on quitte
    if( $pub ){
    	logTxt("Recuperation de la clef publique");
        $key = openssl_pkey_get_public( $filedata );        // recuperation de la cle publique
    } else                                                    // ou recuperation de la cle privee
        $key = openssl_pkey_get_private( array( $filedata, $pass ));
    return $key;                                            // renvoi cle ( ou erreur )
}

function PbxVerSign( $sig, $retour , $keyfile ) {                  // verification signature Paybox
	logTxt("Fonction verification signature Paybox");
    $key = LoadKey( $keyfile );                             // chargement de la cle
    if( !$key ){
    	logTxt("Erreur de recuperation de la clef : -1");
    	return -1;                                  // si erreur chargement cle
    }
	$sig = base64_decode($sig);
	$openSsl=openssl_verify($retour, $sig, $key );
    logTxt("Resultat open SS".$openSsl);
    return $openSsl;            // verification : 1 si valide, 0 si invalide, -1 si erreur
}

function logTxt($log){
  $fp = fopen('log.txt','a+');
  fseek($fp,SEEK_END);
  fputs($fp,"##".date("h:i:s A")."##".$log."\r\n");
  fclose($fp);
}
?>