<?php

try{
$bdd = new PDO('mysql:host=localhost;dbname=prestashop;charset=utf8', 'root', '');
}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}
	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	   //																																			  ///////
	  //-----------------------------------------------------------// PARTIE PRESTASHOP //-----------------------------------------------------------///////
	 //																																				///////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				//connection de l'api au webservice
				include("connection.php");

	//on recupere le fichier xml
	$get_cml_groups = simplexml_load_file("groups.xml");
	
	//début de la boucle pour envoyer les customers
	foreach ($get_cml_groups->children()->children() as $ressources_groups)
	{
		try {
				$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); //connection
				$xml_groups = $webService->get(array('url' => PS_SHOP_PATH.'/api/groups?schema=synopsis'));//on recupere un shemas blanc xml


				//autres données sans l'id dans mon xml
				$id_groups  			     					= $ressources_groups->id;
				$reduction_groups 						        = $ressources_groups->reduction;
				$price_display_method_groups   			  		= $ressources_groups->price_display_method;
				$show_prices_groups  							= $ressources_groups->show_prices;
				$date_add_groups 		    		 			= $ressources_groups->date_add;
				$date_upd_groups     	     	   				= $ressources_groups->date_upd;
				$name1_groups  			      	   				= $ressources_groups->name->language[1];
				$name2_groups  			      	   				= $ressources_groups->name->language[2];

				// var_dump($email);

				//on associe les données de mon xml au xml blank créer au debut
				$xml_groups->group->reduction 					            = $reduction_groups;
				$xml_groups->group->price_display_method  					= $price_display_method_groups;
				$xml_groups->group->show_prices 						    = $show_prices_groups;
				$xml_groups->group->date_add 			        			= $date_add_groups;
				$xml_groups->group->date_upd  					            = $date_upd_groups;
				$xml_groups->group->name1 						            = $name1_groups;
				$xml_groups->group->name2 			        				= $name2_groups;

				
				$opt_groups = array('resource' => 'groups');
				$opt_groups['postXml'] = $xml_groups->asXML();

				// var_dump($opt);
				
				//envoie le xml a prestashop
				$xml_groups = $webService->add($opt_groups);

				$monid_groups = $xml_groups->customer->id;//récupere l'id inséré dans prestashop par auto increment
				$bdd_groups->query('UPDATE `prestashop`.`ps_group` SET `id_group` = '.$id_groups.' WHERE `ps_group`.`id_group` = '.$monid_groups.'');//envoie la nouvelle id				
			}	
			catch (PrestaShopWebserviceException $e)
			{
			  // Here we are dealing with errors
			  $trace = $e->getTrace();
			  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
			  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
			  else echo 'Other error<br />'.$e->getMessage();
			}
	}
	echo 'groupe ok','<br>';
	?>