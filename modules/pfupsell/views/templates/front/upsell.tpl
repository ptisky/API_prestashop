{capture name=path}{l s='Accessories' mod='pfupsell'}{/capture}
{if isset($products)}
	<div class="upsell-checkout"><a href="{$link->getPageLink('order')}" class="button">{l s='Proceed to checkout' mod='pfupsell'}</a></div>
	<!-- Products list -->
	<ul id="product_list" class="clear">
		{foreach from=$products item=product name=products}
			<li class="ajax_block_product {if $smarty.foreach.products.first}first_item{elseif $smarty.foreach.products.last}last_item{/if} {if $smarty.foreach.products.index % 2}alternate_item{else}item{/if} clearfix">
				<div class="center_block">			
					<a href="{$link->getProductLink({$product->id_product})|escape:'htmlall':'UTF-8'}" class="product_img_link" title="{$product->name|escape:'htmlall':'UTF-8'}">
						<img src="{$protocol}{$product->img_link}" alt="{if !empty($product->legend)}{$product->legend|escape:'htmlall':'UTF-8'}{else}{$product->name|escape:'htmlall':'UTF-8'}{/if}" title="{if !empty($product->legend)}{$product->legend|escape:'htmlall':'UTF-8'}{else}{$product->name|escape:'htmlall':'UTF-8'}{/if}"/>
						{if isset($product->new) && $product->new == 1}<span class="new">{l s='New' mod='pfupsell'}</span>{/if} 
					</a>
					<h3>
						<a href="{$link->getProductLink({$product->id_product})|escape:'htmlall':'UTF-8'}" title="{$product->name|escape:'htmlall':'UTF-8'}">
							{$product->name|truncate:25:'...'|escape:'htmlall':'UTF-8'}
						</a>
					</h3>
					<p class="product_desc">
						<a href="{$link->getProductLink({$product->id_product})|escape:'htmlall':'UTF-8'}" title="{$product->description_short|strip_tags:'UTF-8'|truncate:36:'...'}">
							{$product->description_short|strip_tags:'UTF-8'|truncate:80:'...'}
						</a>
					</p>
				</div>
				<div class="right_block">
					{if isset($product->on_sale) && $product->on_sale && isset($product->show_price) && $product->show_price && !$PS_CATALOG_MODE}
						<span class="on_sale">{l s='On sale!' mod='pfupsell'}</span>
					{elseif isset($product->reduction) && $product->reduction && isset($product->show_price) && $product->show_price && !$PS_CATALOG_MODE}
						<span class="discount">{l s='Reduced price!' mod='pfupsell'}</span>
					{/if}
					{if (!$PS_CATALOG_MODE AND ((isset($product->show_price) && $product->show_price) || (isset($product->available_for_order) && $product->available_for_order)))}
						<div class="content_price">
							{if isset($product->show_price) && $product>show_price && !isset($restricted_country_mode)}
								<span class="price">
									{if !$priceDisplay}
										{convertPrice price=$product->price}
									{else}
										{convertPrice price=$product->price_tax_exc}
									{/if}
								</span>
							{/if}
							{if isset($product->available_for_order) && $product->available_for_order && !isset($restricted_country_mode)}
								<span class="availability">
									{if ($product->allow_oosp || $product->quantity > 0)}
										{l s='Available' mod='pfupsell'}
									{elseif (isset($product->quantity_all_versions) && $product->quantity_all_versions > 0)}
										{l s='Product available with different options' mod='pfupsell'}
									{else}
										<span class="warning_inline">{l s='Out of stock' mod='pfupsell'}</span>
									{/if}
								</span>
							{/if}
						</div>
						{if isset($product->online_only) && $product->online_only}<span class="online_only">{l s='Online only' mod='pfupsell'}</span>{/if}
					{/if}
					
					{if ($product->id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product->available_for_order && !isset($restricted_country_mode) && $product->minimal_quantity <= 1 && $product->customizable != 2 && !$PS_CATALOG_MODE}
						{if ($product->allow_oosp || $product->quantity > 0)}
							{if isset($static_token)}
								<a class="button ajax_add_to_cart_button exclusive" rel="ajax_id_product_{$product->id_product|intval}" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product->id_product|intval}&amp;token={$static_token}", false)|escape:'html'}" title="{l s='Add to cart' mod='pfupsell'}"><span></span>{l s='Add to cart' mod='pfupsell'}</a>
							{else}
								<a class="button ajax_add_to_cart_button exclusive" rel="ajax_id_product_{$product->id_product|intval}" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product->id_product|intval}", false)|escape:'html'}" title="{l s='Add to cart' mod='pfupsell'}"><span></span>{l s='Add to cart' mod='pfupsell'}</a>
							{/if}						
						{else}
							<span class="exclusive"><span></span>{l s='Add to cart' mod='pfupsell'}</span>
						{/if}
					{/if}
					<a class="button lnk_view" href="{$link->getProductLink({$product->id_product})|escape:'htmlall':'UTF-8'}" title="{l s='View' mod='pfupsell'}">{l s='View' mod='pfupsell'}</a>	
				</div><!-- right block end -->
			</li>
		{/foreach}
	</ul>
	<div class="upsell-checkout"><a href="{$link->getPageLink('order')}" class="button">{l s='Proceed to checkout' mod='pfupsell'}</a></div>
{/if}
