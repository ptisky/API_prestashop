<?php
include("connection.php");	
function export_API($ressource)
{
	try
	{
		$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); //met la connection dans une variable	
	}
	catch (PrestaShopWebserviceException $e)
	{					
		$trace = $e->getTrace(); // On affiche les erreurs
		if ($trace[0]['args'][0] == 404) echo 'Bad ID';
		else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
		else echo 'Other error';
	}			
	try
	{
		$opt['resource'] = $ressource;  //on récupére la ressource que l'on veux
		$opt['display'] = 'full';		//full = récuperer tout les champs
		$xml = $webService->get($opt);	//afficher le resultat obtenue de prestashop grace a 'get'
		
		$opt = array('resource' => $ressource); 	//créer un tableau des résultats
		$opt['putXml'] = $xml->asXML($ressource.'.xml');//création d'un xml			
		$xml = $webService->edit($opt);//envoie le xml	
	}				
	catch (PrestaShopWebserviceException $ex)
	{
		$trace = $ex->getTrace();//affiche les erreurs
	}	
}//fin fonction

export_API("products"); //execute la fonction
export_API('categories'); //execute la fonction
export_API('product_features'); //execute la fonction
export_API('manufacturers'); //execute la fonction
export_API('suppliers'); //execute la fonction
export_API('tags'); //execute la fonction
export_API('combinations'); //execute la fonction
export_API("product_option_values"); //execute la fonction
export_API('customers'); //execute la fonction
export_API("addresses"); //execute la fonction
?>