<?php

/**

*

*  @package e-legend pro

*  @author    Abouloula Taoufik - E-LEGEND PRO

*  @version    1.0

*  @copyright Copyright (c) Abouloula Taoufik - E-LEGEND PRO

*

*/



include_once('manufacturer.php');

include_once('attribute.php');

include_once('combination.php');



class Product

{



	

	

	public function getApi()

	{

		$sql = Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'pro_elegend_api where id=1');

        

		return $sql;

	}

	

	public function getMaxIdProduct()

	{

		$sql = Db::getInstance()->getValue('SELECT max(`id_product`) FROM '._DB_PREFIX_.'product');

		return $sql + 1;



	}

	

	public function getMaxIdProductAttribut()

	{

		$sql = Db::getInstance()->getValue('SELECT max(`id_product_attribute`) FROM '._DB_PREFIX_.'product_attribute');

		return $sql + 1;



	}

	

	public function getMaxPositionCategoryProduct($id_category)

	{

		$sql = Db::getInstance()->getValue('SELECT max(`position`) FROM '._DB_PREFIX_.'category_product WHERE id_category ='.$id_category);

		return $sql + 1;



	}

	

	public function addCategoryProduct($id_category,$id_product,$position)

	{

		

		$id_parent = Db::getInstance()->getValue('SELECT id_parent FROM '._DB_PREFIX_.'category WHERE id_category ='.$id_category);

		if($id_parent)

		Db::getInstance()->ExecuteS('INSERT INTO '._DB_PREFIX_.'category_product (id_category, id_product, position) VALUES ( '.$id_parent.', '.$id_product.', '.$position.' )');

		Db::getInstance()->ExecuteS('INSERT INTO '._DB_PREFIX_.'category_product (id_category, id_product, position) VALUES ( '.$id_category.', '.$id_product.', '.$position.' )');



	}

	

	public function getCarrier()

	{

		$sql = Db::getInstance()->getValue('SELECT id_reference FROM '._DB_PREFIX_.'carrier WHERE external_module_name = "expedition_ell" AND active=1 AND deleted=0');

		return $sql;



	}

	

	public function addCarrierProduct($id_product,$id_carrier,$id_shop)

	{

		

		return Db::getInstance()->ExecuteS('INSERT INTO '._DB_PREFIX_.'product_carrier (id_product, id_carrier_reference, id_shop) VALUES ( '.$id_product.', '.$id_carrier.', '.$id_shop.' )');





	}	

	public function getParamEl($name)

	{

		return Db::getInstance()->getValue('SELECT `value` FROM `'._DB_PREFIX_.'pro_elegend_param` WHERE name="'.pSQL($name).'"');

	}

	

	public function create_product_image($pid,$idImage){

		

		$id_image = $idImage;

		$folders = str_split((string)$id_image);

		$pat = implode('/', $folders).'/'.$id_image.'.jpg';

        $image = new Image(0);

        $image->id_product = $pid;					

        $image->cover = 1;

        $image->save();							  

        $image->createImgFolder();

        $imgTypes = ImageType::getImagesTypes('products');

        $image_base = _PS_PROD_IMG_DIR_.$image->getExistingImgPath().'.'.$image->image_format;

		

        $url = "".$this->getParamEl('EL_URL')."img/p/".$pat;

        @copy($url, $image_base);

        foreach($imgTypes as $value){

            $t = ImageManager::resize($image_base, _PS_PROD_IMG_DIR_.$image->getExistingImgPath().'-'.stripslashes($value['name']).'.'.$image->image_format,$value['width'],$value['height']);

         }

   }

	

	

	

	/**

	 * mettreajourCatalog Insertion catalogue

	 *

	 * @param string $id_category id de la categorie	 *

	 * @return true

	 */ 

	

	



   public function _products($id_product,$id_category)

	{		

		

		

		

		$row = $this->getApi();		

		define('DEBUG', false);

        define('PS_SHOP_PATH', $row['adresse']);

        define('PS_WS_AUTH_KEY', $row['cleapi']);

		$combinations= new Combination();

		$attributes= new Attribute();

		$manufacturer = new Manufacturer();

		

		





	try

	{	

     

		$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); 

		$opt['resource'] = 'products';	

		$opt['id'] = (int)$id_product; 

		$xml = $webService->get($opt);

		$p = $xml->children()->children();

		

		if($p->active == 1){

		$opt_man = array('resource' => 'manufacturers');

		$opt_man['id'] = (int)$p->id_manufacturer; 

		$xml_man = $webService->get($opt_man);

		

		/*$opt_man = array('url' => PS_SHOP_PATH . 'api/manufacturers/'.$p->id_manufacturer);			

		$xml_man = $webService->get($opt_man);	*/

		$man = $xml_man->children()->children();

	    $getNameManufacturer = $manufacturer->getNameManufacturer($man->name);

		if($getNameManufacturer == 0)

	    $id_manufacture = $manufacturer->_manufacturer($p->id_manufacturer);

		else

		 $id_manufacture = $manufacturer->getIdManufacturer($man->name);

		

		 

	

		



		

		// insertion  table category 	

		$idProduct = $this->getMaxIdProduct();

		$data['id_product'] = $idProduct;

        $data['active'] = 0;

		$data['id_category_default'] = $id_category;

		$data['id_shop_default'] = 1;

		$data['id_supplier'] = 0;

        $data['id_manufacturer'] = $id_manufacture;

        $data['ean13'] = $p->ean13;

        $data['upc'] = $p->upc;

		$data['id_tax_rules_group'] = 0;

		$data['on_sale'] = 0;

		$data['online_only'] = 0;

		$data['reference'] = $p->reference;

		$data['out_of_stock'] = 1;

		$data['available_for_order'] = 1;

		$data['show_price'] = 1;

		$data['indexed'] = 1;

		$data['condition'] = $p->condition;

		$data['available_date'] = pSQL(date('Y-m-d H:i:s'));

		$data['date_add'] = pSQL(date('Y-m-d H:i:s'));

		$data['date_upd'] = pSQL(date('Y-m-d H:i:s'));

		$data['unity'] = '';

		



        // insertion  table product_lang 		

		

		$datal['id_product'] = $idProduct;

        $datal['id_shop'] = 1;

        $datal['id_lang'] = 1;       

        $datal['description'] = str_replace("'", "''", urldecode($p->description->language[0][0]));

		$datal['description_short'] = @str_replace("'", "''", urldecode($p->description_short->language[0][0]));

        $datal['link_rewrite'] = @str_replace("'", "''", urldecode($p->link_rewrite->language[0][0])); 

		$datal['meta_description'] = @str_replace("'", "''", urldecode($p->meta_description->language[0][0]));

		$datal['meta_keywords'] = @str_replace("'", "''", urldecode($p->meta_keywords->language[0][0]));

        $datal['meta_title'] = @str_replace("'", "''", urldecode($p->meta_title->language[0][0]));

		$datal['name'] = @str_replace("'", "''", urldecode($p->name->language[0][0]));       

        $datal['available_now'] = $p->available_now->language[0][0];

        $datal['available_later'] = $p->available_later->language[0][0];

		

		

		

		// insertion table product_shop 

        $dataShop['id_product'] = $idProduct;

        $dataShop['active'] = 0;

		$dataShop['id_category_default'] = $id_category;

		$dataShop['id_shop'] = 1;

		$dataShop['id_tax_rules_group'] = 0;

		$dataShop['on_sale'] = 0;

		$dataShop['online_only'] = 0;

		//$dataShop['price'] = $price;

		//$dataShop['wholesale_price'] = $price_tmp;

		$dataShop['available_for_order'] = 1;

		$dataShop['condition'] = $p->condition;

		$dataShop['show_price'] = 1;

		$dataShop['indexed'] = 1;

		$dataShop['available_date'] = pSQL(date('Y-m-d H:i:s'));

		$dataShop['date_add'] = pSQL(date('Y-m-d H:i:s'));

		$dataShop['date_upd'] = pSQL(date('Y-m-d H:i:s'));

		$dataShop['unity'] = '';       



        if(!DB::getInstance()->insert('product', $data)) echo('Error in product insert : '.$p->id);

        if(!DB::getInstance()->insert('product_lang', $datal)) echo('Error in product lang insert : '.$p->id);

        if(!DB::getInstance()->insert('product_shop', $dataShop)) echo('Error in product shop insert : '.$p->id);

		

		$id_cat = $p->associations->categories->category;

        if (count($id_cat)) {

         foreach ( $id_cat as $value ) {

		

		$position = $this->getMaxPositionCategoryProduct($id_category);

		$this->addCategoryProduct($id_category,$idProduct,$position);

		

	     }

		}

		

		$id_carrier = $this->getCarrier();

		$this->addCarrierProduct($idProduct,$id_carrier,1);

		

		

		$this->create_product_image($idProduct,$p->associations->images->image->id);

		// insertion  table category_group 

		

		//$attribute = $p->associations->product_option_values->product_option_values;

		$attributs = $p->associations->product_option_values->product_option_value;

			  if (count($attributs)) {

			   foreach ($attributs as $attribut) {

				  /* $opt_att = array('resource' => 'product_option_values');

		          $opt_att['id'] = (int)$attribut->id; 

		          $xml_att = $webService->get($opt_att);*/

				  

			  $opt_att = array('url' => PS_SHOP_PATH . 'api/product_option_values/'.$attribut->id);			

		      $xml_att = $webService->get($opt_att);	

		      $att = $xml_att->children()->children();

	          $attributes->_attribute($att->id_attribute_group);

			   }

			  }

		 



		//foreach($p->associations->combinations->combinations as $combinations){	

        $products = $p->associations->combinations->combination;



        if (count($products)) {

         foreach ( $products as $value ) {

		

		$combinations->_combinations($value->id,$idProduct);

			

		

	     }

		 $combinations->calculQuantity($idProduct);

		 

		 

		}

		}

	

	}

	catch (PrestaShopWebserviceException $e)

	{

					

			$trace = $e->getTrace();

			if ($trace[0]['args'][0] == 404) echo 'Bad ID Product';

			else if ($trace[0]['args'][0] == 401) echo 'Bad auth key Product';

			else echo 'Other error'.$e->getMessage().'</b>';

	}			

			return true;

		

	}

	

	

	

} //Fin class Catalog

?>