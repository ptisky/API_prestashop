{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if isset($products)}
	<!-- Products list -->
	<ul id="product_list" class="clear">
	{counter start=0 assign=compteur}
	        {foreach from=$products key=index item=product name=products}
	        {counter}

			<li class="ajax_block_product {if $compteur == 4 || $compteur == 10 } quat {/if} {if $smarty.foreach.products.first}first_item{elseif $smarty.foreach.products.last}last_item{/if} {if $smarty.foreach.products.index % 2}alternate_item{else}item{/if} {if $compteur == 3 || $compteur == 7 } decaler {/if}">

		     {if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}

             <a href="{$product.link|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'htmlall':'UTF-8'}"> <div class="price-list">{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}<span class="price-prod">{convertPrice price=$product.price_tax_exc} HT</span>{/if}</div></a>
             {/if}
             <div class="list-title">
			 <a href="{$product.link|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'htmlall':'UTF-8'}">{$product.name|escape:'htmlall':'UTF-8'}</a>
			</div>
			<!--p class="entry-price price_container"-->
			<!--p>
			{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}<span class="price" style="display: inline;">{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}</span><br />{/if}
			{if isset($product.available_for_order) && $product.available_for_order && !isset($restricted_country_mode)}<span class="availability">{if ($product.allow_oosp || $product.quantity > 0)}{l s='Available'}{elseif (isset($product.quantity_all_versions) && $product.quantity_all_versions > 0)}{l s='Product available with different options'}{else}<span class="warning_inline">{l s='Out of stock'}</span>{/if}</span>{/if}
            </p-->
		    <div class="center_block products">
		    <a href="{$product.link|escape:'htmlall':'UTF-8'}" class="product_img_link" title="{$product.name|escape:'htmlall':'UTF-8'}">
			    <div class="image-res">
				
				<img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html'}" alt="{if !empty($product.legend)}{$product.legend|escape:'htmlall':'UTF-8'}{else}{$product.name|escape:'htmlall':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'htmlall':'UTF-8'}{else}{$product.name|escape:'htmlall':'UTF-8'}{/if}" {if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if} />
				{* if isset($product.new) && $product.new == 1}<span class="new">{l s='New'}</span>{/if *}
				
			    </div>
            </a>
                <div class="list-button">
                		{* if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.minimal_quantity <= 1 && $product.customizable != 2 && !$PS_CATALOG_MODE *}
                		{if $logged}
					<!--/*{if ($product.allow_oosp || $product.quantity > 0)}
						{if isset($static_token)}
						<a class="button ajax_add_to_cart_button exclusive" rel="ajax_id_product_{$product.id_product|intval}" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}&amp;token={$static_token}", false)|escape:'html'}" title="{l s='Add to cart'}"><span></span>{l s='Add to cart'}</a>
						{else}
				        <a class="button ajax_add_to_cart_button exclusive" rel="ajax_id_product_{$product.id_product|intval}" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}", false)|escape:'html'}" title="{l s='Add to cart'}"><span></span>{l s='Add to cart'}</a>
						{/if}	
						<div id="lof-prod" class="lof-dd{$product.id_product}">{l s='produit ajoutée'}</div>						
				    {else}
						<a class="button ajax_add_to_cart_button exclusive" rel="ajax_id_product_{$product.id_product|intval}"href="" onClick="javascript:$('.noqty'+{$product.id_product}).show('slow').delay(3000).fadeOut('slow');" title="{l s='Add to cart'}" id="not"> 
						<span>{l s='Add to cart'}</span></a><br />
						<span id="noqty" class="noqty{$product.id_product}">{l s='Rupture de stock'}</span>
					{/if}*/-->
					{if ($product.allow_oosp || $product.quantity > 0)}
					{if $product.id_product_attribute == 0}

						{if $product.id_product_attribute|@count == 1}
                     <a class="button ajax_add_to_cart_button exclusive" rel="ajax_id_product_{$product.id_product|intval}" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}&amp;token={$static_token}", false)|escape:'html'}" title="{l s='Add to cart'}" > 
                     <span></span>{l s='Add to cart'}</a>
                      <div id="lof-prod" class="lof-dd{$product.id_product}">{l s='Product added'}</div>
                      	{else}	
						 <a class="button ajax_add_to_cart_button2 exclusive" rel="ajax_id_product_{$product.id_product|intval}" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}&amp;token={$static_token}", false)|escape:'html'}" title="{l s='Add to cart'}">
                         <span></span>{l s='Add to cart'}</a>
						 <div id="lof-prod" class="lof-dd{$product.id_product}">{l s='Product added'}</div>	
						{/if}

					{else}
                     <a class="button ajax_add_to_cart_button exclusive" rel="ajax_id_product_{$product.id_product|intval}" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}&amp;token={$static_token}", false)|escape:'html'}" title="{l s='Add to cart'}"><span></span>{l s='Add to cart'}</a>
						 <div id="lof-prod" class="lof-dd{$product.id_product}">{l s='Product added'}</div>	

					{/if}		

					
					{else}
						<a class="button ajax_add_to_cart_button2 exclusive" rel="ajax_id_product_{$product.id_product|intval}" href="#" onClick="javascript:$('.noqty'+{$product.id_product}).show('slow').delay(4000).fadeOut('slow');" title="{l s='Rupture de stock'}" id="not">  
						<span>{l s='Rupture de stock'}</span></a><br />
						<span id="noqty" class="noqty{$product.id_product}">{l s='Rupture de stock'}</span>
					{/if}
                 {else} 

                 <a class=" ajax_add_to_cart_button "  href="{$link->getPageLink('my-account', true)}">
						<span class="ajax-add-cart sinscr">{l s="S'inscrire pour visualiser les tarifs"}
						<!--div id="pp">&nbsp;</div--></span></a><br />
				{/if}
                </div></div>
                <!--div id="divhover">
				    <div class="product-tit">
				    <a href="{$product.link|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'htmlall':'UTF-8'}">{$product.name|truncate:15:'...'|escape:'htmlall':'UTF-8'}</a>
					</div>
				    <div class="product_dc"><a href="{$product.link|escape:'htmlall':'UTF-8'}" title="{$product.description_short|strip_tags:'UTF-8'|truncate:360:'...'}" >{$product.description_short|strip_tags:'UTF-8'|truncate:150:'...'}</a></div>
				 	</div-->
			  <!--a class="button lnk_view" href="{$product.link|escape:'htmlall':'UTF-8'}" title="{l s='View'}">{l s='View'}</a--></li>
	          {/foreach}
	          </ul>
	  <!-- /Products list -->
{/if}

