<?php
//----------------------  recupere la page de connection au web service --------------
				include("connection.php");			
				try
				{
					$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); //met la connection dans une variable					
					//-------------------------PARTIE CUSTOMERS------------------------------------//récupere par 'get' la ressource présent dans le prestashop
					//customers//
					$optcust['resource'] = 'customers';
					$optcust['display'] = 'full';
					$xmlcust = $webService->get($optcust);

					//adresses//
					$optadd['resource'] = 'addresses';
					$optadd['display'] = 'full';
					$xmladd = $webService->get($optadd);

					//groups//
					$optgro['resource'] = 'groups';
					$optgro['display'] = 'full';
					$xmlgro = $webService->get($optgro);				
					//-------------------------PARTIE PRODUCTS------------------------------------//
					//products//
					$optprod['resource'] = 'products';
					$optprod['display'] = 'full';
					$xmlprod = $webService->get($optprod);

					//categories//
					$optcat['resource'] = 'categories';
					$optcat['display'] = 'full';
					$xmlcat = $webService->get($optcat);
					
					//product_features//
					$optpf['resource'] = 'product_features';
					$optpf['display'] = 'full';
					$xmlpf = $webService->get($optpf);
					
					//manufacturers//
					$optmanu['resource'] = 'manufacturers';
					$optmanu['display'] = 'full';
					$xmlmanu = $webService->get($optmanu);
					
					//suppliers//
					$optsup['resource'] = 'suppliers';
					$optsup['display'] = 'full';
					$xmlsup = $webService->get($optsup);
					
					//tags//
					$opttag['resource'] = 'tags';
					$opttag['display'] = 'full';
					$xmltag = $webService->get($opttag);
					
					//combinations//
					$opt_combinations['resource'] = 'combinations';
					$opt_combinations['display'] = 'full';
					$xml_combinations = $webService->get($opt_combinations);
					
					//stock_availables//
					$opt_stock_availables['resource'] = 'stock_availables';
					$opt_stock_availables['display'] = 'full';
					$xml_stock_availables = $webService->get($opt_stock_availables);
			
					//product_option_values//
					$opt_product_option_values['resource'] = 'product_option_values';
					$opt_product_option_values['display'] = 'full';
					$xml_product_option_values = $webService->get($opt_product_option_values);
					
					//product_option_values//
					$opt_images['resource'] = 'images';
					$opt_images['display'] = 'full';
					$xml_images = $webService->get($opt_images);					
					//-------------------------PARTIE PRODUCTS------------------------------------//
					//orders//
					$optorders['resource'] = 'orders';
					$optorders['display'] = 'full';
					$xmlorders = $webService->get($optorders);
					
					//deliveries//
					$optdeliveries['resource'] = 'deliveries';
					$optdeliveries['display'] = 'full';
					$xmldeliveries = $webService->get($optdeliveries);
					
					//order_carriers//
					$optorder_carriers['resource'] = 'order_carriers';
					$optorder_carriers['display'] = 'full';
					$xmlorder_carriers = $webService->get($optorder_carriers);
					
					//order_details//
					$optorder_details['resource'] = 'order_details';
					$optorder_details['display'] = 'full';
					$xmlorder_details = $webService->get($optorder_details);
					
					//order_discounts//
					$optorder_discounts['resource'] = 'order_discounts';
					$optorder_discounts['display'] = 'full';
					$xmlorder_discounts = $webService->get($optorder_discounts);
					
					//order_histories//
					$optorder_histories['resource'] = 'order_histories';
					$optorder_histories['display'] = 'full';
					$xmlorder_histories = $webService->get($optorder_histories);
					
					//order_invoices//
					$optorder_invoices['resource'] = 'order_invoices';
					$optorder_invoices['display'] = 'full';
					$xmlorder_invoices = $webService->get($optorder_invoices);
					
					//order_payments//
					$optorder_payments['resource'] = 'order_payments';
					$optorder_payments['display'] = 'full';
					$xmlorder_payments = $webService->get($optorder_payments);
					
					//order_slip//
					$optorder_slip['resource'] = 'order_slip';
					$optorder_slip['display'] = 'full';
					$xmlorder_slip= $webService->get($optorder_slip);
					
					//order_states//
					$optorder_states['resource'] = 'order_states';
					$optorder_states['display'] = 'full';
					$xmlorder_states = $webService->get($optorder_states);
					
					//carts//
					$optcarts['resource'] = 'carts';
					$optcarts['display'] = 'full';
					$xmlcarts = $webService->get($optcarts);
				}
				catch (PrestaShopWebserviceException $e)
				{
					// On affiche les erreurs
					$trace = $e->getTrace();
					if ($trace[0]['args'][0] == 404) echo 'Bad ID';
					else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
					else echo 'Other error';
				}			
					try
					{
						$opt = array(
										//customers//
									 'resource' => 'customers' , 
									 'resource' => 'orders' , 
									 'resource' => 'addresses',
									 'resource' => 'groups',								 
										//products//
									 'resource' => 'products', 
									 'resource' => 'categories',
									 'resource' => 'product_features',
									 'resource' => 'manufacturers',
									 'resource' => 'suppliers',
									 'resource' => 'tags',
									 'resource' => 'combinations',
									 'resource' => 'stock_availables',									 
									 'resource' => 'product_option_values',									 
									 'resource' => 'images',
										//ORDERS//
									 'resource' => 'orders', 
									 'resource' => 'deliveries',
									 'resource' => 'order_carriers', 
									 'resource' => 'order_details',	
									 'resource' => 'order_discounts', 
									 'resource' => 'order_histories',	
									 'resource' => 'order_invoices', 
									 'resource' => 'order_payments',
									 'resource' => 'order_slip',										 
									 'resource' => 'order_states',
									 'resource' => 'carts'									 
									 );					
						//-------------------------PARTIE CUSTOMERS------------------------------------//
						$opt['putXml'] = $xmlcust					->asXML('customers.xml');//création d'un xml
						$opt['putXml'] = $xmladd					->asXML('addresses.xml');//création d'un xml
						$opt['putXml'] = $xmlgro					->asXML('groups.xml');//création d'un xml						
						//-------------------------PARTIE PRODUCTS------------------------------------//						
						$opt['putXml'] = $xmlprod					->asXML('products.xml');//création d'un xml
						$opt['putXml'] = $xmlcat					->asXML('categories.xml');//création d'un xml
						$opt['putXml'] = $xmlpf						->asXML('product_features.xml');//création d'un xml
						$opt['putXml'] = $xmlmanu					->asXML('manufacturers.xml');//création d'un xml
						$opt['putXml'] = $xmlsup					->asXML('suppliers.xml');//création d'un xml
						$opt['putXml'] = $xmltag					->asXML('tags.xml');//création d'un xml
						$opt['putXml'] = $xml_combinations			->asXML('combinations.xml');//création d'un xml
						$opt['putXml'] = $xml_stock_availables		->asXML('stock_availables.xml');//création d'un xml
						$opt['putXml'] = $xml_product_option_values	->asXML('product_option_values.xml');//création d'un xml
						$opt['putXml'] = $xml_images				->asXML('images.xml');//création d'un xml
						//-------------------------PARTIE ORDERS------------------------------------//						
						$opt['putXml'] = $xmlorders					->asXML('orders.xml');//création d'un xml
						$opt['putXml'] = $xmldeliveries				->asXML('deliveries.xml');//création d'un xml
						$opt['putXml'] = $xmlorder_carriers			->asXML('order_carriers.xml');//création d'un xml
						$opt['putXml'] = $xmlorder_details			->asXML('order_details.xml');//création d'un xml
						$opt['putXml'] = $xmlorder_discounts		->asXML('order_discounts.xml');//création d'un xml
						$opt['putXml'] = $xmlorder_histories		->asXML('order_histories.xml');//création d'un xml
						$opt['putXml'] = $xmlorder_invoices			->asXML('order_invoices.xml');//création d'un xml
						$opt['putXml'] = $xmlorder_payments			->asXML('order_payments.xml');//création d'un xml
						$opt['putXml'] = $xmlorder_slip				->asXML('order_slip.xml');//création d'un xml
						$opt['putXml'] = $xmlorder_states			->asXML('order_states.xml');//création d'un xml							
						$opt['putXml'] = $xmlcarts					->asXML('carts.xml');//création d'un xml							
						
						$xml = $webService->edit($opt);//envoie le xml
					}				
					catch (PrestaShopWebserviceException $ex)
					{
						$trace = $ex->getTrace();//affiche les erreurs
						if ($trace[0]['args'][0] == 404) echo 'Bad ID';
						else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
						else echo 'Other error<br />'.$ex->getMessage();
					}	
?>