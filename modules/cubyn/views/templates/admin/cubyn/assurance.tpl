{**
 * 2015-2016 Cubyn
 * NOTICE OF LICENSE
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 * DISCLAIMER
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * @author    Cubyn <modules@cubyn.com>
 * @copyright 2015-2016 Cubyn
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *}
<br>
<fieldset id="fieldset_assurance">
    <legend>
       Assurance
    </legend>
    <p class="center">
        Toute commande expédiée via Cubyn est assurée par défault à hauteur de 250€ (inclus dans le prix du service).
        <br>Vous pouvez, si vous le souhaitez, souscrire à une extension de cette garantie en vous connectant à votre interface client Cubyn.
    </p>
    <p class="text-center">
        <a href="http://app.cubyn.com">Accéder à mon interface Client</a>
    </p>
</fieldset>