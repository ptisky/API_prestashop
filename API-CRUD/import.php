<?php

	include("connection.php");
	include("connection_bdd.php");
			
	$productsxml 					= simplexml_load_file("products.xml");
	$categoriesxml 					= simplexml_load_file("categories.xml");
	$product_featurexml 			= simplexml_load_file("product_features.xml");
	$manufacturersxml				= simplexml_load_file("manufacturers.xml");
	$suppliersxml 					= simplexml_load_file("suppliers.xml");
	$tagsxml 						= simplexml_load_file("tags.xml");
	$combinationsxml 				= simplexml_load_file("combinations.xml");
	$product_option_valuesxml 		= simplexml_load_file("product_option_values.xml");
	$get_xml_customers 				= simplexml_load_file("customers.xml");	
	$get_xml_addresses 				= simplexml_load_file("addresses.xml");

	//-----------------------------------------------// fonctions //---------------------------------//	
	function Import_customers()
	{	
	global $get_xml_customers;
	global $bdd;
		foreach ($get_xml_customers->children()->children() as $ressources_customers)
		{
			try 
				{
					$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); //connection
					$xml_customers = $webService->get(array('url' => PS_SHOP_PATH.'/api/customers?schema=synopsis'));//on recupere un shemas blanc xml

					//données obligatoires dans mon xml
					$lastname_customers  					         	= $ressources_customers->lastname;
					$firstname_customers			  			      	= $ressources_customers->firstname;
					$email_customers     						        = $ressources_customers->email;

					//autres données sans l'id dans mon xml
					$id_default_group_customers  			     		= $ressources_customers->id_default_group;
					$id_lang_customers 						        	= $ressources_customers->id_lang;
					$newsletter_date_add_customers   			  		= $ressources_customers->newsletter_date_add;
					$ip_registration_newsletter_customers  				= $ressources_customers->ip_registration_newsletter;
					$last_passwd_gen_customers 		    		 		= $ressources_customers->last_passwd_gen;
					$secure_key_customers     	     	   				= $ressources_customers->secure_key;
					$deleted_customers  			      	   			= $ressources_customers->deleted;
					$passwd_customers 					           		= $ressources_customers->passwd;
					$id_gender_customers     		     	 		   	= $ressources_customers->id_gender;
					$birthday_customers  				      	 		= $ressources_customers->birthday;
					$newsletter_customers 			     	 			= $ressources_customers->newsletter;
					$optin_customers     					  	    	= $ressources_customers->optin;
					$website_customers  					 	     	= $ressources_customers->website;
					$company_customers 						        	= $ressources_customers->company;
					$siret_customers     						        = $ressources_customers->siret;
					$ape_customers  							        = $ressources_customers->ape;
					$outstanding_allow_amount_customers	  		 		= $ressources_customers->outstanding_allow_amount;
					$show_public_prices_customers    		  			= $ressources_customers->show_public_prices;
					$id_risk_customers  						        = $ressources_customers->id_risk;
					$max_payment_days_customers 		   			    = $ressources_customers->max_payment_days;
					$active_customers     			       		 		= $ressources_customers->active;
					$note_customers 						        	= $ressources_customers->note;
					$is_guest_customers 						        = $ressources_customers->is_guest;
					$id_shop_customers     				       	    	= $ressources_customers->id_shop;
					$id_shop_group_customers     			   			= $ressources_customers->id_shop_group;
					$date_add_customers     					   	    = $ressources_customers->date_add;
					$date_upd_customers     					   	    = $ressources_customers->date_upd;
					$id_customers										= $ressources_customers->id;
					$group_customers									= $ressources_customers->associations->groups->group->id;	    

					//on associe les données de mon xml au xml blank créer au debut
					$xml_customers->customer->lastname 					            	= $lastname_customers;
					$xml_customers->customer->firstname  					            = $firstname_customers;
					$xml_customers->customer->email 						            = $email_customers;
					$xml_customers->customer->id_default_group 			        		= $id_default_group_customers;
					$xml_customers->customer->id_lang  					            	= $id_lang_customers;
					$xml_customers->customer->newsletter_date_add 		      			= $newsletter_date_add_customers;
					$xml_customers->customer->ip_registration_newsletter 	  			= $ip_registration_newsletter_customers;
					$xml_customers->customer->last_passwd_gen  			        		= $last_passwd_gen_customers;
					$xml_customers->customer->secure_key 					          	= $secure_key_customers;
					$xml_customers->customer->deleted 					            	= $deleted_customers;
					$xml_customers->customer->passwd  					            	= $passwd_customers;
					$xml_customers->customer->id_gender 					          	= $id_gender_customers;
					$xml_customers->customer->birthday 				            		= $birthday_customers;
					$xml_customers->customer->newsletter  				       			= $newsletter_customers;
					$xml_customers->customer->optin 						            = $optin_customers;
					$xml_customers->customer->website 					            	= $website_customers;
					$xml_customers->customer->company  				            		= $company_customers;
					$xml_customers->customer->siret 					            	= $siret_customers;
					$xml_customers->customer->ape 					              		= $ape_customers;
					$xml_customers->customer->outstanding_allow_amount     				= $outstanding_allow_amount_customers;
					$xml_customers->customer->show_public_prices 	       				= $show_public_prices_customers;
					$xml_customers->customer->id_risk 			            			= $id_risk_customers;
					$xml_customers->customer->max_payment_days 	         				= $max_payment_days_customers;
					$xml_customers->customer->active 					             	= $active_customers;
					$xml_customers->customer->note 					               		= $note_customers;
					$xml_customers->customer->is_guest  			          			= $is_guest_customers;
					$xml_customers->customer->id_shop 				             		= $id_shop_customers;
					$xml_customers->customer->id_shop_group 			        		= $id_shop_group_customers;
					$xml_customers->customer->date_add  				        	   	= $date_add_customers;
					$xml_customers->customer->date_upd 				            		= $date_upd_customers;
					$xml_customers->customer->associations->groups->group->id			= $group_customers;
					
					$opt_customers = array('resource' => 'customers');
					$opt_customers['postXml'] = $xml_customers->asXML();
					
					//envoie le xml a prestashop
					$xml_customers = $webService->add($opt_customers);

					$monid_customers = $xml_customers->customer->id;//récupere l'id inséré dans prestashop par auto increment
					$bdd->query('UPDATE `prestashop`.`ps_customer` SET `id_customer` = '.$id_customers.' WHERE `ps_customer`.`id_customer` = '.$monid_customers.'');//envoie la nouvelle id				
				}	
				catch (PrestaShopWebserviceException $e)
				{
				  // Here we are dealing with errors
				  $trace = $e->getTrace();
				  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
				  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
				  else echo 'Other error<br />'.$e->getMessage();
				}
		}
	}
	
	function Import_addresses()
	{	
	global $get_xml_addresses;
	global $bdd;
		foreach ($get_xml_addresses->children()->children() as $ressources_addresses)
		{
			try {
					$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); //connection
					$xml_addresses = $webService->get(array('url' => PS_SHOP_PATH.'/api/addresses?schema=synopsis'));//on recupere un shemas blanc xml

					//autres données sans l'id dans mon xml
					$id_addresses  			     						= $ressources_addresses->id;
					$id_customer_addresses 						    	= $ressources_addresses->id_customer;
					$id_manufacturer_addresses  			  			= $ressources_addresses->id_manufacturer;
					$id_supplier_addresses  							= $ressources_addresses->id_supplier;
					$id_warehouse_addresses 		    		 		= $ressources_addresses->id_warehouse;
					$id_country_addresses      	     	   				= $ressources_addresses->id_country;
					$id_state_addresses  			      	   			= $ressources_addresses->id_state;
					$alias_addresses 					           		= $ressources_addresses->alias;
					$company_addresses     		     	 		   		= $ressources_addresses->company;
					$lastname_addresses  				      	 		= $ressources_addresses->lastname;
					$firstname_addresses 			     	 			= $ressources_addresses->firstname;
					$vat_number_addresses     					  		= $ressources_addresses->vat_number;
					$address1_addresses  					 	     	= $ressources_addresses->address1;
					$address2_addresses 						        = $ressources_addresses->address2;
					$postcode_addresses     						    = $ressources_addresses->postcode;
					$city_addresses  							        = $ressources_addresses->city;
					$phone_addresses	  		 						= $ressources_addresses->phone;
					$phone_mobile_addresses    		  					= $ressources_addresses->phone_mobile;
					$dni_addresses  						     		= $ressources_addresses->dni;
					$deleted_addresses 		   			    			= $ressources_addresses->deleted;
					$date_add_addresses     					   	    = $ressources_addresses->date_add;
					$date_upd_addresses     					   	    = $ressources_addresses->date_upd;

					//on associe les données de mon xml au xml blank créer au debut
					$xml_addresses->address->id_customer 					       		= $id_customer_addresses;
					$xml_addresses->address->id_manufacturer  					    	= $id_manufacturer_addresses;
					$xml_addresses->address->id_supplier 						    	= $id_supplier_addresses;
					$xml_addresses->address->id_warehouse 			        			= $id_warehouse_addresses;
					$xml_addresses->address->id_country  					        	= $id_country_addresses;
					$xml_addresses->address->id_state 		      						= $id_state_addresses;
					$xml_addresses->address->alias 	  									= $alias_addresses;
					$xml_addresses->address->company  			        				= $company_addresses;
					$xml_addresses->address->lastname 					          		= $lastname_addresses;
					$xml_addresses->address->firstname 					           	 	= $firstname_addresses;
					$xml_addresses->address->vat_number  					        	= $vat_number_addresses;
					$xml_addresses->address->address1 					          		= $address1_addresses;
					$xml_addresses->address->address2 				            		= $address2_addresses;
					$xml_addresses->address->postcode  				       				= $postcode_addresses;
					$xml_addresses->address->city 						            	= $city_addresses;
					$xml_addresses->address->phone 					            		= $phone_addresses;
					$xml_addresses->address->phone_mobile  				            	= $phone_mobile_addresses;
					$xml_addresses->address->dni 					            		= $dni_addresses;
					$xml_addresses->address->deleted 					            	= $deleted_addresses;
					$xml_addresses->address->date_add     								= $date_add_addresses;
					$xml_addresses->address->date_upd 	       							= $date_upd_addresses;					
					
					$opt_addresses = array('resource' => 'addresses');
					$opt_addresses['postXml'] = $xml_addresses->asXML();

					//envoie le xml a prestashop
					$xml_addresses = $webService->add($opt_addresses);

					$monid_addresses = $xml_addresses->address->id;//récupere l'id inséré dans prestashop par auto increment
					$bdd->query('UPDATE `prestashop`.`ps_address` SET `id_address` = '.$id_addresses.' WHERE `ps_address`.`id_address` = '.$monid_addresses.'');//envoie la nouvelle id				
				}	
				catch (PrestaShopWebserviceException $e)
				{
				  // Here we are dealing with errors
				  $trace = $e->getTrace();
				  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
				  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
				  else echo 'Other error<br />'.$e->getMessage();
				}
		}	
	}
	
	function Import_manufacturers()
	{
	global $manufacturersxml;
	global $bdd;
		foreach ($manufacturersxml->children()->children() as $ressource_manufacturers)
		{
			try {
					$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);//met la connection dans une variable
					$xml_manufacturers = $webService->get(array('url' => PS_SHOP_PATH.'/api/manufacturers?schema=blank'));//on recupere un shemas blanc xml

					$name_manufacturers   								=    $ressource_manufacturers-> name;
					$id_manufacturers   								=    $ressource_manufacturers-> id;
					$active_manufacturers    							=    $ressource_manufacturers-> active;
					// $link_rewrite_manufacturers    						=    $ressource_manufacturers-> link_rewrite;	
					$date_add_manufacturers    							=    $ressource_manufacturers-> date_add;	
					$date_upd_manufacturers    							=    $ressource_manufacturers-> date_upd;	
					$meta_keywords_manufacturers    					=    $ressource_manufacturers-> meta_keywords;					
					$description1_manufacturers   						=    $ressource_manufacturers-> description->language[0][0];
					$short_description1_manufacturers  					=    $ressource_manufacturers-> short_description->language[0][0];
					$meta_title1_manufacturers   						=    $ressource_manufacturers-> meta_title->language[0][0];
					$meta_description1_manufacturers   					=    $ressource_manufacturers-> meta_description->language[0][0];				

					$xml_manufacturers->manufacturer->name								=			$name_manufacturers;				
					$xml_manufacturers->manufacturer->active							=			$active_manufacturers;
					// $xml_manufacturers->manufacturer->link_rewrite						=			$link_rewrite_manufacturers;
					$xml_manufacturers->manufacturer->date_add							=			$date_add_manufacturers;
					$xml_manufacturers->manufacturer->date_upd							=			$date_upd_manufacturers;
					$xml_manufacturers->manufacturer->meta_keywords						=			$meta_keywords_manufacturers;																							
					$xml_manufacturers->manufacturer->description->language[0][0]		=			$description1_manufacturers;
					$xml_manufacturers->manufacturer->short_description->language[0][0]	=			$short_description1_manufacturers;
					$xml_manufacturers->manufacturer->meta_title->language[0][0]		=			$meta_title1_manufacturers;
					$xml_manufacturers->manufacturer->meta_description->language[0][0]	=			$meta_description1_manufacturers;
		
					$opt_manufacturers = array('resource' => 'manufacturers');//créer un tableau de la ressource que l'on veux
					$opt_manufacturers['postXml'] = $xml_manufacturers->asXML();//met le tableau au format xml
				
					$xml_manufacturers = $webService->add($opt_manufacturers);//envoie le xml a prestashop
					
					$monid_manufacturers = $xml_manufacturers->manufacturer->id;//récupere l'id inséré dans prestashop par auto increment
					$bdd->query('UPDATE `prestashop`.`ps_manufacturer` SET `id_manufacturer` = '.$id_manufacturers.' WHERE `ps_manufacturer`.`id_manufacturer` = '.$monid_manufacturers.'');//envoie la nouvelle id	
				}	
				catch (PrestaShopWebserviceException $e)
				{
				  // Here we are dealing with errors
				  $trace = $e->getTrace();
				  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
				  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
				  else echo 'Other error<br />'.$e->getMessage();
				}
		}			
	}
	
	function Import_suppliers()
	{
	global $suppliersxml;
	global $bdd;	
		foreach ($suppliersxml->children()->children() as $ressource_supplier)
		{
			try {
					$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);//met la connection dans une variable
					$xml_supplier = $webService->get(array('url' => PS_SHOP_PATH.'/api/suppliers?schema=synopsis'));//on recupere un shemas blanc xml

					$id_supplier    						=    $ressource_supplier-> id;
					$link_rewrite_supplier    				=    $ressource_supplier-> link_rewrite;
					$name_supplier    						=    $ressource_supplier-> name;	
					$active_supplier    					=    $ressource_supplier-> active;	
					$date_add_supplier    					=    $ressource_supplier-> date_add;
					$date_upd_supplier    					=    $ressource_supplier-> date_upd;					
					$description1_supplier   				=    $ressource_supplier-> description->language[0][0];
					$meta_title1_supplier   				=    $ressource_supplier-> meta_title->language[0][0];
					$meta_description1_supplier   			=    $ressource_supplier-> meta_description->language[0][0];				
		
					$xml_supplier->supplier->link_rewrite						=			$link_rewrite_supplier;
					$xml_supplier->supplier->name								=			$name_supplier;
					$xml_supplier->supplier->active								=			$active_supplier;
					$xml_supplier->supplier->date_add							=			$date_add_supplier;
					$xml_supplier->supplier->date_upd							=			$date_upd_supplier;
					$xml_supplier->supplier-> description->language[0][0]		=			$description1_supplier;
					$xml_supplier->supplier-> meta_title->language[0][0]		=			$meta_title1_supplier;
					$xml_supplier->supplier-> meta_description->language[0][0]	=			$meta_description1_supplier;
		
					$opt_supplier = array('resource' => 'suppliers');//créer un tableau de la ressource que l'on veux
					$opt_supplier['postXml'] = $xml_supplier->asXML();//met le tableau au format xml
				
					$xml_supplier = $webService->add($opt_supplier);//envoie le xml a prestashop
					
					$monid_supplier = $xml_supplier->product_feature->id;//récupere l'id inséré dans prestashop par auto increment
					$bdd->query('UPDATE `prestashop`.`ps_supplier` SET `id_product_supplier` = '.$id_supplier.' WHERE `ps_product_supplier`.`id_product_supplier` = '.$monid_supplier.'');//envoie la nouvelle id	
				}	
				catch (PrestaShopWebserviceException $e)
				{
				  // Here we are dealing with errors
				  $trace = $e->getTrace();
				  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
				  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
				  else echo 'Other error<br />'.$e->getMessage();
				}
		}		
	}
	
	function Import_tags()
	{
	global $tagsxml;
	global $bdd;
		foreach ($tagsxml->children()->children() as $ressource_tag)
		{
			try {
					$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);//met la connection dans une variable
					$xml_tag = $webService->get(array('url' => PS_SHOP_PATH.'/api/tags?schema=synopsis'));//on recupere un shemas blanc xml

					$id_tag   							=    $ressource_tag-> id;
					$id_lang_tag    					=    $ressource_tag-> id_lang;				
					$name_tag   						=    $ressource_tag-> name;				
		
					$xml_tag->product_feature->id_lang							=			$id_lang_tag;
					$xml_tag->product_feature->name								=			$name_tag;
		
					$opt_tag = array('resource' => 'tags');//créer un tableau de la ressource que l'on veux
					$opt_tag['postXml'] = $xml_tag->asXML();//met le tableau au format xml
				
					$xml_tag = $webService->add($opt_tag);//envoie le xml a prestashop
					
					$monid_tag = $xml_tag->product_feature->id;//récupere l'id inséré dans prestashop par auto increment
					$bdd->query('UPDATE `prestashop`.`ps_tag` SET `id_tag` = '.$id_tag.' WHERE `ps_tag`.`id_tag` = '.$monid_tag.'');//envoie la nouvelle id	
				}	
				catch (PrestaShopWebserviceException $e)
				{
				  // Here we are dealing with errors
				  $trace = $e->getTrace();
				  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
				  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
				  else echo 'Other error<br />'.$e->getMessage();
				}
		}
	}
	
	function Import_combinations()
	{
	global $combinationsxml;
	global $bdd;
		foreach ($combinationsxml->children()->children() as $ressource_combination)
		{
			try {
					$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);//met la connection dans une variable
					$xml_combination = $webService->get(array('url' => PS_SHOP_PATH.'/api/combinations?schema=synopsis'));//on recupere un shemas blanc xml

					$id_combination    								=    $ressource_combination-> id;
					$id_product_combination    						=    $ressource_combination-> id_product;
					$location_combination    						=    $ressource_combination-> location;
					$ean13_combination   							=    $ressource_combination-> ean13;	
					$upc_combination    							=    $ressource_combination-> upc;
					$quantity_combination    						=    $ressource_combination-> quantity;	
					$reference_combination    						=    $ressource_combination-> reference;
					$supplier_reference_combination    				=    $ressource_combination-> supplier_reference;	
					$wholesale_price_combination    				=    $ressource_combination-> wholesale_price;
					$price_combination    							=    $ressource_combination-> price;	
					$ecotax_combination    							=    $ressource_combination-> ecotax;
					$weight_combination   							=    $ressource_combination-> weight;	
					$unit_price_impact_combination    				=    $ressource_combination-> unit_price_impact;
					$minimal_quantity_combination    				=    $ressource_combination-> minimal_quantity;	
					$default_on_combination    						=    $ressource_combination-> default_on;
					$available_date_combination    					=    $ressource_combination-> available_date;					
					$product_option_value1_combination   			=    $ressource_combination-> product_option_value->language[0][0];
					$image1_combination   							=    $ressource_combination-> image->language[0][0];			
		
					$xml_combination->combination->id_product							=			$id_product_combination;
					$xml_combination->combination->location								=			$location_combination;
					$xml_combination->combination->ean13								=			$ean13_combination;
					$xml_combination->combination->upc									=			$upc_combination;
					$xml_combination->combination->quantity								=			$quantity_combination;
					$xml_combination->combination->reference							=			$reference_combination;
					$xml_combination->combination->supplier_reference					=			$supplier_reference_combination;
					$xml_combination->combination->wholesale_price						=			$wholesale_price_combination;
					$xml_combination->combination->price								=			$price_combination;
					$xml_combination->combination->ecotax								=			$ecotax_combination;
					$xml_combination->combination->weight								=			$weight_combination;
					$xml_combination->combination->unit_price_impact					=			$unit_price_impact_combination;
					$xml_combination->combination->minimal_quantity						=			$minimal_quantity_combination;
					$xml_combination->combination->default_on							=			$default_on_combination;
					$xml_combination->combination->available_date						=			$available_date_combination;				
					$xml_combination->combination->product_option_value->language[0][0]	=			$product_option_value1_combination;
					$xml_combination->combination->image->language[0][0]				=			$image1_combination;
		
					$opt_combination = array('resource' => 'combinations');//créer un tableau de la ressource que l'on veux
					$opt_combination['postXml'] = $xml_combination->asXML();//met le tableau au format xml
				
					$xml_combination = $webService->add($opt_combination);//envoie le xml a prestashop
					
					$monid_combination = $xml_combination->combination->id;//récupere l'id inséré dans prestashop par auto increment
					$bdd->query('UPDATE `prestashop`.`ps_combination` SET `id_combination` = '.$id_combination.' WHERE `ps_combination`.`id_combination` = '.$monid_combination.'');//envoie la nouvelle id	
				}	
				catch (PrestaShopWebserviceException $e)
				{
				  // Here we are dealing with errors
				  $trace = $e->getTrace();
				  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
				  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
				  else echo 'Other error<br />'.$e->getMessage();
				}
		}			
	}
	
	function Import_product_option_values()
	{
	global $product_option_valuesxml;
	global $bdd;
		foreach ($product_option_valuesxml->children()->children() as $ressource_product_option_value)
		{
			try {
					$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);//met la connection dans une variable
					$xml_product_option_value = $webService->get(array('url' => PS_SHOP_PATH.'/api/product_option_values?schema=blank'));//on recupere un shemas blanc xml

					$id_product_option_value    							=    $ressource_product_option_value-> id;
					$id_attribute_group_product_option_value    			=    $ressource_product_option_value-> id_attribute_group;
					$color_product_option_value    							=    $ressource_product_option_value-> color;	
					$position_product_option_value    						=    $ressource_product_option_value-> position;					
					$name1_product_option_value   							=    $ressource_product_option_value-> name->language[0][0];
					// $name2_product_option_value   							=    $ressource_product_option_value-> name->language[2];					
		
					$xml_product_option_value->product_option_value->id_attribute_group					=			$id_attribute_group_product_option_value;
					$xml_product_option_value->product_option_value->color								=			$color_product_option_value;
					$xml_product_option_value->product_option_value->position							=			$position_product_option_value;
					$xml_product_option_value->product_option_value->name->language[0][0]					=			$name1_product_option_value;
					// $xml_product_option_value->product_option_value->name->language[2]					=			$name2_product_option_value;
		
					$opt_product_option_value = array('resource' => 'product_option_values');//créer un tableau de la ressource que l'on veux
					$opt_product_option_value['postXml'] = $xml_product_option_value->asXML();//met le tableau au format xml
				
					$xml_product_option_value = $webService->add($opt_product_option_value);//envoie le xml a prestashop
					
					$monid_product_option_value = $xml_product_option_value->product_option_value->id;//récupere l'id inséré dans prestashop par auto increment
					$bdd->query('UPDATE `prestashop`.`ps_product_option_value` SET `id_product_option_value` = '.$id_product_option_value.' WHERE `ps_product_option_value`.`id_product_option_value` = '.$monid_product_option_value.'');//envoie la nouvelle id	
				}	
				catch (PrestaShopWebserviceException $e)
				{
				  // Here we are dealing with errors
				  $trace = $e->getTrace();
				  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
				  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
				  else echo 'Other error<br />'.$e->getMessage();
				}
		}		
	}
	
	function Import_categories()
	{
	global $categoriesxml;
	global $bdd;
		foreach ($categoriesxml->children()->children() as $ressource_category)
		{
			try {
					$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);//met la connection dans une variable
					$xml_category = $webService->get(array('url' => PS_SHOP_PATH.'/api/categories?schema=blank'));//on recupere un shemas blanc xml

					$id_category    						=    $ressource_category-> id;
					$id_parent_category    					=    $ressource_category-> id_parent;
					// $level_depth_category    				=    $ressource_category-> level_depth;
					// $nb_products_recursive_category    		=    $ressource_category-> nb_products_recursive;
					$active_category    					=    $ressource_category-> active;
					$id_shop_default_category    			=    $ressource_category-> id_shop_default;
					$is_root_category_category   			=    $ressource_category-> is_root_category;
					$position_category  					=    $ressource_category-> position;
					$date_add_category     					=    $ressource_category-> date_add;	
					$date_upd_category						= 	 $ressource_category-> date_upd;
					$name1_category    						=    $ressource_category-> name->language[0][0];		
					$link_rewrite1_category 				=    $ressource_category-> link_rewrite->language[0][0];					
					$description1_category					=	 $ressource_category-> description->language[0][0];				
					$meta_description1_category    			=    $ressource_category-> meta_title->language[0][0];			
					$meta_title1_category    				=    $ressource_category-> meta_description->language[0][0];				
					$meta_keywords1_category   				=    $ressource_category-> meta_keywords->language[0][0];			
			
					$xml_category->category->id_parent							=			$id_parent_category;
					// $xml_category->category->level_depth						=			$level_depth_category;
					// $xml_category->category->nb_products_recursive			=			$nb_products_recursive_category;
					$xml_category->category->active								=			$active_category;
					$xml_category->category->id_shop_default					=			$id_shop_default_category;
					$xml_category->category->is_root_category					=			$is_root_category_category;
					$xml_category->category->position							=			$position_category;
					$xml_category->category->date_add							=			$date_add_category;
					$xml_category->category->date_upd							=			$date_upd_category;
					$xml_category->category->name->language[0][0]				=			$name1_category;
					$xml_category->category->link_rewrite->language[0][0]		=			$link_rewrite1_category;
					$xml_category->category->description->language[0][0]		=			$description1_category;
					$xml_category->category->meta_title->language[0][0]			=			$meta_description1_category;
					$xml_category->category->meta_description->language[0][0]	=			$meta_title1_category;
					$xml_category->category->meta_keywords->language[0][0]		=			$meta_keywords1_category;
								
					$opt_category = array('resource' => 'categories');//créer un tableau de la ressource que l'on veux
					$opt_category['postXml'] = $xml_category->asXML();//met le tableau au format xml
				
					$xml_category = $webService->add($opt_category);//envoie le xml a prestashop
					
					$monid_category = $xml_category->category->id;//récupere l'id inséré dans prestashop par auto increment
					$bdd->query('UPDATE `prestashop`.`ps_category` SET `id_category` = '.$id_category.' WHERE `ps_category`.`id_category` = '.$monid_category.'');//envoie la nouvelle id	
				}	
				catch (PrestaShopWebserviceException $e)
				{
				  // Here we are dealing with errors
				  $trace = $e->getTrace();
				  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
				  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
				  else echo 'Other error<br />'.$e->getMessage();
				}
		}	
	}
	
	
	function Import_product_features()
	{
	global $product_featurexml;
	global $bdd;	
		foreach ($product_featurexml->children()->children() as $ressource_product_feature)
		{
			try {
					$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);//met la connection dans une variable
					$xml_product_feature = $webService->get(array('url' => PS_SHOP_PATH.'/api/product_features?schema=synopsis'));//on recupere un shemas blanc xml

					$id_product_feature    							=    $ressource_product_feature-> id;
					$position_product_feature    					=    $ressource_product_feature-> position;				
					$name1_product_feature   						=    $ressource_product_feature-> name->language[0][0];					
		
					$xml_product_feature->product_feature->position							=			$position_product_feature;
					$xml_product_feature->product_feature->name->language[0][0]				=			$name1_product_feature;
		
					$opt_product_feature = array('resource' => 'product_features');//créer un tableau de la ressource que l'on veux
					$opt_product_feature['postXml'] = $xml_product_feature->asXML();//met le tableau au format xml
				
					$xml_product_feature = $webService->add($opt_product_feature);//envoie le xml a prestashop
					
					$monid_product_feature = $xml_product_feature->product_feature->id;//récupere l'id inséré dans prestashop par auto increment
					$bdd->query('UPDATE `prestashop`.`ps_product_feature` SET `id_product_feature` = '.$id_product_feature.' WHERE `ps_product_feature`.`id_product_feature` = '.$monid_product_feature.'');//envoie la nouvelle id	
				}	
				catch (PrestaShopWebserviceException $e)
				{
				  // Here we are dealing with errors
				  $trace = $e->getTrace();
				  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
				  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
				  else echo 'Other error<br />'.$e->getMessage();
				}
		}		
	}
	function Import_products()
	{
	global $productsxml;
	global $bdd;
		foreach ($productsxml->children()->children() as $ressource_product)
		{
				try 
				{				
					$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);//met la connection dans une variable
					$xml_product = $webService->get(array('url' => PS_SHOP_PATH.'/api/products?schema=blank'));//on recupere un shemas blanc xml

					$id_product    						=    $ressource_product-> id;
					$id_manufacturer_product    		=    $ressource_product-> id_manufacturer;
					$id_supplier_product    			=    $ressource_product-> id_supplier;
					$id_category_default_product    	=    $ressource_product-> id_category_default;
					$cache_default_attribute_product    =    $ressource_product-> cache_default_attribute;
					$id_default_image_product    		=    $ressource_product-> id_default_image;
					$id_default_combination_product   	=    $ressource_product-> id_default_combination;
					$id_tax_rules_group_product  		=    $ressource_product-> id_tax_rules_group;
					// $position_in_category_product     	=    $ressource_product-> position_in_category;	
					// $manufacturer_name_product 			= 	 $ressource_product-> manufacturer_name;
					// $quantity_product    				=    $ressource_product-> quantity;
					$type_product 						=    $ressource_product-> type;			
					$id_shop_default_product			=	 $ressource_product-> id_shop_default;
					$reference_product    				=    $ressource_product-> reference;
					$supplier_reference_product    		=    $ressource_product-> supplier_reference;
					$location_product   				=    $ressource_product-> location;
					$width_product    					=    $ressource_product-> width;
					$height_product    					=    $ressource_product-> height;
					$depth_product    					=    $ressource_product-> depth;
					$weight_product         			=    $ressource_product-> weight;
					$quantity_discount_product    		=    $ressource_product-> quantity_discount;
					$ean13_product    					=    $ressource_product-> ean13;
					$upc_product    					=    $ressource_product-> upc;
					$cache_is_pack_product    			=    $ressource_product-> cache_is_pack;
					$cache_has_attachments_product    	=    $ressource_product-> cache_has_attachments;
					$is_virtual_product    				=    $ressource_product-> is_virtual;
					$on_sale_product    				=    $ressource_product-> on_sale;
					$online_only_product	   			=    $ressource_product-> online_only;
					$ecotax_product          			=    $ressource_product-> ecotax;
					$minimal_quantity_product    		=    $ressource_product-> minimal_quantity;
					$price_product    					=    $ressource_product-> price;
					$wholesale_price_product    		=    $ressource_product-> wholesale_price;
					$unity_product   					=    $ressource_product-> unity;
					$unit_price_ratio_product    		=    $ressource_product-> unit_price_ratio;
					$additional_shipping_cost_product   =    $ressource_product-> additional_shipping_cost;
					$customizable_product    			=    $ressource_product-> customizable;
					$text_fields_product    			=    $ressource_product-> text_fields;
					$uploadable_files_product    		=    $ressource_product-> uploadable_files;
					$active_product    					=    $ressource_product-> active;
					$redirect_type_product				=	 $ressource_product-> redirect_type;
					$id_product_redirected_product		=	 $ressource_product-> id_product_redirected;
					$available_for_order_product    	=    $ressource_product-> available_for_order;
					$available_date_product    			=    $ressource_product-> available_date;
					$condition_product    				=    $ressource_product-> condition;
					$show_price_product          		=    $ressource_product-> show_price;
					$indexed_product    				=    $ressource_product-> indexed;
					$visibility_product    				=    $ressource_product-> visibility;
					$advanced_stock_management_product  =    $ressource_product-> advanced_stock_management;
					$date_add_product    				=    $ressource_product-> date_add;
					$date_upd_product    				=    $ressource_product-> date_upd;
					$meta_description1_product			=	 $ressource_product-> meta_description->language[0][0];
					$meta_keywords1_product				=	 $ressource_product-> meta_keywords->language[0][0];
					$meta_title1_product				=	 $ressource_product-> meta_title->language[0][0];
					$link_rewrite1_product				=	 $ressource_product-> link_rewrite->language[0][0];
					$name1_product						=	 $ressource_product-> name->language[0][0];
					$description1_product				=	 $ressource_product-> description->language[0][0];
					$description_short1_product			=	 $ressource_product-> description_short->language[0][0];
					$available_now1_product				=	 $ressource_product-> available_now->language[0][0];
					$available_later1_product			=	 $ressource_product-> available_later->language[0][0];
					// $category_product 					=	 $ressource_product-> categories->category->id;
					// $image_product						=	 $ressource_product-> images->image->id;
					// $combinations_product 				=	 $ressource_product-> combinations->combinations->id;
					// $product_options_values_product 	=	 $ressource_product-> product_option_values->product_options_values->id;
					// $product_feature_product 			=	 $ressource_product-> product_features->product_feature->id;
					// $tags_product 						=	 $ressource_product-> tags;
					// $stock_available_id1_product 		=	 $ressource_product-> stock_availables->stock_available->id;
					// $stock_available_id2_product 		=	 $ressource_product-> stock_availables->stock_available->id_product_attribute;																				

					//on associe les données de mon xml au xml blank créer au debut
					$xml_product->product->meta_description->language[0][0]			=			$meta_description1_product;
					$xml_product->product->meta_keywords->language[0][0]			=			$meta_keywords1_product;					
					$xml_product->product->meta_title->language[0][0]				=			$meta_title1_product;			
					$xml_product->product->link_rewrite->language[0][0]				=			$link_rewrite1_product;		
					$xml_product->product->name->language[0][0]						=			$name1_product;		
					$xml_product->product->description->language[0][0]				=			$description1_product;
					$xml_product->product->description_short->language[0][0]		=			$description_short1_product;
					$xml_product->product->available_now->language[0][0]			=			$available_now1_product;			
					$xml_product->product->available_later->language[0][0]			=			$available_later1_product;	
					// $xml_product->product->category								=			$category_product; 	
					// $xml_product->product->image									=			$image_product; 
					// $xml_product->product->combinations							=			$combinations_product; 
					// $xml_product->product->product_options_values				=			$product_options_values_product;
					// $xml_product->product->product_feature						=			$product_feature_product;
					// $xml_product->product->tags									=			$tags_product; 					
					// $xml_product->product->stock_available_id1					=			$stock_available_id1_product; 
					// $xml_product->product->stock_available_id2					=			$stock_available_id2_product;
					// $xml_product->product->position_in_category	    			=    		$position_in_category_product;					
					$xml_product->product->id_manufacturer							=			$id_manufacturer_product;
					$xml_product->product->id_supplier								=			$id_supplier_product;
					$xml_product->product->id_category_default						=			$id_category_default_product;	
					$xml_product->product->cache_default_attribute					=			$cache_default_attribute_product;			
					$xml_product->product->id_default_image							=			$id_default_image_product;			
					$xml_product->product->id_default_combination					=			$id_default_combination_product;			
					$xml_product->product->id_tax_rules_group						=			$id_tax_rules_group_product;			
					$xml_product->product->id_shop_default							=			$id_shop_default_product;			
					// $xml_product->product->quantity									=			$quantity_product;	
					$xml_product->product->reference								=			$reference_product;			
					$xml_product->product->supplier_reference						=			$supplier_reference_product;
					$xml_product->product->location									=			$location_product;
					$xml_product->product->width									=			$width_product;
					$xml_product->product->height									=			$height_product;
					$xml_product->product->depth									=			$depth_product;
					$xml_product->product->weight									=			$weight_product;
					$xml_product->product->quantity_discount						=			$quantity_discount_product;
					$xml_product->product->ean13									=			$ean13_product;
					$xml_product->product->upc										=			$upc_product;
					$xml_product->product->cache_is_pack							=			$cache_is_pack_product;
					$xml_product->product->cache_has_attachments					=			$cache_has_attachments_product;
					$xml_product->product->is_virtual								=			$is_virtual_product;
					$xml_product->product->on_sale									=			$on_sale_product;
					$xml_product->product->online_only								=			$online_only_product;
					$xml_product->product->ecotax									=			$ecotax_product;
					$xml_product->product->minimal_quantity							=			$minimal_quantity_product;
					$xml_product->product->price									=			$price_product;
					$xml_product->product->wholesale_price							=			$wholesale_price_product;
					$xml_product->product->unity									=			$unity_product;
					$xml_product->product->unit_price_ratio							=			$unit_price_ratio_product;
					$xml_product->product->additional_shipping_cost					=			$additional_shipping_cost_product;
					$xml_product->product->customizable								=			$customizable_product;
					$xml_product->product->text_fields								=			$text_fields_product;
					$xml_product->product->uploadable_files							=			$uploadable_files_product;
					$xml_product->product->active									=			$active_product;
					$xml_product->product->available_for_order						=			$available_for_order_product;
					$xml_product->product->available_date							=			$available_date_product;
					$xml_product->product->condition								=			$condition_product;
					$xml_product->product->show_price								=			$show_price_product;
					$xml_product->product->indexed									=			$indexed_product;
					$xml_product->product->visibility								=			$visibility_product;
					$xml_product->product->advanced_stock_management				=			$advanced_stock_management_product;
					$xml_product->product->date_add									=			$date_add_product;
					$xml_product->product->date_upd									=			$date_upd_product;
					// $xml_product->product->manufacturer_name						=			$manufacturer_name_product;
					$xml_product->product->type										=			$type_product;
					$xml_product->product->redirect_type							=			$redirect_type_product;
					$xml_product->product->id_product_redirected					=			$id_product_redirected_product;
								
					$opt_product = array('resource' => 'products');//créer un tableau de la ressource que l'on veux
					$opt_product['postXml'] = $xml_product->asXML();//met le tableau au format xml
				
					$xml_product = $webService->add($opt_product);//envoie le xml a prestashop
					
					$monid_product = $xml_product->product->id;//récupere l'id inséré dans prestashop par auto increment
					$bdd->query('UPDATE `prestashop`.`ps_product` SET `id_product` = '.$id_product.' WHERE `ps_product`.`id_product` = '.$monid_product.'');//envoie la nouvelle id	
					
					$url = 'http://127.0.0.1/modules/prestashop2/api/images/products/'.$id_product;
					$image_path = 'img/'.$image_product;
			
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_USERPWD, PS_WS_AUTH_KEY.':');
					curl_setopt($ch, CURLOPT_POSTFIELDS, array('image' => '@'.$image_path));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$result = curl_exec($ch);
					curl_close($ch);
				}	
				catch (PrestaShopWebserviceException $e)
				{
				  // Here we are dealing with errors
				  $trace = $e->getTrace();
				  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
				  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
				  else echo 'Other error<br />'.$e->getMessage();
				}
		}
	}

	Import_customers();
	Import_addresses();
	Import_manufacturers();
	Import_suppliers();
	Import_tags();
	Import_combinations();
	Import_product_option_values();
	Import_categories();
	Import_product_features();
	Import_products();
?>	