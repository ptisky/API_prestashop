<?php

class updateProductCatalog
{
  private $_context;
  private $_idShop;
  private $_idLang;
  private $_format;
  private $_model;
  private $_PHPExcelFactory;
  private $_alphabet;
  private $_updateFields;
  private $_head;
  private $_productsForUpdate = 0;
  private $_updatedProducts = 0;
  private $_isFeature = false;

  public function __construct( $idShop, $idLang, $format, $fields ){
    include_once(dirname(__FILE__).'/../../config/config.inc.php');
    include_once(dirname(__FILE__).'/../../init.php');
    include_once(_PS_MODULE_DIR_ . 'updateproducts/libraries/PHPExcel_1.7.9/Classes/PHPExcel.php');
    include_once(_PS_MODULE_DIR_ . 'updateproducts/libraries/PHPExcel_1.7.9/Classes/PHPExcel/IOFactory.php');
    include_once(_PS_MODULE_DIR_ . 'updateproducts/updateproducts.php');
    include_once('datamodel.php');
    $this->_context = Context::getContext();
    $this->_idShop = $idShop;
    $this->_idLang = $idLang;
    $this->_format = $format;
    $this->_model = new productsUpdateModel();
    $obj = new updateProducts();
    $fields['id_product'] = $obj->fieldIdProduct;
    $fields['id_product_attribute'] = $obj->fieldIdProductCombination;
    $fields['id_specific_price'] = $obj->fieldIdSpecificPrice;
    $this->_updateFields = $fields;
    $this->_alphabet = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
      'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
      'BA','BB','BC','BD','BE','BF','BG','BH', 'BI','BJ','BK','BL','BM','BN','BO','BP','BQ', 'BR','BS','BT','BU','BV','BW','BX','BY','BZ',
      'CA','CC','CC','CD','CE','CF','CG','CH', 'CI','CJ','CK','CL','CM','CN','CO','CP','CQ', 'CR','CS','CT','CU','CV','CW','CX','CY','CZ',
      'DA','DD','DD','DD','DE','DF','DG','DH', 'DI','DJ','DK','DL','DM','DN','DO','DP','DQ', 'DR','DS','DT','DU','DV','DW','DX','DY','DZ'
    );
  }

 public function update()
 {
   $this->_clearErrorFile();
   $this->_copyFile();
   $this->_updateData();
   if( $this->_updatedProducts != $this->_productsForUpdate ){
     $res = array(
       'message'     =>  sprintf(Module::getInstanceByName('updateproducts')->l('Successfully updated %1s products from: %2s'), $this->_updatedProducts, $this->_productsForUpdate),
       'error_logs'  => _PS_BASE_URL_.__PS_BASE_URI__.'modules/updateproducts/error/error_logs.csv',
     );
     return $res;
   }

   $res = array(
     'message'     =>  sprintf(Module::getInstanceByName('updateproducts')->l('Successfully updated %s products!'), $this->_updatedProducts),
     'error_logs'  => false
   );

   return $res;

 }


  private function _clearErrorFile()
  {
    $write_fd = fopen('error/error_logs.csv', 'w');
    fwrite($write_fd, 'id_product,error'."\r\n");
    fclose($write_fd);
  }

  private function _checkHead( $fileFields )
  {
    $mappingFields = array();
    foreach( $this->_updateFields as $key => $field ){
      if( $key == "features" ){
        foreach( $fileFields as $fileField ){
          if( stripos( $fileField, "feature" ) !== false ){
            $this->_isFeature = true;
          }
        }
        if( !$this->_isFeature ){
          throw new Exception( sprintf(Module::getInstanceByName('updateproducts')->l('No %s was found in file!'), $field) );
        }
        continue;
      }

      if( !in_array( $field, $fileFields ) ){
        throw new Exception( sprintf(Module::getInstanceByName('updateproducts')->l('Field: %s need to be in file for update!'), $field) );
      }
      else{
        $mappingFields[$field] = $key;
      }
    }

    return $mappingFields;
  }

  private function _updateData()
  {
    foreach ($this->_PHPExcelFactory->getWorksheetIterator() as $worksheet) {
      $highestRow         = $worksheet->getHighestRow(); // e.g. 10
      $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
      $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
      $fileFields = array();

      for ($row = 1; $row <= $highestRow; ++ $row) {
        $product = array();
        for ($col = 0; $col < $highestColumnIndex; ++ $col) {
          $cell = $worksheet->getCellByColumnAndRow($col, $row);
          $val = $cell->getValue();
          if($row == 1){
            if( !$val ){
              continue;
            }
            $fileFields[$col] = $val;
          }
          else{
            if(!isset($fileFields[$col])){
              continue;
            }
            if( $row == 2 && $col == 0 ){
              $mappingFields = $this->_checkHead($fileFields);
            }
            if( isset($mappingFields[$fileFields[$col]]) ){
              $product[$mappingFields[$fileFields[$col]]] = $val;
            }
            if( $this->_isFeature ){
              if( stripos( $fileFields[$col], "feature" ) !== false ){
                $product['features'][$fileFields[$col]] = $val;
              }
            }
          }
        }
        if( $product ){
          $this->_productsForUpdate++;
          $this->_updateProduct($product);
        }
      }
    }

  }

  private function _updateProduct( $product )
  {
    $productObject = new ProductCore( $product['id_product'], true );

    foreach( $product as $field => $value ){

      if( $field == "categories_ids" ){
        $value = str_replace(" ", "", $value);
        $value = explode(";", $value);
        if( $value ){
         foreach( $value as $key => $categoryId ){
           $value[$key] = array('id' => $categoryId);
         }
        }
        $productObject->setWsCategories($value);
      }
      elseif( $field == "combinations_price" ){
        $attributes = $product['id_product_attribute'];
        $attributes = str_replace(" ", "", $attributes);
        $attributes = explode(";", $attributes);

        $values = $product['combinations_price'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $attributes as $key => $attribute ){
          $combination = new Combination($attribute);
          if( !isset($values[$key]) ){
            continue;
          }
          $combination->price = number_format($values[$key], 4, '.', '');

          if( ( $error = $combination->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }

          $combination->update();
        }
      }
      elseif( $field == "combinations_price_with_tax" ){
        $attributes = $product['id_product_attribute'];
        $attributes = str_replace(" ", "", $attributes);
        $attributes = explode(";", $attributes);

        $values = $product['combinations_price_with_tax'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $attributes as $key => $attribute ){
          $combination = new Combination($attribute);
          if( !isset($values[$key]) ){
            continue;
          }

          $taxPrice = (float)$values[$key];
          if( $productObject->tax_rate ){
            $taxPrice = $taxPrice / (($productObject->tax_rate/100)+1);
          }

          $combination->price = number_format($taxPrice, 4, '.', '');

          if( ( $error = $combination->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }

          $combination->update();
        }
      }
      elseif( $field == "combinations_quantity" ){
        $attributes = $product['id_product_attribute'];
        $attributes = str_replace(" ", "", $attributes);
        $attributes = explode(";", $attributes);

        $values = $product['combinations_quantity'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $attributes as $key => $attribute ){
          if( !isset($values[$key]) ){
            continue;
          }
          StockAvailable::setQuantity($product['id_product'], (int)$attribute, (int)$values[$key]);
        }
      }
      elseif( $field == "combinations_wholesale_price" ){
        $attributes = $product['id_product_attribute'];
        $attributes = str_replace(" ", "", $attributes);
        $attributes = explode(";", $attributes);
        $values = $product['combinations_wholesale_price'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $attributes as $key => $attribute ){
          $combination = new Combination($attribute);
          if( !isset($values[$key]) ){
            continue;
          }

          $combination->wholesale_price = number_format($values[$key], 4, '.', '');

          if( ( $error = $combination->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }

          $combination->update();
        }
      }
      elseif( $field == "combinations_unit_price_impact" ){
        $attributes = $product['id_product_attribute'];
        $attributes = str_replace(" ", "", $attributes);
        $attributes = explode(";", $attributes);

        $values = $product['combinations_unit_price_impact'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $attributes as $key => $attribute ){
          $combination = new Combination($attribute);
          if( !isset($values[$key]) ){
            continue;
          }
          $combination->unit_price_impact = number_format($values[$key], 4, '.', '');

          if( ( $error = $combination->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }

          $combination->update();
        }
      }
      elseif( $field == "combinations_minimal_quantity" ){
        $attributes = $product['id_product_attribute'];
        $attributes = str_replace(" ", "", $attributes);
        $attributes = explode(";", $attributes);

        $values = $product['combinations_minimal_quantity'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $attributes as $key => $attribute ){
          $combination = new Combination($attribute);
          if( !isset($values[$key]) ){
            continue;
          }
          $combination->minimal_quantity = $values[$key];

          if( ( $error = $combination->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }

          $combination->update();
        }
      }
      elseif( $field == "combinations_reference" ){
        $attributes = $product['id_product_attribute'];
        $attributes = str_replace(" ", "", $attributes);
        $attributes = explode(";", $attributes);

        $values = $product['combinations_reference'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $attributes as $key => $attribute ){
          $combination = new Combination($attribute);
          if( !isset($values[$key]) ){
            continue;
          }
          $combination->reference = $values[$key];
          if( ( $error = $combination->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }
          $combination->update();
        }
      }
      elseif( $field == "combinations_location" ){
        $attributes = $product['id_product_attribute'];
        $attributes = str_replace(" ", "", $attributes);
        $attributes = explode(";", $attributes);

        $values = $product['combinations_location'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $attributes as $key => $attribute ){
          $combination = new Combination($attribute);
          if( !isset($values[$key]) ){
            continue;
          }
          $combination->location = $values[$key];
          if( ( $error = $combination->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }
          $combination->update();
        }
      }
      elseif( $field == "combinations_weight" ){
        $attributes = $product['id_product_attribute'];
        $attributes = str_replace(" ", "", $attributes);
        $attributes = explode(";", $attributes);

        $values = $product['combinations_weight'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $attributes as $key => $attribute ){
          $combination = new Combination($attribute);
          if( !isset($values[$key]) ){
            continue;
          }
          $combination->weight = $values[$key];
          if( ( $error = $combination->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }
          $combination->update();
        }
      }
      elseif( $field == "combinations_ecotax" ){
        $attributes = $product['id_product_attribute'];
        $attributes = str_replace(" ", "", $attributes);
        $attributes = explode(";", $attributes);

        $values = $product['combinations_ecotax'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $attributes as $key => $attribute ){
          $combination = new Combination($attribute);
          if( !isset($values[$key]) ){
            continue;
          }
          $combination->ecotax = $values[$key];
          if( ( $error = $combination->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }
          $combination->update();
        }
      }
      elseif( $field == "combinations_ean13" ){
        $attributes = $product['id_product_attribute'];
        $attributes = str_replace(" ", "", $attributes);
        $attributes = explode(";", $attributes);

        $values = $product['combinations_ean13'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $attributes as $key => $attribute ){

          $combination = new Combination($attribute);
          if( !isset($values[$key]) ){
            continue;
          }
          $combination->ean13 = $values[$key];
          if( ( $error = $combination->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }
          $combination->update();
        }
      }
      elseif( $field == "combinations_upc" ){
        $attributes = $product['id_product_attribute'];
        $attributes = str_replace(" ", "", $attributes);
        $attributes = explode(";", $attributes);

        $values = $product['combinations_upc'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $attributes as $key => $attribute ){
          $combination = new Combination($attribute);
          if( !isset($values[$key]) ){
            continue;
          }
          $combination->upc = $values[$key];
          if( ( $error = $combination->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }
          $combination->update();
        }
      }
      elseif( $field == "tags" ){
        Tag::deleteTagsForProduct($product['id_product']);
        $tags = str_replace(" ", "", $value);
        if( $tags ){
          Tag::addTags( $this->_idLang, $product['id_product'], $tags );
        }
      }
      elseif( $field == "specific_price_from_quantity" ){
        $specific_price = $product['id_specific_price'];
        $specific_price = str_replace(" ", "", $specific_price);
        $specific_price = explode(";", $specific_price);

        $values = $product['specific_price_from_quantity'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $specific_price as $key => $specific_id ){
          $specificObject = new SpecificPrice($specific_id);
          if( !isset($values[$key]) ){
            continue;
          }
          $specificObject->from_quantity = $values[$key];

          if( ( $error = $specificObject->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }


          $specificObject->update();
        }
      }
      elseif( $field == "specific_price_reduction" ){
        $specific_price = $product['id_specific_price'];
        $specific_price = str_replace(" ", "", $specific_price);
        $specific_price = explode(";", $specific_price);

        $values = $product['specific_price_reduction'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $specific_price as $key => $specific_id ){
          $specificObject = new SpecificPrice($specific_id);
          if( !isset($values[$key]) ){
            continue;
          }
          $specificObject->reduction = $values[$key];

          if( ( $error = $specificObject->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }

          $specificObject->update();
        }
      }
      elseif( $field == "specific_price_reduction_type" ){
        $specific_price = $product['id_specific_price'];
        $specific_price = str_replace(" ", "", $specific_price);
        $specific_price = explode(";", $specific_price);

        $values = $product['specific_price_reduction_type'];
        $values = str_replace(" ", "", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $specific_price as $key => $specific_id ){
          $specificObject = new SpecificPrice($specific_id);
          if( !isset($values[$key]) ){
            continue;
          }
          $specificObject->reduction_type = $values[$key];
          if( ( $error = $specificObject->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }
          $specificObject->update();
        }
      }
      elseif( $field == "specific_price_from" ){
        $specific_price = $product['id_specific_price'];
        $specific_price = str_replace(" ", "", $specific_price);
        $specific_price = explode(";", $specific_price);

        $values = $product['specific_price_from'];
        $values = str_replace("; ", ";", $values);
        $values = str_replace(" ;", ";", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);
        foreach( $specific_price as $key => $specific_id ){
          $specificObject = new SpecificPrice($specific_id);
          if( !isset($values[$key]) ){
            continue;
          }
          $specificObject->from = $values[$key];
          if( ( $error = $specificObject->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }
          $specificObject->update();
        }
      }
      elseif( $field == "specific_price_to" ){
        $specific_price = $product['id_specific_price'];
        $specific_price = str_replace(" ", "", $specific_price);
        $specific_price = explode(";", $specific_price);
        $values = $product['specific_price_to'];
        $values = str_replace("; ", ";", $values);
        $values = str_replace(" ;", ";", $values);
        if( $values == "" ){
          continue;
        }
        $values = explode(";", $values);

        foreach( $specific_price as $key => $specific_id ){
          $specificObject = new SpecificPrice($specific_id);
          if( !isset($values[$key]) ){
            continue;
          }
          $specificObject->to = $values[$key];
          if( ( $error = $specificObject->validateFields(false, true) ) !== true ){
            $this->_createErrorsFile($error, $product['id_product']);
            return false;
          }
          $specificObject->update();
        }
      }
      elseif( $field == "features" ){
        $productObject->deleteProductFeatures();
        foreach( $value as $feature => $featureValue ){
          if( !$featureValue ){
            continue;
          }
          $featureId = explode('_FEATURE_',$feature);
          $featureId = $featureId[0];
          $featureValueId = FeatureValue::addFeatureValueImport( $featureId, $featureValue, false, $this->_idLang );
          $productObject->addFeatureProductImport( $product['id_product'], $featureId, $featureValueId );
        }
      }
      elseif( $field == "quantity" && !($product['id_product_attribute'])){
        StockAvailable::setQuantity($product['id_product'], 0, $value);
      }
      elseif( $field == "redirect_type" ){
        $productObject->redirect_type = (string)$value;
      }
      elseif( $field == "meta_description" ){
        $currentValue = $productObject->meta_description;
        $currentValue[$this->_idLang] = (string)$value;
        $productObject->meta_description = $currentValue;
      }
      elseif( $field == "meta_keywords" ){
        $currentValue = $productObject->meta_keywords;
        $currentValue[$this->_idLang] = (string)$value;
        $productObject->meta_keywords = $currentValue;
      }
      elseif( $field == "meta_title" ){
        $currentValue = $productObject->meta_title;
        $currentValue[$this->_idLang] = (string)$value;
        $productObject->meta_title = $currentValue;
      }
      elseif( $field == "link_rewrite" ){
        $currentValue = $productObject->link_rewrite;
        $currentValue[$this->_idLang] = (string)$value;
        $productObject->link_rewrite = $currentValue;
      }
      elseif( $field == "name" ){
        $currentValue = $productObject->name;
        $currentValue[$this->_idLang] = (string)$value;
        $productObject->name = $currentValue;
      }
      elseif( $field == "description" ){
        $currentValue = $productObject->description;
        $currentValue[$this->_idLang] = (string)$value;
        $productObject->description = $currentValue;
      }
      elseif( $field == "description_short" ){
        $currentValue = $productObject->description_short;
        $currentValue[$this->_idLang] = (string)$value;
        $productObject->description_short = $currentValue;
      }
      elseif( $field == "available_now" ){
        $currentValue = $productObject->available_now;
        $currentValue[$this->_idLang] = (string)$value;
        $productObject->available_now = $currentValue;
      }
      elseif( $field == "available_later" ){
        $currentValue = $productObject->available_later;
        $currentValue[$this->_idLang] = (string)$value;
        $productObject->available_later = $currentValue;
      }
      elseif( $field == "suppliers_ids" ){
        $productObject->deleteFromSupplier();
        $values = explode(";", $value);
        foreach ($values as $id) {
          $supplierExists = SupplierCore::supplierExists($id);
          if($supplierExists){
            $product_supplier = new ProductSupplier();
            $product_supplier->id_product = $product['id_product'];
            $product_supplier->id_product_attribute = 0;
            $product_supplier->id_supplier = $id;
            $product_supplier->save();
          }
        }
      }
      elseif( $field == "base_price" ){
        $productObject->price = number_format($value, 4, '.', '');
      }
      elseif( $field == "base_price_with_tax" ){
        $taxPrice = (float)$value;
        if( $productObject->tax_rate ){
          $taxPrice = $taxPrice / (($productObject->tax_rate/100)+1);
        }
        $productObject->price = number_format($taxPrice, 4, '.', '');
      }
      elseif( $field == "wholesale_price" ){
        $productObject->wholesale_price = number_format($value, 4, '.', '');
      }
      else{
        if( $field != "id_product" && $field != "id_product_attribute" && $field != "id_specific_price" ){
          $productObject->$field = $value;
        }
      }
    }

    if( ( $error = $productObject->validateFields(false, true) ) !== true ){
      $this->_createErrorsFile($error, $product['id_product']);
      return false;
    }

    $productObject->update();
    $this->_updatedProducts++;
    return true;
  }

  private function _copyFile()
  {
    if ( !isset($_FILES['file']) )
    {
      throw new Exception( Module::getInstanceByName('updateproducts')->l('Please select file for update!') );
    }

    $file_type = Tools::substr($_FILES['file']['name'], strrpos($_FILES['file']['name'], '.')+1);

    if( $file_type != $this->_format && $this->_format == 'xlsx' ){
      throw new Exception( Module::getInstanceByName('updateproducts')->l('Error: Your select in setting XLSX extension file. Uploaded file must have XLSX extension!') );
    }

    if( $file_type != $this->_format && $this->_format == 'csv' ){
      throw new Exception( Module::getInstanceByName('updateproducts')->l('Error: Your select in setting CSV extension file. Uploaded file must have CSV extension!') );
    }

    if (!Tools::copy($_FILES['file']['tmp_name'],  dirname(__FILE__).'/files/import_products.'. $this->_format)){
      throw new Exception(Module::getInstanceByName('updateproducts')->l('An error occurred while uploading: ', 'import').$_FILES['file']['tmp_name']);
    }

    $this->_PHPExcelFactory = PHPExcel_IOFactory::load("files/import_products." . $this->_format );
  }


  private function _createErrorsFile($error, $nameProduct)
  {

    $write_fd = fopen('error/error_logs.csv', 'a+');
    if (@$write_fd !== false){
      fwrite($write_fd, $nameProduct . ',' . $error . "\r\n");
    }
    fclose($write_fd);
  }

}
