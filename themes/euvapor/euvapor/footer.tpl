{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

		{if !$content_only}
				</div>

<!-- Right -->
				<!--div id="right_column" class="column grid_2 omega">
					{* $HOOK_RIGHT_COLUMN *}
				</div-->
			<!--/div-->

<!-- Footer -->
      <div id="footer" class="grid_9">
       {hook h='displayRech'}
        

      <div class="foo">  {hook h='displayFooter'}


      <div class="foo">
<div id="log1"><a href="http://www.fivape.org/" target="_blank"><img src="{$img_ps_dir}cms/fivap2.png"></a></div>
<div id="log2"><a href="http://www.efvi.eu/index.fr.html" target="_blank"><img src="{$img_ps_dir}cms/support-white.png"></a></div>
      </div>
            <div class="copyright">© Copyright 2014 by <a href="http://sitenco.com/" TARGET="_blank">sitenco  </a>   |    <a href="{$link->getCMSLink('3', 'conditions-generales-de-ventes')} ">{l s="Conditions d'utilisation"}</a> | 
            <a href="{$link->getCMSLink('2', 'mentions-legales')}">{l s="Mention légale"}</a> | 
            <a href="{$link->getCMSLink('17', 'nos-partenaires')}">{l s="Nos partenaires"}</a> | 
            <a href="{$link->getPageLink('contact-form.php')}" title="{l s='Contact Us' mod='blockcms'}">{l s='Contactez-nous'}</a>
            </div>
             {if $PS_ALLOW_MOBILE_DEVICE}
          <p class="center clearBoth"><a href="{$link->getPageLink('index', true)}?mobile_theme_ok">{l s='Browse the mobile site'}</p>
        {/if}
      </div></div>
    </div>
  {/if}

		<script>
(function(i, s, o, g, r, a, m){
  i['GoogleAnalyticsObject'] = r; // Acts as a pointer to support renaming.

  // Creates an initial ga() function.  The queued commands will be executed once analytics.js loads.
  i[r] = i[r] || function() {
    (i[r].q = i[r].q || []).push(arguments)
  },

  // Sets the time (as an integer) this tag was executed.  Used for timing hits.
  i[r].l = 1 * new Date();

  // Insert the script tag asynchronously.  Inserts above current tag to prevent blocking in
  // addition to using the async attribute.
  a = s.createElement(o),
  m = s.getElementsByTagName(o)[0];
  a.async = 1;
  a.src = g;
  m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-25179921-1', 'wholesale.euvapors.com'); // Creates the tracker with default parameters.
ga('require', 'displayfeatures');
ga('send', 'pageview');            // Sends a pageview hit.
</script>
	</body>
</html>
