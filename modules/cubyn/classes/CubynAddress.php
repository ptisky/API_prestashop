<?php
/**
 * 2015-2016 Cubyn
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Cubyn <modules@cubyn.com>
 * @copyright 2015-2016 Cubyn
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class CubynAddress
{

    protected $line_1;
    protected $line_2;
    protected $zip;
    protected $city;
    protected $country;
    protected $additionalInformation;
    protected $lang;
    private $context;

    /**
     * @return string
     */
    public function getLine1()
    {
        return $this->line_1;
    }

    /**
     * @return string
     */
    public function getLine2()
    {
        return $this->line_2;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function getAdditionalInformation()
    {
        return $this->additionalInformation;
    }

    /**
     * @return string
     */
    public function getCustomerLastName()
    {
        if (isset($this->_deliveryAddress->lastname)) {
            return $this->_deliveryAddress->lastname;
        }
        return $this->_customer->lastname;
    }

    /**
     * @return string
     */
    public function getCustomerFirstName()
    {
        if (isset($this->_deliveryAddress->firstname)) {
            return $this->_deliveryAddress->firstname;
        }
        return $this->_customer->firstname;
    }

    /**
     * @return string
     */
    public function getCustomerCompany()
    {
        if (isset($this->_deliveryAddress->company)) {
            return $this->_deliveryAddress->company;
        } elseif (isset($this->_customer->company)) {
            return $this->_customer->company;
        }
        return null;
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->_customer->email;
    }

    /**
     * @var AddressCore
     */
    private $_deliveryAddress;

    /**
     * @var CustomerCore
     */
    private $_customer;

    public function __construct($id_customer, $id_address)
    {
        $this->initContext();
        $this->lang                  = $this->context->language->id;
        $this->_deliveryAddress      = new Address($id_address, $this->lang);
        $this->_customer             = new Customer($id_customer);
        $this->line_1                = $this->_deliveryAddress->address1;
        $this->line_2                = $this->_deliveryAddress->address2;
        $this->zip                   = $this->_deliveryAddress->postcode;
        $this->city                  = $this->_deliveryAddress->city;
        $this->country               = $this->getCountryName($this->_deliveryAddress->id_country);
        $this->additionalInformation = $this->_deliveryAddress->other;
    }

    private function initContext()
    {
        $this->ajax    = true;
        $this->context = Context::getContext();
    }

    private function getCountryName($id_country)
    {
        $country = new Country($id_country, $this->lang);
        return $country->iso_code;
    }

    public function toArray()
    {
        return array(
            'line1'                 => $this->line_1,
            'line2'                 => $this->line_2,
            'zip'                   => $this->zip,
            'city'                  => $this->city,
            'country'               => $this->country,
            'additionalInformation' => $this->additionalInformation,
        );
    }

    public function getDeliveryPhone()
    {
        if ($this->_deliveryAddress->phone_mobile == '') {
            return $this->_deliveryAddress->phone;
        }
        return $this->_deliveryAddress->phone_mobile;
    }
}
