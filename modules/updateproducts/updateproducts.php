<?php

if (!defined('_PS_VERSION_')){
  exit;
}

class updateProducts extends Module{

  public $fieldIdProduct;
  public $fieldIdProductCombination;
  public $fieldIdSpecificPrice;
  private $_model;
  private $_exportBase;
  private $_exportCombinations;
  private $_exportSpecificPrice;
  private $_updateBase;
  private $_updateCombinations;
  private $_updateSpecificPrice;


  public function __construct(){
    include_once(_PS_MODULE_DIR_ . 'updateproducts/datamodel.php');
    $this->_model = new productsUpdateModel();

    if( isset(Context::getContext()->shop->id_shop_group) ){
      $this->_shopGroupId = Context::getContext()->shop->id_shop_group;
    }
    elseif( isset(Context::getContext()->shop->id_group_shop) ){
      $this->_shopGroupId = Context::getContext()->shop->id_group_shop;
    }

    $this->_shopId = Context::getContext()->shop->id;

    $this->name = 'updateproducts';
    $this->tab = 'quick_bulk_update';
    $this->version = '2.5.0';
    $this->author = 'MyPrestaModules';
    $this->need_instance = 0;
    $this->bootstrap = true;
    $this->module_key = "c6c523426a37f6035086d59f2f48981f";

    parent::__construct();

    $this->displayName = $this->l('Product Catalog Export/Update');
    $this->description = $this->l('Product Catalog Export/Update module is a convenient module especially designed to perform export and update operations with the PrestaShop products.');
    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

    $this->fieldIdProduct = $this->l('ID Product');
    $this->fieldIdProductCombination = $this->l('ID Product Combination');
    $this->fieldIdSpecificPrice = $this->l('ID Specific Price');

    $this->_updateBase = array(
      array(
        'val' => 'name',
        'name' => $this->l('Name'),
        'hint' => $this->l('The public name for product.')
      ),
      array(
        'val' => 'reference',
        'name' => $this->l('Reference code'),
        'hint' => $this->l('Your internal reference code for this product.')
      ),
      array(
        'val' => 'link_rewrite',
        'name' => $this->l('Friendly URL')
      ),
      array(
        'val' => 'active',
        'name' => $this->l('Enabled'),
        'hint' => $this->l('Value 0 or 1')
      ),
      array(
        'val' => 'show_price',
        'name' => $this->l('Show price')
      ),
      array(
        'val' => 'base_price',
        'name' => $this->l('Pre-tax retail price'),
        'hint' => $this->l('The pre-tax retail price is the price for which you intend sell this product to your customers. It should be higher than the pre-tax wholesale price: the difference between the two will be your margin.'),
      ),
      array(
        'val' => 'base_price_with_tax',
        'name' => $this->l('Retail price with tax')
      ),
      array(
        'val' => 'wholesale_price',
        'name' => $this->l('Pre-tax wholesale price'),
        'hint' => $this->l('The wholesale price is the price you paid for the product. Do not include the tax.')
      ),
      array(
        'val' => 'unit_price_ratio',
        'name' => $this->l('Unit price ratio')
      ),
      array(
        'val' => 'unity',
        'name' => $this->l('Unity (for unit price)')
      ),
      array(
        'val' => 'quantity',
        'name' => $this->l('Quantity'),
        'hint' => $this->l('Available quantities for sale')
      ),
      array(
        'val' => 'minimal_quantity',
        'name' => $this->l('Minimum quantity'),
        'hint' => $this->l('The minimum quantity to buy this product (set to 1 to disable this feature)')
      ),
      array(
        'val' => 'description_short',
        'name' => $this->l('Short description'),
        'hint' => $this->l('Appears in the product list(s), and at the top of the product page.')
      ),
      array(
        'val' => 'description',
        'name' => $this->l('Description'),
        'hint' => $this->l('Appears in the body of the product page.')
      ),
      array(
        'val' => 'categories_ids',
        'name' => $this->l('Associated categories Ids'),
        'hint' => $this->l('Each associated category id separated by a semicolon')
      ),
      array(
        'val' => 'id_category_default',
        'name' => $this->l('ID Category Default'),
        'hint' => $this->l('')
      ),
      array(
        'val' => 'id_manufacturer',
        'name' => $this->l('ID Manufacturer')
      ),
      array(
        'val' => 'suppliers_ids',
        'name' => $this->l('Supplier Ids'),
        'hint' => $this->l('Each supplier ID separated by a semicolon')
      ),
      array(
        'val' => 'id_supplier_default',
        'name' => $this->l('ID supplier default')
      ),
      array(
        'val' => 'supplier_reference',
        'name' => $this->l('Supplier reference default')
      ),
      array(
        'val' => 'features',
        'name' => $this->l('Features'),
        'hint' => $this->l('Each product feature would be exported in a separate column!'),
      ),
      array(
        'val' => 'tags',
        'name' => $this->l('Tags'),
        'hint' => $this->l('Will be displayed in the tags block when enabled. Tags help customers easily find your products.')
      ),
      array(
        'val' => 'id_tax_rules_group',
        'name' => $this->l('Id tax rules group')
      ),
      array(
        'val' => 'meta_title',
        'name' => $this->l('Meta title'),
        'hint' => $this->l('Public title for the products page, and for search engines. Leave blank to use the product name. The number of remaining characters is displayed to the left of the field.'),
      ),
      array(
        'val' => 'meta_description',
        'name' => $this->l('Meta description'),
        'hint' => $this->l('This description will appear in search engines. You need a single sentence, shorter than 160 characters (including spaces).'),
      ),
      array(
        'val' => 'meta_keywords',
        'name' => $this->l('Meta keywords'),
        'hint' => $this->l('Keywords for HTML header, separated by commas.'),
      ),
      array(
        'val' => 'ean13',
        'name' => $this->l('EAN-13 or JAN barcode'),
        'hint' => $this->l('This type of product code is specific to Europe and Japan, but is widely used internationally. It is a superset of the UPC code: all products marked with an EAN will be accepted in North America.'),
      ),
      array(
        'val' => 'ecotax',
        'name' => $this->l('Ecotax (tax incl.)'),
        'hint' => $this->l('The ecotax is a local set of taxes intended to "promote ecologically sustainable activities via economic incentives". It is already included in retail price: the higher this ecotax is, the lower your margin will be.')
      ),
      array(
        'val' => 'upc',
        'name' => $this->l('UPC barcode'),
        'hint' => $this->l('This type of product code is widely used in the United States, Canada, the United Kingdom, Australia, New Zealand and in other countries.'),
      ),
      array(
        'val' => 'width',
        'name' => $this->l('Package width')
      ),
      array(
        'val' => 'height',
        'name' => $this->l('Package height')
      ),
      array(
        'val' => 'depth',
        'name' => $this->l('Package depth')
      ),
      array(
        'val' => 'weight',
        'name' => $this->l('Package weight')
      ),
      array(
        'val' => 'condition',
        'name' => $this->l('Condition')
      ),
      array(
        'val' => 'on_sale',
        'name' => $this->l('Display the  on sale  icon'),
        'hint' => $this->l('Display the  on sale  icon on the product page, and in the text found within the product listing. Value 0 or 1'),
      ),
      array(
        'val' => 'available_now',
        'name' => $this->l('Displayed text when in-stock')
      ),
      array(
        'val' => 'available_later',
        'name' => $this->l('Displayed text when backordering is allowed'),
        'hint' => $this->l('Displayed text when backordering is allowed. If empty, the message "in stock" will be displayed.')
      ),
      array(
        'val' => 'available_for_order',
        'name' => $this->l('Available for order')
      ),
      array(
        'val' => 'online_only',
        'name' => $this->l('Online only'),
        'hint' => $this->l('Online only (not sold in your retail store)')
      ),
      array(
        'val' => 'is_virtual',
        'name' => $this->l('Is virtual product')
      ),
      array(
        'val' => 'location',
        'name' => $this->l('Location')
      ),
      array(
        'val' => 'cache_is_pack',
        'name' => $this->l('cache_is_pack')
      ),
      array(
        'val' => 'cache_has_attachments',
        'name' => $this->l('cache_has_attachments')
      ),
      array(
        'val' => 'additional_shipping_cost',
        'name' => $this->l('Additional shipping fees'),
        'hint' => $this->l('Additional shipping fees (for a single item)')
      ),
      array(
        'val' => 'quantity_discount',
        'name' => $this->l('quantity_discount')
      ),
      array(
        'val' => 'customizable',
        'name' => $this->l('Customizable')
      ),
      array(
        'val' => 'uploadable_files',
        'name' => $this->l('Customization File fields'),
        'hint' => $this->l('Number of upload file fields to be displayed to the user.')
      ),
      array(
        'val' => 'text_fields',
        'name' => $this->l('Customization Text fields'),
        'hint' => $this->l('Number of text fields to be displayed to the user.')
      ),
      array(
        'val' => 'redirect_type',
        'name' => $this->l('redirect_type')
      ),
      array(
        'val' => 'id_product_redirected',
        'name' => $this->l('id_product_redirected')
      ),
      array(
        'val' => 'visibility',
        'name' => $this->l('Visibility')
      ),
      array(
        'val' => 'advanced_stock_management',
        'name' => $this->l('advanced_stock_management')
      ),
      array(
        'val' => 'pack_stock_type',
        'name' => $this->l('pack_stock_type')
      ),
      array(
        'val' => 'available_date',
        'name' => $this->l('Availability date'),
        'hint' => $this->l('The next date of availability for this product when it is out of stock.')
      ),
      array(
        'val' => 'date_add',
        'name' => $this->l('Date add')
      ),
      array(
        'val' => 'date_upd',
        'name' => $this->l('Date update')
      ),
    );

    $this->_updateCombinations = array(
      array(
        'val' => 'combinations_reference',
        'name' => $this->l('Combinations Reference code')
      ),
      array(
        'val' => 'combinations_price',
        'name' => $this->l('Combinations Impact on price (pre-tax)')
      ),
      array(
        'val' => 'combinations_price_with_tax',
        'name' => $this->l('Combinations Impact on price (with-tax)')
      ),
      array(
        'val' => 'combinations_unit_price_impact',
        'name' => $this->l('Combinations Impact on unit price')
      ),
      array(
        'val' => 'combinations_wholesale_price',
        'name' => $this->l('Combinations wholesale price')
      ),
      array(
        'val' => 'combinations_quantity',
        'name' => $this->l('Combinations quantity'),
        'hint' => $this->l('Available quantities for sale')
      ),
      array(
        'val' => 'combinations_minimal_quantity',
        'name' => $this->l('Combinations Minimum quantity'),
        'hint' => $this->l('The minimum quantity to buy this product (set to 1 to disable this feature)')
      ),
      array(
        'val' => 'cache_default_attribute',
        'name' => $this->l('ID Product Combination Default')
      ),
      array(
        'val' => 'combinations_ean13',
        'name' => $this->l('Combinations EAN-13 or JAN barcode')
      ),
      array(
        'val' => 'combinations_upc',
        'name' => $this->l('Combinations UPC barcode')
      ),
      array(
        'val' => 'combinations_ecotax',
        'name' => $this->l('Ecotax (tax excl.)'),
        'hint' => $this->l('Overrides the ecotax from the "Prices" tab.')
      ),
      array(
        'val' => 'combinations_location',
        'name' => $this->l('Combinations location')
      ),
      array(
        'val' => 'combinations_weight',
        'name' => $this->l('Impact on weight')
      ),
    );
    $this->_updateSpecificPrice = array(
      array(
        'val' => 'specific_price_reduction',
        'name' => $this->l('Specific price reduction')
      ),
      array(
        'val' => 'specific_price_reduction_type',
        'name' => $this->l('Specific price reduction type'),
        'hint' => $this->l('Reduction type (amount or percentage)')
      ),
      array(
        'val' => 'specific_price_from',
        'name' => $this->l('Specific price Available from (date)')
      ),
      array(
        'val' => 'specific_price_to',
        'name' => $this->l('Specific price Available to (date)')
      ),
      array(
        'val' => 'specific_price_from_quantity',
        'name' => $this->l('Specific price Starting at (unit)')
      ),
    );

    $this->_exportBase = array(
      array(
        'val'      => 'id_product',
        'name'     => $this->l('ID Product'),
        'disabled' => true
      ),
      array(
        'val'  => 'name',
        'name' => $this->l('Name'),
        'hint' => $this->l('The public name for product.')
      ),
      array(
        'val' => 'reference',
        'name' => $this->l('Reference code'),
        'hint' => $this->l('Your internal reference code for this product.')
      ),
      array(
        'val' => 'link_rewrite',
        'name' => $this->l('Friendly URL')
      ),
      array(
        'val' => 'active',
        'name' => $this->l('Enabled'),
        'hint' => $this->l('Value 0 or 1')
      ),
      array(
        'val' => 'show_price',
        'name' => $this->l('Show price')
      ),
      array(
        'val' => 'wholesale_price',
        'name' => $this->l('Pre-tax wholesale price'),
        'hint' => $this->l('The wholesale price is the price you paid for the product. Do not include the tax.')
      ),
      array(
        'val' => 'base_price',
        'name' => $this->l('Pre-tax retail price'),
        'hint' => $this->l('The pre-tax retail price is the price for which you intend sell this product to your customers. It should be higher than the pre-tax wholesale price: the difference between the two will be your margin.'),
      ),
      array(
        'val' => 'base_price_with_tax',
        'name' => $this->l('Retail price with tax')
      ),
      array(
        'val' => 'price',
        'name' => $this->l('Pre-tax Final price'),
        'hint' => $this->l('Pre-tax retail price plus default combination impact price')
      ),
      array(
        'val' => 'final_price_with_tax',
        'name' => $this->l('Final price with tax'),
        'hint' => $this->l('Retail price plus default combination impact price with tax')
      ),
      array(
        'val' => 'unit_price_ratio',
        'name' => $this->l('Unit price ratio')
      ),
      array(
        'val' => 'unit_price',
        'name' => $this->l('Unit price (tax excl.)'),
        'hint' => $this->l('When selling a pack of items, you can indicate the unit price for each item of the pack. For instance, "per bottle" or "per pound".')
      ),
      array(
        'val' => 'unity',
        'name' => $this->l('Unity (for unit price)')
      ),
      array(
        'val' => 'quantity',
        'name' => $this->l('Quantity'),
        'hint' => $this->l('Available quantities for sale')
      ),
      array(
        'val' => 'minimal_quantity',
        'name' => $this->l('Minimum quantity'),
        'hint' => $this->l('The minimum quantity to buy this product (set to 1 to disable this feature)')
      ),
      array(
        'val' => 'description_short',
        'name' => $this->l('Short description'),
        'hint' => $this->l('Appears in the product list(s), and at the top of the product page.')
      ),
      array(
        'val' => 'description',
        'name' => $this->l('Description'),
        'hint' => $this->l('Appears in the body of the product page.')
      ),
      array(
        'val' => 'categories_ids',
        'name' => $this->l('Associated categories Ids'),
        'hint' => $this->l('Each associated category id separated by a semicolon')
      ),
      array(
        'val' => 'categories_names',
        'name' => $this->l('Associated categories name'),
        'hint' => $this->l('Each associated category name separated by a semicolon')
      ),
      array(
        'val' => 'id_category_default',
        'name' => $this->l('ID Category Default')
      ),
      array(
        'val' => 'id_manufacturer',
        'name' => $this->l('ID Manufacturer')
      ),
      array(
        'val' => 'manufacturer_name',
        'name' => $this->l('Manufacturer name')
      ),
      array(
        'val'  => 'suppliers_ids',
        'name' => $this->l('Supplier Ids'),
        'hint' => $this->l('Each supplier ID separated by a semicolon')
      ),
      array(
        'val' => 'suppliers_name',
        'name' => $this->l('Suppliers name'),
        'hint' => $this->l('Each supplier name separated by a semicolon')
      ),
      array(
        'val' => 'id_supplier',
        'name' => $this->l('ID supplier default')
      ),
      array(
        'val' => 'supplier_name',
        'name' => $this->l('Supplier name default')
      ),
      array(
        'val' => 'supplier_reference',
        'name' => $this->l('Supplier reference default')
      ),
      array(
        'val' => 'features',
        'name' => $this->l('Features'),
        'hint' => $this->l('Each product feature would be exported in a separate column!'),
      ),
      array(
        'val' => 'tags',
        'name' => $this->l('Tags'),
        'hint' => $this->l('Will be displayed in the tags block when enabled. Tags help customers easily find your products.')
      ),
      array(
        'val' => 'tax_rate',
        'name' => $this->l('Tax rate')
      ),
      array(
        'val' => 'id_tax_rules_group',
        'name' => $this->l('Id tax rules group')
      ),
      array(
        'val' => 'meta_title',
        'name' => $this->l('Meta title'),
        'hint' => $this->l('Public title for the products page, and for search engines. Leave blank to use the product name. The number of remaining characters is displayed to the left of the field.'),
      ),
      array(
        'val' => 'meta_description',
        'name' => $this->l('Meta description'),
        'hint' => $this->l('This description will appear in search engines. You need a single sentence, shorter than 160 characters (including spaces).'),
      ),
      array(
        'val' => 'meta_keywords',
        'name' => $this->l('Meta keywords'),
        'hint' => $this->l('Keywords for HTML header, separated by commas.'),
      ),
      array(
        'val' => 'ean13',
        'name' => $this->l('EAN-13 or JAN barcode'),
        'hint' => $this->l('This type of product code is specific to Europe and Japan, but is widely used internationally. It is a superset of the UPC code: all products marked with an EAN will be accepted in North America.'),
      ),
      array(
        'val' => 'ecotax',
        'name' => $this->l('Ecotax (tax incl.)'),
        'hint' => $this->l('The ecotax is a local set of taxes intended to "promote ecologically sustainable activities via economic incentives". It is already included in retail price: the higher this ecotax is, the lower your margin will be.')
      ),
      array(
        'val' => 'upc',
        'name' => $this->l('UPC barcode'),
        'hint' => $this->l('This type of product code is widely used in the United States, Canada, the United Kingdom, Australia, New Zealand and in other countries.'),
      ),
      array(
        'val' => 'width',
        'name' => $this->l('Package width')
      ),
      array(
        'val' => 'height',
        'name' => $this->l('Package height')
      ),
      array(
        'val' => 'depth',
        'name' => $this->l('Package depth')
      ),
      array(
        'val' => 'weight',
        'name' => $this->l('Package weight')
      ),
      array(
        'val' => 'condition',
        'name' => $this->l('Condition')
      ),
      array(
        'val' => 'new',
        'name' => $this->l('new')
      ),
      array(
        'val' => 'on_sale',
        'name' => $this->l('Display the  on sale  icon'),
        'hint' => $this->l('Display the  on sale  icon on the product page, and in the text found within the product listing. Value 0 or 1'),
      ),
      array(
        'val' => 'available_now',
        'name' => $this->l('Displayed text when in-stock')
      ),
      array(
        'val' => 'available_later',
        'name' => $this->l('Displayed text when backordering is allowed'),
        'hint' => $this->l('Displayed text when backordering is allowed. If empty, the message "in stock" will be displayed.')
      ),
      array(
        'val' => 'available_for_order',
        'name' => $this->l('Available for order')
      ),
      array(
        'val' => 'online_only',
        'name' => $this->l('Online only'),
        'hint' => $this->l('Online only (not sold in your retail store)')
      ),
      array(
        'val' => 'is_virtual',
        'name' => $this->l('Is virtual product')
      ),
      array(
        'val' => 'out_of_stock',
        'name' => $this->l('When out of stock'),
        'hint' => $this->l('0 - Deny orders, 1 - Allow orders, 2 - Default'),
      ),
      array(
        'val' => 'id_shop_default',
        'name' => $this->l('Id shop default')
      ),
      array(
        'val' => 'location',
        'name' => $this->l('Location')
      ),
      array(
        'val' => 'cache_is_pack',
        'name' => $this->l('cache_is_pack')
      ),
      array(
        'val' => 'cache_has_attachments',
        'name' => $this->l('cache_has_attachments')
      ),
      array(
        'val' => 'additional_shipping_cost',
        'name' => $this->l('Additional shipping fees'),
        'hint' => $this->l('Additional shipping fees (for a single item)')
      ),
      array(
        'val' => 'quantity_discount',
        'name' => $this->l('quantity_discount')
      ),
      array(
        'val' => 'customizable',
        'name' => $this->l('Customizable')
      ),
      array(
        'val' => 'uploadable_files',
        'name' => $this->l('Customization File fields'),
        'hint' => $this->l('Number of upload file fields to be displayed to the user.')
      ),
      array(
        'val' => 'text_fields',
        'name' => $this->l('Customization Text fields'),
        'hint' => $this->l('Number of text fields to be displayed to the user.')
      ),
      array(
        'val' => 'redirect_type',
        'name' => $this->l('redirect_type')
      ),
      array(
        'val' => 'id_product_redirected',
        'name' => $this->l('id_product_redirected')
      ),
      array(
        'val' => 'indexed',
        'name' => $this->l('Indexed')
      ),
      array(
        'val' => 'visibility',
        'name' => $this->l('Visibility')
      ),
      array(
        'val' => 'id_color_default',
        'name' => $this->l('id_color_default')
      ),
      array(
        'val' => 'advanced_stock_management',
        'name' => $this->l('advanced_stock_management')
      ),
      array(
        'val' => 'depends_on_stock',
        'name' => $this->l('depends_on_stock')
      ),
      array(
        'val' => 'isFullyLoaded',
        'name' => $this->l('isFullyLoaded')
      ),
      array(
        'val' => 'id_pack_product_attribute',
        'name' => $this->l('Id pack product attribute')
      ),
      array(
        'val' => 'pack_stock_type',
        'name' => $this->l('pack_stock_type')
      ),
      array(
        'val' => 'images',
        'name' => $this->l('Product Image urls')
      ),
      array(
        'val' => 'product_link',
        'name' => $this->l('Product url')
      ),
      array(
        'val' => 'available_date',
        'name' => $this->l('Availability date'),
        'hint' => $this->l('The next date of availability for this product when it is out of stock.')
      ),
      array(
        'val' => 'date_add',
        'name' => $this->l('Date add')
      ),
      array(
        'val' => 'date_upd',
        'name' => $this->l('Date update')
      ),
    );

    $this->_exportSpecificPrice = array(
      array(
        'val' => 'id_specific_price',
        'name' => $this->l('ID Specific Price'),
        'disabled' => true
      ),
      array(
        'val' => 'specific_price_reduction',
        'name' => $this->l('Specific price reduction')
      ),
      array(
        'val' => 'specific_price_reduction_type',
        'name' => $this->l('Specific price reduction type'),
        'hint' => $this->l('Reduction type (amount or percentage)')
      ),
      array(
        'val' => 'specific_price_from',
        'name' => $this->l('Specific price Available from (date)')
      ),
      array(
        'val' => 'specific_price_to',
        'name' => $this->l('Specific price Available to (date)')
      ),
      array(
        'val' => 'specific_price_from_quantity',
        'name' => $this->l('Specific price Starting at (unit)')
      ),
    );

    $this->_exportCombinations = array(
      array(
        'val' => 'id_product_attribute',
        'name' => $this->l('ID Product Combination'),
        'disabled' => true
      ),
      array(
        'val' => 'combinations_name',
        'name' => $this->l('Combinations (Attribute - value pair)'),
        'hint' => $this->l('Each combination name separated by a semicolon')
      ),
      array(
        'val' => 'combinations_reference',
        'name' => $this->l('Combinations Reference code'),
      ),
      array(
        'val' => 'combinations_price',
        'name' => $this->l('Combinations Impact on price (pre-tax)')
      ),
      array(
        'val' => 'combinations_price_with_tax',
        'name' => $this->l('Combinations Impact on price (with-tax)')
      ),
      array(
        'val' => 'combinations_unit_price_impact',
        'name' => $this->l('Combinations Impact on unit price')
      ),
      array(
        'val' => 'combinations_wholesale_price',
        'name' => $this->l('Combinations wholesale price')
      ),
      array(
        'val' => 'combinations_quantity',
        'name' => $this->l('Combinations quantity'),
        'hint' => $this->l('Available quantities for sale')
      ),
      array(
        'val' => 'combinations_minimal_quantity',
        'name' => $this->l('Combinations Minimum quantity'),
        'hint' => $this->l('The minimum quantity to buy this product (set to 1 to disable this feature)')
      ),
      array(
        'val' => 'cache_default_attribute',
        'name' => $this->l('ID Product Combination Default')
      ),
      array(
        'val' => 'combinations_ean13',
        'name' => $this->l('Combinations EAN-13 or JAN barcode')
      ),
      array(
        'val' => 'combinations_upc',
        'name' => $this->l('Combinations UPC barcode')
      ),
      array(
        'val' => 'combinations_ecotax',
        'name' => $this->l('Ecotax (tax excl.)'),
        'hint' => $this->l('Overrides the ecotax from the "Prices" tab.')
      ),

      array(
        'val' => 'combinations_location',
        'name' => $this->l('Combinations location')
      ),
      array(
        'val' => 'combinations_weight',
        'name' => $this->l('Impact on weight')
      ),

    );


  }

  public function install()
  {
    Configuration::updateValue('GOMAKOIL_PRODUCTS_CHECKED', '', false, $this->_shopGroupId, Context::getContext()->shop->id);
    Configuration::updateValue('GOMAKOIL_MANUFACTURERS_CHECKED', '', false, $this->_shopGroupId, Context::getContext()->shop->id);
    Configuration::updateValue('GOMAKOIL_SUPPLIERS_CHECKED', '', false, $this->_shopGroupId, Context::getContext()->shop->id);
    Configuration::updateValue('GOMAKOIL_CATEGORIES_CHECKED', '', false, $this->_shopGroupId, Context::getContext()->shop->id);
    Configuration::updateValue('GOMAKOIL_FIELDS_CHECKED', '', false, $this->_shopGroupId, Context::getContext()->shop->id);
    Configuration::updateValue('GOMAKOIL_ALL_SETTINGS', '', false, $this->_shopGroupId, Context::getContext()->shop->id);
    Configuration::updateValue('GOMAKOIL_ALL_UPDATE_SETTINGS', '', false, $this->_shopGroupId, Context::getContext()->shop->id);
    if ( !parent::install()  || !$this->registerHook('ActionAdminControllerSetMedia') ) {
      return false;
    }
    return true;
  }

  public function uninstall(){

    $this->removeAllSettings();

    Configuration::deleteByName('GOMAKOIL_PRODUCTS_CHECKED');
    Configuration::deleteByName('GOMAKOIL_MANUFACTURERS_CHECKED');
    Configuration::deleteByName('GOMAKOIL_SUPPLIERS_CHECKED');
    Configuration::deleteByName('GOMAKOIL_CATEGORIES_CHECKED');
    Configuration::deleteByName('GOMAKOIL_FIELDS_CHECKED');
    Configuration::deleteByName('GOMAKOIL_ALL_SETTINGS');
    Configuration::deleteByName('GOMAKOIL_ALL_UPDATE_SETTINGS');

    return parent::uninstall();
  }

  public function removeAllSettings(){
    $all_setting = array();
    $all_setting =Tools::unserialize( Configuration::get('GOMAKOIL_ALL_SETTINGS','',$this->_shopGroupId, Context::getContext()->shop->id));

    foreach($all_setting as $value){
      Configuration::deleteByName('GOMAKOIL_PRODUCTS_CHECKED_'.$value);
      Configuration::deleteByName('GOMAKOIL_MANUFACTURERS_CHECKED_'.$value);
      Configuration::deleteByName('GOMAKOIL_SUPPLIERS_CHECKED_'.$value);
      Configuration::deleteByName('GOMAKOIL_CATEGORIES_CHECKED_'.$value);
      Configuration::deleteByName('GOMAKOIL_FIELDS_CHECKED_'.$value);
      Configuration::deleteByName('GOMAKOIL_LANG_CHECKED_'.$value);
      Configuration::deleteByName('GOMAKOIL_NAME_SETTING_'.$value);
      Configuration::deleteByName('GOMAKOIL_TYPE_FILE_'.$value);
    }

    $all_setting_update = array();
    $all_setting_update = Tools::unserialize( Configuration::get('GOMAKOIL_ALL_UPDATE_SETTINGS','',$this->_shopGroupId, Context::getContext()->shop->id));

    foreach($all_setting_update as $value){
      Configuration::deleteByName('GOMAKOIL_NAME_SETTING_UPDATE_'.$value);
      Configuration::deleteByName('GOMAKOIL_FIELDS_CHECKED_UPDATE_'.$value);
      Configuration::deleteByName('GOMAKOIL_LANG_CHECKED_UPDATE_'.$value);
      Configuration::deleteByName('GOMAKOIL_TYPE_FILE_UPDATE_'.$value);
    }

  }

  public function hookActionAdminControllerSetMedia()
  {
    $this->context->controller->addCSS($this->_path.'views/css/style.css');
    $this->context->controller->addJS($this->_path.'views/js/main.js');
    $this->context->controller->addJqueryUI('ui.sortable');
  }

  public function getContent()
  {
    $logo = '<img class="logo_myprestamodules" src="../modules/'.$this->name.'/logo.png" />';
    $name = '<h2 id="bootstrap_products">'.$logo.$this->displayName.'</h2>';

    if( Tools::getValue('settings') ){
     $id = Tools::getValue('settings');
    }
    else{
      $id = false;
    }

    if( Tools::getValue('settingsUpdate') ){
      $idUpdate = Tools::getValue('settingsUpdate');
    }
    else{
      $idUpdate = false;
    }

    return $name.$this->displayForm().$this->listSettings($id, $idUpdate);
  }

  public function getPath()
  {
    return $_SERVER['REWRITEBASE'] . "modules/updateproducts/";
  }

  public function displayForm()
  {

    if(Tools::getValue('settings')){
      $last_id = Tools::getValue('settings');
      $this->replaceConfig($last_id);
    }
    else{
      $all_setting = array();
      Configuration::updateValue('GOMAKOIL_PRODUCTS_CHECKED', '', false, $this->_shopGroupId, Context::getContext()->shop->id);
      Configuration::updateValue('GOMAKOIL_MANUFACTURERS_CHECKED', '', false, $this->_shopGroupId, Context::getContext()->shop->id);
      Configuration::updateValue('GOMAKOIL_SUPPLIERS_CHECKED', '', false, $this->_shopGroupId, Context::getContext()->shop->id);
      Configuration::updateValue('GOMAKOIL_CATEGORIES_CHECKED', '', false, $this->_shopGroupId, Context::getContext()->shop->id);
      Configuration::updateValue('GOMAKOIL_FIELDS_CHECKED', '', false, $this->_shopGroupId, Context::getContext()->shop->id);
      $all_setting =Tools::unserialize( Configuration::get('GOMAKOIL_ALL_SETTINGS','',$this->_shopGroupId, Context::getContext()->shop->id));
      if($all_setting){
        $all_setting = max($all_setting);
        $last_id = $all_setting + 1;
      }
      else{
        $last_id = 1;
      }
    }

    if( Tools::getValue('settingsUpdate') ){
      $last_id_update = Tools::getValue('settingsUpdate');
      $this->replaceConfigUpdate($last_id_update);
    }
    else{
      $all_setting_update = array();
      Configuration::updateValue('GOMAKOIL_FIELDS_CHECKED_UPDATE', '', false, $this->_shopGroupId, Context::getContext()->shop->id);
      $all_setting_update =Tools::unserialize( Configuration::get('GOMAKOIL_ALL_UPDATE_SETTINGS','',$this->_shopGroupId, Context::getContext()->shop->id));

      if($all_setting_update){
        $all_setting_update = max($all_setting_update);
        $last_id_update = $all_setting_update + 1;
      }
      else{
        $last_id_update = 1;
      }
    }

    $products = Product::getProducts(Context::getContext()->language->id, 0, 300, 'name', 'asc' );
    $manufacturers = Manufacturer::getManufacturers(false, Context::getContext()->language->id, true, false, false, false, true );
    $suppliers = Supplier::getSuppliers(false, Context::getContext()->language->id);
    $selected_products = Tools::unserialize(Configuration::get('GOMAKOIL_PRODUCTS_CHECKED','',$this->_shopGroupId, Context::getContext()->shop->id));
    $selected_manufacturers = Tools::unserialize(Configuration::get('GOMAKOIL_MANUFACTURERS_CHECKED','',$this->_shopGroupId, Context::getContext()->shop->id));
    $selected_suppliers = Tools::unserialize(Configuration::get('GOMAKOIL_SUPPLIERS_CHECKED','',$this->_shopGroupId, Context::getContext()->shop->id));
    $selected_categories = Tools::unserialize(Configuration::get('GOMAKOIL_CATEGORIES_CHECKED','',$this->_shopGroupId, Context::getContext()->shop->id));

    $this->fields_form[0]['form'] = array(
      'tabs' => array(
        'export' => $this->l('Export'),
        'update' => $this->l('Update'),
      ),
      'input' => array(
        array(
          'type' => 'radio',
          'label' => $this->l('Select format file:'),
          'name' => 'format_file',
          'required' => true,
          'class' => 'format_file',
          'br' => true,
          'tab' => 'export',
          'values' => array(
            array(
              'id' => 'format_csv',
              'value' => 'csv',
              'label' => $this->l('CSV')
            ),
            array(
              'id' => 'format_xlsx',
              'value' => 'xlsx',
              'label' => $this->l('XLSX')
            )
          )
        ),
        array(
          'type' => 'radio',
          'label' => $this->l('Select format file:'),
          'name' => 'format_file_update',
          'required' => true,
          'class' => 'format_file',
          'br' => true,
          'tab' => 'update',
          'values' => array(
            array(
              'id' => 'format_csv',
              'value' => 'csv',
              'label' => $this->l('CSV')
            ),
            array(
              'id' => 'format_xlsx',
              'value' => 'xlsx',
              'label' => $this->l('XLSX')
            )
          )
        ),
        array(
          'type' => 'file',
          'label' => $this->l('File'),
          'name' => 'file',
          'tab' => 'update',
        ),
        array(
          'type' => 'html',
          'name' => 'html_data',
          'tab' => 'export',
          'html_content' => '<div class="module_hind">' . $this->l('If no filter is selected, module will export all products!') . '</div>'
        ),
        array(
          'type' => 'checkbox_table',
          'name' => 'products[]',
          'class_block' => 'product_list',
          'label' => $this->l('Select products:'),
          'class_input' => 'select_products',
          'lang' => true,
          'hint' => '',
          'tab' => 'export',
          'search' => true,
          'display'=> true,
          'values' => array(
            'query' => $products,
            'id' => 'id_product',
            'name' => 'name',
            'value' => $selected_products
          )
        ),
        array(
          'type'  => 'categories',
          'label' => $this->l('Select categories'),
          'name'  => 'categories',
          'tab'   => 'export',
          'tree'  => array(
            'id'  => 'categories-tree',
            'use_checkbox' => true,
            'use_search' => true,
            'selected_categories' => $selected_categories ? $selected_categories : array()
          ),
        ),
        array(
          'type' => 'checkbox_table',
          'name' => 'manufacturers[]',
          'class_block' => 'manufacturer_list',
          'label' => $this->l('Select manufacturers:'),
          'class_input' => 'select_manufacturers',
          'lang' => true,
          'hint' => '',
          'tab' => 'export',
          'search' => true,
          'display'=> true,
          'values' => array(
            'query' => $manufacturers,
            'id' => 'id_manufacturer',
            'name' => 'name',
            'value' => $selected_manufacturers
          )
        ),
        array(
          'type' => 'checkbox_table',
          'name' => 'suppliers[]',
          'class_block' => 'supplier_list',
          'label' => $this->l('Select suppliers:'),
          'class_input' => 'select_suppliers',
          'lang' => true,
          'hint' => '',
          'tab' => 'export',
          'search' => true,
          'display'=> true,
          'values' => array(
            'query' => $suppliers,
            'id' => 'id_supplier',
            'name' => 'name',
            'value' => $selected_suppliers
          )
        ),
        array(
          'type' => 'select',
          'label' => $this->l('Language'),
          'name' => 'id_lang',
          'required' => true,
          'default_value' => (int)$this->context->language->id,
          'tab' => 'export',
          'options' => array(
            'query' => Language::getLanguages(),
            'id' => 'id_lang',
            'name' => 'name',
          )
        ),
        array(
          'type' => 'select',
          'label' => $this->l('Language'),
          'name' => 'id_lang_update',
          'required' => true,
          'default_value' => (int)$this->context->language->id,
          'tab' => 'update',
          'options' => array(
            'query' => Language::getLanguages(),
            'id' => 'id_lang',
            'name' => 'name',
          )
        ),

        array(
          'type' => 'html',
          'name' => 'html_data',
          'form_group_class' => 'updateFields',
          'tab' => 'update',
          'html_content' => $this->updateFields(),
        ),
        array(
          'type' => 'html',
          'name' => 'html_data',
          'form_group_class' => 'exportFields',
          'tab' => 'export',
          'html_content' => $this->exportFields(),
        ),

        array(
          'type' => 'hidden',
          'name' => 'id_shop',
        ),
        array(
          'type' => 'hidden',
          'name' => 'last_id',
        ),
        array(
          'type' => 'hidden',
          'name' => 'last_id_update',
        ),
        array(
          'type' => 'hidden',
          'name' => 'base_url',
        ),
        array(
          'type' => 'hidden',
          'name' => 'shopGroupId',
        ),
        array(
          'type' => 'html',
          'name' => 'html_data',
          'form_group_class' => 'exportButton',
          'tab' => 'export',
          'html_content' => '<button type="button" class="btn btn-default export">'.$this->l('Export').'</button>'
        ),
        array(
          'type' => 'html',
          'name' => 'html_data',
          'form_group_class' => 'updateButton',
          'tab' => 'update',
          'html_content' => '<button type="button" class="btn btn-default update">'.$this->l('Update').'</button>'
        ),
        array(
          'type' => 'html',
          'name' => 'html_data',
          'tab' => 'export',
          'html_content' => '<div class="progres_bar_ex"><div class="loading"><div></div></div></div>',
        ),
        array(
          'type' => 'html',
          'tab' => 'update',
          'name' => 'html_data',
          'html_content' => '<div class="progres_bar_ex"><div class="loading"><div></div></div></div>',
        ),
        array(
          'type' => 'html',
          'tab' => 'update',
          'name' => '<div class="panel-heading"><i class="icon-cogs"></i>'.$this->l('  Save Settings').'</div>'
        ),
        array(
          'label' => $this->l('Name setting'),
          'type' => 'text',
          'name' => 'save_setting_update',
          'tab' => 'update',
        ),
        array(
          'type' => 'html',
          'tab' => 'update',
          'form_group_class' => 'saveSettingsUpdateButton',
          'name' => '<button type="button" class="btn btn-default saveSettingsUpdate" style="padding: 4px 30px;font-size: 16px;">'.$this->l('Save').'</button>'
        ),

        array(
          'type' => 'html',
          'tab' => 'export',
          'name' => '<div class="panel-heading"><i class="icon-cogs"></i>'.$this->l('  Save Settings').'</div>'
        ),
        array(
          'label' => $this->l('Name setting'),
          'type' => 'text',
          'name' => 'save_setting',
          'tab' => 'export',
        ),
        array(
          'type' => 'html',
          'tab' => 'export',
          'form_group_class' => 'saveSettingsExportButton',
          'name' => '<button type="button" class="btn btn-default saveSettingsExport" style="padding: 4px 30px;font-size: 16px;">'.$this->l('Save').'</button>'
        ),
      ),
    );



    $helper = new HelperForm();
    $helper->module = $this;
    $helper->name_controller = $this->name;
    $helper->token = Tools::getAdminTokenLite('AdminModules');
    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
    $helper->default_form_language = $default_lang;
    $helper->allow_employee_form_lang = $default_lang;
    $helper->title = $this->displayName;
    $helper->show_toolbar = true;        // false -> remove toolbar
    $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
    $helper->submit_action = 'submit'.$this->name;


    $helper->fields_value['last_id'] = $last_id;
    $helper->fields_value['last_id_update'] = $last_id_update;
    $helper->fields_value['id_shop'] = $this->_shopId;
    $helper->fields_value['base_url'] = AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules');
    $helper->fields_value['shopGroupId'] = $this->_shopGroupId;
    $helper->fields_value['id_lang'] = Context::getContext()->language->id;


    $helper->fields_value['search_field'] = '';


    if(Tools::getValue('settings')){
      $config = Configuration::get('GOMAKOIL_LANG_CHECKED_'.Tools::getValue('settings'), '' ,$this->_shopGroupId, Context::getContext()->shop->id);
      $type = Configuration::get('GOMAKOIL_TYPE_FILE_'.Tools::getValue('settings'), '' ,$this->_shopGroupId, Context::getContext()->shop->id);
      $name_setting = Configuration::get('GOMAKOIL_NAME_SETTING_'.Tools::getValue('settings'), '' ,$this->_shopGroupId, Context::getContext()->shop->id);
      $helper->fields_value['save_setting'] = $name_setting;
      $helper->fields_value['id_lang'] = $config;
      if($type){
        $helper->fields_value['format_file'] = $type;
      }
      else{
        $helper->fields_value['format_file'] = 'xlsx';
      }

    }
    else{
      $helper->fields_value['id_lang'] = Context::getContext()->language->id;
      $helper->fields_value['format_file'] = 'xlsx';
      $helper->fields_value['save_setting'] = '';
    }



    if(Tools::getValue('settingsUpdate')){
      $config = Configuration::get('GOMAKOIL_LANG_CHECKED_UPDATE_'.Tools::getValue('settingsUpdate'), '' ,$this->_shopGroupId, Context::getContext()->shop->id);
      $type = Configuration::get('GOMAKOIL_TYPE_FILE_UPDATE_'.Tools::getValue('settingsUpdate'), '' ,$this->_shopGroupId, Context::getContext()->shop->id);
      $name_setting = Configuration::get('GOMAKOIL_NAME_SETTING_UPDATE_'.Tools::getValue('settingsUpdate'), '' ,$this->_shopGroupId, Context::getContext()->shop->id);
      $helper->fields_value['save_setting_update'] = $name_setting;
      $helper->fields_value['id_lang_update'] = $config;
      if($type){
        $helper->fields_value['format_file_update'] = $type;
      }
      else{
        $helper->fields_value['format_file_update'] = 'xlsx';
      }
    }
    else{
      $helper->fields_value['format_file_update'] = 'xlsx';
      $helper->fields_value['save_setting_update'] = ' ';
      $helper->fields_value['id_lang_update'] = Context::getContext()->language->id;
    }



    return $helper->generateForm($this->fields_form);
  }

  public function searchProducts($search,$id_shop, $id_lang, $shopGroupId)
  {
    $name_config = 'GOMAKOIL_PRODUCTS_CHECKED';
    $products = $this->_model->searchProduct($id_shop, $id_lang, $search);
    $products_check = Tools::unserialize(Configuration::get($name_config, '' ,$shopGroupId, $id_shop));
    $this->context->smarty->assign(
      array(
        'data'        => $products,
        'items_check' => $products_check,
        'name'        => 'products[]',
        'id'          => 'id_product',
        'title'       => 'name',
        'class'       => 'select_products'
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/filterForm.tpl');
  }

  public function searchManufacturers($search, $id_shop, $shopGroupId)
  {
    $name_config = 'GOMAKOIL_MANUFACTURERS_CHECKED';
    $items = $this->_model->searchManufacturer($search);
    $items_check = Tools::unserialize(Configuration::get($name_config, '' ,$shopGroupId, $id_shop));
    $this->context->smarty->assign(
      array(
        'data'        => $items,
        'items_check' => $items_check,
        'name'        => 'manufacturers[]',
        'id'          => 'id_manufacturer',
        'title'       => 'name',
        'class'       => 'select_manufacturers'
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/filterForm.tpl');
  }

  public function searchSuppliers($search, $id_shop, $shopGroupId)
  {
    $name_config = 'GOMAKOIL_SUPPLIERS_CHECKED';
    $items = $this->_model->searchSupplier($search);
    $items_check = Tools::unserialize(Configuration::get($name_config, '' ,$shopGroupId, $id_shop));
    $this->context->smarty->assign(
      array(
        'data'        => $items,
        'items_check' => $items_check,
        'name'        => 'suppliers[]',
        'id'          => 'id_supplier',
        'title'       => 'name',
        'class'       => 'select_suppliers'
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/filterForm.tpl');
  }

  public function showCheckedProducts($id_shop, $id_lang, $shopGroupId)
  {
    $name_config = 'GOMAKOIL_PRODUCTS_CHECKED';
    $products_check = Tools::unserialize(Configuration::get($name_config, '' ,$shopGroupId, $id_shop));
    if( !$products_check ){
      $products_check = "";
    }
    $products = $this->_model->showCheckedProducts($id_shop, $id_lang, $products_check);
    $this->context->smarty->assign(
      array(
        'data'        => $products,
        'items_check' => $products_check,
        'name'        => 'products[]',
        'id'          => 'id_product',
        'title'       => 'name',
        'class'       => 'select_products'
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/filterForm.tpl');
  }

  public function showCheckedManufacturers($id_shop,$shopGroupId)
  {
    $name_config = 'GOMAKOIL_MANUFACTURERS_CHECKED';
    $items_check = Tools::unserialize(Configuration::get($name_config, '' ,$shopGroupId, $id_shop));
    if( !$items_check ){
      $items_check = "";
    }
    $items = $this->_model->showCheckedManufacturers($items_check);
    $this->context->smarty->assign(
      array(
        'data'        => $items,
        'items_check' => $items_check,
        'name'        => 'manufacturers[]',
        'id'          => 'id_manufacturer',
        'title'       => 'name',
        'class'       => 'select_manufacturers'
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/filterForm.tpl');
  }

  public function showCheckedSuppliers($id_shop, $shopGroupId)
  {
    $name_config = 'GOMAKOIL_SUPPLIERS_CHECKED';
    $items_check = Tools::unserialize(Configuration::get($name_config, '' ,$shopGroupId, $id_shop));
    if( !$items_check ){
      $items_check = "";
    }
    $items = $this->_model->showCheckedSuppliers($items_check);
    $this->context->smarty->assign(
      array(
        'data'        => $items,
        'items_check' => $items_check,
        'name'        => 'suppliers[]',
        'id'          => 'id_supplier',
        'title'       => 'name',
        'class'       => 'select_suppliers'
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/filterForm.tpl');
  }

  public function showAllProducts($id_shop, $id_lang, $shopGroupId)
  {
    $name_config = 'GOMAKOIL_PRODUCTS_CHECKED';
    $products_check = Tools::unserialize(Configuration::get($name_config, '' ,$shopGroupId, $id_shop));
    $products = $this->_model->showCheckedProducts($id_shop, $id_lang, false);
    $this->context->smarty->assign(
      array(
        'data'        => $products,
        'items_check' => $products_check,
        'name'        => 'products[]',
        'id'          => 'id_product',
        'title'       => 'name',
        'class'       => 'select_products'
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/filterForm.tpl');
  }

  public function showAllManufacturers($id_shop, $shopGroupId)
  {
    $name_config = 'GOMAKOIL_MANUFACTURERS_CHECKED';
    $items_check = Tools::unserialize(Configuration::get($name_config, '' ,$shopGroupId, $id_shop));
    if( !$items_check ){
      $items_check = "";
    }
    $items = $this->_model->showCheckedManufacturers(false);
    $this->context->smarty->assign(
      array(
        'data'        => $items,
        'items_check' => $items_check,
        'name'        => 'manufacturers[]',
        'id'          => 'id_manufacturer',
        'title'       => 'name',
        'class'       => 'select_manufacturers'
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/filterForm.tpl');
  }


  public function showAllSuppliers($id_shop, $shopGroupId)
  {
    $name_config = 'GOMAKOIL_SUPPLIERS_CHECKED';
    $items_check = Tools::unserialize(Configuration::get($name_config, '' ,$shopGroupId, $id_shop));
    if( !$items_check ){
      $items_check = "";
    }
    $items = $this->_model->showCheckedSuppliers(false);
    $this->context->smarty->assign(
      array(
        'data'        => $items,
        'items_check' => $items_check,
        'name'        => 'suppliers[]',
        'id'          => 'id_supplier',
        'title'       => 'name',
        'class'       => 'select_suppliers'
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/filterForm.tpl');
  }


  public function replaceConfigUpdate($id){

    $config = Configuration::get('GOMAKOIL_FIELDS_CHECKED_UPDATE_'.$id, '' ,$this->_shopGroupId, Context::getContext()->shop->id);
    Configuration::updateValue('GOMAKOIL_FIELDS_CHECKED_UPDATE', $config, false, $this->_shopGroupId, Context::getContext()->shop->id);

  }



  public function replaceConfig($id){

    $config = Configuration::get('GOMAKOIL_PRODUCTS_CHECKED_'.$id, '' ,$this->_shopGroupId, Context::getContext()->shop->id);
    Configuration::updateValue('GOMAKOIL_PRODUCTS_CHECKED', $config, false, $this->_shopGroupId, Context::getContext()->shop->id);

    $config = Configuration::get('GOMAKOIL_MANUFACTURERS_CHECKED_'.$id, '' ,$this->_shopGroupId, Context::getContext()->shop->id);
    Configuration::updateValue('GOMAKOIL_MANUFACTURERS_CHECKED', $config, false, $this->_shopGroupId, Context::getContext()->shop->id);

    $config = Configuration::get('GOMAKOIL_SUPPLIERS_CHECKED_'.$id, '' ,$this->_shopGroupId, Context::getContext()->shop->id);
    Configuration::updateValue('GOMAKOIL_SUPPLIERS_CHECKED', $config, false, $this->_shopGroupId, Context::getContext()->shop->id);


    $config = Configuration::get('GOMAKOIL_CATEGORIES_CHECKED_'.$id, '' ,$this->_shopGroupId, Context::getContext()->shop->id);
    Configuration::updateValue('GOMAKOIL_CATEGORIES_CHECKED', $config, false,  $this->_shopGroupId, Context::getContext()->shop->id);

    $config = Configuration::get('GOMAKOIL_FIELDS_CHECKED_'.$id, '' ,$this->_shopGroupId, Context::getContext()->shop->id);
    Configuration::updateValue('GOMAKOIL_FIELDS_CHECKED', $config, false,  $this->_shopGroupId, Context::getContext()->shop->id);

  }


  public function listSettings($id, $idUpdate){

    $setting = array();
    $setting_update = array();
    $all_setting =Tools::unserialize( Configuration::get('GOMAKOIL_ALL_SETTINGS','',$this->_shopGroupId, Context::getContext()->shop->id));
    $all_setting_update = Tools::unserialize( Configuration::get('GOMAKOIL_ALL_UPDATE_SETTINGS','',$this->_shopGroupId, Context::getContext()->shop->id));

    if($all_setting_update){
      foreach($all_setting_update as $value){
        $name_conf = 'GOMAKOIL_NAME_SETTING_UPDATE_'.$value;
        $name =  Configuration::get($name_conf,'',$this->_shopGroupId, Context::getContext()->shop->id);
        $setting_update[] = array(
          'id'    => $value,
          'name'  => $name

        );
      }
    }
    else{
      $setting = false;
    }

    if($all_setting){
      foreach($all_setting as $value){
        $name_conf = 'GOMAKOIL_NAME_SETTING_'.$value;
        $name =  Configuration::get($name_conf,'',$this->_shopGroupId, Context::getContext()->shop->id);
        $setting[] = array(
          'id'    => $value,
          'name'  => $name

        );
      }
    }
    else{
      $setting = false;
    }


    $this->context->smarty->assign(
      array(
        'id'              => $id,
        'idUpdate'        => $idUpdate,
        'setting_update'  => $setting_update,
        'setting'         => $setting,
        'base_url'        => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules')
      )
    );
    return $this->display(__FILE__, "views/templates/hook/listSettings.tpl");
  }


  public function exportFields()
  {
    $set = Tools::unserialize(Configuration::get('GOMAKOIL_FIELDS_CHECKED','',$this->_shopGroupId, Context::getContext()->shop->id));
    if(!$set){
      $set = array('id_product' => $this->l('ID Product'), 'id_product_attribute' => $this->l('ID Product Combination'), 'id_specific_price' => $this->l('ID Specific Price'));
    }

    $newArray = array_merge($this->_exportBase, $this->_exportCombinations, $this->_exportSpecificPrice);
    $selected = array();
    foreach($set as $value) {
      foreach ($newArray as $new) {
        if ($value == $new['name']) {
          if(isset($new['hint']) && $new['hint']){
            $hint = $new['hint'];
          }
          else{
            $hint = '';
          }
          $selected[$new['val']] = array('name' => $new['name'], 'hint' => $hint);
        }
      }
    }

    $this->context->smarty->assign(
      array(
        'set'                   => $set,
        'selected'              => $selected,
        'url_base'              => AdminController::$currentIndex . '&token=' . Tools::getAdminTokenLite('AdminModules') . '&configure=updateproducts',
        'exportBase'            => $this->_exportBase,
        'exportCombinations'    => $this->_exportCombinations,
        'exportSpecificPrice'   => $this->_exportSpecificPrice,
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/selectFieldsExport.tpl');
  }

  public function  updateFields(){

    $set = Tools::unserialize(Configuration::get('GOMAKOIL_FIELDS_CHECKED_UPDATE','',$this->_shopGroupId, Context::getContext()->shop->id));
    $selected = array();
    if($set){
      $newArray = array_merge($this->_exportBase, $this->_exportCombinations, $this->_exportSpecificPrice);
      foreach($set as $value) {
        foreach ($newArray as $new) {
          if ($value == $new['name']) {
            if(isset($new['hint']) && $new['hint']){
              $hint = $new['hint'];
            }
            else{
              $hint = '';
            }
            $selected[$new['val']] = array('name' => $new['name'], 'hint' => $hint);
          }
        }
      }
    }
    else{
      $set = false;
    }

    $this->context->smarty->assign(
      array(
        'set'                   => $set,
        'selected'              => $selected,
        'url_base'              => AdminController::$currentIndex . '&token=' . Tools::getAdminTokenLite('AdminModules') . '&configure=updateproducts',
        'updateBase'            => $this->_updateBase,
        'updateCombinations'    => $this->_updateCombinations,
        'updateSpecificPrice'   => $this->_updateSpecificPrice,
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/selectFieldsUpdate.tpl');
  }

}
