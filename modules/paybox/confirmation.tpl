<p>{l s='Your order on' mod='paybox'} <span class="bold">{$shop_name}</span> {l s='is complete.' mod='paybox'}</p>
<p>{l s='You have chosen the bank card method.' mod='paybox'}</p>

<p>{l s='For any questions or for further information, please contact our' mod='paybox'} <a href="{$base_dir_ssl}contact-form.php">{l s='customer support' mod='paybox'}</a>.</p>
