/**
 * 2015-2016 Cubyn
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Cubyn <modules@cubyn.com>
 * @copyright 2015-2016 Cubyn
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
$(document).ready(function() {
    // cubyn button already added
    if ($('.toolbar-cubyn').length) return;

    // PS 1.5
    if ($('#cubyn_toolbar ul.cc_button').length) {
        $('#cubyn_toolbar ul.cc_button').prepend('<li class="toolbar-cubyn">'
            + '<a class="toolbar_btn btn-go" target="_blank" href="http://app.cubyn.com" title="Espace Cubyn">'
            + '     <span class="process-icon-back"></span>'
            + '     <div>Espace Cubyn</div>'
            + '</a></li>');
        return;
    }
    
    // PS 1.6
    if ($('#toolbar-nav').length) {
        $('#toolbar-nav').prepend('<li class="toolbar-cubyn">'
            + '<a class="toolbar_btn btn-go" target="_blank" href="http://app.cubyn.com" title="Espace Cubyn">'
            + '     <i class="process-icon-next"></i>'
            + '     <div>Espace Cubyn</div>'
            + '</a></li>');
        return;
    }
});