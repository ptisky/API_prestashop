<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{updateproducts}prestashop>send_ef8c112c51e58b58a2f272213809070d'] = 'S\'il vous plaît sélectionner les champs à mettre à jour!';
$_MODULE['<{updateproducts}prestashop>update_94804c923f1613e30456474b650d30fb'] = 'Correctement mis à jour %1s produits de %2s!';
$_MODULE['<{updateproducts}prestashop>update_7136a449e132b219b02adfd8dc226482'] = 'Mis à jour avec succès %s produits';
$_MODULE['<{updateproducts}prestashop>update_5a29012fd225e961d5b4787e7a28a351'] = 'Aucun champ %s des mises à jour de fichiers';
$_MODULE['<{updateproducts}prestashop>update_a39d04c012090b241ddf9c45b990c05a'] = 'Сhamp %s devrait être dans la mise à jour du fichier';
$_MODULE['<{updateproducts}prestashop>update_dce437d10c37e468d6e94666cefda9a5'] = 'S\'il vous plaît sélectionnez le fichier de mise à jour!';
$_MODULE['<{updateproducts}prestashop>update_e7aad9035b8abe93f6309441bd362225'] = 'Une erreur s\'est produite: Vous avez selectionné le format de fichier XLSX dans les paramètres. Le fichier téléchargé doit avoir le format XLSX';
$_MODULE['<{updateproducts}prestashop>update_0ffadf1b32372abf3dbfef830c148d77'] = 'Une erreur s\'est produite: Vous avez selectionné le format de fichier CSV dans les paramètres. Le fichier téléchargé doit avoir le format CSV';
$_MODULE['<{updateproducts}prestashop>update_a475ff61e6404ee8697600a3b78e43fb'] = 'Une erreur est survenue lors du chargement:';
$_MODULE['<{updateproducts}prestashop>updateproducts_876f23178c29dc2552c0b48bf23cd9bd'] = 'Etes-vous sûr que vous voulez supprimer?';
$_MODULE['<{updateproducts}prestashop>updateproducts_e3c441de1df56865ab27457967916296'] = 'Sélectionnez tous les champs';
$_MODULE['<{updateproducts}prestashop>updateproducts_0095a9fa74d1713e43e370a7d7846224'] = 'Exportations';
$_MODULE['<{updateproducts}prestashop>updateproducts_06933067aafd48425d67bcb01bba5cb6'] = 'Rafraîchir';
$_MODULE['<{updateproducts}prestashop>updateproducts_da7e8129b414146d4790a13505d16d40'] = 'Sélectionnez le format de fichier:';
$_MODULE['<{updateproducts}prestashop>updateproducts_cc8d68c551c4a9a6d5313e07de4deafd'] = 'CSV';
$_MODULE['<{updateproducts}prestashop>updateproducts_3f191d96d6c2c6bc91e4f271801221a0'] = 'XLSX';
$_MODULE['<{updateproducts}prestashop>updateproducts_0b27918290ff5323bea1e3b78a9cf04e'] = 'Fichier';
$_MODULE['<{updateproducts}prestashop>updateproducts_e65ec984245c22d3d7a79469eebecfac'] = 'Si vous ne précisez pas filtre, module exporter tous les produits!';
$_MODULE['<{updateproducts}prestashop>updateproducts_b0241a856fb8f086364c01ad1c662fe7'] = 'Sélectionnez un produit:';
$_MODULE['<{updateproducts}prestashop>updateproducts_99078a8f3786817fbaa593c08ff04a5d'] = 'Choisir une catégorie:';
$_MODULE['<{updateproducts}prestashop>updateproducts_efd50c56fe5c6ab8298396a4eff4e141'] = 'Choisir un fabricant:';
$_MODULE['<{updateproducts}prestashop>updateproducts_f60018e45c624f274a9e2c0b564ca936'] = 'Sélectionner les fournisseurs:';
$_MODULE['<{updateproducts}prestashop>updateproducts_4994a8ffeba4ac3140beb89e8d41f174'] = 'Langue';
$_MODULE['<{updateproducts}prestashop>updateproducts_9b47369d72e17f6b7fe04d5bf2ad5986'] = 'Champ de recherche';
$_MODULE['<{updateproducts}prestashop>updateproducts_68569ac9644f619f799abb447a9eb82e'] = 'Le filtre dans le domaine du produit';
$_MODULE['<{updateproducts}prestashop>form_060bf2d587991d8f090a1309b285291c'] = 'Sélectionner';
$_MODULE['<{updateproducts}prestashop>form_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{updateproducts}prestashop>form_6ee26c0ecfbae307fee2f67ef77e99e0'] = 'Afficher sélectionné';
$_MODULE['<{updateproducts}prestashop>form_8cd0d0c99b062b3d22e8c7188ba33ab2'] = 'Montre tout';
$_MODULE['<{updateproducts}prestashop>form_d77a48a51f8c62087570ab8f9d6ca55a'] = 'recherche...';
