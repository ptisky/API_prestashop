<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2015 PrestaShop SA
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class CubynRelayPoint extends ObjectModel
{

    /**
     * @var mixed
     */
    public $id_relay_point;
    /**
     * @var mixed
     */
    public $id_cart;
    /**
     * @var mixed
     */
    public $datas;
    /**
     * @var mixed
     */
    public $date_add;

    /**
     * @var array
     */
    public static $definition = array(
        'table'     => 'cubyn_relay_point',
        'primary'   => 'id_cubyn_relay_point',
        'multilang' => false,
        'fields'    => array(
            'id_relay_point' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'id_cart'        => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'datas'          => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'date_add'       => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
        ),
    );

    /**
     * @param $id_n
     * @param false $id_relay_point
     * @param false $id_cart
     * @param false $datas
     */
    public static function getElements($id_n = false, $id_relay_point = false, $id_cart = false, $datas = false)
    {
        $query = new DbQuery();
        $query->select(self::$definition['primary'].', '.implode(', ', array_keys(self::$definition['fields'])));
        $query->from(self::$definition['table']);

        if ($id_n && Validate::isInt($id_n)) {
            $query->where('id_n = '.(int) $id_n);
        }

        if ($id_relay_point && Validate::isInt($id_relay_point)) {
            $query->where('id_relay_point = "'.pSQL($id_relay_point).'" ');
        }

        if ($id_cart && Validate::isInt($id_cart)) {
            $query->where('id_cart = "'.pSQL($id_cart).'" ');
        }

        if ($datas && Validate::isCleanHTML($datas)) {
            $query->where('datas = "'.pSQL($datas).'" ');
        }

        return Db::getInstance()->executeS($query);
    }

    /**
     * @param $id_relay_point
     * @param $id_cart
     */
    public static function getByCart($id_cart)
    {
        $results = CubynRelayPoint::getElements(false, false, $id_cart);

        if ($results && count($results)) {
            return current($results);
        }

        return false;
    }

    public static function install()
    {
        // Create Category Table in Database
        $sql   = array();
        $sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.self::$definition['table'].'` (
                    `'.self::$definition['primary'].'` int(16) NOT NULL AUTO_INCREMENT,
                    `id_relay_point` VARCHAR(11) NOT NULL,
                    `id_cart` INT(11) unsigned NOT NULL,
                    `datas` TEXT NOT NULL,
                    date_add DATETIME NOT NULL,
                    date_upd DATETIME NOT NULL,
                    UNIQUE(`'.self::$definition['primary'].'`),
                    PRIMARY KEY  ('.self::$definition['primary'].')
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

        foreach ($sql as $q) {
            if (!Db::getInstance()->Execute($q)) {
                return false;
            }
        }

        return true;
    }

    public static function uninstall()
    {
        // Create Category Table in Database
        $sql   = array();
        $sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.self::$definition['table'].'`';

        foreach ($sql as $q) {
            if (!Db::getInstance()->Execute($q)) {
                return false;
            }
        }

        return true;
    }
}
