<?php
/**
 * 2015-2016 Cubyn
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Cubyn <modules@cubyn.com>
 * @copyright 2015-2016 Cubyn
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class CubynOrderModel extends ObjectModel
{

    public $id_cubyn;
    public $id_order;
    public $id_carrier;
    public $id_state;
    public $exported;
    public $shipped;

    public static $definition = array(
        "table"   => "cubyn",
        "primary" => "id_cubyn",
        "fields"  => array(
            "id_order"   => array("type" => self::TYPE_INT),
            "id_state"   => array("type" => self::TYPE_INT),
            "id_carrier" => array("type" => self::TYPE_INT),
            "exported"   => array("type" => self::TYPE_BOOL),
            "shipped"    => array("type" => self::TYPE_BOOL),
        ),
    );

    public static function exportedOrders()
    {
        $sql = "SELECT COUNT(*) FROM "._DB_PREFIX_."cubyn where exported = 1 AND shipped = 0";
        return Db::getInstance()->getValue($sql);
    }

    public static function updateByOrderState($cubyn_id, $order_state_id)
    {
        Db::getInstance()->update('cubyn', array('id_state' => (int) $order_state_id), "id_cubyn =".$cubyn_id, 1);
    }

    public static function findByOrderId($order_id)
    {
        $sql = "SELECT * FROM "._DB_PREFIX_."cubyn WHERE id_order =".$order_id;
        if ($row = Db::getInstance()->getRow($sql)) {
            return $row;
        }
        return false;
    }

    public static function alreadyExported($id_cubyn)
    {
        $sql = "SELECT count(*) c FROM "._DB_PREFIX_."cubyn WHERE id_cubyn =".$id_cubyn." AND exported=1";
        if ($row = Db::getInstance()->getRow($sql)) {
            return $row['c'] > 0;
        }
        return false;
    }

    public static function updateCarrier($new, $old)
    {
        Db::getInstance()->update('cubyn', array('id_carrier' => (int) $new), "id_carrier =".$old);
    }
}
