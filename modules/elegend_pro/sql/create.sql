CREATE TABLE IF NOT EXISTS `PREFIX_pro_elegend_api` (
 `id` int(1) NOT NULL AUTO_INCREMENT,
 `adresse` varchar(255) NOT NULL,
 `cleapi`  varchar(128) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `PREFIX_pro_elegend_api`(`id`, `adresse`, `cleapi`) VALUES ('1','http://pro.e-legend.fr/','');

CREATE TABLE IF NOT EXISTS `PREFIX_pro_elegend_param` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `PREFIX_pro_elegend_param` (`id`, `name`, `value`) VALUES (1, 'ID_SHOP', '1');
INSERT INTO `PREFIX_pro_elegend_param` (`id`, `name`, `value`) VALUES (2, 'ID_LANG', '1');
INSERT INTO `PREFIX_pro_elegend_param` (`id`, `name`, `value`) VALUES (3, 'EL_SUPPLIER', 'ELEGEND');
INSERT INTO `PREFIX_pro_elegend_param` (`id`, `name`, `value`) VALUES (4, 'EL_URL', 'http://pro.e-legend.fr/');
INSERT INTO `PREFIX_pro_elegend_param` (`id`, `name`, `value`) VALUES (5, 'EL_UPC', '13860138600');
INSERT INTO `PREFIX_pro_elegend_param` (`id`, `name`, `value`) VALUES (20, 'EL_STOCK', '20');
INSERT INTO `PREFIX_pro_elegend_param` (`id`, `name`, `value`) VALUES (6, 'EL_TOKEN', '00A212B6C18D66E45F50');
INSERT INTO `PREFIX_pro_elegend_param` (`id`, `name`, `value`) VALUES (7, 'EL_CLIENT', '18664550');

