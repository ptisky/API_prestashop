<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{paybox}prestashop>confirmation_2e2117b7c81aa9ea6931641ea2c6499f'] = 'Votre commande sur';
$_MODULE['<{paybox}prestashop>confirmation_75fbf512d744977d62599cc3f0ae2bb4'] = 'est bien enregistrée.';
$_MODULE['<{paybox}prestashop>confirmation_96fc63c9e97897c5350bc6eeb30e3b21'] = 'Vous avez choisi de payer par Carte bancaire.';
$_MODULE['<{paybox}prestashop>confirmation_0db71da7150c27142eef9d22b843b4a9'] = 'Pour toute question ou information complémentaire merci de contacter notre';
$_MODULE['<{paybox}prestashop>confirmation_64430ad2835be8ad60c59e7d44e4b0b1'] = 'support client';
$_MODULE['<{paybox}prestashop>errormagavenue_2c1b4301a31f09338af8d09c6eb96caa'] = 'Une erreur s\'est produite durant votre commande sur';
$_MODULE['<{paybox}prestashop>errormagavenue_0db71da7150c27142eef9d22b843b4a9'] = 'Pour toute question, merci de contacter notre';
$_MODULE['<{paybox}prestashop>errormagavenue_64430ad2835be8ad60c59e7d44e4b0b1'] = 'support technique';
$_MODULE['<{paybox}prestashop>paybox_529c5757531f623faff48ec453410002'] = 'CB avec Paybox';
$_MODULE['<{paybox}prestashop>paybox_c1c9e4f516043faffac510f545e6098b'] = 'Acceptez les paiements par carte de crédits avec Paybox (par Magavenue)';
$_MODULE['<{paybox}prestashop>paybox_fa214007826415a21a8456e3e09f999d'] = 'Etes vous sûr de supprimer vos informations ?';
$_MODULE['<{paybox}prestashop>paybox_e7f9e382dc50889098cbe56f2554c77b'] = 'Carte de crédit';
$_MODULE['<{paybox}prestashop>paybox_ec22ef9fd89a4d129903886c08bb9a09'] = 'Le numéro de site paybox est obligatoire';
$_MODULE['<{paybox}prestashop>paybox_8503643e75e8cdaab54f5d7b4818fe91'] = 'Le numéro de rang paybox est obligatoire';
$_MODULE['<{paybox}prestashop>paybox_7c0c47a841e8a922e33c960232486505'] = 'Le numéro d\'identifiant paybox est obligatoire';
$_MODULE['<{paybox}prestashop>paybox_5bb03df5d22e30b26bbbaabe7d17c793'] = 'Chemin vers le module CGI requis.';
$_MODULE['<{paybox}prestashop>paybox_f4d1ea475eaa85102e2b4e6d95da84bd'] = 'Confirmation';
$_MODULE['<{paybox}prestashop>paybox_c888438d14855d7d96a2724ee9c306bd'] = 'Paramètres mis à jour';
$_MODULE['<{paybox}prestashop>paybox_6357d3551190ec7e79371a8570121d3a'] = 'Il y a';
$_MODULE['<{paybox}prestashop>paybox_4ce81305b7edb043d0a7a5c75cab17d0'] = 'Il y a';
$_MODULE['<{paybox}prestashop>paybox_07213a0161f52846ab198be103b5ab43'] = 'des erreurs';
$_MODULE['<{paybox}prestashop>paybox_cb5e100e5a9a3e7f6d1fd97512215282'] = 'une erreur';
$_MODULE['<{paybox}prestashop>paybox_bd1cf8238eb44da5f218f9cf8b2db8cb'] = 'Ce module vous permet d\'accepter les paiements avec Paybox';
$_MODULE['<{paybox}prestashop>paybox_f4f70727dc34561dfde1a3c529b6205c'] = 'Paramètres';
$_MODULE['<{paybox}prestashop>paybox_14d9440cd2f6658077a2cefb8651c85c'] = 'Site paybox';
$_MODULE['<{paybox}prestashop>paybox_a930d900599d272b8e4c73e2caa87196'] = 'Rang paybox';
$_MODULE['<{paybox}prestashop>paybox_1e87b1137aafdad7b6a26d1cf69c4b8f'] = 'Identifiant paybox';
$_MODULE['<{paybox}prestashop>paybox_4de0d09951f68fe11e5e72d8955c465a'] = 'Chemin absolu du CGI';
$_MODULE['<{paybox}prestashop>paybox_7b8f140063bdcaf9cb43b409af17edb9'] = 'Adresse Url Http ';
$_MODULE['<{paybox}prestashop>paybox_93cba07454f06a4a960172bbd6e2a435'] = 'Oui';
$_MODULE['<{paybox}prestashop>paybox_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Non';
$_MODULE['<{paybox}prestashop>paybox_4245499695408b974322be6f01b0d17a'] = 'Mode de test';
$_MODULE['<{paybox}prestashop>paybox_7874f489c1b35fcc5ad8d02f16f7ecde'] = 'Formulaire';
$_MODULE['<{paybox}prestashop>paybox_a3d49c3607374d71a1919766c7a82c0c'] = 'Paiement en 3x';
$_MODULE['<{paybox}prestashop>paybox_6d3fb6141cc9d34804d2387a70d45d54'] = 'Minimum paiement en 3x';
$_MODULE['<{paybox}prestashop>paybox_1f13a4bccaf425060aa01019d070058c'] = 'Montant minimum pour le paiement multiple (10.00)';
$_MODULE['<{paybox}prestashop>paybox_c7bb9371bd28c912b0055abe7d79c044'] = 'Nombre de jours entre les paiements';
$_MODULE['<{paybox}prestashop>paybox_f53f6e6e630ca2f47ed34a9ea959fe7d'] = 'Nombre de jours entre les paiements multiples';
$_MODULE['<{paybox}prestashop>paybox_b17f3f4dcf653a5776792498a9b44d6a'] = 'Mettre à jour les paramètres';
$_MODULE['<{paybox}prestashop>paybox_9fd97914f7b20017574250e8f9b5efd0'] = 'Informations : ';
$_MODULE['<{paybox}prestashop>paybox_c4c95c36570d5a8834be5e88e2f0f6b2'] = 'Vous devez installer le module CGI de Paybox dans un premier temps. Si vous ne savez pas comment faire, ou si vous souhaitez améliorer le module, contactez-nous à contact@magavenue.com.';
$_MODULE['<{paybox}prestashop>paybox_25065e3ef9274f9b0e3088340f6d79e6'] = 'Payer par carte bleue (Visa / MasterCard / e-Carte)';
$_MODULE['<{paybox}prestashop>paybox_3df5b938038a7fe50219aaa2fb023f58'] = 'Payer par carte bleue (Visa / MasterCard / e-Carte), en trois fois';
$_MODULE['<{paybox}prestashop>redirect_84e8e9483e8836a1781aba80dbb62069'] = 'Connexion ....';
$_MODULE['<{paybox}prestashop>redirect_3051087f382fc202e9ebb92455b988e4'] = 'adresse ou client invalide ';
