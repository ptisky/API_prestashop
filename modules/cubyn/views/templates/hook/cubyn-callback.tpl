{**
 * 2015-2016 Cubyn
 * NOTICE OF LICENSE
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 * DISCLAIMER
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * @author    Cubyn <modules@cubyn.com>
 * @copyright 2015-2016 Cubyn
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *}
<script type="application/javascript">
    (function() {
        document.domain = document.domain;

        if (typeof window.parent.setRelay === 'function') {
            window.parent.setRelay(parseGetParams());
        } else {
            alert('Error with parent frame.');
        }

        function parseGetParams() {
            var vars = window.location.search.substring(1).split('&');
            var getPrms = {};

            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split('=');
                getPrms[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
            }
            return getPrms;
        }
    })();
</script>