<?php
/**
*
*  @package e-legend pro
*  @author    E-LEGEND 
*  @version    1.0
*  @copyright Copyright (c) E-LEGEND PRO
*
*/


if (!defined('_PS_VERSION_'))
	exit;


require_once(dirname(__FILE__).'/class/carrier.php');
require_once(dirname(__FILE__).'/class/catalog.php');
require_once(dirname(__FILE__).'/class/order.php');
require_once(dirname(__FILE__).'/class/product.php');
class elegend_pro extends Module
{
    private $_html = '';
	const INSTALL_SQL_FILE = 'create.sql';
	const UNINSTALL_SQL_FILE = 'drop.sql';
	
	public function __construct()
	{
		$this->name = 'elegend_pro';
		$this->tab = 'shipping_logistics';
		$this->version = '1.0.1';
		$this->need_instance = 0;
		$this->author = 'E-LEGEND';
		$this->displayName = $this->l('E-LEGEND - Dropshipping');
		$this->description = $this->l('Importer vos produits en Drop shipping avec E-LEGEND');
		$this->confirmUninstall = $this->l('Etes vous sur de vouloir désinstaller le module ?');

		parent::__construct();


		 
		
	}
	public function install()
	{
		if (!$this->executeSQLFile(self::INSTALL_SQL_FILE) || !parent::install())
			return false;
	}
	
	
	public function uninstall()
	{
		return $this->executeSQLFile(self::UNINSTALL_SQL_FILE) && parent::uninstall();
	}
	
	public function getApi()
	{
		return Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'pro_elegend_api where id=1');

	}
	
	public function getStatutProduct($id_category_default)
	{
		$sql = Db::getInstance()->getValue('SELECT active FROM '._DB_PREFIX_.'product_shop ps
		                                    INNER JOIN '._DB_PREFIX_.'category_product cp ON ps.id_product = cp.id_product
											WHERE cp.id_category = '.pSQL((int)$id_category_default));
		return $sql;

	}
	
	public function getStatistiqueProduct($id_category_default)
	{
		
		//$product = new Product();
		$product_list = $this->context->controller->getProducts($cookie->id_lang, 0,0, 'id_product', 'ASC', $id_category_default, true); 
		return $product_list;

	}
	
	public function getPriceProduct($id_category_default)
	{
		$price = Db::getInstance()->getValue('SELECT pas.price FROM '._DB_PREFIX_.'product_attribute_shop pas
		                                      INNER JOIN '._DB_PREFIX_.'product_attribute pa ON pa.id_product_attribute = pas.id_product_attribute
		                                      INNER JOIN '._DB_PREFIX_.'category_product cp ON pa.id_product = cp.id_product 
		                                      WHERE cp.id_category = "'.pSQL((int)$id_category_default).'"');
		
		//$price = ($price)+($price*20/100);
		$price = number_format(round($price, 2),2, '.', '');  
		
		return $price;

	}
	
  
	public function setPriceProduct($id_category_default,$price)
	{
		
			   
		Db::getInstance()->ExecuteS('UPDATE '._DB_PREFIX_.'product_attribute_shop pas
		                             INNER JOIN '._DB_PREFIX_.'product_attribute pa ON pa.id_product_attribute = pas.id_product_attribute
									 INNER JOIN '._DB_PREFIX_.'category_product cp ON cp.id_product = pa.id_product
									 set pas.price = "'.pSQL($price).'"
									 WHERE cp.id_category = "'.pSQL($id_category_default).'"');


	}


	public function activeProduct($id_shop,$id_category_default,$action)
	{
		Db::getInstance()->ExecuteS('UPDATE '._DB_PREFIX_.'product p 
                                     INNER JOIN '._DB_PREFIX_.'category_product cp ON p.id_product = cp.id_product
                                     set p.active ='.pSQL($action).' 
                                      WHERE cp.id_category = '.pSQL($id_category_default));
		
		Db::getInstance()->ExecuteS('UPDATE '._DB_PREFIX_.'product_shop ps 
                                     INNER JOIN '._DB_PREFIX_.'category_product cp ON ps.id_product = cp.id_product
                                      set ps.active ='.pSQL($action).' 
                                      WHERE cp.id_category = '.pSQL($id_category_default));

	}
	public function hookHeader()
    {
      return '<meta http-equiv ="refresh" content="0">';
   } 
	
	public function getParamEl($name)
	{
		return Db::getInstance()->getValue('SELECT `value` FROM `'._DB_PREFIX_.'pro_elegend_param` WHERE name="'.pSQL($name).'"');
	}
	
	public function executeSQLFile($file)
	{
		$path = realpath(_PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.$this->name).DIRECTORY_SEPARATOR.'sql/';

		if (!file_exists($path.$file))
			return false;
		elseif (!$sql = Tools::file_get_contents($path.$file))
			return false;

		$sql = preg_split("/;\s*[\r\n]+/", str_replace('PREFIX_', _DB_PREFIX_, $sql));

		foreach ($sql as $query)
		{
			$query = trim($query);
			if ($query)
				if (!Db::getInstance()->Execute($query))
				{
					$this->_postErrors[] = Db::getInstance()->getMsgError().' '.$query;
					return false;
				}
		}
		return true;
	}

	public function getContent() {
		
		$catalog = new Catalog();
		$output = '';
                                                                            
    if(Tools::isSubmit('submit_api')) {
                                                                            
   
				$api = Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'pro_elegend_api SET cleapi = "'.$_POST["cleapib"].'" WHERE id = 1');
                if (!$api)
                $output .= $this->displayConfirmation($this->l('Error activation clé api.'));
				else            
               $output .= $this->displayConfirmation($this->l('Votre clè api est active.'));                                                                   
    }
	
	   if (Tools::isSubmit('submit3'))
		{			
			$output .= $this->displayConfirmation($this->l('L\'importation s\'est bien déroulée.'));
		}
		
		
		
		if (Tools::isSubmit('submit_active'))
		{				
			$output .= $this->displayConfirmation($this->l('Opération réussite'));
				
		}
		
		if (Tools::isSubmit('submit_price'))
		{ 				
			$output .= $this->displayConfirmation($this->l('Prix : Opération réussite'));
		}
		
		if (Tools::isSubmit('maj_tax'))
		{
			$output .= $this->displayConfirmation($this->l('Paramètres de taxe correctement mis à jour.'));
		}
                                                                            
    return $output.$this->displayForm();

     }
	 
	 public function displayForm()
    {
    	
		global $cookie;
		require_once('PSWebServiceLibrary.php');
		$html = '';
		$onglet = "Accueil";
		$catalog = new Catalog();
		$carriers= new Carriers();
		$order= new Order();

       $sql = 'SELECT * FROM '._DB_PREFIX_.'pro_elegend_api where id=1';
		if ($results = Db::getInstance()->ExecuteS($sql))
		foreach ($results as $row)
		{
		$adresse = $row['adresse'];
		$cle = $row['cleapi'];
		};
		
	
		 		
		define('DEBUG', false);
        define('PS_SHOP_PATH', $row['adresse']);
        define('PS_WS_AUTH_KEY', $row['cleapi']);
		
		
		
		// Category
		
		
		$html .= '<link href="../modules/elegend_pro/css/el_style.css" rel="stylesheet">		          
		          <script type="text/javascript">
				   $(document).ready(function() {
				    $("#tabs-2").hide();
					$("#tabs-3").hide();
					$("#tabs-4").hide();
					$("#tabs-5").hide();
					$("#tabs-6").hide();
					$("#load").hide();
                   $(".tabs-menu a").click(function(event) {
                   event.preventDefault();
                   $(this).parent().addClass("current");
                   $(this).parent().siblings().removeClass("current");
                   var tab = $(this).attr("href");
                   $(".tab-content").not(tab).css("display", "none");
                   $(tab).fadeIn();
				   $(tab).tabs({ selected: 3 });
                   });
                 });
			    function loadCat() {
                    $("#load").show();

                       }
				 </script>';
	
		$html .= '';
        //Barre de menu
		 $output;
		$html .= '<div id="tabs-container">
         <ul class="tabs-menu">
           <a href="#tab-1"><li class="current"><i class="icon-home icon-awesome"></i><br />'.$this->l('Accueil').'</li></a>
           <a href="#tabs-2"><li><i class="icon-book icon-awesome"></i><br />'.$this->l('Catalogues').'</li></a>
		   <a href="#tabs-6"><li><i class="icon-inbox icon-awesome"></i><br />'.$this->l('Produits').'</li></a>
           <a href="#tabs-3"><li><i class="icon-shopping-cart icon-awesome"></i><br />'.$this->l('Commandes').'</li></a>
		   <a href="#tabs-4"><li><i class="icon-truck icon-awesome"></i><br />'.$this->l('Suivi').'</li></a>
           <a href="#tabs-5"><li><i class="icon-wrench icon-awesome" ></i><br />'.$this->l('Réglages').'</li></a>
          </ul>
        <div class="tab">
		<div id="tab-1" class="tab-content">
		<h3>'.$this->l('Bienvenue dans votre module E-LEGEND WEB SERVICE').'</h3>';
		$html .= '<iframe border="0" frameborder="no" marginwidth="0" marginheight="0" height="450px;" src="'.$this->getParamEl('EL_URL').'content/10-actualites?content_only=1" class="barcent" ></iframe>';
		
		$html .= '<fieldset id="catalogue">
		<legend> Activer la clè API : </legend>';
		 $html .= '<form method="post">';
         $html .= '<div class="margin-form">';
	     $html .= ''.$this->l('Clé API : ').'<br>';
	     $html .= '<input name="cleapib" size="30" type="password" value="'.$cle.'" /><br>';
         $html .= '<input type="submit" name="submit_api" value="'.$this->l('Enregistrer la clé').'" class="button" />';
         $html .= '</div>';
         $html .= '</form>';
		 $html .= '</div>
		
        <div id="tabs-2" class="tab-content">
		<h3>'.$this->l('Importation de catalogue').'</h3>
		<fieldset id="catalogue">
		<legend> Aspirer une categorie par WebService : </legend>
		
		<form method="post">
		<label>
		<select name="id_category">';
		 $showName = array();
		 
		 try
		{	
			
			$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
			 $opt = array('resource' => 'categories/&filter[active]=1&filter[level_depth]=2');		
			//$opt = array('url' => PS_SHOP_PATH . 'api/categories/&filter[active]=1&filter[level_depth]=2');			
			$xml = $webService->get($opt);	
			$resources = $xml->children()->children();	
			foreach ($resources as $resource)
		{

			$xml_o = $webService->get(array('url' => PS_SHOP_PATH.'api/categories/'.$resource->attributes()));
			$resources_o =  $xml_o->children()->children();
			 $name_category = $resources_o->name->language[0][0];
			  $getName = $catalog->getNameCategory($name_category);
			  if(!$getName){
			 $html .='<option value='.$resources_o->id.'>'.strtoupper($resources_o->name->language[0][0]).'</option>';
			
			  }else{
				 $showName[] = $resources_o->name->language[0][0]; 
			  }
		}
			
		}
		catch (PrestaShopWebserviceException $e)
		{
				
			$trace = $e->getTrace();
			if ($trace[0]['args'][0] == 404) $html .= $this->displayConfirmation($this->l('Bad ID'));
			else if ($trace[0]['args'][0] == 401) $html .= $this->displayConfirmation($this->l('Bad auth key'));
			else echo 'Other error'.$e->getMessage().'</b>';
		
		}		
		
		  
	  
	   
		$html .= '</select>
		</label>
		<input name="submit3" type="submit" id="api" value="'.$this->l("Démarrer l'importation").'" class="button" onclick="loadCat();" />
     	</form>';
		$html .= '<img src="../modules/elegend_pro/img/ajax-cat.gif" id="load" />';
		foreach($showName as $name)	    
		$html .= '<h4>'.$this->l("Ciblage des catégories installées  : ").' : '.strtoupper($this->l($name)).'</h3>';
		$html .= '</fieldset><br /></div>';
		
		$html .= '<div id="tabs-6" class="tab-content">
		<h3>'.$this->l('Espace produits').'</h3>';
		$html .= '<fieldset id="catalogue">';
		$html .= '<legend> Ajouter les nouveaux produits : </legend>';
		$html .=  $catalog->getNewProduct("used");
		$html .= '</fieldset>';
		$html .= '<fieldset id="catalogue">';
		$html .= '<legend> Les produits fin de vie : </legend>';
		$html .=  $catalog->getFinProduct("refurbished");
		$html .= '</fieldset>';
		$html .= '</div>';
		
		
		// Commandes
		$html .= '<div id="tabs-3" class="tab-content">
		<h3>'.$this->l('Espace commandes').'</h3>';

		$infoOrder = $order->getOrders($this->getParamEl('EL_UPC'));

		if (isset($infoOrder) && count($infoOrder) > 0)
		{
			$dossAdmin = explode('/index.php?', $_SERVER['REQUEST_URI']);
			$dossAdmin = explode('/', $dossAdmin[0]);
			$dossAdmin = $dossAdmin[count($dossAdmin) - 1];

			$html .= '<table id="list_order" class="table" style="margin-bottom: 20px;">';
			$html .= '<tr>';
			$html .= '<th>'.$this->l('ID').'</th>';
			$html .= '<th>'.$this->l('Reference').'</th>';
			$html .= '<th>'.$this->l('Total').'</th>';
			$html .= '<th>'.$this->l('Date').'</th>';
			$html .= '<th>'.$this->l('Voir').'</th>';
			$html .= '<th>'.$this->l('Statut').'</th>';
			$html .= '</tr>';

			foreach ($infoOrder as $com)
			{
				$html .= '<tr id="orderMan'.$com['id_order'].'">';
				$html .= '<td>'.$com['id_order'].'</td>';
				$html .= '<td>'.$com['reference'].'</td>';
				$html .= '<td>'.number_format($com['total_paid'], 2, ',', ' ').'</td>';
				$html .= '<td>'.$com['DatI'].'</td>';

				$html .= '<td><a target="_blank" href="'.__PS_BASE_URI__.$dossAdmin.'/index.php?'.(version_compare(_PS_VERSION_, '1.5', '<')?'tab':'controller').'=AdminOrders&id_order='.Tools::safeOutput($com['id_order']).'&vieworder&token='.Tools::getAdminTokenLite('AdminOrders').'">Voir</a></td>';
				if($com['current_state'] == 100)
				$html .= '<td>'.$this->l('Prêt à transmettre').'</td>';
				if($com['current_state'] == 102)
				$html .= '<td>'.$this->l('Pris en charge par la logistique').'</td>';
				if($com['current_state'] == 105)
				$html .= '<td>'.$this->l('Colis prêt').'</td>';
				if($com['current_state'] == 110)
				$html .= '<td>'.$this->l('Colis envoyé').'</td>';
				
				$html .= '</tr>';
			}
			$html .= '</table>';
		}
		else
			$html .= $this->l('Aucune commande en attente');

		$html .= '</div>';
		
		// Espace suivi
				$html .= '<div id="tabs-4" class="tab-content">
		<h3>'.$this->l('Espace suivi').'</h3>';

		$infoTrack = $order->getTrack($this->getParamEl('EL_UPC'));

		if (isset($infoTrack) && count($infoTrack) > 0)
		{
			$dossAdmin = explode('/index.php?', $_SERVER['REQUEST_URI']);
			$dossAdmin = explode('/', $dossAdmin[0]);
			$dossAdmin = $dossAdmin[count($dossAdmin) - 1];

			$html .= '<table id="list_order" class="table" style="margin-bottom: 20px;">';
			$html .= '<tr>';
			$html .= '<th>'.$this->l('ID').'</th>';
			$html .= '<th>'.$this->l('Reference').'</th>';
			$html .= '<th>'.$this->l('Date').'</th>';
			$html .= '<th>'.$this->l('Suivi de colis').'</th>';
		
			$html .= '</tr>';

			foreach ($infoTrack as $track)
			{
				$html .= '<tr id="orderMan'.$track['id_order'].'">';
				$html .= '<td>'.$track['id_order'].'</td>';
				$html .= '<td>'.$track['reference'].'</td>';
				$html .= '<td>'.$track['DatI'].'</td>';

				$html .= '<td><a target="_blank" href="http://express-parcel-tracking.com/suivi_colis/?'.$track['shipping_number'].'">'.$track['shipping_number'].'</a></td>';
				$html .= '</tr>';
			}
			$html .= '</table>';
		}
		else
			$html .= $this->l('Aucun n° de suivi');

		    $html .= ' </div>';
				  
		// Espace reglage : 
		$parametres = '';

		// 1 * activation des produits
		$html .= '<div id="tabs-5" class="tab-content">
		          <h3>'.$this->l('Espace de réglage').'</h3>
				  <fieldset>
		          <legend>'.$this->l("Activer / Désactiver tous les produits de la catégorie : ").' : </legend>
				  <form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" name="form_focus" method="post" style="padding: 8px;">';
				   $i = 0;
		           foreach($showName as $name){
					   $id_category_default = (int)$catalog->getIdCategory($name);					  
					   $statut = $this->getStatutProduct($id_category_default);     
		$html .= '<div><h3 style="padding-top: 10px;margin-bottom: -14px;">'.strtoupper($this->l($name)).'</h3></div>';
		            if($statut == 0){
		            
		$html .= '<div><input type="radio" name="active_'.$id_category_default.'" value="1" class="radio" id="radio_'.$name.'_a"> <label for="radio_'.$name.'_a">Activer</label></div>
				  <div><input type="radio" name="active_'.$id_category_default.'" value="0" checked="checked" class="radio" id="radio_'.$name.'_d"> <label for="radio_'.$name.'_d">Désactiver</label></div>';
					}else{
		$html .= '<div><input type="radio" name="active_'.$id_category_default.'" value="1" checked="checked" class="radio" id="radio_'.$name.'_a"> <label for="radio_'.$name.'_a">Activer</label></div>
				  <div><input type="radio" name="active_'.$id_category_default.'" value="0" class="radio" id="radio_'.$name.'_d"> <label for="radio_'.$name.'_d">Désactiver</label></div>';
					}
						
		$html .= '<input type="hidden" name="name" value="'.$name.'" checked="checked">';
				   }
		
		$html .= '<div><input type="submit" class="button" name="submit_active"  value="'.$this->l("Mise à jour statut").'"  style="float:right !important" /></div>';
		$html .= '</form></fieldset><br />';
		
		// 2 * Paramètres de taxe
		$html .= '<fieldset>
		          <legend>'.$this->l('Paramètres de taxe').'</legend>';
		$html .= '<h3>'.$this->l('Parametrage Taxe').'</h3>';
		$html .= '<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" name="form_focus" method="post">';
		$html .= $catalog->getTax();
		$html .= '<p><input type="submit" class="button" name="maj_tax" value="'.$this->l('Mise à jour taxe').'" style="float:right !important"/></p>';
		$html .= '</form></fieldset><br />';
		
		// 3 * Changement de prix 
		
		$html .='<fieldset>
		          <legend>'.$this->l("Changer le prix des produits de la catégorie : ").' : </legend>
				  <form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" name="form_focus" method="post" style="padding: 8px;">';
				  
				  foreach($showName as $name){
					   $id_category_default = (int)$catalog->getIdCategory($name);
					   $price = $this->getPriceProduct($id_category_default);	
					   //$price =number_format($price, 2, ',', ' ');		                
					   if($name != "E-Accessoire"){
		               $html .= '<h3>'.strtoupper($this->l($name)).'</h3>';
					   $html .= '<h4>'.$this->l("Prix HT").' : 
					   <input type="text" name="price_'.$id_category_default.'"  value="'.$price.'"></h4>';
					   }

				   }
		$html .= '<p><input type="submit" class="button" name="submit_price" value="'.$this->l('Mise à jour prix').'" style="float:right !important"/></p>';
		$html .= '';
		$html .= '</form></fieldset><br>';
		
		
		$html .= '</div>';
		$html .= '</div>';
		
		$html .= '<div class="footerel">';
		$html .='<p class="fooel">'.$this->l('E-LEGEND EUROPE Legal notice : CGV CUV ©2015 e-legend™. Adresse Postale : E-LEGEND, Ordman House, 31 Arden Close Bradley Stoke BS328AX Bristol United Kingdom.').'<br/><a href="http://www.e-legend.fr" target="_blank">http://www.e-legend.fr</a></p>
		</div>';
		
		
		
		
			
		

		if (Tools::isSubmit('submit_active'))
		{
				foreach($showName as $name)	{ 
				$id_category_default = (int)$catalog->getIdCategory($name); 
			    $this->activeProduct(1,$id_category_default,$_POST['active_'.$id_category_default.'']);
			
				}	
				$html .=$this->hookHeader();
							
		}
		
		if (Tools::isSubmit('submit3'))
		{
			if (!empty($_POST['id_category']))
			{
			$carriers->_carrier(100);
			$catalog->_catalog($_POST['id_category']);
			
			}
			
			$html .=$this->hookHeader();
		}
		
 		if (isset($_POST['submit_price']))
		{           
				foreach($showName as $name)	{ 
				 if($name != "E-Accessoire"){
				$id_category_default = (int)$catalog->getIdCategory($name);			
			    $this->setPriceProduct($id_category_default,$_POST['price_'.$id_category_default.'']);	
				 }
					

		   }
			$html .=$this->hookHeader();
		}
		
		if (Tools::isSubmit('maj_tax'))
		{
			
			$catalog->updateTax();
			$html .=$this->hookHeader();
		}
		
	
		
			return $html;
		
		
		 
    } //end function _displaystart

	  
	  
}
?>

