
var bestkitLazyLoading = {
    init : function(){
        bestkitLazyLoading.loadImages();
    },

    loadImages : function(){
        $("img.lazy").lazyload({
            effect : "fadeIn"
        });
    }
}

$(document).ready(function(){
    bestkitLazyLoading.init();
})