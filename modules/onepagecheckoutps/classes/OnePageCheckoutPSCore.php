<?php
/**
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * We are experts and professionals in PrestaShop
 *
 * @category  PrestaShop
 * @category  Module
 * @author    PresTeamShop.com <support@presteamshop.com>
 * @copyright 2011-2015 PresTeamShop
 * @license   see file: LICENSE.txt
 */

class OnePageCheckoutPSCore extends Module
{
    public $config_vars       = array();
    public $prefix_module     = '';
    protected $configure_vars = array();
    protected $errors         = array();
    protected $warnings       = array();
    protected $html           = '';
    protected $smarty;
    protected $cookie;
    protected $success;
    protected $params_back;
    public $globals;

    const CODE_SUCCESS = 0;
    const CODE_ERROR   = -1;

    public function __construct($name = null, $context = null)
    {
        $this->errors      = array();
        $this->warnings    = array();
        $this->params_back = array();
        $this->globals     = new stdClass();
        $this->fillGlobalVars();

        parent::__construct($name, $context);

        /* Backward compatibility */
        if (_PS_VERSION_ < '1.5') {
            require _PS_MODULE_DIR_.$this->name.'/backward_compatibility/backward.php';
        }

        $this->smarty = $this->context->smarty;
        $this->cookie = $this->context->cookie;

        $this->fillConfigVars();
    }

    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->config_vars)) {
            Configuration::updateValue($name, $value);
            $this->config_vars[$name] = $value;
        } else {
            $this->{$name} = $value;
        }
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->config_vars)) {
            return $this->config_vars[$name];
        }
    }

    public function install()
    {
        foreach ($this->configure_vars as $config) {
            if (!Configuration::updateValue($config['name'], $config['default_value'], $config['is_html'])) {
                return false;
            }
        }

        if (!parent::install() || !$this->executeFileSQL('install')) {
            return false;
        }

        return true;
    }

    public function uninstall()
    {
        foreach ($this->configure_vars as $config) {
            Configuration::deleteByName($config['name']);
        }

        if (!parent::uninstall() || !$this->executeFileSQL('uninstall')) {
            return false;
        }

        return true;
    }

    private function fillGlobalVars()
    {
        $this->globals->type_control = (object) array(
                'select'   => 'select',
                'textbox'  => 'textbox',
                'textarea' => 'textarea',
                'radio'    => 'radio',
                'checkbox' => 'checkbox'
        );

        $this->globals->lang               = new stdClass();
        $this->globals->lang->type_control = array(
            'select'   => $this->l('List'),
            'textbox'  => $this->l('Textbox'),
            'textarea' => $this->l('Textarea'),
            'radio'    => $this->l('Radio button'),
            'checkbox' => $this->l('Checkbox')
        );
    }

    protected function getContent()
    {
        if (!function_exists('curl_init')
            && !function_exists('curl_setopt')
            && !function_exists('curl_exec')
            && !function_exists('curl_close')) {
            $this->errors[] = $this->l('CURL functions not available for registration module.');
        } else {
            $id_shop_group = 0;
            $id_shop       = 0;

            $params = array(
                'server'         => $_SERVER,
                'module_name'    => $this->name,
                'version_module' => Configuration::get($this->prefix_module.'_VERSION'),
                'ps_version'     => _PS_VERSION_,
                'url_store'      => $this->context->shop->getBaseURL()
            );

            if (Tools::isSubmit('sent_register')) {
                if (Tools::getIsset('email') && Tools::getIsset('number_order')) {
                    $params['email']          = Tools::getValue('email');
                    $params['number_order']   = Tools::getValue('number_order');
                    $params['is_domain_test'] = (
                        (Tools::getIsset('is_domain_test') && Tools::getValue('is_domain_test', 'off') == 'on') ? 1 : 0
                        );

                    if (!empty($params['email']) && !empty($params['number_order'])) {
                        $response = $this->jsonDecode($this->sendRequest($params));

                        if (is_object($response)) {
                            if ($response->code == self::CODE_ERROR) {
                                $this->errors[] = $response->message;
                            } elseif ($response->code == self::CODE_SUCCESS) {
                                $this->html .= $this->displayConfirmation($response->message);
                            }
                        }
                    } else {
                        $this->errors[] = 'Please enter the information marked as mandatory for registration module.';
                    }
                }
            } elseif (Tools::isSubmit('validate_license')) {
                $params['license_number'] = Tools::getValue('license_number');

                if (Tools::getIsset('license_number') && !empty($params['license_number'])) {
                    $response = $this->jsonDecode($this->sendRequest($params));

                    if (is_object($response)) {
                        if ($response->code == self::CODE_ERROR) {
                            $this->errors[] = $response->message;
                        } elseif ($response->code == self::CODE_SUCCESS) {
                            Configuration::deleteByName($this->prefix_module.'_DOMAIN');
                            Configuration::deleteByName($this->prefix_module.'_RM');

                            if (version_compare(_PS_VERSION_, '1.5') >= 0) {
                                Configuration::updateValue(
                                    $this->prefix_module.'_DOMAIN',
                                    $response->domain,
                                    $id_shop_group,
                                    $id_shop
                                );

                                Configuration::updateValue($this->prefix_module.'_RM', '1', $id_shop_group, $id_shop);
                            } else {
                                Configuration::updateValue($this->prefix_module.'_DOMAIN', $response->domain);
                                Configuration::updateValue($this->prefix_module.'_RM', '1');
                            }

                            $this->fillConfigVars();

                            $this->html .= $this->displayConfirmation($response->message);
                        }
                    }
                } else {
                    $this->errors[] = 'Please enter the license to do the validation of the module.';
                }
            }
        }
    }

    protected function displayForm()
    {
        if (!array_key_exists('JS_FILES', $this->params_back)) {
            $this->params_back['JS_FILES'] = array();
        }
        if (!array_key_exists('CSS_FILES', $this->params_back)) {
            $this->params_back['CSS_FILES'] = array();
        }

        //add anothers scripts
        if (version_compare(_PS_VERSION_, '1.6') < 0) {
            array_unshift(
                $this->params_back['JS_FILES'],
                $this->_path.'views/js/lib/bootstrap/bootstrap.min/bootstrap.min.js'
            );

            if (version_compare(_PS_VERSION_, '1.5') < 0) {
                //add jquery in lower version than 1.5
                array_unshift(
                    $this->params_back['JS_FILES'],
                    $this->_path.'views/js/lib/jquery/jquery.min/jquery.min.js'
                );
            }

            //add bootstrap files if issen't 1.6
        }

        array_push($this->params_back['CSS_FILES'], $this->_path.'views/css/lib/jquery/plugins/growl/jquery.growl.css');
        array_push($this->params_back['JS_FILES'], $this->_path.'views/js/lib/jquery/plugins/growl/jquery.growl.js');

        //own bootstrap
        array_push($this->params_back['CSS_FILES'], $this->_path.'views/css/lib/bootstrap/pts/pts-bootstrap.css');

        //switch
        array_push($this->params_back['CSS_FILES'], $this->_path.'views/css/lib/simple-switch/simple-switch.css');

        //back
        array_push($this->params_back['JS_FILES'], $this->_path.'views/js/admin/configure.js');
        array_push($this->params_back['JS_FILES'], $this->_path.'views/js/lib/pts/tools.js');
        array_push($this->params_back['CSS_FILES'], $this->_path.'views/css/admin/configure.css');
        array_push($this->params_back['CSS_FILES'], $this->_path.'views/css/lib/pts/tools.css');
        array_push($this->params_back['CSS_FILES'], $this->_path.'views/css/lib/pts/pts-menu.css');

        //icons
        array_push(
            $this->params_back['CSS_FILES'],
            $this->_path.'views/css/lib/bootstrap/plugins/fontawesome/font-awesome.css'
        );

        $iso = Language::getIsoById((int) Configuration::get('PS_LANG_DEFAULT'));

        $server_name = Tools::strtolower($_SERVER['SERVER_NAME']);
        $server_name = str_ireplace('www.', '', $server_name);

        $url_store = $this->getUrlStore().$this->context->shop->getBaseURI().'modules/'.$this->name;

        $this->params_back = array_merge(array(
            'MODULE_DIR'                         => $this->_path,
            'MODULE_IMG'                         => $this->_path.'img/',
            'MODULE_NAME'                        => $this->name,
            'MODULE_TPL'                         => _PS_ROOT_DIR_.'/modules/'.$this->name.'/',
            'CONFIGS'                            => $this->config_vars,
            'ISO_LANG'                           => $iso,
            'GLOBALS'                            => $this->globals,
            'VERSION'                            => $this->version,
            'SUCCESS_CODE'                       => self::CODE_SUCCESS,
            'ERROR_CODE'                         => self::CODE_ERROR,
            'SERVER_NAME'                        => $server_name,
            'MODULE_PATH_ABSOLUTE'               => dirname(__FILE__).'/',
            'URL_STORE'                          => $url_store,
            'ACTION_URL' => Tools::safeOutput($_SERVER['PHP_SELF']).'?'.$_SERVER['QUERY_STRING'],
            'WARNINGS'                           => $this->warnings,
            $this->prefix_module.'_STATIC_TOKEN' => Tools::encrypt($this->name.'/index'),
            ), $this->params_back);

        $this->smarty->assign('paramsBack', $this->params_back);
    }

    private function executeFileSQL($file_name)
    {
        if (!file_exists(dirname(__FILE__).'/../sql/'.$file_name.'.sql')) {
            return true;
        } elseif (!$sql = Tools::file_get_contents(dirname(__FILE__).'/../sql/'.$file_name.'.sql')) {
            return false;
        }

        $sql = str_replace('PREFIX_', _DB_PREFIX_, $sql);
        $sql = preg_split("/;\s*[\r\n]+/", $sql);

        foreach ($sql as $query) {
            if (!Db::getInstance()->Execute(trim($query))) {
                return false;
            }
        }

        return true;
    }

    protected function addFrontOfficeJS($path)
    {
        if (version_compare(_PS_VERSION_, '1.5') >= 0) {
            $this->context->controller->addJS($path);
        } elseif (method_exists('Tools', 'addJS')) {
            Tools::addJS($path);
        }
    }

    protected function addFrontOfficeCSS($path, $media)
    {
        if (version_compare(_PS_VERSION_, '1.5') >= 0) {
            $this->context->controller->addCSS($path, $media);
        } elseif (method_exists('Tools', 'addCSS')) {
            Tools::addCSS($path);
        }
    }

    protected function fillConfigVars()
    {
        $languages = Language::getLanguages(false);
        foreach ($this->configure_vars as $config) {
            if (isset($config['is_bool']) && $config['is_bool']) {
                $this->config_vars[$config['name']] = (bool)Configuration::get($config['name']);
            } else {
                $this->config_vars[$config['name']] = Configuration::get($config['name']);

                if ($this->config_vars[$config['name']] === false) {
                    $this->config_vars[$config['name']] = array();
                    foreach ($languages as $language) {
                        $this->config_vars[$config['name']][$language['id_lang']] = Configuration::get(
                            $config['name'],
                            $language['id_lang']
                        );
                    }
                }
            }
        }
        $this->config_vars[$this->prefix_module.'_RM'] = Configuration::get($this->prefix_module.'_RM');
    }

    public function jsonDecode($json, $assoc = false)
    {
        if (function_exists('json_decode')) {
            return Tools::jsonDecode($json, $assoc);
        } else {
            include_once dirname(__FILE__).'/../lib/JSON.php';
            $pear_json = new Services_JSON(($assoc) ? SERVICES_JSON_LOOSE_TYPE : 0);

            return $pear_json->decode($json);
        }
    }

    public function jsonEncode($data)
    {
        if (function_exists('json_encode')) {
            return Tools::jsonEncode($data);
        } else {
            include_once dirname(__FILE__).'/../lib/JSON.php';
            $pear_json = new Services_JSON();

            return $pear_json->encode($data);
        }
    }

    protected function displayErrors($return = true)
    {
        if (count($this->errors)) {
            $html      = '';
            $nb_errors = count($this->errors);

            $html .= '
    		<div class="error">
    			<h3>'.($nb_errors > 1 ? $this->l('There are') : $this->l('There is')).' '.$nb_errors.' '
                .($nb_errors > 1 ? $this->l('errors') : $this->l('error')).'</h3>
    			<ol>';
            foreach ($this->errors as $error) {
                $html .= '<li>'.$error.'</li>';
            }
            $html .= '
    			</ol>
    		</div>';

            if ($return) {
                $this->html = $html;
            } else {
                echo $html;
            }
        }
    }

    protected function displayWarnings($return = true)
    {
        if (count($this->warning)) {
            $html        = '';
            $nb_warnings = count($this->warning);

            $html .= '
    		<div class="warn">
    			<h3>'.($nb_warnings > 1 ? $this->l('There are') : $this->l('There is')).' '
                .$nb_warnings.' '.($nb_warnings > 1 ? $this->l('warnings') : $this->l('warning')).'</h3>
    			<ol>';
            foreach ($this->warning as $warning) {
                $html .= '<li>'.$warning.'</li>';
            }
            $html .= '
    			</ol>
    		</div>';

            if ($return) {
                $this->html = $html;
            } else {
                echo $html;
            }
        }
    }

    protected function sendEmail(
        $email,
        $subject,
        $values = array(),
        $template_name = 'default',
        $email_from = null,
        $to_name = null,
        $lang = null,
        $file_attachment = null
    ) {
        if ($lang == null) {
            $lang = (int) Configuration::get('PS_LANG_DEFAULT');
        }
        if ($email_from == null) {
            $email_from = (string) Configuration::get('PS_SHOP_EMAIL');
        }

        return Mail::Send(
            $lang,
            $template_name,
            $subject,
            $values,
            $email,
            $to_name,
            $email_from,
            null,
            $file_attachment,
            null,
            _PS_MODULE_DIR_.$this->name.'/mails/'
        );
    }

    protected function updateVersion($module)
    {
        $registered_version = Configuration::get($this->prefix_module.'_VERSION');

        if ($registered_version != $this->version) {
            $list = array();

            $upgrade_path = _PS_MODULE_DIR_.$module->name.'/upgrades/';

            // Check if folder exist and it could be read
            if (file_exists($upgrade_path) && ($files = scandir($upgrade_path))) {
                // Read each file name
                foreach ($files as $file) {
                    if (!in_array($file, array('.', '..', '.svn', 'index.php'))) {
                        $tab          = explode('-', $file);
                        $file_version = basename($tab[1], '.php');
                        // Compare version, if minor than actual, we need to upgrade the module
                        if (count($tab) == 2 && version_compare($registered_version, $file_version) < 0) {
                            $list[] = array(
                                'file'             => $upgrade_path.$file,
                                'version'          => $file_version,
                                'upgrade_function' => 'upgrade_module_'.str_replace('.', '_', $file_version));
                        }
                    }
                }
            }

            usort($list, array($this, 'moduleVersionSort'));

            foreach ($list as $num => $file_detail) {
                include $file_detail['file'];

                // Call the upgrade function if defined
                if (function_exists($file_detail['upgrade_function'])) {
                    $file_detail['upgrade_function']($module);
                }

                unset($list[$num]);
            }

            Configuration::updateValue($this->prefix_module.'_VERSION', $this->version);

            $this->fillConfigVars();
        }
    }

    public function checkModulePTS()
    {
        $server_name = Tools::strtolower($_SERVER["SERVER_NAME"]);
        $server_name = str_ireplace("www.", "", $server_name);

        $match = "/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/";
        $is_ip = preg_match($match, $server_name);

        $tmp_valid = false;
        if (Configuration::get("PS_MULTISHOP_FEATURE_ACTIVE")) {
            if (!$is_ip && $server_name != "localhost") {
                $shops = Shop::getShops();

                $is_find_domain = false;
                foreach ($shops as $shop) {
                    $domain_multishop = Tools::strtolower($shop["domain"]);
                    $domain_multishop = str_ireplace("www.", "", $domain_multishop);

                    if ($domain_multishop == $server_name) {
                        $is_find_domain = true;

                        break;
                    }
                }

                if ($is_find_domain) {
                    $tmp_valid = true;
                }
            } else {
                $tmp_valid = true;
            }
        } else {
            if (!$is_ip && $server_name != "localhost") {
                $opc_domain = Configuration::get($this->prefix_module."_DOMAIN");

                if ($opc_domain == md5($server_name."t3mp0r4l")) {
                    $tmp_valid = true;
                }
            } else {
                $tmp_valid = true;
            }
        }

        if (!$tmp_valid) {
            Configuration::updateValue($this->prefix_module."_RM", "0");
            
            return false;
        }

        return true;
    }

    protected function sendRequest($params)
    {
        $ch = curl_init();

        $params = array('params' => $this->jsonEncode($params));
        $url    = 'http://www.presteamshop.com/pts_rm.php';

        if (Configuration::get('PS_SSL_ENABLED')) {
            $url = 'https://www.presteamshop.com/pts_rm.php';
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }

    protected function copyOverride($file)
    {
        $source = _PS_MODULE_DIR_.$this->name.'/public/'.$file;
        $dest   = _PS_ROOT_DIR_.'/'.$file;

        $path_dest = dirname($dest);

        if (!is_dir($path_dest)) {
            if (!mkdir($path_dest, 0777, true)) {
                return false;
            }
        }

        if (@copy($source, $dest)) {
            $path_cache_file = _PS_ROOT_DIR_.'/cache/class_index.php';
            if (file_exists($path_cache_file)) {
                unlink($path_cache_file);
            }

            return true;
        }

        return false;
    }

    protected function existOverride($filename, $key = false)
    {
        $file = _PS_ROOT_DIR_.'/'.$filename;

        if (file_exists($file)) {
            if ($key) {
                $file_content = Tools::file_get_contents($file);
                if (preg_match($key, $file_content) > 0) {
                    return true;
                }

                return false;
            }

            return true;
        }

        return false;
    }

    public function isModuleActive($name_module, $function_exist = false)
    {
        if (Module::isInstalled($name_module)) {
            $module = Module::getInstanceByName($name_module);
            if (Validate::isLoadedObject($module) && $module->active) {
                if ($function_exist) {
                    if (method_exists($module, $function_exist)) {
                        return $module;
                    } else {
                        return false;
                    }
                }

                return $module;
            }
        }

        return false;
    }

    public function getContext()
    {
        return $this->context;
    }

    public function getUrlStore()
    {
        return (Configuration::get('PS_SSL_ENABLED') ? Tools::getShopDomainSsl(true) : Tools::getShopDomain(true));
    }

    public static function getServerIpAddress()
    {
        $server_addr = $_SERVER['SERVER_ADDR'];
        if ($server_addr === '::1') {
            $hostname    = php_uname('n');
            $server_addr = gethostbyname($hostname);
        }

        return $server_addr;
    }

    private function moduleVersionSort($a, $b)
    {
        return version_compare($a['version'], $b['version']);
    }

    /**
     * Customize save data from form.
     * @param type $option
     * @param string $config_var_value
     */
    protected function saveCustomConfigValue($option, &$config_var_value)
    {
        switch ($option['name']) {
            case 'custom':
                $config_var_value = '';
                break;
        }
    }

    /**
     * @internal This method is not editable, use <b>saveCustomConfigValue</b> if necessary
     * @param type $option
     */
    protected function saveConfigValue($option)
    {
        $config_var_name = $this->prefix_module.'_'.$option['name'];
        $config_var_name = Tools::strtoupper($config_var_name);

        if (array_key_exists($config_var_name, $this->config_vars)) {
            if (isset($option['multilang'])) {
                $languages        = Language::getLanguages(false);
                $config_var_value = array();

                foreach ($languages as $language) {
                    $config_var_value[$language['id_lang']] = Tools::getValue($option['name'].'_'.$language['id_lang']);
                }
            } else {
                $config_var_value = Tools::getValue($option['name'], null);
            }

            switch ($option['type']) {
                case $this->globals->type_control->checkbox:
                    $config_var_value = (int) ((is_null($config_var_value)) ? false : true);
                    break;
                case $this->globals->type_control->select:
                    if (isset($option['multiple']) && $option['multiple']) {
                        if (is_array($config_var_value) && count($config_var_value)) {
                            $config_var_value = implode(',', $config_var_value);
                        } else {
                            $config_var_value = '';
                        }
                    }
                    break;
                default:
                    $config_var_value = (is_null($config_var_value)) ? '' : $config_var_value;
                    break;
            }

            //call function to save some options by custom restrictions or data treatment
            $this->saveCustomConfigValue($option, $config_var_value);

            //save value
            if (!Configuration::updateValue($config_var_name, $config_var_value)) {
                $this->errors[] = $this->l('An error occurred while trying update').': '.$option['label'];
            }

            //if dependencies
            if (isset($option['depends']) && is_array($option['depends']) && count($option['depends'])) {
                foreach ($option['depends'] as $dependency_option) {
                    $this->saveConfigValue($dependency_option);
                }
            }
        }
    }

    /**
     * Save data configuration from post form.
     * @param type $form
     */
    protected function saveFormData($form)
    {
        if (isset($form['options']) && is_array($form['options']) && count($form['options'])) {
            foreach ($form['options'] as $option) {
                $this->saveConfigValue($option);
            }
            $this->fillConfigVars();
        }
    }

    public function writeLog($error = null)
    {
        $name_error = Tools::getValue('name_error', 'Internal error');
        $code_error = Tools::getValue('code_error', '000');
        $error      = Tools::getValue('error', $error);
        $data_sent  = Tools::getValue('data_sent');

        $name_log = date('Ymd').'_error.log';

        $file_log = fopen(dirname(__FILE__).'/../log/'.$name_log, 'a+');
        fwrite($file_log, '['.$code_error.'] '.$name_error."\n".$error."\n\n".$data_sent."\n");
        fwrite($file_log, '----------------------------------------------------------------'."\n\n");
        fclose($file_log);

        return 'An internal error has occurred. Please inform the administrator of the store, thank you.';
    }

    protected function truncateChars($text, $limit, $ellipsis = '...')
    {
        if (Tools::strlen($text) > $limit) {
            $text = trim(Tools::substr($text, 0, $limit)).$ellipsis;
        }

        return $text;
    }
}
