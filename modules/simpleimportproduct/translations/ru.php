<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{simpleimportproduct}prestashop>import_3d6b3733b53a9cadba2118164df82e48'] = 'Успешно импортировано %1s товаров из %2s';
$_MODULE['<{simpleimportproduct}prestashop>import_80ecac9d1ab80db67673eae894df8c32'] = 'Успешно импортировано %s товаров ';
$_MODULE['<{simpleimportproduct}prestashop>send_87810c4404993af3601717276d095026'] = 'Произошла ошибка при загрузке, файл должен быть в формате XLSX';
$_MODULE['<{simpleimportproduct}prestashop>send_022435517a77e0c5f0ce07cad32cb3a6'] = 'Произошла ошибка при загрузке, файл должен быть в формате CSV ';
$_MODULE['<{simpleimportproduct}prestashop>send_668c37239cc66c665e4b42851dad354b'] = 'Произошла ошибка при загрузке, файл должен быть в ';
$_MODULE['<{simpleimportproduct}prestashop>send_1ddcb92ade31c8fbd370001f9b29a7d9'] = 'формате';
$_MODULE['<{simpleimportproduct}prestashop>send_e86a5b2cb041c3c0a9eb88c6ec6a95f9'] = 'Выберите файл для импорта';
$_MODULE['<{simpleimportproduct}prestashop>send_dbf808ea93023749d20f80a0e998155f'] = 'Данные успешно сохранены!';
$_MODULE['<{simpleimportproduct}prestashop>send_858a2b552953bc182248fe9374646398'] = 'Импортировано';
$_MODULE['<{simpleimportproduct}prestashop>send_86024cad1e83101d97359d7351051156'] = 'товаров';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c6d34dd6fa87e5922e13131aad9f302a'] = 'Импорт товаров';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_f2bbdf9f72c085adc4d0404e370f0f4c'] = 'Атрибут';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_ed3f8939becb4f60258f84fad6774671'] = 'Атрибут (Наименование : Тип, Наименование : Тип)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_689202409e48743b914713f96d93947c'] = 'Значение';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_b5ee3655098360fcfc5ccdcef04eb85d'] = 'Атрибут (Значение, Значение )';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_448aec4f94026678585f09d528ba417c'] = 'Артикул';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_60d2f5b05dc2e92d8167bb71809def2f'] = 'Общеизвестное название для комбинации. Недопустимые символы ;=#{}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c8effe37e0fefd95ff0cc4873de6e6a6'] = 'EAN-13 или JAN штрих-код';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_5bb4529a7441854e00e00f8ed0096756'] = 'UPC штрих-код';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c804723ccdde3d7a46933b208c6f928d'] = 'Оптовая цена';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_85bed720d784e4cfca7944e96579f23b'] = 'Заменяет оптовую цену на вкладке \"Цены\".';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_6ebc48c8cea15d33e2f95bf63c330e3c'] = 'Влияние на цену (количество)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1182810eb2ab365892590f1c4bb0402d'] = 'Влияние на вес (количество)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_4fc9f49cb85af9ba5caab7747fe35846'] = 'Эко-налог (без учета прочих налогов)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_108a2296d474989de3b95c5431002f73'] = 'Переопределяет Экологический налог на вкладке \"Цены\".';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1b72aed8e84b91d73896efd48cf1a4e0'] = 'Минимальное количество';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1c5f0be6c267baf1982f51ba25dd850f'] = 'Минимальное количество при котором можно купить этот товар (1 выключит эту функцию )';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_694e8d1f2ee056f98ee488bdc4982d73'] = 'Количество';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_79d3a9336845a659b537a14862522b52'] = 'Доступное количество для продажи.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_7c52393eaa879dd8c3081f4c3c847846'] = 'Комбинация по умолчанию';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_f86b4cc6b57bcabf6eb47816d56f0185'] = 'Назначить комбинацией по умолчанию для этого товара.  Значение: 0 или 1.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_7b28d39c479ba659159b3356616b0a04'] = 'Тип скидки';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c1fd556728f5475359edd7d92162033d'] = 'Тип скидки (количество или в процентах)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_9e834f13e35e4edf64863ab414a6217a'] = 'Скидка';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_14759772455cb478f99133cb419f3e2f'] = 'Величина уменьшения';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0e1e48c5a509fcb7cb904aa2418c68a6'] = 'Доступна с (дата)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_139fd814d0da2c69756467fb52a4ac00'] = 'Доступна до (дата)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_6bc323cd5254af4efd7b138cc9de277b'] = 'Фиксированная цена (без НДС)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_9b69fa8e03a5e5a8d949a85f9692ef01'] = 'Начиная с (количества)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_89357c5634fc97255d5c5b0170970915'] = 'Доступна с (количество)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_120f8d2cc7f58179b8a7a48753300464'] = 'Наименование характеристики';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1145fa97c0885a434db7e371f5a07c6c'] = 'Наименование  характеристики продукта. Недопустимые символы ;=#{}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_cef09b40844ebc72dcaff9813e0504da'] = 'Значение характеристики';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_b4942eb197fbadcc2bd8c269a2f46d44'] = 'Значение характеристики продукта.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_6f4a225b29c0975663d0e4b9e1f4547f'] = 'Шаг 1';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_e31ad1284b7618a9c10777ed99b12e64'] = 'Шаг 2';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1c97e434cef50560c03877bd8177d692'] = 'Пример файла импорта (XLSX)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0b4c0dfdc51829024e115056fe1500b2'] = 'Сохраненные настройки';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_3f191d96d6c2c6bc91e4f271801221a0'] = 'XLSX';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_cc8d68c551c4a9a6d5313e07de4deafd'] = 'CSV';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_ec211f7c20af43e742bf2570c3cb84f9'] = 'Добавить';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_976ac75c2985c988956385ba30a17e4a'] = 'Добавить/обновить';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_df644ae155e79abf54175bd15d75f363'] = 'Название товара';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1dec4f55522b828fe5dacf8478021a9e'] = 'ЧПУ';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_39080ffac9ef208828d1d5fc4aa434e3'] = 'Выберите формат файла';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_4994a8ffeba4ac3140beb89e8d41f174'] = 'Язык';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_abb968fcf23cfb09b36e4d92bbbe7500'] = 'Разделитель';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_b5a528f642ee412523c909fe6c0e4235'] = 'Тип импорта';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_f0468b8afcc44a24054c525bedbeb98a'] = 'Ключ для обновления';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0b27918290ff5323bea1e3b78a9cf04e'] = 'Файл';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'Дальше';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_630f6dc397fe74e52d5189e2c80f282b'] = 'Обратно к списку';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_4ee7906cfc80deaf7c5a4742b14cd16d'] = 'Общеизвестное название для этого продукта. Недопустимые символы ;=#{}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_59d4e474aa07c59634473dd3618ba3af'] = 'Ваш внутренний код ссылки для этого продукта. Разрешены специальные символы .-_#';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_9e11e4b371570340ca07913bc4783a7a'] = 'Мета-заголовок';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_ef53513cd86526414bec442e1a044e28'] = 'Публичное название для страницы товара, и для поисковых систем. Оставьте пустым, чтобы использовать название товара. The number of remaining characters is displayed to the left of the field.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_7d7559ccac6bc30a4d985db11cb34a3a'] = 'Мета ключевые слова';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_f7fd5f0f9bacf6c76bfbe496658c8447'] = 'Ключевые слова для заголовка HTML, разделенных запятыми.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_3f64b2beede1082fd32ddb0bf11a641f'] = 'Мета описание';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_352ceee04b365bba230157c960b1412d'] = 'Это описание появится в поисковых системах. Вам нужна простая фраза, короче 160 символов (включая пробелы).';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c1069a480848e06782b81b8bea9c0c94'] = 'Короткое описание';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_765a5021c58a220cb2a0bec7d0e83b17'] = 'Появится в списке продуктов, и в верхней части странице продукта.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Описание';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_9a2d3323b05a18f9723ea91b05dd9988'] = 'Появление в теле странице.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_4c32794a96aec43624f14839274174bf'] = 'Розничная цена без НДС';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_90cc6a03b5edaf79e8c3fb8f3451fe31'] = 'Розничная цена до налогообложения, по которой вы собираетесь продавать этот продукт своим клиентам. Она должна быть выше, чем до налогообложения оптовой цены: разница между ними будет ваша маржа.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_20a34c4e30c5bbe1d3f870ac55f0d831'] = 'Ставка налога';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_620d66a334f161bdb5c766601fe6824f'] = 'Значение должно быть от 0 до 100';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_d84d5a260e0501f0c1c01c912396bd1b'] = 'Ссылки на изображения';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_414180f1919858ce9f11df12192de6d5'] = 'Каждая ссылка изображение  должна отделяться запятой. Формат JPG, PNG. Размер файла 8.00 Мб макс.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0f60ea0e9d979de5ac9d37c94b207869'] = 'Это доступная для прочтения ссылка, сгенерированная из названия товара. Вы можете изменить её, если хотите.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_a17f5bc22553d897f6363867c0165256'] = 'Категория по умолчанию';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_be696601fc51ce3f87b06f0d124a5afb'] = 'Главная категория является основной категорией для вашего продукта, и отображается по умолчанию.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_a171a9b7de01d023bad650e778493ea6'] = 'Ширина упаковки';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_895a9b117143b191564e634c2fa67cef'] = 'Высота упаковки';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_22759d4c7ba574eebd9faf5310857d1f'] = 'Длина упаковки';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0829bbc64dd88308c23d94fecc3c7e88'] = 'Вес отправления';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1814d65a76028fdfbadab64a5a8076df'] = 'Поставщики';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_70af813064e91d72cad1a8e7fc130393'] = 'Каждое имя поставщика должно отделяться запятой. Недопустимые символы  = # {}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0b5c5a9176b94b7577e998f7440228a7'] = 'Поставщик по умолчанию.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_96d36948c94caa13500290e783528bf7'] = 'Название поставщика по умолчанию. Недопустимые символы  = # {}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Производитель';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_d4daa70843924b7f4ae259388c095cfa'] = 'Наименование производителя. Недопустимые символы ;=#{}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_189f63f277cd73395561651753563065'] = 'Тэги';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_18302b5faa19bb432b8331c6402ec77f'] = 'Каждый тег должен быть разделен запятой. Следующие символы запрещены: !;?=+#\"°{}_$%';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0041a5c39ca241f883f4e0516a32d290'] = 'Этот тип кода продукта является специфичным для Европы и Японии, но широко используется в мире. Это расширенный вариант кода UPC: Все продукты, отмеченные EAN будет принято в Северной Америке.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_39bd7eba8aa2033579b5ae60bf0c774d'] = 'Это тип кода товара, широко используемый в США, Канаде, Великобритании, Австралии, Новой Зеландии и в других странах.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0147896b0a27d9c02a92ae0d4fab2791'] = 'Заголовки изображений';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_3dcbf8cf3cb0bbd8b648fc7a5f889104'] = 'Каждый заголовок изображения должен отделяться запятой. Недопустимые символы  = # {}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_f2149c422ab7577f063b69a2884d17f0'] = 'Доступен для заказа';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_19b3596c48d5493544e722037fb567cf'] = 'Значение 0 или 1';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Включено';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_9e2941b3c81256fac10392aaca4ccfde'] = 'Состояние';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_feeec928c30160cc983d4461ca0289c2'] = 'Значение: new, used или refurbished';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1729a56cfc89021478498fe0c89a843a'] = 'Видимость';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_3824ef587e10d871a6a2b813c32d92a3'] = 'Значение: both, catalog, search или none';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_5a915a9e984b5f42d941be9d18072a9c'] = 'Показывать иконку \"распродажа\"';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_ef270c5b5ab343ad251f5ab86cc3ea23'] = 'Показывать иконку \"распродажа\" на странице товара и в тексте со списком товаров. Значение 0 или 1';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_e1a5e653bc356ed6745d6814d50213eb'] = 'Отображать цену';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_cddb91d856108b4431ebc2a9f79d4769'] = 'Цена за единицу (налог не включен) ';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_a00cc83ae38424bb93f5179e1ab50850'] = 'При продаже упаковки предметов, вы можете указать цены за единицу для каждого элемента упаковки. Например, \"за бутылку\" или \"за фунт\".';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_cc790edb31f5984a5754e4bc6aadf7bb'] = 'Закупочная цена без НДС';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_7a2215a4f812433c5e171eff2f95834b'] = 'Оптовая цена, которую вы заплатили за товар. Не включает налоги.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c265e7bd60b9bf0470d39a8ef87e0727'] = 'ЭКО-налог (включительно)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_4b8c01975c469777b0e0bffe373e5a0c'] = 'Экологический налог является местным набором налогов, предназначенных для \"продвижения экологически устойчивой деятельности через экономические стимулы\". Он уже включен в розничную цену: чем выше этот Экологический налог, тем меньше ваша маржа.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c40f15cd6deedb63c8c555b22c9b2f30'] = 'Показывать текст когда товар в наличии';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_5373df5f8f5dc40cd7a4a4ae7a9671bf'] = 'Отображается текст, когда есть в наличии. Недопустимые символы ;=#{}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_69dc68c6a765187d6450467f42f994f2'] = 'Показывать текст, если разрешен заказ по телефону';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c5e753d76d950b20fb84e358fb6285df'] = 'Displayed text when backordering is allowed. If empty, the message \"in stock\" will be displayed. Forbidden characters ;=#{}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c24c379ede5dfa7b82a91bfcc5c3deef'] = 'Связанные категории';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_a5e2a36615cbcae27d5dfca282a2c719'] = 'Все категории и подкатегории продукта';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_fa1d72220129e2192a7568e1fa4df71c'] = 'Показать больше...';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_ee42125205cfb2f312f62dab1eebfd9f'] = 'Есть комбинации';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_93cba07454f06a4a960172bbd6e2a435'] = 'Да';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Нет';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_75e017cc992f7a6090803efcb6ac1c53'] = 'добавить комбинацию';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_15bde11773b7bc84e7ef3aa6d98ca5ee'] = 'Есть скидки';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_572dda2167773d211243ecfeec52555e'] = 'Есть характеристики';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_64b644ce026182d76faca40823c3b372'] = 'добавить характеристики';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_d4dccb8ca2dac4e53c01bd9954755332'] = 'Сохранить настройки';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_72d6d7a1885885bb55a565fd1070581a'] = 'Импорт';
$_MODULE['<{simpleimportproduct}prestashop>form_fb32534540c610809da070e9cca8d90e'] = 'добавить подкатегорию';
$_MODULE['<{simpleimportproduct}prestashop>form_cabb8270408172ee3c7bb9ba7391fac0'] = 'добавить категорию';
$_MODULE['<{simpleimportproduct}prestashop>form_fa1d72220129e2192a7568e1fa4df71c'] = 'Показать больше...';
$_MODULE['<{simpleimportproduct}prestashop>form_b9c139957b9394c7d4a9fd0aaa999c3d'] = 'Свернуть...';
$_MODULE['<{simpleimportproduct}prestashop>addcategories_fb32534540c610809da070e9cca8d90e'] = 'добавить подкатегорию';
$_MODULE['<{simpleimportproduct}prestashop>addcategories_cabb8270408172ee3c7bb9ba7391fac0'] = 'добавить категорию';
$_MODULE['<{simpleimportproduct}prestashop>addcombinations_75e017cc992f7a6090803efcb6ac1c53'] = 'добавить комбинацию';
$_MODULE['<{simpleimportproduct}prestashop>addcombinations_099af53f601532dbd31e0ea99ffdeb64'] = 'удалить';
$_MODULE['<{simpleimportproduct}prestashop>addfeatures_64b644ce026182d76faca40823c3b372'] = 'добавить характеристики';
$_MODULE['<{simpleimportproduct}prestashop>addfeatures_099af53f601532dbd31e0ea99ffdeb64'] = 'удалить';
$_MODULE['<{simpleimportproduct}prestashop>morecategory_fb32534540c610809da070e9cca8d90e'] = 'добавить подкатегорию';
$_MODULE['<{simpleimportproduct}prestashop>morecombination_75e017cc992f7a6090803efcb6ac1c53'] = 'добавить комбинацию';
$_MODULE['<{simpleimportproduct}prestashop>morecombination_099af53f601532dbd31e0ea99ffdeb64'] = 'удалить';
$_MODULE['<{simpleimportproduct}prestashop>morediscount_addec426932e71323700afa1911f8f1c'] = 'больше';
$_MODULE['<{simpleimportproduct}prestashop>morediscount_099af53f601532dbd31e0ea99ffdeb64'] = 'удалить';
$_MODULE['<{simpleimportproduct}prestashop>morefeatured_64b644ce026182d76faca40823c3b372'] = 'добавить характеристики';
$_MODULE['<{simpleimportproduct}prestashop>morefeatured_099af53f601532dbd31e0ea99ffdeb64'] = 'удалить';
