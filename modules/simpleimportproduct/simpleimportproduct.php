<?php

if (!defined('_PS_VERSION_')){
  exit;
}

class Simpleimportproduct extends Module {
  private $_html;
  private $_has_hint_combinations;
  private $_has_hint_discount;
  private $_has_hint_featured;

  public function __construct(){

    $this->name = 'simpleimportproduct';
    $this->tab = 'quick_bulk_update';
    $this->version = '2.2.6';
    $this->author = 'MyPrestaModules';
    $this->need_instance = 0;
    $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    $this->bootstrap = true;
    $this->module_key = "659ea36715aa55ad9ce03afc739e4ade";

    parent::__construct();

    $this->displayName = $this->l('Product Catalog CSV and XLSX Import');
    $this->description = $this->l('Products Import');

    $this->_has_hint_combinations = array(
      'attribute'                   => array(
        'name' => $this->l('Attribute'),
        'hint' => $this->l('Attribute (Name:Type, Name:Type)'),
      ),
      'value'                       => array(
      'name' => $this->l('Value'),
      'hint' => $this->l('Attribute value (Value,Value)'),
    ),
      'reference_combination'       => array(
      'name' => $this->l('Reference code'),
      'hint' => $this->l('Your internal reference code for this combination. Allowed special characters .-_#'),
    ),
      'ean13_combination'           => array(
      'name' => $this->l('EAN-13 or JAN barcode'),
      'hint' => $this->l(''),
    ),
      'upc_combination'             => array(
      'name' => $this->l('UPC barcode'),
      'hint' => $this->l(''),
    ),
      'wholesale_price_combination' => array(
      'name' => $this->l('Wholesale price'),
      'hint' => $this->l('Overrides the wholesale price from the "Prices" tab.'),
    ),
      'impact_price'                => array(
      'name' => $this->l('Impact on price (amount)'),
      'hint' => $this->l(''),
    ),
      'impact_weight'               => array(
      'name' => $this->l('Impact on weight (amount)'),
      'hint' =>$this->l('')
    ),
      'ecotax_combination'          => array(
      'name' => $this->l('Ecotax (tax excl.)'),
      'hint' => $this->l('Overrides the ecotax from the "Prices" tab.'),
    ),
      'min_quantity_combination'    => array(
      'name' => $this->l('Minimum quantity'),
      'hint' => $this->l('The minimum quantity to buy this product (set to 1 to disable this feature).'),
    ),
      'quantity_combination'        => array(
      'name' => $this->l('Quantity'),
      'hint' => $this->l('Available quantities for sale.'),
    ),
      'default'                     => array(
      'name' => $this->l('Default combination'),
      'hint' => $this->l('Make this combination the default combination for this product. Value 0 or 1.'),
    ),
      'images_combination'                     => array(
        'name' => $this->l('Combination images'),
        'hint' => $this->l('Combination images'),
      ),

    );
    $this->_has_hint_discount = array(
      'reduction_type'   => array(
        'name' => $this->l('Reduction type'),
        'hint' => $this->l('Reduction type (amount or percentage)'),
      ),
      'reduction'        => array(
      'name' => $this->l('Reduction'),
      'hint' => $this->l('Amount of reduction'),
    ),
      'reduction_from'   => array(
      'name' => $this->l('Available from (date)'),
      'hint' => $this->l(''),
    ),
      'reduction_to'     => array(
      'name' => $this->l('Available to (date)'),
      'hint' =>$this->l(''),
    ),
      'fixed_price'      => array(
      'name' => $this->l('Fixed price (tax excl.)'),
      'hint' =>$this->l(''),
    ),
      'from_quantity'    => array(
      'name' => $this->l('Starting at (quantity)'),
      'hint' => $this->l('Available from (quantity)'),
    ),
    );

    $this->_has_hint_featured = array(
      'features_name'  => array(
        'name' => $this->l('Features name'),
        'hint' => $this->l('Product features name. Invalid characters <>;=#{}'),
      ),
      'features_value' => array(
      'name' => $this->l('Features value'),
      'hint' => $this->l('Product features value'),
    ),
    );
  }

  public function install()
  {
    if (!parent::install()|| !$this->registerHook('actionAdminControllerSetMedia')) {
      return false;
    }
    return true;
  }

  public function uninstall(){
    return parent::uninstall() ;
  }

  public function hookActionAdminControllerSetMedia()
  {
    if( Tools::getValue('configure') == "simpleimportproduct" ){
      $this->context->controller->addCSS($this->_path.'views/css/simpleimportproduct.css');
      $this->context->controller->addJS($this->_path.'views/js/simpleimportproduct.js');
    }
  }

  public function getContent(){

    $this->_html .= '<div class="panel-heading panel-heading-gomakoil">
      <div class="panel">
        <div class="panel-step">';

    if(Tools::getValue('step') == 2){
      $this->_html .= '<div class="step_1">'.$this->l('Step 1').'</div>
                      <div class="step_2  active">'.$this->l('Step 2').'</div>';

    }
    else{
      $this->_html .= '<div class="step_1 active">'.$this->l('Step 1').'</div>
                      <div class="step_2">'.$this->l('Step 2').'</div>';
    }
       $this->_html .= ' </div>
       <div class="file_example_import">
        <a href="'.$this->_path.'example_file/example.xlsx">'.$this->l('Example of import file (XLSX)').'</a>
      </div>
        <div class="name_module"><img class="logo_myprestamodules" src="../modules/'.$this->name.'/logo.png" /><span>'.$this->displayName.' </span></div>
      </div><div class="clear"></div>
		</div>';

    $this->_displayForm();
    return $this->_html;
  }
  private function _displayForm()
  {
    $step = "";
    if( Tools::getValue('step') != 2 ){
      $step = " step_1";
    }
    $this->_html .= '<div style="overflow:hidden;margin-top: -2px;"><div class="panel content_import_page form-horizontal'.$step.'">';
    if(Tools::getValue('step') == 2){
      $this->initFormImportFields();
      $save = array();
      $save = Tools::unserialize(Configuration::get('GOMAKOIL_IMPORT_PRODUCTS',null,Context::getContext()->shop->id_shop_group,Context::getContext()->shop->id));
      if(isset($save) && $save){
        $this->_html .= '<div class="panel" id="fieldset_0_0"><div class="panel-heading">
                        <i class="icon-cogs"></i><span class="your_saved_settings">'.$this->l('Your saved settings').'</span></div><ul class="save_scroll"> ';
          foreach($save as $key => $value){
            $this->_html .= '<li><a class="one_config" href='.AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules').'&configure=simpleimportproduct&step=2&save='.$key.'>'.$value['name_save'].'</a><a class="delete_config btn btn-default" settings="'.$key.'"><i class="icon-trash"></i></a></li>';
      }
        $this->_html .= '</ul></div>';
      }
    }
    else{
      $this->initFormImport();
    }

    $this->_html .= '</div></div>';
  }

  public function initFormImport()
  {
    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
    $fields_form = array();
    $format = array(
      array(
        'id' => 'xlsx',
        'name' => $this->l('XLSX')
      ),
      array(
        'id' => 'csv',
        'name' => $this->l('CSV')
      ),
    );
    $delimiter = array(
      array(
        'id' => ';',
        'name' => ';',
      ),
      array(
        'id' => ':',
        'name' => ':',
      ),
      array(
        'id' => ',',
        'name' => ',',
      ),
      array(
        'id' => '.',
        'name' => '.',
      ),
      array(
        'id' => '/',
        'name' => '/',
      ),
      array(
        'id' => '|.',
        'name' => '|',
      ),
    );
    $type_import = array(
      array(
        'id' => 'Add',
        'name' => $this->l('Add')
      ),
      array(
        'id' => 'Add/update',
        'name' => $this->l('Add/update')
      ),
    );
    $parser = array(
      array(
        'id' => 'name',
        'name' => $this->l('Product name')
      ),
      array(
        'id' => 'reference',
        'name' => $this->l('Reference code')
      ),
      array(
        'id' => 'link_rewrite',
        'name' => $this->l('Friendly URL')
      ),
    );
    $fields_form[0]['form'] = array(
      'input' => array(
        array(
          'type' => 'select',
          'label' => $this->l('Select file format'),
          'name' => 'format_file',
          'class' => 'format_file',
          'required' => true,
          'options' => array(
            'query' =>$format,
            'id' => 'id',
            'name' => 'name'
          )
        ),
        array(
          'type' => 'select',
          'label' => $this->l('Language'),
          'name' => 'id_lang',
          'class' => 'id_lang',
          'required' => true,
          'default_value' => (int)$this->context->language->id,
          'options' => array(
            'query' => Language::getLanguages(),
            'id' => 'id_lang',
            'name' => 'name',
          )
        ),
        array(
          'type' => 'select',
          'label' => $this->l('Delimiter'),
          'name' => 'delimiter_val',
          'class' => 'delimiter_val',
          'options' => array(
            'query' =>$delimiter,
            'id' => 'id',
            'name' => 'name'
          )
        ),
        array(
          'type' => 'select',
          'label' => $this->l('Type of import'),
          'name' => 'import_type_val',
          'class' => 'import_type_val',
          'required' => true,
          'options' => array(
            'query' =>$type_import,
            'id' => 'id',
            'name' => 'name'
          )
        ),
        array(
          'type' => 'select',
          'label' => $this->l('Key for update'),
          'name' => 'parser_import_val',
          'class' => 'parser_import_val',
          'options' => array(
            'query' =>$parser,
            'id' => 'id',
            'name' => 'name'
          )
        ),
        array(
          'type'     => 'file',
          'label'    => $this->l('File'),
          'required' => true,
          'name'     => 'file_import',
        ),
        array(
          'type'  => 'hidden',
          'name'  => 'id_shop',
          'class' => 'id_shop',
        ),
        array(
          'type'  => 'hidden',
          'name'  => 'id_shop_group',
          'class' => 'id_shop_group',
        ),
        array(
          'type'  => 'hidden',
          'name'  => 'location_href',
          'class' => 'location_href',
        ),
        array(
          'type' => 'html',
          'name' => '<button type="button" class="btn btn-default next_button_import">'.$this->l('Next').'</button>',
          'hint' => '',

        ),
      ),
    );
    $helper = new HelperForm();
    $helper->module = $this;
    $helper->name_controller = $this->name;
    $helper->token = Tools::getAdminTokenLite('AdminModules');
    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
    $helper->default_form_language = $default_lang;
    $helper->allow_employee_form_lang = $default_lang;
    $helper->title = $this->displayName;
    $helper->show_toolbar = true;        // false -> remove toolbar
    $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
    $helper->submit_action = 'saveSubmitImport';
    $helper->toolbar_btn = array(
      'save' =>
      array(
        'desc' => $this->l('Save'),
        'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
          '&token='.Tools::getAdminTokenLite('AdminModules'),
      ),
      'back' => array(
        'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
        'desc' => $this->l('Back to list')
      )
    );
    $helper->fields_value['format_file'] = 'xlsx';
    $helper->fields_value['id_lang'] = Context::getContext()->language->id;
    $helper->fields_value['id_shop'] = Context::getContext()->shop->id;
    $helper->fields_value['id_shop_group'] = Context::getContext()->shop->id_shop_group;
    $helper->fields_value['delimiter_val'] = ';';
    $helper->fields_value['import_type_val'] = 'Add';
    $helper->fields_value['parser_import_val'] = 'name';
    $helper->fields_value['location_href'] = AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules').'&configure=simpleimportproduct';
    $this->_html .= $helper->generateForm($fields_form);
  }

  public function initFormImportFields(){

    $config = Tools::unserialize(Configuration::get('GOMAKOIL_CONFIG_IMPORT_PRODUCTS',null,Context::getContext()->shop->id_shop_group,Context::getContext()->shop->id));

    $fields = $config['name_fields_upload'];

    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
    $fields_form = array();

    $array_fields = array('input' => array());
    $array_combination = array('input' => array());
    $array_discount = array('input' => array());
    $array_featured = array('input' => array());
    $array_import = array('input' => array());
    $array_save = array('input' => array());

    $has_hint = array(
      'name'                => array(
        'name' => $this->l('Product name'),
        'hint' => $this->l('The public name for this product. Invalid characters <>;=#{}')
      ),
      'reference'           => array(
        'name' => $this->l('Reference code'),
        'hint' => $this->l('Your internal reference code for this product. Allowed special characters .-_#')
      ),
      'meta_title'          => array(
        'name' => $this->l('Meta title'),
        'hint' => $this->l('Public title for the products page, and for search engines. Leave blank to use the product name. The number of remaining characters is displayed to the left of the field.'),
      ),
      'meta_keywords'       => array(
        'name' => $this->l('Meta keywords'),
        'hint' => $this->l('Keywords for HTML header, separated by commas.'),
      ),

      'meta_description'    => array(
        'name' => $this->l('Meta description'),
        'hint' => $this->l('This description will appear in search engines. You need a single sentence, shorter than 160 characters (including spaces).'),
      ),
      'short_description'   => array(
        'name' => $this->l('Short description'),
        'hint' => $this->l('Appears in the product list(s), and at the top of the product page.'),
      ),
      'description'         => array(
        'name' => $this->l('Description'),
        'hint' => $this->l('Appears in the body of the product page.'),
      ),
      'price'               => array(
        'name' => $this->l('Pre-tax retail price'),
        'hint' => $this->l('The pre-tax retail price is the price for which you intend sell this product to your customers. It should be higher than the pre-tax wholesale price: the difference between the two will be your margin.'),
      ),
      'tax'                 => array(
        'name' => $this->l('Tax rate'),
        'hint' => $this->l('The value must be from 0 to 100'),
      ),
      'quantity'            => array(
        'name' => $this->l('Quantity'),
        'hint' => $this->l('Available quantities for sale.'),
      ),
      'images_url'          => array(
        'name' => $this->l('Images urls'),
        'hint' => $this->l('Each image url has to be followed by a comma. Format JPG, PNG. Filesize 8.00 MB max.'),
      ),
      'link_rewrite'        => array(
        'name' => $this->l('Friendly URL'),
        'hint' => $this->l('This is the human-readable URL, as generated from the products name. You can change it if you want.'),
      ),
      'default_category'    => array(
        'name' => $this->l('Default category'),
        'hint' => $this->l('The default category is the main category for your product, and is displayed by default.')
      ),
      'width'               => array(
        'name' => $this->l('Package width'),
        'hint' => $this->l(''),
      ),
      'height'              => array(
        'name' => $this->l('Package height'),
        'hint' =>$this->l(''),
      ),
      'depth'               => array(
        'name' => $this->l('Package depth'),
        'hint' =>$this->l(''),
      ),
      'weight'              => array(
        'name' => $this->l('Package weight'),
        'hint' =>$this->l(''),
      ),
      'supplier'            => array(
        'name' => $this->l('Suppliers'),
        'hint' => $this->l('Each supplier name has to be followed by a comma. Invalid characters &lt;&gt;;=#{}'),
      ),
      'supplier_default'    => array(
        'name' => $this->l('Default supplier'),
        'hint' =>$this->l('The default supplier name. Invalid characters &lt;&gt;;=#{}'),
      ),
      'manufacturer'        => array(
        'name' => $this->l('Manufacturer'),
        'hint' => $this->l('Manufacturers Name. Invalid characters &lt;&gt;;=#{}'),
      ),
      'tags'                => array(
        'name' => $this->l('Tags'),
        'hint' => $this->l('Appear on the shop in the "Tags" block, when enabled. Tags help customers easily find your products.'),
      ),
      'ean13'               => array(
        'name' => $this->l('EAN-13 or JAN barcode'),
        'hint' => $this->l('This type of product code is specific to Europe and Japan, but is widely used internationally. It is a superset of the UPC code: all products marked with an EAN will be accepted in North America.'),
      ),

      'upc'                 => array(
        'name' => $this->l('UPC barcode'),
        'hint' => $this->l('This type of product code is widely used in the United States, Canada, the United Kingdom, Australia, New Zealand and in other countries.'),
      ),
      'images_alt'          => array(
        'name' => $this->l('Images caption'),
        'hint' => $this->l('Each image caption has to be followed by a comma. Invalid characters <>;=#{}'),
      ),
      'available_for_order' => array(
        'name' => $this->l('Available for order'),
        'hint' => $this->l('Value 0 or 1')
      ),
      'active'              => array(
      'name' => $this->l('Enabled'),
      'hint' =>$this->l('Value 0 or 1')
    ),
      'condition'           => array(
      'name' => $this->l('Condition'),
      'hint' => $this->l('Value new, used or refurbished'),
    ),
      'visibility'          => array(
      'name' => $this->l('Visibility'),
      'hint' =>$this->l('Value both, catalog, search or none'),
    ),

      'on_sale'             => array(
      'name' => $this->l('Display the "on sale" icon'),
      'hint' => $this->l('Display the "on sale" icon on the product page, and in the text found within the product listing. Value 0 or 1'),
    ),
      'min_quantity'        => array(
      'name' => $this->l('Minimum quantity'),
      'hint' =>$this->l('The minimum quantity to buy this product (set to 1 to disable this feature).'),
    ),
      'show_price'          => array(
      'name' => $this->l('Show price'),
      'hint' => $this->l('Value 0 or 1'),
    ),
      'unit_price'          => array(
      'name' => $this->l('Unit price (tax excl.)'),
      'hint' => $this->l('When selling a pack of items, you can indicate the unit price for each item of the pack. For instance, "per bottle" or "per pound".'),
    ),

      'wholesale_price'     => array(
      'name' => $this->l('Pre-tax wholesale price'),
      'hint' => $this->l('The wholesale price is the price you paid for the product. Do not include the tax.'),
    ),
    'ecotax'              => array(
      'name' => $this->l('Ecotax (tax incl.)'),
      'hint' => $this->l('The ecotax is a local set of taxes intended to "promote ecologically sustainable activities via economic incentives". It is already included in retail price: the higher this ecotax is, the lower your margin will be.'),
    ),
      'available_now'       => array(
      'name' => $this->l('Displayed text when in-stock'),
      'hint' =>$this->l('Displayed text when in-stock. Forbidden characters <>;=#{}'),
    ),
      'available_later'     => array(
      'name' => $this->l('Displayed text when backordering is allowed'),
      'hint' =>$this->l('Displayed text when backordering is allowed. If empty, the message "in stock" will be displayed. Forbidden characters <>;=#{}'),
    ),
    );

    foreach($has_hint as $key => $val){
      $array_fields['input'][]=  array(
        'type' => 'select',
        'label' => $val['name'],
        'name' => $key,
        'class' => 'chosen',
        'hint' => $val['hint'],
        'options' => array(
          'query' =>$fields,
          'id' => 'name',
          'name' => 'name',
          'value' => 'name'
        )
      );
      if( $key == "default_category" ){
        $array_fields['input'][]=  array(
          'type' => 'html_categories',
          'label' => $this->l('Associated categories'),
          'name' => 'html_data',
          'class' => 'categories_all',
          'value' => $fields,
          'options' => '',
          'hint' => $this->l('All categories and subcategories product'),
        );
        break;
      }
    }
    $fields_form[0]['form'] = $array_fields;

    $more = false;
    $array_fields = array();
    foreach($has_hint as $key => $val){
      if( $more ){
        $array_fields['input'][]=  array(
          'type' => 'select',
          'label' => $val['name'],
          'name' => $key,
          'class' => 'chosen',
          'hint' => $val['hint'],
          'options' => array(
            'query' =>$fields,
            'id' => 'name',
            'name' => 'name',
            'value' => 'name'
          )
        );
      }

      if( $key == "default_category" ){
        $more = true;
      }
    }
    foreach( $array_fields['input'] as $key => $value ){
      if( !$value['hint'] ){
        unset($array_fields['input'][$key]['hint']);
      }
    }
    $array_fields['input'][] = array(
      'type' => 'html',
      'name' => 'html_data',
      'form_group_class' => 'more',
      'html_content' => '<div class="show_more">'.$this->l('Show more...').'</div>'
    );
    $fields_form[1]['form'] = $array_fields;


    $fields_form[2]['form'] = array('input' => array(array(
      'type' => 'switch',
      'label' => $this->l('Has combination'),
      'name' => 'has_combination',
      'is_bool' => true,
      'values' => array(
        array(
          'id' => 'display_on',
          'value' => 1,
          'label' => $this->l('Yes')),
        array(
          'id' => 'display_off',
          'value' => 0,
          'label' => $this->l('No')),
      ),
    )));
    foreach($this->_has_hint_combinations as $key => $val){
      $array_combination['input'][]=  array(
        'type' => 'select',
        'label' => $val['name'],
        'name' => $key,
        'class' => 'chosen',
        'hint' => $val['hint'],
        'options' => array(
          'query' =>$fields,
          'id' => 'name',
          'name' => 'name'
        )
      );
    }
    foreach( $array_combination['input'] as $key => $value ){
      if( !$value['hint'] ){
        unset($array_combination['input'][$key]['hint']);
      }
    }
    $array_combination['input'][] = array(
      'type' => 'html',
      'name' => 'html_data',
      'hint' => '',
      'html_content' => '<button type="button" class="btn btn-default more_combination">'.$this->l('add combination').'</button>'
    );
    $fields_form[3]['form'] = $array_combination;


    $fields_form[4]['form'] = array('input' => array(array(
      'type' => 'switch',
      'label' => $this->l('Has discount'),
      'name' => 'has_discount',
      'is_bool' => true,
      'values' => array(
        array(
          'id' => 'display_on',
          'value' => 1,
          'label' => $this->l('Yes')),
        array(
          'id' => 'display_off',
          'value' => 0,
          'label' => $this->l('No')),
      ),
    )));
    foreach($this->_has_hint_discount as $key => $val){
      $array_discount['input'][]=  array(
        'type' => 'select',
        'label' => $val['name'],
        'name' => $key,
        'class' => 'chosen',
        'hint' => $val['hint'],
        'options' => array(
          'query' =>$fields,
          'id' => 'name',
          'name' => 'name'
        )
      );
    }
    foreach( $array_discount['input'] as $key => $value ){
      if( !$value['hint'] ){
        unset($array_discount['input'][$key]['hint']);
      }
    }
    $fields_form[5]['form'] = $array_discount;


    $fields_form[6]['form'] = array('input' => array(array(
      'type' => 'switch',
      'label' => $this->l('Has features'),
      'name' => 'has_featured',
      'is_bool' => true,
      'values' => array(
        array(
          'id' => 'display_on',
          'value' => 1,
          'label' => $this->l('Yes')),
        array(
          'id' => 'display_off',
          'value' => 0,
          'label' => $this->l('No')),
      ),
    )));
    foreach($this->_has_hint_featured as $key => $val){
      $array_featured['input'][]=  array(
        'type' => 'select',
        'label' => $val['name'],
        'name' => $key,
        'class' => 'chosen',
        'hint' => $val['hint'],
        'options' => array(
          'query' =>$fields,
          'id' => 'name',
          'name' => 'name'
        )
      );
    }
    $array_featured['input'][] = array(
      'type' => 'html',
      'name' => 'html_data',
      'hint' => '',
      'html_content' => '<button type="button" class="btn btn-default more_featured">'.$this->l('add features').'</button>'
    );
    $fields_form[7]['form'] = $array_featured;

    $fields_form[8]['form'] = array('input' => array(array(
      'type' => 'switch',
      'label' => $this->l('Save settings'),
      'name' => 'save_config',
      'is_bool' => true,
      'values' => array(
        array(
          'id' => 'display_on',
          'value' => 1,
          'label' => $this->l('Yes')),
        array(
          'id' => 'display_off',
          'value' => 0,
          'label' => $this->l('No')),
      ),
    )));

    $array_save['input'][] = array(
      'type'  => 'text',
      'name'  => 'settings_save',
      'class' => 'settings_save',
    );
    $array_save['input'][] = array(
      'type' => 'html',
      'name' => 'html_data',
      'hint' => '',
      'html_content' => '<button type="button" class="btn btn-default save_all_settings">'.$this->l('Save').'</button>'
    );
    $fields_form[9]['form'] = $array_save;


    $array_import['input'][] = array(
      'type'  => 'hidden',
      'name'  => 'id_shop',
      'class' => 'id_shop',
    );
    $array_import['input'][] = array(
      'type'  => 'hidden',
      'name'  => 'id_shop_group',
      'class' => 'id_shop_group',
    );
    $array_import['input'][] = array(
      'type'  => 'hidden',
      'name'  => 'combinations_more',
      'class' => 'combinations_more',
    );
    $array_import['input'][] = array(
      'type'  => 'hidden',
      'name'  => 'features_more',
      'class' => 'features_more',
    );
    $array_import['input'][] = array(
      'type'  => 'hidden',
      'name'  => 'key_settings',
      'class' => 'key_settings',
    );
    $array_import['input'][] = array(
      'type'  => 'hidden',
      'name'  => 'location_href',
      'class' => 'location_href',
    );
    $array_import['input'][] = array(
      'type' => 'html',
      'name' => 'html_data',
      'hint' => '',
      'html_content' => '<button type="button" class="btn btn-default button_import">'.$this->l('Import').'</button>'
    );
    $fields_form[10]['form'] = $array_import;


    $helper = new HelperForm();
    $helper->module = $this;
    $helper->name_controller = $this->name;
    $helper->token = Tools::getAdminTokenLite('AdminModules');
    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
    $helper->default_form_language = $default_lang;
    $helper->allow_employee_form_lang = $default_lang;
    $helper->title = $this->displayName;
    $helper->show_toolbar = true;        // false -> remove toolbar
    $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
    $helper->submit_action = 'saveSubmitImport';
    $helper->toolbar_btn = array(
      'save' =>
      array(
        'desc' => $this->l('Save'),
        'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
          '&token='.Tools::getAdminTokenLite('AdminModules'),
      ),
      'back' => array(
        'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
        'desc' => $this->l('Back to list')
      )
    );

    $helper->fields_value['format_file'] = 'xlsx';
    $helper->fields_value['id_lang'] = Context::getContext()->language->id;
    $helper->fields_value['id_shop'] = Context::getContext()->shop->id;
    $helper->fields_value['id_shop_group'] = Context::getContext()->shop->id_shop_group;
    $helper->fields_value['location_href'] = AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules').'&configure=simpleimportproduct';

    if(Tools::getValue('step') && Tools::getValue('save') !== false){
      $save = Tools::unserialize(Configuration::get('GOMAKOIL_IMPORT_PRODUCTS',null,Context::getContext()->shop->id_shop_group,Context::getContext()->shop->id));
      $save = $save[Tools::getValue('save')];

      $helper->fields_value['key_settings'] = Tools::getValue('save');
      foreach($save['base_field'] as $key => $value){
        $helper->fields_value[$key] = $value;
      }

      if($save['field_combinations']){
        $helper->fields_value['has_combination'] = 1;
        if(count($save['field_combinations'] > 1)){
          $helper->fields_value['combinations_more'] = 1;
        }
        else{
          $helper->fields_value['combinations_more'] = 0;
        }
        foreach($save['field_combinations'][0] as $key => $value){
          $helper->fields_value[$key] = $value;
        }
      }
      else{
        $helper->fields_value['combinations_more'] = 0;
        $helper->fields_value['has_combination'] = 0;
        foreach($this->_has_hint_combinations as $key => $value){
          $helper->fields_value[$key] = 'no';
        }
      }
      if($save['field_discount']){
        $helper->fields_value['has_discount'] = 1;
        foreach($save['field_discount'][0] as $key => $value){
          $helper->fields_value[$key] = $value;
        }
      }
      else{
        $helper->fields_value['has_discount'] = 0;
        foreach($this->_has_hint_discount as $key => $value){
          $helper->fields_value[$key] = 'no';
        }
      }
      if($save['field_featured']){
        $helper->fields_value['has_featured'] = 1;
        if(count($save['field_featured'] > 1)){
          $helper->fields_value['features_more'] = 1;
        }
        else{
          $helper->fields_value['features_more'] = 0;
        }
        foreach($save['field_featured'][0] as $key => $value){
          $helper->fields_value[$key] = $value;
        }
      }
      else{
        $helper->fields_value['has_featured'] = 0;
        foreach($this->_has_hint_featured as $key => $value){
          $helper->fields_value[$key] = 'no';
        }
        $helper->fields_value['features_more'] = 0;
      }

      $helper->fields_value['save_config'] = 1;
      $helper->fields_value['settings_save'] = $save['name_save'];
    }
    else{
      $helper->fields_value['key_settings'] = false;
      $helper->fields_value['has_combination'] = 0;
      $helper->fields_value['has_discount'] = 0;
      $helper->fields_value['has_featured'] = 0;
      $helper->fields_value['save_config'] = 0;
      $helper->fields_value['settings_save'] = '';
      $helper->fields_value['combinations_more'] = 0;
      $helper->fields_value['features_more'] = 0;

      foreach($has_hint as $key => $val){
        $helper->fields_value[$key] = 'no';
      }
      foreach($this->_has_hint_combinations as $key => $val){
        $helper->fields_value[$key] = 'no';
      }
      foreach($this->_has_hint_discount as $key => $val){
        $helper->fields_value[$key] = 'no';
      }
      foreach($this->_has_hint_featured as $key => $val){
        $helper->fields_value[$key] = 'no';
      }
    }
    $this->_html .= $helper->generateForm($fields_form);

  }

  public function moreCombination(){
    $config = Tools::unserialize(Configuration::get('GOMAKOIL_CONFIG_IMPORT_PRODUCTS',null,Context::getContext()->shop->id_shop_group,Context::getContext()->shop->id));
    $fields = $config['name_fields_upload'];
    $this->context->smarty->assign(
      array(
        'has_hint_combinations'  => $this->_has_hint_combinations,
        'fields'                 => $fields,
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/moreCombination.tpl');
  }

  public function moreFeatures(){
    $config = Tools::unserialize(Configuration::get('GOMAKOIL_CONFIG_IMPORT_PRODUCTS',null,Context::getContext()->shop->id_shop_group,Context::getContext()->shop->id));
    $fields = $config['name_fields_upload'];
    $this->context->smarty->assign(
      array(
        'has_hint_featured'  => $this->_has_hint_featured,
        'fields'             => $fields,
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/moreFeatured.tpl');
  }

  public function moreSubcategory($hidden_count_subcategory){
    $config = Tools::unserialize(Configuration::get('GOMAKOIL_CONFIG_IMPORT_PRODUCTS',null,Context::getContext()->shop->id_shop_group,Context::getContext()->shop->id));
    $fields = $config['name_fields_upload'];
    $this->context->smarty->assign(
      array(
        'fields'                   => $fields,
        'hidden_count_subcategory' => $hidden_count_subcategory,
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/moreSubcategory.tpl');
  }

  public function moreCategory($hidden_count_category){
    $config = Tools::unserialize(Configuration::get('GOMAKOIL_CONFIG_IMPORT_PRODUCTS',null,Context::getContext()->shop->id_shop_group,Context::getContext()->shop->id));
    $fields = $config['name_fields_upload'];
    $this->context->smarty->assign(
      array(
        'fields'                 => $fields,
        'hidden_count_category'  => $hidden_count_category,
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/moreCategory.tpl');
  }

  public function addCombinations($key_settings){
    $config = Tools::unserialize(Configuration::get('GOMAKOIL_CONFIG_IMPORT_PRODUCTS',null,Context::getContext()->shop->id_shop_group,Context::getContext()->shop->id));
    $fields = $config['name_fields_upload'];
    $save = Tools::unserialize(Configuration::get('GOMAKOIL_IMPORT_PRODUCTS',null,Context::getContext()->shop->id_shop_group,Context::getContext()->shop->id));
    $this->context->smarty->assign(
      array(
        'has_hint_combinations'  => $this->_has_hint_combinations,
        'save'                   => $save[$key_settings]['field_combinations'],
        'fields'                 => $fields,
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/addCombinations.tpl');
  }



  public function addFeatures($key_settings){
    $config = Tools::unserialize(Configuration::get('GOMAKOIL_CONFIG_IMPORT_PRODUCTS',null,Context::getContext()->shop->id_shop_group,Context::getContext()->shop->id));
    $fields = $config['name_fields_upload'];
    $save = Tools::unserialize(Configuration::get('GOMAKOIL_IMPORT_PRODUCTS',null,Context::getContext()->shop->id_shop_group,Context::getContext()->shop->id));
    $this->context->smarty->assign(
      array(
        'has_hint_featured'  => $this->_has_hint_featured,
        'save'                   => $save[$key_settings]['field_featured'],
        'fields'                 => $fields,
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/addFeatures.tpl');
  }

  public function addCategories($key_settings){
    $config = Tools::unserialize(Configuration::get('GOMAKOIL_CONFIG_IMPORT_PRODUCTS',null,Context::getContext()->shop->id_shop_group,Context::getContext()->shop->id));
    $fields = $config['name_fields_upload'];
    $save = Tools::unserialize(Configuration::get('GOMAKOIL_IMPORT_PRODUCTS',null,Context::getContext()->shop->id_shop_group,Context::getContext()->shop->id));
    $this->context->smarty->assign(
      array(
        'has_hint_featured'      => $this->_has_hint_featured,
        'save'                   => $save[$key_settings]['field_category'],
        'fields'                 => $fields,
      )
    );
    return $this->display(__FILE__, 'views/templates/hook/addCategories.tpl');
  }

}
