<div class="panel" id="fieldset_5_5" style="display: block;">
<div class="form-wrapper">

{foreach from=$has_hint_discount item=discount  key=k }
  <div class="form-group">
    <label class="control-label col-lg-3">
      <span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="{$discount|escape:'htmlall':'UTF-8'}">{$k|escape:'htmlall':'UTF-8'}</span>
    </label>
    <div class="col-lg-9 ">
      <select name="{$k|escape:'htmlall':'UTF-8'}" class="chosen fixed-width-xl" id="{$k|escape:'htmlall':'UTF-8'}"  style="display: none; width:350px;">
        {foreach from=$fields key=key item=field}
          <option value="{$field['name']|escape:'htmlall':'UTF-8'}">{$field['name']|escape:'htmlall':'UTF-8'}</option>
        {/foreach}
      </select>
    </div>
  </div>
{/foreach}


<div class="form-group">
  <div class="col-lg-9 col-lg-offset-3">
    <button type="button" class="btn btn-default more_discount">{l s='more' mod='simpleimportproduct'}</button>&nbsp;&nbsp;<button type="button" class="btn btn-default delete_discount">{l s='delete' mod='simpleimportproduct'}</button>
  </div>
</div>
</div><!-- /.form-wrapper -->
</div>

<script type="text/javascript">
  $('.chosen').chosen()
</script>