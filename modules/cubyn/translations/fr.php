<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{cubyn}prestashop>cubyn_c7aefa9963a2c4b3517971bce203441a'] = 'Cubyn';
$_MODULE['<{cubyn}prestashop>cubyn_e4f0291c20c2d2af30dd3475f6df5296'] = 'La logistique pour tous à 1€.';
$_MODULE['<{cubyn}prestashop>cubyn_876f23178c29dc2552c0b48bf23cd9bd'] = 'Êtes-vous certain de vouloir désinstaller ?';
$_MODULE['<{cubyn}prestashop>cubyn_6da51c62fc2f6d182d33935f44257ae9'] = 'CUBYN n\'est pas compatible avec les versions PrestaShop inférieure à 1.5.';
$_MODULE['<{cubyn}prestashop>cubyn_91c4868ebc847471ed9642d8492ce8b4'] = 'CUBYN requiert l\'extension PHP curl';
$_MODULE['<{cubyn}prestashop>cubyn_871c40caef50769c96eea70fdc1cf129'] = 'Merci de choisir un point relais.';
$_MODULE['<{cubyn}prestashop>cubyn_bccfc939c0b086f1823a8626b16afe8a'] = 'Une erreur est intervenue lors de la sauvegarde Cubyn.';
$_MODULE['<{cubyn}prestashop>cubyn_58cfaf75d4efb6a245643768fef34e4d'] = 'Configuration générale.';
$_MODULE['<{cubyn}prestashop>cubyn_89f9ff590dfeb7896c7038eb40c771a3'] = 'Exporter vers Cubyn les commandes dont le statut est :';
$_MODULE['<{cubyn}prestashop>cubyn_c9cc8cce247e49bae79f15173ce97354'] = 'Sauvegarder';
$_MODULE['<{cubyn}prestashop>cubyn_6d5e48eb4aff04ea75f5abfb76e88c55'] = 'SYNCHRONISATION DES STATUTS ET NUMÉROS DE SUIVI AVEC CUBYN';
$_MODULE['<{cubyn}prestashop>cubyn_d8665c222adcf8cb8888ad014a744945'] = 'Une fois les commandes collectées par nos coursiers, appliquer le statut :';
$_MODULE['<{cubyn}prestashop>cubyn_5d2095e1fcf34f537f4e19baaceb2b8c'] = 'Une fois les commandes expédiées, appliquer le statut :';
$_MODULE['<{cubyn}prestashop>cubyn_9e95e3dd7112b69a70b87b2e997979d6'] = '-- Désactivé --';
$_MODULE['<{cubyn}prestashop>cubyn_4667e970257a391bf21f28968f017af5'] = 'Vous devez configurer une clef API Cubyn';
$_MODULE['<{cubyn}prestashop>cubyn_29f6ae547aaf8c34d5b205bb47cd6913'] = 'Votre clef API Cubyn est obligatoire';
$_MODULE['<{cubyn}prestashop>cubyn_73c94ecd057c38004a11c2f8a1f42b14'] = 'Votre clef API cubyn est incorrecte';
$_MODULE['<{cubyn}prestashop>cubyn_257100132c097f0b44649f0b68cbf2d2'] = 'Votre clef Api Cubyn est sauvegardée';
$_MODULE['<{cubyn}prestashop>cubyn_b3f3c7444650002cd8cae9fb5e2abc19'] = 'Vous devez configurer le statut à exporter';
$_MODULE['<{cubyn}prestashop>cubyn_059d1505aff3ed187ddcc611f2bc3f51'] = 'Configuration du statut à exporter mis à jour';
$_MODULE['<{cubyn}prestashop>cubyn_68e79920af4bce3893c39b9e8d8201d6'] = 'Configuration de la synchronisation mise à jour. Attention : veuillez consulter http://help.cubyn.com/hc/fr/articles/207488955#synchro-suivi pour que le changement soit effectif.';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_0095a9fa74d1713e43e370a7d7846224'] = 'Export';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_40b59bff8c51de2b811b30aafc8a12cd'] = 'Exporter vers Cubyn';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_bd760afa69ef285a5121096eb9375dc3'] = 'Référence';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_ce26601dac0dea138b7295f02b7620a7'] = 'Client';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_87a831bc99060c55b50dbe91c6aab6e9'] = 'Statut';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_96b0141273eabab320119c467cdcaf17'] = 'Total';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_bb8ecbd9a446c1868a8232c83a6a871f'] = 'Transporteur';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_62b555a4eea8be604895627e87c76ae1'] = 'Programmer une collecte';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_9b9520070a6cdf9afde854a95d4f885e'] = 'Choisissez un créneau';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_760aadf0037745e3252346693211202c'] = 'Quelle contenance pour le coursier';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_f726ffa34e02272573de64f7dc1a00bd'] = '2 roues maximum 40x40x40 cm';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_1ff71e43d401da6199cb99431e59c121'] = '2 Roues';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_0f30bc479f4c6dd12fb2f0a7d6255120'] = '4 Roues';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_258b317b81789d49e5ed34b9737411e1'] = 'Vous devez choisir au moins une commande';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_56865847974e7dda924e10d3050cc92b'] = 'Aucune commande exportée';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_e21e0515d8ed9278e8204d0e8f8d30c4'] = '1 commande exportée';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_e3e5dca86e046ef7e794d24b348cd48a'] = 'Cette commande est déjà exportée';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_0b8414b6642fd867d6e46fa37a0b293c'] = 'Fonds insuffisants. Veuillez alimenter votre compte Cubyn.';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_4e50b6047dca67f985d9c68382a46f87'] = 'Vous devez choisir un créneau';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_b5850965afaa82ca0b30c397f5714b1f'] = 'Vous devez choisir une type de coursier';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_9f32a956c1bca368ea54635fd0481e0c'] = 'Fonds insuffisants. Veuillez alimenter votre compte Cubyn.';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_c30ccd7c2135241c39553971e8308337'] = 'Dés que possible';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_f046c5bcb689ce2e4418e7d5c177ea69'] = 'Aujourd\'hui';
$_MODULE['<{cubyn}prestashop>admincubyncontroller_b3a46a9f4cbf9bd55c64602ae9b70476'] = 'Demain';
$_MODULE['<{cubyn}prestashop>order-carrier-relai_e9645f3d9550b84677d4bca09675cf38'] = 'Choisir son point relais';
$_MODULE['<{cubyn}prestashop>order-carrier-relai_45f3cdb81c5b195e002e03112c4dc452'] = 'Changer de point relais';
$_MODULE['<{cubyn}prestashop>order-carrier-relai_ab9849c369f6e71cb5482f66a7b2bd59'] = 'Merci de sélectionner un point relais.';
