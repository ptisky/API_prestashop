<?php
/**
*
*  @package e-legend pro
*  @author    Abouloula Taoufik - E-LEGEND PRO
*  @version    1.0
*  @copyright Copyright (c) Abouloula Taoufik - E-LEGEND PRO
*
*/

class Manufacturer
{

	
	
	public function getApi()
	{
		return Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'pro_elegend_api where id=1');

	}
	
	public function getMaxId()
	{
		$sql = Db::getInstance()->getValue('SELECT max(`id_manufacturer`) FROM '._DB_PREFIX_.'manufacturer');
		return $sql + 1;

	}
	
	public function getNameManufacturer($name_manufacturer)
	{
		$sql = Db::getInstance()->ExecuteS('SELECT name FROM '._DB_PREFIX_.'manufacturer WHERE name LIKE "%'.$name_manufacturer.'%"');
		return count($sql);

	}
	
	public function getIdManufacturer($name_manufacturer)
	{
		$sql = Db::getInstance()->getValue('SELECT id_manufacturer FROM '._DB_PREFIX_.'manufacturer  WHERE name LIKE "%'.$name_manufacturer.'%"');
		return $sql;

	}
	
	/**
	 * mettreajourManufacturer Insertion manufacturer
	 *
	 * @param string $id_category id de la categorie	 *
	 * @return true
	 */ 
	

   public function _manufacturer($id_manufacturer)
	{		
		
		$sql = 'SELECT * FROM '._DB_PREFIX_.'pro_elegend_api';
		if ($results = Db::getInstance()->ExecuteS($sql))
		foreach ($results as $row)
		{
		$adresse = $row['adresse'];
		$cle = $row['cleapi'];
		};
		
	
		 		
		define('DEBUG', false);
        define('PS_SHOP_PATH', $row['adresse']);
        define('PS_WS_AUTH_KEY', $row['cleapi']);


	try
	{	

		$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);        	
		$opt = array('resource' => 'manufacturers');
		$opt['id'] = (int)$id_manufacturer; 
		$xml = $webService->get($opt);
		$m = $xml->children()->children();
		$id_m = (int)$this->getMaxId();
		
		
		 $getNameManufacturer = $this->getNameManufacturer($m->name);
		if($getNameManufacturer == 0){
		
		// insertion  table manufacturer 	
         if(!Db::getInstance()->insert('manufacturer', array(
          'id_manufacturer' => (int)$id_m,
          'name'      => pSQL($m->name),
	      'date_add'  => pSQL(date('Y-m-d H:i:s')),
	      'date_upd'  => pSQL(date('Y-m-d H:i:s')),
	      'active'    => pSQL($m->active),
         ))) die('Error in manufacturer insert : '.$m->id);


        // insertion  table manufacturer_lang 
		if(!Db::getInstance()->insert('manufacturer_lang', array(
          'id_manufacturer' => (int)$id_m,
          'id_lang'      => 1,
	      'description'  => pSQL($m->description->language[0][0]),
	      'short_description'  => pSQL($m->short_description->language[0][0]),
	      'meta_title'    => pSQL($m->meta_title->language[0][0]),
		  'meta_keywords'    => pSQL($m->meta_keywords->language[0][0]),
		  'meta_description'    => pSQL($m->meta_description->language[0][0]),
         ))) die('Error in manufacturer_lang insert : '.$m->id);
		 
		
		// insertion table manufacturer_shop 
		
		 if(!Db::getInstance()->insert('manufacturer_shop', array(
          'id_manufacturer' => (int)$id_m,
	      'id_shop'    => 1,
         ))) die('Error in manufacturer_shop insert : '.$m->id);
		
	
			return $id_m;
		}
	}
	catch (PrestaShopWebserviceException $e)
	{
					
			$trace = $e->getTrace();
			if ($trace[0]['args'][0] == 404) echo 'Bad ID';
			else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
			else echo 'Other error'.$e->getMessage().'</b>';
	}			
		
		
	}
	
	
} //Fin class Catalog
?>