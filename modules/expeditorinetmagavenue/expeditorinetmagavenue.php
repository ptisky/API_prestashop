<?php
/**
 * expeditorinetmagavenue class, expeditorinetmagavenue.php, Order export for Expeditor Inet
 *
 * @category modules
 *
 * @author    Magavenue <contact@magavenue.com>
 * @copyright Magavenue
 * @license   http://www.opensource.org/licenses/osl-3.0.php Open-source licence 3.0
 * @version 2.5 compatible with Prestashop >= 1.5
 */

//@ini_set('display_errors', 'on');

class Expeditorinetmagavenue extends Module
{
    /* Dossier contenant les fichiers generes */
    private $dossierExport = "/export/";
    
    public function __construct()
    {
        $this->name = 'expeditorinetmagavenue';
        $this->tab = 'shipping_logistics';
        $this->version = '2.7.3';
        $this->author = 'MagAvenue';
        $this->module_key = '2e65ea4b2a0ff38b8a74f5a32074c611';

        parent::__construct();

        $this->displayName = $this->l('Expeditor Inet');
        $this->description = $this->l('Export orders for the Expeditor Inet software of La Poste');
        $this->confirmUninstall = $this->l('Are you sure you want to delete all your details ?');
    }

    public function install()
    {
        if (!parent::install()
            or ! $this->installModuleTab('AdminExpeditorinetMagavenue', 'AdminParentShipping')
            or ! Configuration::updateValue('EXPEDITOR_STATUT', 0)
            or ! Configuration::updateValue('EXPEDITOR_TRANSPORTEUR', 0)
            or ! Configuration::updateValue('EXPEDITORM_OLD', 1)) {
            return false;
        }
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()
            or ! $this->uninstallModuleTab('AdminExpeditorinetMagavenue')
            or ! Configuration::deleteByName('EXPEDITOR_STATUT')
            or ! Configuration::deleteByName('EXPEDITOR_TRANSPORTEUR')) {
            return false;
        }
        return true;
    }

    private function installModuleTab($tabClass, $nameTabParent)
    {
        @copy(_PS_MODULE_DIR_ . $this->name . '/logo.gif', _PS_IMG_DIR_ . 't/' . $tabClass . '.gif');
        $tab = new Tab();
        //$tab->name = $tabName;
        foreach (Language::getLanguages(false) as $language) {
            $tab->name[$language['id_lang']] = $this->l('Expeditor Inet');
        }
        $tab->class_name = $tabClass;
        $tab->module = $this->name;
        $tab->id_parent = Tab::getIdFromClassName($nameTabParent);
        if (!$tab->save()) {
            return false;
        }
        return true;
    }

    // tab uninstall
    private function uninstallModuleTab($tabClass)
    {
        $idTab = Tab::getIdFromClassName($tabClass);
        @unlink(_PS_IMG_DIR_ . 't/' . $tabClass . '.gif');
        if ($idTab != 0) {
            $tab = new Tab($idTab);
            $tab->delete();
            return true;
        }
        return false;
    }

    public function displayTab()
    {

        $ouput = '';

        /* Si un fichier est importe */
        if (isset($_FILES['CSV'])) {
            $nameCsv = date("YmdHis");
            if ($this->uploadCSV($nameCsv)) {
                $ouput = $this->importOrders($nameCsv);
                $ouput .= '<div class="conf confirm">' . $this->l('Numbers imported packages') . '</div>';
            } else {
                $ouput = $this->displayError($this->l('Erreur upload CSV.'));
            }
        }

        /* recuperation des commandes */
        $tableauOrders = $this->getOrders(
            Configuration::get('EXPEDITOR_STATUT'),
            Configuration::get('EXPEDITOR_TRANSPORTEUR')
        );
        /* affichage du tableau permettant de modifier les commandes a exporter */
        //$ouput.= $this->displayExport($tableauOrders);

        $id_order_state = Configuration::get('EXPEDITOR_EXPORTSTATE');

        /* generation de lexport */
        if (Tools::getIsset('export')) {
            if (Tools::getIsset('order')) {
                $orders = Tools::getValue('order');
                /* Modification du tableau pour prendre en compte les poids entree dans le formulaire */
                foreach ($orders as $id_order => $order) {
   
                    //suppression du tableau des commandes non cochees
                    if (isset($order['checkbox']) && $order['checkbox'] == 'on') {
                        $tableauOrders[$id_order]['order'][15]['value'] = $order['poids'];
                        $tableauOrders[$id_order]['order'][18]['value'] = ($order['assure'] != 0) ? $order['assure'] : '';

                        $idTMP = $id_order;

                        if ($id_order_state != 0) {
                            /* Changer le statut de la commande */
                            $orderHistory = new OrderHistory();
                            $orderHistory->id_order = (int)($id_order);
                            $orderHistory->changeIdOrderState((int) $id_order_state, $idTMP);
                            $orderHistory->addWithemail();
                            $orderHistory->update();
                        }
                    } else {
                        unset($tableauOrders[$id_order]);
                        continue;
                    }
                }
                
            }
            /* Ecriture du fichier texte */
            $this->writeTxt('commandes.csv', $tableauOrders);
            $ouput.= '<div class="conf confirm">'.$this->l('The ExpeditorInet file as been generated').'</div>';
        }

        /* recuperation des commandes */
        $tableauOrders = $this->getOrders(
            Configuration::get('EXPEDITOR_STATUT'),
            Configuration::get('EXPEDITOR_TRANSPORTEUR')
        );
        /* affichage du tableau permettant de modifier les commandes a exporter */
        $ouput.= $this->displayExport($tableauOrders);
        /* Affichage du formulaire pour limport des numeros de colis */
        $ouput.=$this->displayImportCsv();

        return $ouput;
    }

    public function displayExport($tableauOrders)
    {

        ob_start();

        $tab = '
                <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
                        <input type="submit" name="export" value="' . $this->l('Generate') . '" class="button" />

                        <table cellspacing="0" cellpadding="0" class="table space" width="98%" align="center">
                                        <tbody>
                                                <tr>
                                                        <th style="width:60px;cursor:pointer">
                                                            ' . $this->l('Exporter') . '
                                                            <a onclick="$(\'td\').find(\':checkbox:checked\').click();$(\'td\').find(\':checkbox\').click();">'
                                                                .$this->l('All') . ''.
                                                            '</a> / '.
                                                            '<a onclick="$(\'td\').find(\':checkbox:checked\').click();">'.
                                                                $this->l('None').
                                                            '</a>
                                                        </th>			
                                                        <th>
                                                                ' . $this->l('Order id') . '
                                                        </th>
                                                        <th>
                                                                ' . $this->l('Customer') . '
                                                        </th>
                                                        <th>
                                                                ' . $this->l('Price total (excluding VAT)') . '
                                                        </th>
                                                        <th>
                                                                ' . $this->l('Amount ensure') . '
                                                        </th>
                                                        <th>
                                                                ' . $this->l('Date') . '
                                                        </th>
                                                        <th>
                                                                ' . $this->l('Weight in grams') . '
                                                        </th>
                                                        <th>
                                                                ' . $this->l('View') . '
                                                        </th>
                                                </tr>';
        foreach ($tableauOrders as $id_order => $tableau) {
            if (!is_numeric($id_order)) {
                continue;
            }
            $order = new Order((int) $id_order);
            $tab .= '<tr>';
            $tab .= '<td>'.
                    '<input type="checkbox" name="order['.$id_order.'][checkbox]" value="on" checked="checked" />'
                    .'</td>';
            $tab .= '<td>' . $tableau['order'][1]['value'] . '</td>';
            $tab .= '<td>' . $tableau['order'][6]['value'] . '</td>';
            $tab .= '<td><input type="text" name="order['.$id_order.'][prix]" value="'
                    .round($order->getTotalProductsWithoutTaxes(), 2).'" /></td>';
            $tab .= '<td><input type="text" name="order['.$id_order.'][assure]" value="'
                    .round($tableau['order'][18]['value'], 2).'" /></td>';
            $tab .= '<td>' . date("d/m/Y") . '</td>';
            $tab .= '<td>
                        <input type="text" name="order['.$id_order.'][poids]" value="'.
                            round($tableau['order'][15]['value']*1000, 2).
                        '" />
                    </td>';
            $tab .= '<td>'
                        .'<a href="index.php?tab=AdminOrders&id_order='.$id_order.'&vieworder&token='.
                        Tools::getAdminToken('AdminOrders3'.(int)($this->context->cookie->id_employee)).
                        '" target="_blank" title="'.$this->l('View').'">
                        <img border="0" alt="'.$this->l('Voir').'" src="../img/admin/edit.gif">
                    </td>';
            $tab .= '<tr>';
        }
        $tab .= '
                                        </tbody>
                                </table>
                                <br />
                                <input type="submit" name="export" value="'.$this->l('Generate').'" class="button" />
                                <p>
                                    <a href="'.__PS_BASE_URI__.
                                        'modules/expeditorinetmagavenue'.$this->dossierExport
                                        .'commandes.csv" target="_blank">'.
                                        $this->l('Download the export file').
                                    '</a>
                                </p>
                        </form>';
        ob_get_clean();
        return $tab;
    }

    public function getContent()
    {

        $output = '<h2>' . $this->displayName . '</h2>';
        $state = Tools::getValue('addState');
        $carrier = Tools::getValue('addTransporteur');
        if (Tools::isSubmit('submitExpeditor')) {
            if ((!$state = Tools::getValue('addState') or empty($state))
                    or (!$carrier = Tools::getValue('addTransporteur') or empty($carrier))) {
                $output .= '<div class="alert error">' . $this->l('You must fill the form') . '</div>';
            } else {
                (null !== Tools::getValue('socolissimo') ? Configuration::updateValue('EXPEDITORM_SOC', (int) Tools::getValue('socolissimo')) : '');
                (null !== Tools::getValue('oldrelease') ? Configuration::updateValue('EXPEDITORM_OLD', (int) Tools::getValue('oldrelease')) : '');
                (null !== Tools::getValue('socolissimo_name') ? Configuration::updateValue('EXPEDITORM_NAME', pSQL(Tools::getValue('socolissimo_name'))) : '');
                (Tools::getIsset("addState") ? Configuration::updateValue('EXPEDITOR_STATUT', implode(',', Tools::getValue("addState"))) : '');

                (Tools::getIsset("countries") ? Configuration::updateValue('EXPEDITOR_CN23', implode(',', Tools::getValue("countries"))) : '');
                (null !== Tools::getValue('importState') ? Configuration::updateValue('EXPEDITOR_IMPORTSTATE', (int) Tools::getValue('importState')) : '');
                (null !== Tools::getValue('exportState') ? Configuration::updateValue('EXPEDITOR_EXPORTSTATE', (int) Tools::getValue('exportState')) : '');
                (Tools::getIsset("addTransporteur") ? Configuration::updateValue('EXPEDITOR_TRANSPORTEUR', implode(',', Tools::getValue("addTransporteur"))) : '');

                $output .= '<div class="conf confirm">
                                <img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.
                                $this->l('Settings updated').
                            '</div>';
            }
        }
        return $output . $this->displayForm();
    }

    public function displayForm()
    {
        $output = '';

        $statuts = explode(',', Configuration::get('EXPEDITOR_STATUT'));
        $countriesSelected = explode(',', Configuration::get('EXPEDITOR_CN23'));
        $countries = Country::getCountries($this->context->employee->id_lang);

        //if (count($statuts) > 0)
        //    $output.=$this->displayTab();

        $selected_carriers = explode(',', Configuration::get('EXPEDITOR_TRANSPORTEUR'));

        $sqlSelectStates = 'SELECT DISTINCT id_order_state, name '
                            .'FROM `'._DB_PREFIX_.'order_state_lang` '
                            .'WHERE `id_lang`="'.(int) Context::getContext()->cookie->id_lang.'";';
        $selectedStates = Db::getInstance()->ExecuteS($sqlSelectStates);

        $output.= '
		<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
			<fieldset>
                                <legend>
                                    <img src="'.$this->_path.'views/img/info.gif" alt="'.$this->l('Information').'" />'.
                                    $this->l('Information').
                                '</legend>
                                <p>' . $this->l('If you want to view the manual of the module,') . '&nbsp;
                                    <a target="_blank" href="'.$this->_path.'readme_fr.pdf">'.
                                        $this->l('click here.').
                                    '</a>
                                </p>
                        </fieldset>
                        <br />
                        <fieldset>
                                <legend>
                                    <img src="'.$this->_path.'logo.gif" alt="" title="" />'
                                    .$this->l('Settings').'
                                </legend>
                                <label>' . $this->l('Version Expeditor Inet >= 3.2') . '</label>
                                <div class="margin-form">
                                        <input type="radio" name="oldrelease" id="oldrelease_on" value="1" '.
                                            (Configuration::get('EXPEDITORM_OLD') ? 'checked="checked" ' : '').'/>
                                        <label class="t" for="oldrelease_on">
                                            <img src="../img/admin/enabled.gif" alt="'.
                                            $this->l('Enabled').'" title="'.$this->l('Enabled').'" />
                                        </label>
                                        <input type="radio" name="oldrelease" id="oldrelease_off" value="0" '.
                                            (!Configuration::get('EXPEDITORM_OLD') ? 'checked="checked" ' : '').
                                        '/>
                                        <label class="t" for="oldrelease_off">
                                            <img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').
                                            '" title="'.$this->l('Disabled').'" />
                                        </label>
                                        <p class="clear">'.
                                            $this->l('Change this valeure, only if you use a version of Expeditor Inet < at 3.2').
                                        '</p>
                                </div>                     
                                <br />
                                <label>' . $this->l('So Colissimo') . '</label>
                                <div class="margin-form">
                                        <input type="radio" name="socolissimo" id="socolissimo_on" value="1" '.
                                            (Configuration::get('EXPEDITORM_SOC') ? 'checked="checked" ' : '').
                                        '/>
                                        <label class="t" for="socolissimo_on">
                                            <img src="../img/admin/enabled.gif" alt="'
                                                .$this->l('Enabled').'" title="'.$this->l('Enabled').'" />
                                        </label>
                                        <input type="radio" name="socolissimo" id="socolissimo_off" value="0" '
                                            .(!Configuration::get('EXPEDITORM_SOC') ? 'checked="checked" ' : '').'/>
                                        <label class="t" for="socolissimo_off">
                                            <img src="../img/admin/disabled.gif" alt="'
                                            .$this->l('Disabled').'" title="'.$this->l('Disabled').'" />
                                        </label>
                                </div>

                                <label for="socolissimo_name">' . $this->l('So Colissimo commercial name') . '</label>
                                <div class="margin-form">
                                        <input type="text" name="socolissimo_name" id="socolissimo_name" value="'
                                            .Configuration::get('EXPEDITORM_NAME').'"/>
                                        <p class="clear">'.
                                            $this->l('If you use So Colissimo, you must check the previous choice and indicate the Colissimo name that was assigned to you.').
                                        '</p>
                                </div>
                                <br />
				<label>' . $this->l('CN23') . '</label>
				<div class="margin-form">
					<select name="countries[]" id="countries" multiple="multiple">';
        foreach ($countries as $country) {
            $output.='<option '
                .(in_array($country['id_country'], $countriesSelected) ? 'selected="selected"' : '')
                .' value="'.$country['id_country'].'">'.$country['name']
                .'</option>';
        }
        $output.='
					</select>	
					<p class="clear">'.
                                            $this->l('Export customs document for these countries. Warning products must have a feature called Numero_tarifaire containing their tariff number').
                                        '</p>
				</div>
				<label>' . $this->l('Order status') . '</label>
				<div class="margin-form">
					<select name="addState[]" id="addState" multiple="multiple">';
        foreach ($selectedStates as $state) {
            $output.='<option '.(in_array($state['id_order_state'], $statuts) ? 'selected="selected"' : '')
                .' value="'.$state['id_order_state'].'">'.$state['name']
                .'</option>';
        }
        $output.='
					</select>	
					<p class="clear">'.
                                            $this->l('Only orders with this state will be exported to Expeditor Inet. Hold Ctrl to select multiple choice.')
                                        .'</p>
				</div>
		
                                <label>' . $this->l('Statut export') . '</label>
				<div class="margin-form">
					<select name="exportState" id="exportState">
                                            <option value="0">- - -</option>
                                        ';
        foreach ($selectedStates as $state) {
            $output.='<option '.
                ((int) $state['id_order_state']==(int) Configuration::get('EXPEDITOR_EXPORTSTATE') ? 'selected="selected"' : '')
                .' value="'.$state['id_order_state'].'">'.$state['name']
                .'</option>';
        }
        $output.='
					</select>
					<p class="clear">' . $this->l('Orders exported to Expeditor Inet status change with it.') . '</p>
				</div>
				
				<label>' . $this->l('Statut import') . '</label>
				<div class="margin-form">
					<select name="importState" id="importState">
                                            <option value="0">- - -</option>
                                        ';
        foreach ($selectedStates as $state) {
            $output.='<option '
                .((int) $state['id_order_state']==(int) Configuration::get('EXPEDITOR_IMPORTSTATE') ? 'selected="selected"' : '')
                .' value="' . $state['id_order_state'].'">'.$state['name']
                .'</option>';
        }
        $output.='
					</select>
					<p class="clear">' . $this->l('Orders imported from Expeditor Inet, status change with it.') . '</p>
				</div>

				<label>' . $this->l('Carrier') . '</label>
				<div class="margin-form">
					<select name="addTransporteur[]" id="addTransporteur" multiple="multiple">';
        
        
        $selectedCarriers = Carrier::getCarriers(
            (int) Configuration::get('PS_LANG_DEFAULT'),
            true,
            false,
            false,
            null,
            Carrier::ALL_CARRIERS
        );

        foreach ($selectedCarriers as $carrier) {
            $search_carrier = (version_compare(_PS_VERSION_, '1.5', '>')) ? $carrier['id_reference'] : $carrier['id_carrier'];

            $output.='<option value="' . $search_carrier . '"
                    ' . ( in_array($search_carrier, $selected_carriers) ? 'selected="selected"' : '') . '
                    >' . $carrier['name'] .
                    '</option>';
        }
        //foreach($selectedCarriers as $carrier)
        //$output.='<option '.
        //( in_array($carrier['id_carrier'], $carriers)?'selected="selected"':'').
        //' value="'.$carrier['id_carrier'].'">'.$carrier['name'].'</option>';
        $output.='
                                    </select>	
                                    <p class="clear">'.
                                        $this->l('Only orders with this carrier will be exported to Expeditor Inet. Hold Ctrl to select multiple.')
                                    .'</p>
				</div>
				
				<center><input type="submit" name="submitExpeditor" value="' . $this->l('Save') . '" class="button" /></center>			
			</fieldset>
		</form>';
        return $output;
    }

    /* Recupere le dernier statut de la commande */

    public function getLastOrderState($id_order)
    {
        $result = Db::getInstance()->getRow('
		SELECT `id_order_state`
		FROM `' . _DB_PREFIX_ . 'order_history`
		WHERE `id_order` = ' . (int)($id_order) . '
		ORDER BY `date_add` DESC, `id_order_history` DESC');
        if (!$result or empty($result) or ! key_exists('id_order_state', $result)) {
            return false;
        }
        return (int)($result['id_order_state']);
    }

    private function getOrders($states, $carriers)
    {
        if (version_compare(_PS_VERSION_, '1.5', '>=')) {
            $cmpl_sql = 'LEFT JOIN `'._DB_PREFIX_
                    .'shop` shop ON (shop.id_shop = '
                    .(int) Context::getContext()->shop->id.')';
            $search_carrier = 'c.`id_reference`';
        } else {
            $cmpl_sql = '';
            $search_carrier = 'c.`id_carrier`';
        }

        $sqlSelectOrder = '
               SELECT o.`id_order` FROM `' . _DB_PREFIX_ . 'orders` o
               LEFT JOIN `' . _DB_PREFIX_ . 'order_history` oh ON (o.`id_order` = oh.`id_order`)
               LEFT JOIN `' . _DB_PREFIX_ . 'carrier` c ON (o.`id_carrier` = c.`id_carrier`)
                ' . $cmpl_sql . ' 
               WHERE
                        oh.`id_order_state` IN (' . $states . ')
                AND
                        ' . $search_carrier . ' IN (' . $carriers . ')

               ORDER BY o.`id_order` ASC
               ;';
        $selectedOrder = Db::getInstance()->ExecuteS($sqlSelectOrder);

        $txtfile = array();

        $states = explode(',', $states);
        if ($selectedOrder) {
            foreach ($selectedOrder as $order) {
                if (in_array($this->getLastOrderState((int) $order['id_order']), $states)) {
                    $txtfile[(int) $order['id_order']]['order'] = $this->generateOrders((int) $order['id_order']);
                    if ($cn23 = $this->generateCN23((int) $order['id_order'])) {
                        foreach ($cn23 as $cn23Item) {
                            $txtfile[(int) $order['id_order']]['CN23'][] = $cn23Item;
                        }
                    }
                }
            }
        }

        return $txtfile;
    }

    private function generateCN23($id_order)
    {
        $order = new Order((int) $id_order);
        $destinataire = new Address((int)($order->id_address_delivery));

        if (!in_array($destinataire->id_country, explode(',', Configuration::get('EXPEDITOR_CN23')))) {
            return;
        }

        $products = $order->getProducts();
        $productsArray = array();
        if ($products) {
            foreach ($products as $product) {
                if ($product['total_price'] <= 0) {
                    continue;
                }

                $nTarifaire = '';
                $productObj = new Product((int) $product['product_id']);
                $features = $productObj->getFrontFeatures(2);
                if ($features) {
                    foreach ($features as $feature) {
                        if ($feature['name'] == 'Numero_tarifaire') {
                            $nTarifaire = $feature['value'];
                        }
                    }
                }
                $productsArray[$product['id_order_detail']] = array(
                    0 => array('long' => 3, 'value' => 'CN2', 'name' => 'Type d enregistrement'),
                    1 => array(
                        'long' => 25,
                        'value' => Tools::substr($this->cleanString($product['product_name']), 0, 24),
                        'name' => 'Libelle'
                    ),
                    2 => array('long' => 3, 'value' => $product['product_quantity'], 'name' => 'Quantite'),
                    3 => array('long' => 5, 'value' => $product['product_weight'], 'name' => 'Poids'),
                    4 => array('long' => 8, 'value' => $product['product_price'], 'name' => 'Valeur'),
                    5 => array('long' => 11, 'value' => $nTarifaire, 'name' => 'N tarifaire'),
                    6 => array('long' => 9, 'value' => '', 'name' => 'Filler'),
                    6 => array('long' => 2, 'value' => 'FR', 'name' => 'Pays d origine')
                );
            }
        }
        return $productsArray;
    }

    /* Generate order list for Expeditor Inet system */

    private function generateOrders($id_order)
    {
        $deliveryInfos = array();

        if (Configuration::get('EXPEDITORM_OLD') == 0) {
            return $this->generateOrdersv3($id_order);
        }

        $order = new Order((int) $id_order);
        $destinataire = new Address((int)($order->id_address_delivery));
        $customer = new Customer((int)($order->id_customer));
        $country = new Country((int)($destinataire->id_country));

        if (isset($destinataire->phone_mobile) && $destinataire->phone_mobile != '') {
            if (Tools::substr($destinataire->phone_mobile, 0, 2) == '00') {//Remplace 00XX par 0
                $phone_mobile_reduit = '0' . Tools::substr($destinataire->phone_mobile, 4);
            } elseif (Tools::substr($destinataire->phone_mobile, 0, 1) == '+') {//Remplace +XX par 0
                $phone_mobile_reduit = '0' . Tools::substr($destinataire->phone_mobile, 3);
            } else {
                $phone_mobile_reduit = $destinataire->phone_mobile;
            }
            $phone_mobile_reduit = Tools::substr(
                str_replace(
                    array('(', ')', '.', ' '),
                    array('', '', '', ''),
                    $phone_mobile_reduit
                ),
                0,
                10
            );
        } else {
            $phone_mobile_reduit = '';
        }
 
        $values = array(
            0 => array('long' => 3, 'value' => 'EXP', 'name' => 'Type d\'enregistrement'),
            1 => array(
                'long' => 35,
                'value' => '' . sprintf("%06d", $order->id),
                'name' => 'Reference du colis pour lexpediteur'
            ),
            2 => array('long' => 8, 'value' => date("Ymd"), 'name' => 'Date dexpedition'),
            3 => array('long' => 4, 'value' => '', 'name' => 'Code produit'),
            4 => array('long' => 6, 'value' => '', 'name' => 'Code expediteur'),
            5 => array(
                'long' => 17,
                'value' => $destinataire->id_customer,
                'name' => 'Code destinataire'
            ),
            6 => array(
                'long' => 35,
                'value' => Tools::substr(utf8_decode($destinataire->lastname), 0, 35),
                'name' => 'Nom destinataire'
            ),
            7 => array(
                'long' => 35,
                'value' => Tools::substr(utf8_decode($destinataire->address1), 0, 35),
                'name' => 'Adresse 1'
            ),
            8 => array(
                'long' => 35,
                'value' => Tools::substr(utf8_decode($destinataire->address2), 0, 35),
                'name' => 'Adresse 2'
            ),
            9 => array('long' => 35, 'value' => '', 'name' => 'Adresse 3'),
            10 => array(
                'long' => 9,
                'value' => Tools::substr($this->cleanString($destinataire->postcode), 0, 9),
                'name' => 'Code postal'
            ),
            11 => array(
                'long' => 35,
                'value' => Tools::substr(utf8_decode($destinataire->city), 0, 35),
                'name' => 'Commune'
            ),
            12 => array('long' => 3, 'value' => $country->iso_code, 'name' => 'Code pays'),
            13 => array(
                'long' => 70,
                'value' => Tools::substr(utf8_decode(str_replace("\r\n", " ", $destinataire->other)), 0, 70),
                'name' => 'Instruction de livraison'
            ),
            14 => array('long' => 12, 'value' => '', 'name' => 'Filler'),
            15 => array(
                'long' => 15,
                'value' => Tools::substr(($order->getTotalWeight()), 0, 15),
                'name' => 'Poids'
            ),
            16 => array('long' => 15, 'value' => '', 'name' => 'Montant CRBT'),
            17 => array('long' => 3, 'value' => '', 'name' => 'Filler'),
            18 => array('long' => 15, 'value' => '', 'name' => 'Montant ADV (assurance)'),
            19 => array('long' => 3, 'value' => '', 'name' => 'Filler'),
            20 => array('long' => 1, 'value' => 'N', 'name' => 'Non mecanisable'),
            21 => array('long' => 17, 'value' => '', 'name' => 'Filler'),
            22 => array(
                'long' => 25,
                'value' => (isset($destinataire->phone) ? $destinataire->phone : $destinataire->phone_mobile),
                'name' => 'N telephone'
            ),
            23 => array('long' => 4, 'value' => '', 'name' => 'Filler'),
            24 => array('long' => 1, 'value' => '0', 'name' => 'Recommandation'),
            25 => array('long' => 1, 'value' => 'N', 'name' => 'Avis de reception'),
            26 => array('long' => 1, 'value' => 'N', 'name' => 'FTD(Franc de taxes et de droits)'),
            27 => array('long' => 1, 'value' => '2', 'name' => 'Instruction de non livraison'),
            28 => array(
                'long' => 75,
                'value' => isset($customer->email) ? Tools::substr($customer->email, 0, 80) : '',
                'name' => 'Adresse email'
            ),
            29 => array('long' => 1, 'value' => 4, 'name' => 'Nature envoi'),
            30 => array('long' => 25, 'value' => '', 'name' => 'Numero de licence'),
            31 => array('long' => 25, 'value' => '', 'name' => 'Numero de certificat'),
            32 => array('long' => 25, 'value' => '', 'name' => 'Numero de facture'),
            33 => array('long' => 4, 'value' => '', 'name' => 'Reference en douane'),
            34 => array('long' => 35, 'value' => '', 'name' => 'Adresse 4'),
            35 => array('long' => 35, 'value' => '', 'name' => 'Numero daffaire'),
            36 => array(
                'long' => 35,
                'value' => '#' . sprintf("%06d", $order->id),
                'name' => 'Numero de commande'
            ), //adding a filler of 73 space -> 35 + 73 = 108
            37 => array('long' => 35, 'value' => '', 'name' => 'Filler'),
            38 => array('long' => 11, 'value' => '', 'name' => 'BIC'),
            39 => array('long' => 27, 'value' => '', 'name' => 'IBAN'),
            40 => array('long' => 1, 'value' => '', 'name' => 'Civilite du destinataire'),
            41 => array(
                'long' => 29,
                'value' => Tools::substr(utf8_decode($destinataire->firstname), 0, 29),
                'name' => 'Prenom du destinataire'
            ),
            42 => array('long' => 38,
                'value' => Tools::substr(utf8_decode($destinataire->company), 0, 38),
                'name' => 'Raison sociale du destinataire'),
            43 => array('long' => 10, 'value' => $phone_mobile_reduit, 'name' => 'Portable du destinataire'),
            44 => array('long' => 8, 'value' => '', 'name' => 'Code porte 1 de Ladresse du destinataire'),
            45 => array('long' => 8, 'value' => '', 'name' => 'Code porte 2 de ladresse du destinataire'),
            46 => array('long' => 30, 'value' => '', 'name' => 'Interphone de ladresse du destinataire'),
            47 => array('long' => 6, 'value' => '', 'name' => 'Code point de retrait'),
            48 => array('long' => 1, 'value' => '', 'name' => 'Filler'),
            49 => array(
                'long' => 38,
                'value' => Configuration::get('EXPEDITORM_NAME'),
                'name' => 'Nom commercial de lexpediteur'
            ),
            50 => array('long' => 30, 'value' => '', 'name' => 'Filler'),
            51 => array('long' => 15, 'value' => '', 'name' => 'Code avoir/promotion')
        );

        $deliveryInfos['delivery_mode'] = '';
        $deliveryInfos['cedeliveryinformation'] = str_replace("\r\n", " ", $destinataire->other);
        $deliveryInfos['ceemail'] = $customer->email;
        $deliveryInfos['cephonenumber'] = $phone_mobile_reduit;
        $deliveryInfos['cecompanyname'] = $destinataire->company;
        $deliveryInfos['cefirstname'] = isset($deliveryInfos['cefirstname']) ? $deliveryInfos['cefirstname'] : '';
        $deliveryInfos['prfirstname'] = isset($deliveryInfos['cefirstname']) ? $deliveryInfos['cefirstname'] : isset($deliveryInfos['prfirstname']) ? $deliveryInfos['prfirstname'] : utf8_decode($destinataire->firstname);
        $deliveryInfos['prid'] = '';

        //So colissimo part
        if (Configuration::get('EXPEDITORM_SOC') == 1) {
            $deliveryInfos = $this->getDeliveryInfos((int)($order->id_cart), (int)($order->id_customer));
            $values[4] = array(
                'long' => 6,
                'value' => utf8_decode(Tools::substr($deliveryInfos, 0, 6)),
                'name' => 'Code expediteur'
            );
            if (in_array($deliveryInfos['delivery_mode'], array('DOS', 'DOM', 'RDV'))) {
                $values[7] = array(
                    'long' => 35,
                    'value' => utf8_decode(Tools::substr($deliveryInfos['pradress3'], 0, 35)),
                    'name' => 'Adresse 1'
                );
                $values[8] = array(
                    'long' => 35,
                    'value' => utf8_decode(Tools::substr($deliveryInfos['pradress1'], 0, 35)),
                    'name' => 'Adresse 2'
                );
                $values[9] = array(
                    'long' => 35,
                    'value' => utf8_decode(Tools::substr($deliveryInfos['pradress2'], 0, 35)),
                    'name' => 'Adresse 3'
                );
                $values[10] = array(
                    'long' => 9,
                    'value' => utf8_decode(Tools::substr($deliveryInfos['przipcode'], 0, 9)),
                    'name' => 'Code postal'
                );
                $values[11] = array(
                    'long' => 35,
                    'value' => utf8_decode(Tools::substr($deliveryInfos['prtown'], 0, 35)),
                    'name' => 'City'
                );
                $values[34] = array(
                    'long' => 35,
                    'value' => utf8_decode(Tools::substr($deliveryInfos['pradress4'], 0, 35)),
                    'name' => 'Adresse 4'
                );
                $values[44] = array(
                    'long' => 8,
                    'value' => utf8_decode(Tools::substr($deliveryInfos['cedoorcode1'], 0, 8)),
                    'name' => 'Code porte 1 de Ladresse du destinataire'
                );
                $values[45] = array(
                    'long' => 8,
                    'value' => utf8_decode(Tools::substr($deliveryInfos['cedoorcode2'], 0, 8)),
                    'name' => 'Code porte 2 de ladresse du destinataire'
                );
                $values[46] = array(
                    'long' => 30,
                    'value' => '',
                    'name' => 'Interphone de ladresse du destinataire'
                );
            }

            //commun a tous les produits so colissimo
            $cecivility = (int)($customer->id_gender) + 1;
            if ($cecivility > 3) {
                $cecivility = 1;
            }
            $facture = new Address((int)($order->id_address_invoice));
            $values[3] = array(
                'long' => 4,
                'value' => Tools::substr($deliveryInfos['delivery_mode'], 0, 4),
                'name' => 'Code produit'
            );
            $values[13] = array(
                'long' => 70,
                'value' => utf8_decode(Tools::substr(trim($deliveryInfos['cedeliveryinformation']), 0, 70)),
                'name' => 'Instruction de livraison'
            );
            $values[28] = array(
                'long' => 75,
                'value' => Tools::substr(
                    ($deliveryInfos['ceemail'] != '' ? $deliveryInfos['ceemail'] : $customer->email),
                    0,
                    80
                ),
                'name' => 'Adresse email'
            );
            $values[40] = array(
                'long' => 1,
                'value' => $cecivility,
                'name' => 'Civilite du destinataire'
            );
            $values[41] = array(
                'long' => 29,
                'value' => utf8_decode(Tools::substr($deliveryInfos['prfirstname'], 0, 29)),
                'name' => 'Prenom du destinataire'
            );
            $values[42] = array(
                'long' => 38,
                'value' => utf8_decode(Tools::substr($deliveryInfos['cecompanyname'], 0, 38)),
                'name' => 'Raison sociale du destinataire'
            );
            $values[43] = array(
                'long' => 10,
                'value' => $deliveryInfos['cephonenumber'],
                'name' => 'Portable du destinataire'
            );
            $values[47] = array(
                'long' => 6,
                'value' => Tools::substr($deliveryInfos['prid'], 0, 6),
                'name' => 'Code point de retrait'
            );
            //$values[49] = array(
            //  'long' => 38,
            //  'value' => utf8_decode(
            //      Tools::substr(($deliveryInfos['cecompanyname'] != '' ? $deliveryInfos['cecompanyname'] : Configuration::get('EXPEDITORM_NAME')),
            //      0,
            //      38)
            //   ),
            //  'name' => 'Nom commercial de lexpéditeur'
            //  );
            // colis point relais
            if (in_array($deliveryInfos['delivery_mode'], array('BPR', 'A2P'))) {
                $values[6] = array(
                    'long' => 35,
                    'value' => utf8_decode(Tools::substr($facture->lastname, 0, 35)),
                    'name' => 'Nom destinataire'
                );
                $values[7] = array(
                    'long' => 35,
                    'value' => utf8_decode(Tools::substr($facture->address1, 0, 35)),
                    'name' => 'Adresse 1'
                );
                $values[8] = array(
                    'long' => 35,
                    'value' => utf8_decode(Tools::substr($facture->address2, 0, 35)),
                    'name' => 'Adresse 2'
                );
                $values[9] = array(
                    'long' => 35,
                    'value' => utf8_decode(''),
                    'name' => 'Adresse 3'
                );
                $values[10] = array(
                    'long' => 9,
                    'value' => utf8_decode(Tools::substr($facture->postcode, 0, 9)),
                    'name' => 'Code postal'
                );
                $values[11] = array(
                    'long' => 35,
                    'value' => utf8_decode(Tools::substr($facture->city, 0, 35)),
                    'name' => 'City'
                );
                $values[34] = array(
                    'long' => 35,
                    'value' => utf8_decode(''),
                    'name' => 'Adresse 4'
                );
                $values[41] = array(
                    'long' => 29,
                    'value' => utf8_decode(Tools::substr($facture->firstname, 0, 29)),
                    'name' => 'Prenom du destinataire'
                );
                $values[42] = array(
                    'long' => 38,
                    'value' => utf8_decode(Tools::substr($facture->company, 0, 38)),
                    'name' => 'Raison sociale du destinataire'
                );
            }
        }
        return $values;
    }

    /* Generate order list for Expeditor Inet system older than 3.2 */

    private function generateOrdersv3($id_order)
    {
        $order = new Order((int) $id_order);
        $destinataire = new Address((int)($order->id_address_delivery));
        $country = new Country((int)($destinataire->id_country));

        $values = array(
            0 => array('long' => 3, 'value' => 'EXP', 'name' => 'Type d\'enregistrement'),
            1 => array(
                'long' => 35,
                'value' => 'Col'.sprintf("%06d", $order->id),
                'name' => 'Reference du colis pour lexpediteur'
            ),
            2 => array('long' => 8, 'value' => date("Ymd"), 'name' => 'Date dexpedition'),
            3 => array('long' => 4, 'value' => '', 'name' => 'Code produit'),
            4 => array('long' => 6, 'value' => '', 'name' => 'Code expediteur'),
            5 => array('long' => 17, 'value' => $destinataire->id_customer, 'name' => 'Code destinataire'),
            6 => array(
                'long' => 35,
                'value' => utf8_decode($destinataire->lastname.' '.$destinataire->firstname),
                'name' => 'Nom destinataire'
            ),
            7 => array('long' => 35, 'value' => utf8_decode($destinataire->address1), 'name' => 'Adresse 1'),
            8 => array('long' => 35, 'value' => utf8_decode($destinataire->address2), 'name' => 'Adresse 2'),
            9 => array('long' => 35, 'value' => '', 'name' => 'Adresse 3'),
            10 => array('long' => 9, 'value' => $destinataire->postcode, 'name' => 'Code postal'),
            11 => array('long' => 35, 'value' => utf8_decode($destinataire->city), 'name' => 'Commune'),
            12 => array('long' => 3, 'value' => $country->iso_code, 'name' => 'Code pays'),
            13 => array(
                'long' => 70,
                'value' => utf8_decode(str_replace("\r\n", " ", $destinataire->other)),
                'name' => 'Instruction de livraison'
            ),
            14 => array('long' => 12, 'value' => '', 'name' => 'Filler'),
            15 => array('long' => 15, 'value' => ($order->getTotalWeight() * 1000), 'name' => 'Poids'),
            16 => array('long' => 15, 'value' => '', 'name' => 'Montant CRBT'),
            17 => array('long' => 3, 'value' => '', 'name' => 'Filler'),
            18 => array(
                'long' => 15,
                'value' => $order->getTotalProductsWithoutTaxes(),
                'name' => 'Montant ADV'
            ),
            19 => array('long' => 3, 'value' => '', 'name' => 'Filler'),
            20 => array('long' => 1, 'value' => '', 'name' => 'Non mecanisable'),
            21 => array('long' => 17, 'value' => '', 'name' => 'Filler'),
            22 => array(
                'long' => 25,
                'value' => (isset($destinataire->phone) ? $destinataire->phone : $destinataire->phone_mobile),
                'name' => 'N telephone'
            ),
            23 => array('long' => 4, 'value' => '', 'name' => 'Filler'),
            24 => array('long' => 1, 'value' => '', 'name' => 'Recommandation'),
            25 => array('long' => 1, 'value' => 'O', 'name' => 'Avis de reception'),
            26 => array('long' => 1, 'value' => '', 'name' => 'FTD'),
            27 => array('long' => 1, 'value' => '', 'name' => 'Instruction de non livraison'),
            28 => array('long' => 75, 'value' => '', 'name' => 'Filler'),
            29 => array('long' => 1, 'value' => 4, 'name' => 'Nature envoi'),
            30 => array('long' => 25, 'value' => '', 'name' => 'Numero de licence'),
            31 => array('long' => 25, 'value' => '', 'name' => 'Numero de certificat'),
            32 => array('long' => 25, 'value' => '', 'name' => 'Numero de facture'),
            33 => array('long' => 4, 'value' => '', 'name' => 'Reference en douane'),
            34 => array('long' => 35, 'value' => '', 'name' => 'Adresse 4'),
            35 => array('long' => 35, 'value' => '#' . sprintf("%06d", $order->id), 'name' => 'Numero de commande'),
            36 => array('long' => 35, 'value' => '', 'name' => 'Numero daffaire'),
            37 => array('long' => 2, 'value' => '', 'name' => 'CRLF')
        );

        return $values;
    }

    /* Ecriture du fichier texte */
    
    private function writeTxt($file, $tableau)
    {
        /* Ecriture du fichier texte */
        $errors = true;
        if ($file = fopen(_PS_MODULE_DIR_ . $this->name . $this->dossierExport . $file, "w+")) {
            foreach ($tableau as $row) {
                //envoi commande
                $ligne = '';
                foreach ($row['order'] as $value) {
                    $ligne.=str_pad(
                        (isset($value['value']) ? $value['value'] : ''),
                        (int) $value['long'],
                        ' ',
                        STR_PAD_RIGHT
                    );
                }
                fputs($file, $ligne . "\r\n");
                
                //envoi CN23
                if (isset($row['CN23'])) {
                    foreach ($row['CN23'] as $line) {
                        $ligne = '';
                        foreach ($line as $value) {
                            $ligne.=str_pad((isset($value['value']) ? $value['value'] : ''), (int) $value['long'], ' ', STR_PAD_RIGHT);
                        }
                        fputs($file, $ligne . "\r\n");
                    }
                }
            }
            fclose($file);
        } else {
            $errors = false;
        }
        return $errors;
    }

    /*
     * So Colissimo method : get So colissimo infos from database
     */

    public function getDeliveryInfos($idCart, $idCustomer)
    {
        $result = Db::getInstance()->getRow(
            'SELECT * '
            .'FROM '._DB_PREFIX_.'socolissimo_delivery_info '
            .'WHERE id_cart = '.(int)($idCart).
            ' AND id_customer = '.(int)($idCustomer)
        );
        return $result;
    }

    /* Retourne une chaine au format voulu par expeditor */

    private function cleanString($string)
    {
        $string = utf8_decode($string);

        $string = Tools::safeOutput($string, false);
        $string = html_entity_decode($string, ENT_QUOTES);

        setlocale(LC_ALL, 'en_CA.utf8');
        $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);

        $string = Tools::strtoupper($string);

        //$string = Tools::safeOutput($string);
        $string = str_replace("\r\n", " ", $string); // get rid of illegal SGML chars
        $string = str_replace("&nbsp;", " ", $string);
        $string = str_replace("&#39;", "' ", $string);
        $string = str_replace("&#150;", "-", $string);
        $string = str_replace(chr(9), " ", $string);
        $string = str_replace(chr(10), " ", $string);
        $string = str_replace(chr(13), " ", $string);


        $string = str_replace("|", " ", $string); // get rid of illegal SGML chars
        $string = strip_tags($string); // get rid of illegal SGML chars

        return $string;
    }

    private function importOrders($nameCsv)
    {
        $fichier = fopen(dirname(__FILE__) . '/import/' . $nameCsv . '.txt', 'rb');
        $output = '';
        $i = 0;

        for ($ligne = fgetcsv($fichier, 1024); !feof($fichier); $ligne = fgetcsv($fichier, 1024)) {
            $i++;
            $PrixTransport = '';
            /* recuperation des noms des cases */
            if ($i == 1) {
                $cases = explode(';', $ligne[0]);
                foreach ($cases as $key => $case) {
                    if (
                            isset($case)
                            && ($case == 'NumeroColis'
                            || $case == '"NumeroColis"'
                            || $case == 'ReferenceExpedition'
                            || $case == '"ReferenceExpedition"'
                            || $case == 'PrixTransport'
                            || $case == '"PrixTransport"')
                        ) {
                        if ($case == 'NumeroColis' || $case == '"NumeroColis"') {
                            $numeroColis = $key;
                        }
                        if ($case == 'ReferenceExpedition' || $case == '"ReferenceExpedition"') {
                            $referenceColis = $key;
                        }
                        if ($case == 'PrixTransport' || $case == '"PrixTransport"') {
                            $PrixTransport = $key;
                        }
                    }
                }
                continue;
            }

            /* recuperation des valeurs du csv */
            $output .= $this->procLigneImport($ligne, $numeroColis, $referenceColis, $PrixTransport);
        }
        return $output;
    }

    private function procLigneImport($ligne, $numeroColis, $referenceColis, $PrixTransport)
    {
        $cases = explode(';', $ligne[0]);

        $colis = (string)($cases[$numeroColis]);
        $reference = (string)($cases[$referenceColis]);
        if ($PrixTransport != '') {
            $prixtransport = (string)($cases[$PrixTransport]);
            $prixtransport = str_replace('"', '', $prixtransport);
            $prixtransport = str_replace('"', '', $prixtransport);
        }

        $reference = str_replace("COLS", "", $reference);
        $reference = str_replace("COL", "", $reference);
        $reference = str_replace('"', '', $reference);
        $colis = str_replace('"', '', $colis);


        $id_order_state = Configuration::get('EXPEDITOR_IMPORTSTATE');

        if (isset($reference) && $reference != '') {
            $order = new Order((int)($reference));
            $carrier = new Carrier((int) $order->id_carrier);
            if (isset($order->id) && $order->id != '') {
                if (true || $order->shipping_number == '') {
                    $customer = new Customer((int) $order->id_customer);

                    /* Ajout du numero de colis */
                    $order->shipping_number = $colis;
                    $order->update();
                    $cart = new Cart((int) $order->id_cart);

                    // Update order_carrier
                    $id_order_carrier = Db::getInstance()->getValue('
					SELECT `id_order_carrier`
					FROM `' . _DB_PREFIX_ . 'order_carrier`
					WHERE `id_order` = ' . (int) $order->id . '
					AND (`id_order_invoice` IS NULL or `id_order_invoice` = 0)');

                    if ($PrixTransport != '') {
                        Db::getInstance()->Execute(
                            'UPDATE `' . _DB_PREFIX_ . 'order_carrier` '
                            . 'SET tracking_number = "' . pSQL($colis) . '", '
                            . 'shipping_cost_tax_incl = "' . (float) $PrixTransport . '"
                            WHERE `id_order` = ' . (int) $order->id
                        );
                    } else {
                        Db::getInstance()->Execute(
                            'UPDATE `' . _DB_PREFIX_ . 'order_carrier` '
                            . 'SET tracking_number = "' . pSQL($colis) . '"
                            WHERE `id_order` = ' . (int) $order->id
                        );
                    }
                    if ($id_order_carrier) {

                        $order_carrier = new OrderCarrier($id_order_carrier);
                        $order_carrier->tracking_number = pSQL($colis);
                        $order_carrier->update();
                    } else {
                        //ajout Order Carrier
                        $order_carrier = new OrderCarrier();
                        $order_carrier->id_order = (int) $order->id;
                        $order_carrier->id_carrier = (int) $order->id_carrier;
                        $order_carrier->weight = (float) $cart->getTotalWeight();
                        if ($PrixTransport != '') {
                            $order_carrier->shipping_cost_tax_incl = (float) $PrixTransport;
                        }
                        $order_carrier->tracking_number = pSQL($colis);
                        $order_carrier->add();
                    }

                    $idTMP = (int) $order->id;
                    if ($id_order_state != 0) {
                        /* Changer le statut de la commande */
                        $orderHistory = new OrderHistory();
                        $orderHistory->id_order = $order->id;
                        $orderHistory->changeIdOrderState((int) $id_order_state, $idTMP);
                        $orderHistory->addWithemail();
                    }

                    $templateVars = array(
                        '{followup}' => str_replace('@', $order->shipping_number, $carrier->url),
                        '{firstname}' => $customer->firstname,
                        '{lastname}' => $customer->lastname,
                        '{id_order}' => $order->id,
                        '{order_name}' => $order->getUniqReference()
                    );

                    if (@Mail::Send(
                        (int) $order->id_lang,
                        'in_transit',
                        Mail::l(
                            'Package in transit',
                            (int) $order->id_lang
                        ),
                        $templateVars,
                        $customer->email,
                        $customer->firstname . ' ' . $customer->lastname,
                        null,
                        null,
                        null,
                        null,
                        _PS_MAIL_DIR_,
                        true,
                        (int) $order->id_shop
                    )
                    ) {
                        Hook::exec('actionAdminOrdersTrackingNumberUpdate', array('order' => $order));
                    }
                }
            } else {
                return '<div class="alert error">'.
                        $this->l('The order : ').((int) $reference).
                        $this->l(',does not exist. Please check your import file.').
                        '</div>';
            }
        }
    }

    /*
     * Upload le fichier CSV dans le dossier csv du module. 
     * Nomme le fichier avec la date précise
     */

    private function uploadCSV($nameCsv)
    {
        if (!move_uploaded_file($_FILES['CSV']['tmp_name'], dirname(__FILE__) . '/import/' . $nameCsv . '.txt')) {
            return false;
        }
        return true;
    }

    public function displayImportCsv()
    {
        $ouput = '
		<br />
		<form method="post" action="' . $_SERVER['REQUEST_URI'] . '" enctype="multipart/form-data">
			<fieldset>
				<legend><img src="' . $this->_path . 'logo.gif" alt="" title="" />' . $this->l('Import parcel numbers') . '</legend>
				<label for="CSV">' . $this->l('File .txt') . '</label> : <input type="file" id="CSV" name="CSV" />
				<div class="margin-form">
                                    <p class="clear">'.
                                        $this->l('The text file must contain only the parcel number and the shipping reference. (You can set export in the management part of Expeditor)')
                                    .'</p>
				</div>
				<center><input type="submit" class="button" name="submitCSV" value="' . $this->l('Import') . '" /></center>
			</fieldset>
			<br />
		</form>
		';
        return $ouput;
    }
}
