<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');

if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || Tools::strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest'){
  header('HTTP/1.0 403 Forbidden');
  echo 'You are forbidden!';
  die;
}

try {
  include_once(dirname(__FILE__).'/datamodel.php');
  include_once(dirname(__FILE__).'/import.php');
  require_once(dirname(__FILE__).'/simpleimportproduct.php');
  include_once(_PS_MODULE_DIR_ . 'simpleimportproduct/libraries/PHPExcel_1.7.9/Classes/PHPExcel.php');
  include_once(_PS_MODULE_DIR_ . 'simpleimportproduct/libraries/PHPExcel_1.7.9/Classes/PHPExcel/IOFactory.php');

  $simpleimportproduct = new Simpleimportproduct();
  $model = new importProductData();

  $json = array();
  $config  = array();

  if ( Tools::getValue('addCategories') == true){
    $json['page'] = Module::getInstanceByName('simpleimportproduct')->addCategories(Tools::getValue('key_settings'));
  }

  if ( Tools::getValue('addFeatures') == true){
    $json['page'] = Module::getInstanceByName('simpleimportproduct')->addFeatures(Tools::getValue('key_settings'));
  }

  if ( Tools::getValue('addCombinations') == true){
    $json['page'] = Module::getInstanceByName('simpleimportproduct')->addCombinations(Tools::getValue('key_settings'));
  }

  if ( Tools::getValue('moreCategory') == true){
    $json['page'] = Module::getInstanceByName('simpleimportproduct')->moreCategory(Tools::getValue('hidden_count_category'));
  }
  if ( Tools::getValue('moreSubcategory') == true){
    $json['page'] = Module::getInstanceByName('simpleimportproduct')->moreSubcategory(Tools::getValue('hidden_count_subcategory'));
  }
  if ( Tools::getValue('moreCombination') == true){
    $json['page'] = Module::getInstanceByName('simpleimportproduct')->moreCombination();
  }
//  if ( Tools::getValue('moreDiscount') == true){
//    $json['page'] = Module::getInstanceByName('simpleimportproduct')->moreDiscount();
//  }
  if ( Tools::getValue('moreFeatures') == true){
    $json['page'] = Module::getInstanceByName('simpleimportproduct')->moreFeatures();
  }

  if ( Tools::getValue('stepTwo') == true){
    if (isset($_FILES['file']) AND !empty($_FILES['file']['tmp_name']))
    {
      $file_name = $_FILES['file']['name'];
      $file_type = Tools::substr($file_name, strrpos($file_name, '.')+1);

      if(Tools::getValue('format_file') == 'xlsx' && ($file_type == 'xlsx' || $file_type == 'xls')){
        if (!Tools::copy($_FILES['file']['tmp_name'],  dirname(__FILE__).'/data/import_products.xls')){
          throw new Exception($simpleimportproduct->l('An error occurred while uploading, file must be XLSX format', 'send'));
        }
        $delimiter_val = false;
      }elseif(Tools::getValue('format_file') == 'csv' && $file_type == 'csv'){
        if (!Tools::copy($_FILES['file']['tmp_name'],   dirname(__FILE__).'/data/import_products.csv')){
          throw new Exception($simpleimportproduct->l('An error occurred while uploading, file must be CSV format ', 'send'));
        }
        $delimiter_val = Tools::getValue('delimiter_val');
      }
      else{
        throw new Exception($simpleimportproduct->l('An error occurred while uploading, file must be ', 'send').Tools::getValue('format_file').' '.$simpleimportproduct->l('format', 'send'));
      }

      $name_fields_upload = array(array('name' => 'no'));
      if($file_type == 'xlsx' || $file_type == 'xls'){
        $objPHPExcel = PHPExcel_IOFactory::load("data/import_products.xls");
      }
      elseif($file_type == 'csv'){
        $reader = PHPExcel_IOFactory::createReader("CSV");
        $reader->setDelimiter(Tools::getValue('delimiter_val'));
        $objPHPExcel = $reader->load("data/import_products.csv");
      }
      $count_prod = 0;
      foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
        $highestRow         = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        for ($row = 1; $row <= $highestRow; ++ $row) {
          for ($col = 0; $col < $highestColumnIndex; ++ $col) {
            $cell = $worksheet->getCellByColumnAndRow($col, $row);
            $val = $cell->getValue();
            if($row == 1){
              if( !$val ){
                continue;
              }
              $name_fields_upload[] = array('name' => $val);
            }
          }
          $count_prod = $count_prod + 1;
        }
      }

      if(Tools::getValue('import_type_val') == 'Add'){
        $parser_import_val = false;
      }
      else{
        $parser_import_val = Tools::getValue('parser_import_val');
      }
      $config = array(
        'format_file'          =>  (Tools::getValue('format_file')),
        'delimiter_val'        =>  ($delimiter_val),
        'import_type_val'      =>  (Tools::getValue('import_type_val')),
        'id_lang'              =>  (int)Tools::getValue('id_lang'),
        'parser_import_val'    =>  ($parser_import_val),
        'count_prod'           =>  (int)$count_prod,
        'name_fields_upload'   =>  ($name_fields_upload),
      );

      $config_save =serialize($config);
      Configuration::updateValue('GOMAKOIL_CONFIG_IMPORT_PRODUCTS', $config_save, false, Tools::getValue('id_shop_group'), Tools::getValue('id_shop'));
      $json['page'] = true;
    }
    else{
      throw new Exception($simpleimportproduct->l('Select file for import', 'send'));
    }
  }

  if ( Tools::getValue('save') == true){

    $config = array();
    $config = Tools::unserialize(Configuration::get('GOMAKOIL_IMPORT_PRODUCTS','',Tools::getValue('id_shop_group'), Tools::getValue('id_shop')));

    $base_field = Tools::getValue('field');
    $field_category = Tools::getValue('field_category');
    $field_combinations = Tools::getValue('field_combinations');
    $field_discount = Tools::getValue('field_discount');
    $field_featured = Tools::getValue('field_featured');

    $key_settings = Tools::getValue('key_settings');
    if((isset($key_settings) &&  $key_settings) ||  $key_settings == '0'){
      $key = $key_settings;
      $name = $config[$key]['name_save'];
     
      if($name !== Tools::getValue('name_save')){
        $config[] = array(
          'base_field'        =>  ($base_field),
          'field_category'    =>  ($field_category),
          'field_discount'    =>  ($field_discount),
          'field_combinations'=>  ($field_combinations),
          'field_featured'    =>  ($field_featured),
          'name_save'         =>  (Tools::getValue('name_save')),
        );
        $setting = count($config) - 1;
      }
      else{
        $config[$key] = array(
          'base_field'        =>  ($base_field),
          'field_category'    =>  ($field_category),
          'field_discount'    =>  ($field_discount),
          'field_combinations'=>  ($field_combinations),
          'field_featured'    =>  ($field_featured),
          'name_save'         =>  (Tools::getValue('name_save')),
        );
        $setting = $key;
      }
    }
    else{
      $config[] = array(
        'base_field'        =>  ($base_field),
        'field_category'    =>  ($field_category),
        'field_discount'    =>  ($field_discount),
        'field_combinations'=>  ($field_combinations),
        'field_featured'    =>  ($field_featured),
        'name_save'         =>  (Tools::getValue('name_save')),
      );
      $setting = count($config) - 1;
    }
    
    $config_save =serialize($config);
    Configuration::updateValue('GOMAKOIL_IMPORT_PRODUCTS', $config_save, Tools::getValue('id_shop_group'), Tools::getValue('id_shop') );

    $json['count_settings'] = $setting;
    $json['success'] = $simpleimportproduct->l('Data successfully saved!', 'send');
  }

  if ( Tools::getValue('import') == true){
    $id_shop = Tools::getValue('id_shop');
    $base_field = Tools::getValue('field');
    $field_category = Tools::getValue('field_category');
    $field_combinations = Tools::getValue('field_combinations');
    $field_discount = Tools::getValue('field_discount');
    $field_featured = Tools::getValue('field_featured');
    $config = array(
      'base_field'        =>  ($base_field),
      'field_category'    =>  ($field_category),
      'field_discount'    =>  ($field_discount),
      'field_combinations'=>  ($field_combinations),
      'field_featured'    =>  ($field_featured),
    );
    $import = new importProducts($config ,$id_shop, Tools::getValue('id_shop_group'));

    $res = $import->import();
    $json['success'] = $res;
  }

  if ( Tools::getValue('remove') == true){
    $config = array();
    $config = Tools::unserialize(Configuration::get('GOMAKOIL_IMPORT_PRODUCTS','',Tools::getValue('id_shop_group'), Tools::getValue('id_shop')));
    $key = Tools::getValue('key');
    unset($config[$key]);

    $config = array_values($config);
    $config_save =serialize($config);
    Configuration::updateValue('GOMAKOIL_IMPORT_PRODUCTS', $config_save, Tools::getValue('id_shop_group'), Tools::getValue('id_shop') );
    $json['success'] = $simpleimportproduct->l('Data successfully saved!', 'send');
  }
  if ( Tools::getValue('returnCount') == true){
    $count = Configuration::get('GOMAKOIL_IMPORT_PRODUCTS_COUNT', '',Tools::getValue('id_shop_group'), Tools::getValue('id_shop'));
    $rez = $simpleimportproduct->l('Imported', 'send'). ' ' . $count . ' '  .$simpleimportproduct->l('products', 'send');
    $json['count'] = $rez;
  }

  echo Tools::jsonEncode($json);
}
catch( Exception $e ){
  $json['error'] = $e->getMessage();
  echo Tools::jsonEncode($json);
}