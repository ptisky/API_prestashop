{**
 * 2015-2016 Cubyn
 * NOTICE OF LICENSE
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 * DISCLAIMER
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * @author    Cubyn <modules@cubyn.com>
 * @copyright 2015-2016 Cubyn
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *}
 <div id="CubynRelaiCarrier">
    <input type="hidden" name="cubyn_point_relai" value="{if isset($relay_point)}{$relay_point|escape:'htmlall':'UTF-8'}{/if}" />
    <p class="cubynSelect"{if isset($relay_point)} style="display:none;"{/if}>
        <a class="iframeCubynRelai btn btn-default button" href="https://app.cubyn.com/tools/relay-iframe/index.html?address={$address->address1|escape:'htmlall':'UTF-8'} {$address->address2|escape:'htmlall':'UTF-8'} {$address->postcode|escape:'htmlall':'UTF-8'} {$address->city|escape:'htmlall':'UTF-8'} {$address->country|escape:'htmlall':'UTF-8'}&callback={$link->getModuleLink('cubyn', 'CubynCallback')|urlencode}">{l s='Choose your relay point' mod='cubyn'}</a>
    </p>
    <p class="cubynChange"{if !isset($relay_point)} style="display:none;"{/if}>
        <a class="iframeCubynRelai btn btn-default button" href="https://app.cubyn.com/tools/relay-iframe/index.html?address={$address->address1|escape:'htmlall':'UTF-8'} {$address->address2|escape:'htmlall':'UTF-8'} {$address->postcode|escape:'htmlall':'UTF-8'} {$address->city|escape:'htmlall':'UTF-8'} {$address->country|escape:'htmlall':'UTF-8'}&callback={$link->getModuleLink('cubyn', 'CubynCallback')|urlencode}">{l s='Change your relay point' mod='cubyn'}</a><span class="address">{if isset($relay_point_decode)}<strong>{$relay_point_decode->name}</strong>: {$relay_point_decode->street} {$relay_point_decode->zip} {$relay_point_decode->city}{/if}</span>
    </p>
</div>
{literal}
<script type="text/javascript">

    {/literal}
    var selectRelayPointError = "{l s='Please select cubyn relay point.' mod='cubyn'}";
    {literal}

    (function() {

        document.domain = document.domain;

        window.setRelay = function(info) {
            if (typeof setCubynRelayPoint == 'function') {
                setCubynRelayPoint(JSON.stringify(info, null, 2));
            }
            else {
                $('[name=cubyn_point_relai]').val(JSON.stringify(info, null, 2));
            }

            var name = $('<strong/>').html(info.name);
            $('.cubynChange > .address').html('').append(name).append(' — '+info.street+' '+info.zip+' '+info.city);


            $('.cubynSelect').hide();
            $('.cubynChange').show();
            $.fancybox.close();
        }

        $('document').ready(function() {
            $('.iframeCubynRelai').fancybox({
                type: 'iframe'
            });
            if ($('[name=cubyn_point_relai]').val() == '') {
                $('.iframeCubynRelai').click();
            }
        });
    })();
</script>
{/literal}