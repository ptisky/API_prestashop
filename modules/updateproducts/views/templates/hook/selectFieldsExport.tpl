<div class="content_fields">
    <div class="block_all_fields">
        <div class="url_base_setting"><a href="{$url_base|escape:'htmlall':'UTF-8'}"><i class="process-icon-new process-icon-new-setting"></i>{l s='Create new settings' mod='updateproducts'}</a></div>
        <div class="field_list_header">
            <label>{l s='General data' mod='updateproducts'}</label>
            <input class="search_base_fields" placeholder="{l s='Search' mod='updateproducts'}">
        </div>
        <ul class="block_base_fields">
            {foreach $exportBase AS $base}
                {if $set && !in_array($base['name'], $set)}
                    <li data-name="{$base['name']|escape:'htmlall':'UTF-8'}" data-value="{$base['val']|escape:'htmlall':'UTF-8'}"  {if isset($base['hint']) && $base['hint']}class="isset_hint" data-hint="{$base['hint']|escape:'htmlall':'UTF-8'}"{/if}>
                        {if isset($base['hint']) && $base['hint']}  <i class="icon-info icon-info-fields"></i>  {/if}
                        {$base['name']|escape:'htmlall':'UTF-8'}
                    </li>
                {/if}
                {if !$set}
                    <li data-name="{$base['name']|escape:'htmlall':'UTF-8'}" data-value="{$base['val']|escape:'htmlall':'UTF-8'}" {if isset($base['hint']) && $base['hint']}class="isset_hint"  data-hint="{$base['hint']|escape:'htmlall':'UTF-8'}"{/if}>
                        {if isset($base['hint']) && $base['hint']}  <i class="icon-info icon-info-fields"></i>  {/if}
                        {$base['name']|escape:'htmlall':'UTF-8'}
                    </li>
                {/if}
            {/foreach}
        </ul>
        <div class="navigation-fields navigation-fields-base">
            <button type="button" class="btn btn-default add_base_filds_all add_fild_right">{l s='Add all ' mod='updateproducts'}<i class="icon-arrow-right"></i></button>
            <button type="button" class="btn btn-default add_base_filds add_fild_right">{l s='Add ' mod='updateproducts'}<i class="icon-arrow-right"></i></button>
            <button type="button" class="btn btn-default remove_base_filds add_fild_right">{l s='Remove ' mod='updateproducts'}<i class="icon-arrow-left"></i></button>
            <button type="button" class="btn btn-default remove_base_filds_all add_fild_right">{l s='Remove all ' mod='updateproducts'}<i class="icon-arrow-left"></i></button>
        </div> <div style="clear: both"></div>
        <div class="field_list_header">
            <label>{l s='Combinations data' mod='updateproducts'}</label>
            <input class="search_combinations_fields" placeholder="{l s='Search' mod='updateproducts'}">
        </div>
        <ul class="block_combinations_fields">
            {foreach $exportCombinations AS $combinations}
                {if $set && !in_array($combinations['name'], $set)}
                    <li data-name="{$combinations['name']|escape:'htmlall':'UTF-8'}" data-value="{$combinations['val']|escape:'htmlall':'UTF-8'}" {if isset($combinations['hint']) && $combinations['hint']}class="isset_hint" data-hint="{$combinations['hint']|escape:'htmlall':'UTF-8'}"{/if}>
                        {if isset($combinations['hint']) && $combinations['hint']}  <i class="icon-info icon-info-fields"></i>  {/if}
                        {$combinations['name']|escape:'htmlall':'UTF-8'}
                    </li>
                {/if}
                {if !$set}
                    <li data-name="{$combinations['name']|escape:'htmlall':'UTF-8'}" data-value="{$combinations['val']|escape:'htmlall':'UTF-8'}" {if isset($combinations['hint']) && $combinations['hint']}class="isset_hint" data-hint="{$combinations['hint']|escape:'htmlall':'UTF-8'}"{/if}>
                        {if isset($combinations['hint']) && $combinations['hint']}  <i class="icon-info icon-info-fields"></i>  {/if}
                        {$combinations['name']|escape:'htmlall':'UTF-8'}
                    </li>
                {/if}
            {/foreach}
        </ul>
        <div class="navigation-fields navigation-fields-combinations">
            <button type="button" class="btn btn-default add_combinations_filds_all add_fild_right">{l s='Add all ' mod='updateproducts'}<i class="icon-arrow-right"></i></button>
            <button type="button" class="btn btn-default add_combinations_filds add_fild_right">{l s='Add ' mod='updateproducts'}<i class="icon-arrow-right"></i></button>
            <button type="button" class="btn btn-default remove_combinations_filds add_fild_right">{l s='Remove ' mod='updateproducts'}<i class="icon-arrow-left"></i></button>
            <button type="button" class="btn btn-default remove_combinations_filds_all add_fild_right">{l s='Remove all ' mod='updateproducts'}<i class="icon-arrow-left"></i></button>
        </div> <div style="clear: both"></div>
        <div class="field_list_header">
            <label>{l s='Specific price data' mod='updateproducts'}</label>
            <input class="search_specificPrice_fields" placeholder="{l s='Search' mod='updateproducts'}">
        </div>
        <ul class="block_specificPrice_fields">
            {foreach $exportSpecificPrice AS $specificPrice}
                {if $set && !in_array($specificPrice['name'], $set)}
                    <li data-name="{$specificPrice['name']|escape:'htmlall':'UTF-8'}" data-value="{$specificPrice['val']|escape:'htmlall':'UTF-8'}" {if isset($specificPrice['hint']) && $specificPrice['hint']}class="isset_hint" data-hint="{$specificPrice['hint']|escape:'htmlall':'UTF-8'}"{/if}>
                        {if isset($specificPrice['hint']) && $specificPrice['hint']}  <i class="icon-info icon-info-fields"></i>  {/if}
                        {$specificPrice['name']|escape:'htmlall':'UTF-8'}
                    </li>
                {/if}
                {if !$set}
                    <li data-name="{$specificPrice['name']|escape:'htmlall':'UTF-8'}" data-value="{$specificPrice['val']|escape:'htmlall':'UTF-8'}" {if isset($specificPrice['hint']) && $specificPrice['hint']}class="isset_hint" data-hint="{$specificPrice['hint']|escape:'htmlall':'UTF-8'}"{/if}>
                        {if isset($specificPrice['hint']) && $specificPrice['hint']}  <i class="icon-info icon-info-fields"></i>  {/if}
                        {$specificPrice['name']|escape:'htmlall':'UTF-8'}
                    </li>
                {/if}
            {/foreach}
        </ul>
        <div class="navigation-fields navigation-fields-specificPrice">
            <button type="button" class="btn btn-default add_specificPrice_filds_all add_fild_right">{l s='Add all ' mod='updateproducts'}<i class="icon-arrow-right"></i></button>
            <button type="button" class="btn btn-default add_specificPrice_filds add_fild_right">{l s='Add ' mod='updateproducts'}<i class="icon-arrow-right"></i></button>
            <button type="button" class="btn btn-default remove_specificPrice_filds add_fild_right">{l s='Remove ' mod='updateproducts'}<i class="icon-arrow-left"></i></button>
            <button type="button" class="btn btn-default remove_specificPrice_filds_all add_fild_right">{l s='Remove all ' mod='updateproducts'}<i class="icon-arrow-left"></i></button>
        </div> <div style="clear: both"></div>
    </div>
    <div class="block_selected_fields">
        <div class="field_list_header">
            <label>{l s='Selected fields' mod='updateproducts'}</label>
            <input class="search_selected_fields" placeholder="{l s='Search' mod='updateproducts'}">
        </div>
        <ul class="selected_fields">
            {foreach from=$selected key=key item=select}
                <li data-name="{$select['name']|escape:'htmlall':'UTF-8'}" data-value="{$key|escape:'htmlall':'UTF-8'}" class="{if $key == 'id_product' || $key == 'id_product_attribute' || $key == 'id_specific_price'} disable_fields {/if}{if isset($select['hint']) && $select['hint']} isset_hint {/if}"  {if isset($select['hint']) && $select['hint']}data-hint="{$select['hint']|escape:'htmlall':'UTF-8'}"{/if}>
                    {if isset($select['hint']) && $select['hint']}  <i class="icon-info icon-info-fields"></i>  {/if}
                    {$select['name']|escape:'htmlall':'UTF-8'}
                    <i class="icon-arrows icon-arrows-select-fields"></i>
                </li>
            {/foreach}
        </ul>

    </div>
</div>