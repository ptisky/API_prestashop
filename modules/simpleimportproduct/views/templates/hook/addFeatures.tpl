{foreach from=$save item=fieldset  key=n }

  {if $n !== 0}
    <div class="panel" id="fieldset_7_7" style="display: block;">
    <div class="form-wrapper">

    {foreach from=$has_hint_featured item=featured  key=k }
      <div class="form-group">
        <label class="control-label col-lg-3">
          {$featured.name|escape:'htmlall':'UTF-8'}
        </label>
        <div class="col-lg-9 ">
          <select name="{$k|escape:'htmlall':'UTF-8'}" class="chosen fixed-width-xl" id="{$k|escape:'htmlall':'UTF-8'}"  style="display: none; width:350px;">
            {foreach from=$fields key=key item=field}
              <option {if $fieldset[$k] == $field['name']}selected="selected"{/if} value="{$field['name']|escape:'htmlall':'UTF-8'}">{$field['name']|escape:'htmlall':'UTF-8'}</option>
            {/foreach}
          </select>
        </div>
      </div>
    {/foreach}


    <div class="form-group">
      <div class="col-lg-9 col-lg-offset-3">
        <button type="button" class="btn btn-default more_featured">{l s='add features' mod='simpleimportproduct'}</button>&nbsp;&nbsp;<button type="button" class="btn btn-default delete_featured">{l s='delete' mod='simpleimportproduct'}</button>
      </div>
    </div>
    </div><!-- /.form-wrapper -->
    </div>
  {/if}
{/foreach}
<script type="text/javascript">
  $('.chosen').chosen()
</script>