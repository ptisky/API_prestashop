<?php 
if (!defined('_PS_VERSION_'))
	exit;

class PFUpSellUpsellModuleFrontController extends ModuleFrontController{
	
	public function __construct()
	{
		parent::__construct();
		$this->display_column_left = (bool)(Configuration::get('PF_UPSELL_LEFT_COL'));
		$this->display_column_right = (bool)(Configuration::get('PF_UPSELL_RIGHT_COL'));
	}
	
	public function getVersion()
	{
		return floatval(Tools::substr(_PS_VERSION_, 0, 3));
	}
	
	public function setMedia()
	{
		parent::setMedia();
		$psv = $this->getVersion();
		if($psv >= 1.6)
			$this->addCSS(_MODULE_DIR_.'/pfupsell/views/css/upsell.css', 'all');
		else
			$this->addCSS(_MODULE_DIR_.'/pfupsell/views/css/upsell1.5.css', 'all');
	}
	
	
	public function getUpsellProduct()
	{	
		$psv = $this->getVersion();
		$upsell_pr_id = unserialize(Configuration::get('PF_UPSELL_PRODUCTS'));
		$link = new Link();	
		$products = array();
		$i = 0;
		$id_language = Configuration::get('PS_LANG_DEFAULT');
	
		if (!empty($upsell_pr_id))
		{
			foreach ($upsell_pr_id as $res_id)
			{
				$i++;
				$products[$i] = new Product($res_id, true, $id_language, Shop::getContextShopID());
				$products[$i]->id_product = $res_id;
				$products[$i]->allow_oosp =  $products[$i]->isAvailableWhenOutOfStock(StockAvailable::outOfStock($res_id));
				$products[$i]->quantity =  $products[$i]->getQuantity($res_id, $products[$i]->getDefaultAttribute($res_id));
				$products[$i]->quantity_all_versions =  $products[$i]->getQuantity($res_id);
				$products[$i]->price_tax_exc =  $products[$i]->getPrice(false);
				$products[$i]->price_without_reduction =  $products[$i]->getPriceWithoutReduct();
				$products[$i]->price = $products[$i]->getPriceStatic(trim($res_id), true, null, 2);
				if ($psv >= 1.6)
				{ 
					$products_id = array();
					$products_id[] = $res_id;
					$product_color_type =  $products[$i]->getAttributesColorList($products_id, true);
					if (!empty($product_color_type))
					{
						foreach($product_color_type as $color)
						{																
							$products[$i]->color_list = '<ul class="color_to_pick_list clearfix">';
							foreach($color as $color_type)
							{
								$products[$i]->color_list .= '
									<li style="margin-right: 5px;">
										<a href="'.$link->getProductLink($color_type['id_product'], null, null, null, null, null, $color_type['id_product_attribute']).'" id="color_'.$color_type['id_product_attribute'].'" style="background:'.$color_type['color'].'" class="color_pick"></a>
									</li>';
							}
							$products[$i]->color_list .= '</ul>';							
						}
					}
				}
				
				$def_attr = $products[$i]->getDefaultAttribute($res_id); 
				if (!empty($def_attr))
					$products[$i]->id_product_attribute = $def_attr;
				else
					$products[$i]->id_product_attribute = 0;
			
				$cover_image = $products[$i]->getCover($res_id);
				$legend = $products[$i]->getImages($id_language);
				foreach($legend as $leg)
				{
					if ($leg['cover'] == 1)
						$products[$i]->legend = $leg['legend'];						
				}
				
				$avalibale_image = Image::getImages($id_language, $res_id);
				if (isset($avalibale_image) && !empty($avalibale_image))
					$products[$i]->img_link = $link->getImageLink($products[$i]->link_rewrite, $cover_image['id_image'], 'home_default');
				else
					$products[$i]->img_link = $link->getImageLink($products[$i]->link_rewrite, $products[$i]->defineProductImage($products[$i]->getImages($id_language), $id_language), 'home_default');
			}
		}
		return $products;
	}

	public function initContent()
	{
		$psv = $this->getVersion();
		parent::initContent();
		if (Configuration::get('PF_UPSELL_ON'))
		{
			$this->context->smarty->assign(array(
				'products' => $this->getUpsellProduct(),
				'protocol' => Tools::getProtocol(),
				'static_token' => Tools::getToken(false)
			));	
			if ($psv < 1.6)
				$this->setTemplate('upsell.tpl');
			else
				$this->setTemplate('upsell1.6.tpl');
		}
	}	  
}
?>