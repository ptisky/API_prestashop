<p class="payment_module" style="text-align:center">
	<a href="javascript:$('#paybox_form').submit();" title="{l s='Pay by credit card' mod='paybox'}">
		{l s='Pay by credit card' mod='paybox'}
		<br /><br />
		<img src="{$module_template_dir}paiement-securise.png" alt="{l s='Pay by credit card' mod='paybox'}" />
	</a>
</p>

<form action="{$base_dir_ssl}modules/paybox/redirect.php" method="post" id="paybox_form" class="hidden">
	<input type="hidden" name="paybox" value="1"> 
</form>


{if $paybox3x eq "true"}
<p class="payment_module" style="text-align:center">
	<a href="javascript:$('#paybox_form3x').submit();" title="{l s='Pay by credit card' mod='paybox'}">
		{l s='Pay by credit card in multiple times (3x)' mod='paybox'}
		<br /><br />
		<img src="{$module_template_dir}paiement-securise.png" alt="{l s='Pay by credit card' mod='paybox'}" />
	</a>
</p>
<form action="{$base_dir}modules/paybox/redirect.php" method="post" id="paybox_form3x" class="hidden">
	<input type="hidden" name="paybox3x" value="1"> 
</form>
{/if}