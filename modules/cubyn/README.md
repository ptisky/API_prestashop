# Module Prestashop pour Cubyn
##Installation
###Version 1.5.X > 1.6.1.1

L'installation de ce module nécéssite une version fonctionnel de prestashop avec une version minimun de 1.5
si vous n'avez pas de boutique installez je vous invite a suivre la documentation officiel de prestashop pour installer une boutique sur votre serveur

le module fonctionne également sur les version cloud de prestashop



Pour installer cd module recuperé l'intégraliter de ce repository et zipper le dossier.
- Copier ce cette archive vers le dossier module de votre installation 
	<Your_prestashop_root_folder>/modules/cubyn.zip
- dezipper l'archive 
- de maniére optionnel vous pouvez supprimmer l'archive mais cela reste une solution de sauvegarde en cas de mauvaise manipulation
		_en effet la supression du module est trés simple depuis l'interface 

#### Vérification de l'installation 

Si l'installation c'est bien passer rendez vous sur votre interface de gestion de votre boutique
eg: http://www.myShop.com/admin12RTE

Entrez votre login mot de passe 

Version 1.6 +

![Image of modules pages]
(http://ibin.co/2IGUHj5CkGyX)

Version 1.5+
![Image of modules pages]
(http://ibin.co/2IGW3JZWpaQs)


### Utilisation
#### version 1.5 et 1.6

##### Configuration
en dehors de différence graphique majeur le module suis une configuration identique 
###### Clé d'api
le champ clé d'api doit être utilisé pour sauvegardé la clé d'API fournit par cubyn 
###### Gestion des états de commande 
les états de commande sont utiliser dans le cas du webHook ainsi que dans la selection des commandes qui seront exportable 

##### Utilisation
Une fois configurer le module fonctionne de maniére automatique, toute commandes qui est saisie depuis le front office ou le backOffice, ayant comme transporteur l'un des transporteur cubyn créer pendant l'installation du modules sera sauvegardés en base de données 

Une fois sauvegardé, toute commande dont le statut est equivalent a celui définit pendant la configuration du module sera exportable 

les commandes sont exportable à n'importe quel moments 

##### Exportations des commandes 
	Pour exporté une commande rendez-vous sur le menu Cubyn
	*Version 1.5 +*
		le menu cubyn est situé dans le sous menu transport 
	*Version 1.6 +*
	  le menu cubyn est situé dans le menu principal en bas de l'écran 


# Utilisation des WebHooks


curl -X POST -H "X-Application: z78uibb6f2bde874f82106cc" -H "Authorization: Basic bnVsbDpudWxs" -H "Content-Type: application/json" -H "Cache-Control: no-cache" -H "Postman-Token: 52f751f8-1f63-a517-4284-da235f56b6f7" -d '{
    "updated":[
    {"order":"AZSXERT","trackingId":"15454545","state":"picked"},
    {"order":"AZSXERT","trackingId":"15454545","state":"picked"},
    {"order":"AZSXERT","trackingId":"15454545","state":"picked"},
    {"order":"AZSXERT","trackingId":"15454545","state":"picked"}
    ]
}
' 'http://local.cubyncarrier.dev/modules/cubyn/ajax.php'

## reponse

### success or mixed
{
  "message": "[< nb_updated_order>] order(s) update",
  "orderCount":[< nb_updated_order>],
  "type": "sucess||mixed",
  "errorList": [
    {
      "ref": "<errored_order>",
      "message": "<errored_order>is not a valid ref_order"
    },
  ]
}


