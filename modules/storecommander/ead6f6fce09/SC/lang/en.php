<?php
/**
 * Store Commander
 *
 * @category administration
 * @author Store Commander - support@storecommander.com
 * @version 2015-09-15
 * @uses Prestashop modules
 * @since 2009
 * @copyright Copyright &copy; 2009-2015, Store Commander
 * @license commercial
 * All rights reserved! Copying, duplication strictly prohibited
 *
 * *****************************************
 * *           STORE COMMANDER             *
 * *   http://www.StoreCommander.com       *
 * *            V 2015-09-15               *
 * *****************************************
 *
 * Compatibility: PS version: 1.1 to 1.6.1
 *
 **/

$LANG=array(

'You must be logged to use Store Commander.'=>'You must be logged to use Store Commander.',
':'=>':',
'of'=>'of',
'Home'=>'Home',
'You have to register your license key in the [Help > Register your license] menu to update Store Commander.'=>'You have to register your license key in the [Help > Register your license] menu to update Store Commander.',
'Server IP address or application path has changed, you need to reset your license'=>'Server IP address or application path has changed, you need to reset your license',
'Welcome to Store Commander!'=>'Welcome to Store Commander!',
'Your shop URL'=>'Your shop URL',
'Company'=>'Company',
'Name'=>'Name',
'Firstname'=>'Firstname',
'Email'=>'Email',
'Number of products in your e-shop'=>'Number of products in your e-shop',
'Subscribe to our newsletter'=>'Subscribe to our newsletter',
'Receive special offers from our partners'=>'Receive special offers from our partners',
'M.'=>'M.',
'Ms'=>'Ms',
'Error with archive (filesize = 0 Ko)'=>'Error with archive (filesize = 0 Ko)',
'Visit storecommander.com and purchase your license'=>'Visit storecommander.com and purchase your license',
'Register your license'=>'Register your license',
'Start now!'=>'Start now!',
'You can use the 14-day free trial on your own shop with your own products. However, this free trial is limited to the first 1/4 of the products of your shop being displayed.'=>'You can use the 14-day free trial on your own shop with your own products. However, this free trial is limited to the first 1/4 of the products of your shop being displayed.',
'Start your trial'=>'Start your trial',
'Purchase your license'=>'Purchase your license',
'More info'=>'More info',
'You must validate your license on your online shop. The trial version cannot be launched on local websites.'=>'You must validate your license on your online shop. The trial version cannot be launched on local websites.',
'Categories and products'=>'Categories and products',
'Products list'=>'Products list',
'Configuration'=>'Configuration',
'Links'=>'Links',
'Create link in the PrestaShop Quick access menu'=>'Create link in the PrestaShop Quick access menu',
'Create files example for CSV import'=>'Create files example for CSV import',
'Your Shop'=>'Your Shop',
'PrestaShop BackOffice'=>'PrestaShop BackOffice',
'Visit PrestaShop.com'=>'Visit PrestaShop.com',
'Visit StoreCommander.com'=>'Visit StoreCommander.com',
'Help'=>'Help',
'Update Store Commander'=>'Update Store Commander',
'Buy or extend your license'=>'Buy or extend your license',
'Your version is up to date!'=>'Your version is up to date!',
'NEW VERSION AVAILABLE!'=>'NEW VERSION AVAILABLE!',
'Wrong license key'=>'Wrong license key',
'Installation'=>'Installation',
'Catalog language'=>'Catalog language',
'Tools'=>'Tools',
'Clear grid preferences for products'=>'Clear grid preferences for products',
'Clear all grids preferences for products'=>'Clear all grids preferences for products',
'Clear grid preferences for combinations'=>'Clear grid preferences for combinations',
'Send a comment, a bug, a request'=>'Send a comment, a bug, a request',
'Change history'=>'Change history',
'Reset prices drop dates'=>'Reset prices drop dates',
'Reset prices drop reductions'=>'Reset prices drop reductions',
'Discounts'=>'Discounts',
'Are you sure you want to reset prices drop dates?'=>'Are you sure you want to reset prices drop dates?',
'Are you sure you want to reset prices drop reductions?'=>'Are you sure you want to reset prices drop reductions?',
'Are you sure you want to reset all prices drop?'=>'Are you sure you want to reset all prices drop?',
'Delete all prices drop'=>'Delete all prices drop',
'Update history'=>'Update history',
'Documentation'=>'Documentation',
'Attributes and groups'=>'Attributes and groups',
'You need to refresh the whole page (F5 or Apple+R) to reset the application.'=>'You need to refresh the whole page (F5 or Apple+R) to reset the application.',
'Check and fix language fields of products for all languages'=>'Check and fix language fields of products for all languages',
'Extend your support and automatic updates'=>'Extend your support and automatic updates',
'LICENSE SOON LIMITED!'=>'LICENSE SOON LIMITED!',
'LICENSE LIMITED!'=>'LICENSE LIMITED!',
'This license is active.'=>'This license is active.',
'Error: This license has been suspended.'=>'Error: This license has been suspended.',
'Error: This license has expired.'=>'Error: This license has expired.',
'Error: This license has now expired.'=>'Error: This license has now expired.',
'Error: This license is pending review.'=>'Error: This license is pending review.',
'Error: The license key did not match any in the database.'=>'Error: The license key did not match any in the database.',
'Error: The local license key is invalid for this location.'=>'Error: The local license key is invalid for this location.',
'Server'=>'Server',
'Laboratory'=>'Laboratory',
'Empty Smarty cache'=>'Empty Smarty cache',
'The shortcut has been created. The installation is finished you can now use Store Commander!'=>'The shortcut has been created. The installation is finished you can now use Store Commander!',
'The CSV files have been installed. You can use them in the Import CSV tool.'=>'The CSV files have been installed. You can use them in the Import CSV tool.',
'The CSV files have not been installed. Check write permissions on '=>'The CSV files have not been installed. Check write permissions on ',
'This folder cannot be created by Store Commander, you need to create it by FTP with writing permission.'=>'This folder cannot be created by Store Commander, you need to create it by FTP with writing permission.',
'License key:'=>'License key:',
'License key saved! Thank you!'=>'License key saved! Thank you!',
'The application will be reloaded.'=>'The application will be reloaded.',
'Enter your license key'=>'Enter your license key',
'Wrong support date.'=>'Wrong support date.',
'Click here to refresh the application'=>'Click here to refresh the application',
'Store Commander update'=>'Store Commander update',
'Some files are not writable, please change the permission of these files:'=>'Some files are not writable, please change the permission of these files:',
'No update found.'=>'No update found.',
'Updating...'=>'Updating...',
'Downloading pack'=>'Downloading pack',
'Opening zip archive...'=>'Opening zip archive...',
'Extracting zip archive...'=>'Extracting zip archive...',
'End of extraction'=>'End of extraction',
'Copying files...'=>'Copying files...',
'The installation of ionCube is required to use the automatic updater.'=>'The installation of ionCube is required to use the automatic updater.',
'Click here to check your installation.'=>'Click here to check your installation.',
'Your support period has expired, please renew your support package'=>'Your support period has expired, please renew your support package',
'Update finished!'=>'Update finished!',
'See on shop'=>'See on shop',
'Edit in PrestaShop BackOffice'=>'Edit in PrestaShop BackOffice',
'Set selected fields to'=>'Set selected fields to',
'Expand'=>'Expand',
'Collapse'=>'Collapse',
'Copy'=>'Copy',
'Paste'=>'Paste',
'Permanently delete the selected products everywhere in the shop.'=>'Permanently delete the selected products everywhere in the shop.',
'Mass update'=>'Mass update',
'Sell price exc. tax'=>'Sell price exc. tax',
'Sell price inc. tax'=>'Sell price inc. tax',
'Modify sell price exc. tax, possible values: -10.50%, +5.0, -5.25,...'=>'Modify sell price exc. tax, possible values: -10.50%, +5.0, -5.25,...',
'Modify sell price inc. tax, possible values: -10.50%, +5.0, -5.25,...'=>'Modify sell price inc. tax, possible values: -10.50%, +5.0, -5.25,...',
'Modify quantity, possible values: -10%, +5, -5, 5.25,...'=>'Modify quantity, possible values: -10%, +5, -5, 5.25,...',
'Apply margin (use the math formula choosen in Settings for modify sale price):'=>'Apply margin (use the math formula choosen in Settings for modify sale price):',
'Set the cheapest combination as default combination'=>'Set the cheapest combination as default combination',
'Enable / Disable'=>'Enable / Disable',
'Margin'=>'Margin',
'Catalog'=>'Catalog',
'Category'=>'Category',
'Categories'=>'Categories',
'Stock available'=>'Stock available',
'Wholesale price'=>'Wholesale price',
'PriceLTaxes'=>'PriceLTaxes',
'Price+Taxes'=>'Price+Taxes',
'Weight'=>'Weight',
'Width'=>'Width',
'Height'=>'Height',
'Depth'=>'Depth',
'Add. shipping cost'=>'Add. shipping cost',
'Tax rule'=>'Tax rule',
'Show price'=>'Show price',
'New'=>'New',
'Refurbished'=>'Refurbished',
'Online only'=>'Online only',
'Condition'=>'Condition',
'Available for order'=>'Available for order',
'Minimal quantity'=>'Minimal quantity',
'Supplier Ref.'=>'Supplier Ref.',
'Manufacturer'=>'Manufacturer',
'Supplier'=>'Supplier',
'Tax'=>'Tax',
'Location'=>'Location',
'Reduction price'=>'Reduction price',
'Reduction percent'=>'Reduction percent',
'Reduction from'=>'Reduction from',
'Reduction to'=>'Reduction to',
'Price with reduction'=>'Price with reduction',
'Price with reduction percent'=>'Price with reduction percent',
'On sale'=>'On sale',
'Active'=>'Active',
'Products'=>'Products',
'Product'=>'Product',
'Product:'=>'Product:',
'Category:'=>'Category:',
'Refresh grid'=>'Refresh grid',
'Refresh tree'=>'Refresh tree',
'Stock available +/-'=>'Stock available +/-',
'Light navigation (simple click on grid)'=>'Light navigation (simple click on grid)',
'Expand all items'=>'Expand all items',
'Collapse all items'=>'Collapse all items',
'Yes'=>'Yes',
'No'=>'No',
'Msg available now'=>'Msg available now',
'Msg available later'=>'Msg available later',
'If enabled: display products from all subcategories'=>'If enabled: display products from all subcategories',
'If enabled: display products only from their default category'=>'If enabled: display products only from their default category',
'products'=>'products',
'product'=>'product',
'in this category and all subcategories'=>'in this category and all subcategories',
'in this category'=>'in this category',
'Loading in progress, please wait...'=>'Loading in progress, please wait...',
'Create new category with the PrestaShop form'=>'Create new category with the PrestaShop form',
'Create new category'=>'Create new category',
'Create new product'=>'Create new product',
'Select all products'=>'Select all products',
'If enabled: link products in the target category when you drag and drop products. Not enabled: move products'=>'If enabled: link products in the target category when you drag and drop products. Not enabled: move products',
'Select catalog language'=>'Select catalog language',
'Language'=>'Language',
'Alert: wholesale price higher than sell price!'=>'Alert: wholesale price higher than sell price!',
'Filter options'=>'Filter options',
'Reset filters'=>'Reset filters',
'To Translate:'=>'To Translate:',
'Create new product with the PrestaShop form'=>'Create new product with the PrestaShop form',
'Save product positions in the grid as category positions'=>'Save product positions in the grid as category positions',
'You need to select a parent category before creating a category'=>'You need to select a parent category before creating a category',
'If out of stock'=>'If out of stock',
'Deny orders'=>'Deny orders',
'Allow orders'=>'Allow orders',
'Default(Pref)'=>'Default(Pref)',
'Show all columns'=>'Show all columns',
'Hide all columns'=>'Hide all columns',
'SC Recycle Bin'=>'SC Recycle Bin',
'Empty bin'=>'Empty bin',
'Are you sure to delete all categories and products placed in the recycled bin?'=>'Are you sure to delete all categories and products placed in the recycled bin?',
'Light view'=>'Light view',
'Large view'=>'Large view',
'Delivery'=>'Delivery',
'Prices'=>'Prices',
'SEO'=>'SEO',
'References'=>'References',
'Combination prices'=>'Combination prices',
'Date add'=>'Date add',
'Date update'=>'Date update',
'Public price'=>'Public price',
'public price'=>'public price',
'Grid view settings'=>'Grid view settings',
'You must to select a product.'=>'You must to select a product.',
'You must to select a customizable product.'=>'You must to select a customizable product.',
'Create a category:'=>'Create a category:',
'Default color group'=>'Default color group',
'Do not display'=>'Do not display',
'Discount price'=>'Discount price',
'Margin/Coef'=>'Margin/Coef',
'This will permanently delete the selected products everywhere in the shop.'=>'This will permanently delete the selected products everywhere in the shop.',
'Properties'=>'Properties',
'Combinations'=>'Combinations',
'Descriptions'=>'Descriptions',
'combinations'=>'combinations',
'combination'=>'combination',
'Select properties panel'=>'Select properties panel',
'Save'=>'Save',
'ID'=>'ID',
'Reference'=>'Reference',
'Supplier reference'=>'Supplier reference',
'Qty'=>'Qty',
'Qty +/-'=>'Qty +/-',
'Prod. price'=>'Prod. price',
'Attr. price'=>'Attr. price',
'Prod. price ex. tax'=>'Prod. price ex. tax',
'Attr. price ex. tax'=>'Attr. price ex. tax',
'Prod. weight'=>'Prod. weight',
'Att. weight'=>'Att. weight',
'Please select a category'=>'Please select a category',
'Please select a product'=>'Please select a product',
'Please select a combination'=>'Please select a combination',
'Delete category relation'=>'Delete category relation',
'Open and select category'=>'Open and select category',
'Save descriptions'=>'Save descriptions',
'Do you want to save the descriptions?'=>'Do you want to save the descriptions?',
'Delete combination'=>'Delete combination',
'Groups'=>'Groups',
'groups'=>'groups',
'group'=>'group',
'Attributes'=>'Attributes',
'attributes'=>'attributes',
'attribute'=>'attribute',
'Color group?'=>'Color group?',
'Public name'=>'Public name',
'Color'=>'Color',
'Open PrestaShop combination creation form'=>'Open PrestaShop combination creation form',
'Modify the combinations and close this window to refresh the grid'=>'Modify the combinations and close this window to refresh the grid',
'Are you sure you want to delete the selected items?'=>'Are you sure you want to delete the selected items?',
'Create new combination'=>'Create new combination',
'The product price should not be equal to 0. You have to enter the product price before the combination price.'=>'The product price should not be equal to 0. You have to enter the product price before the combination price.',
'Open attributes and groups window'=>'Open attributes and groups window',
'Number of combinations to create when clicking on the Create button'=>'Number of combinations to create when clicking on the Create button',
'Default'=>'Default',
'Combination:'=>'Combination:',
'The combinations grid should be displayed before using this tool.'=>'The combinations grid should be displayed before using this tool.',
'Images'=>'Images',
'images'=>'images',
'image'=>'image',
'Delete selected images'=>'Delete selected images',
'Legend'=>'Legend',
'Please select an image'=>'Please select an image',
'Save image positions'=>'Save image positions',
'Are you sure you want to reset the legend of the selected images?'=>'Are you sure you want to reset the legend of the selected images?',
'Set legend of selected images to product name'=>'Set legend of selected images to product name',
'Select all images'=>'Select all images',
'Upload new images'=>'Upload new images',
'Used'=>'Used',
'Features'=>'Features',
'Feature values'=>'Feature values',
'feature values'=>'feature values',
'features'=>'features',
'Feature'=>'Feature',
'feature'=>'feature',
'Value'=>'Value',
'Custom'=>'Custom',
'Display only features used by products in the same category'=>'Display only features used by products in the same category',
'Quantity discounts'=>'Quantity discounts',
'Accessories'=>'Accessories',
'accessories'=>'accessories',
'accessorie'=>'accessorie',
'category'=>'category',
'Create new quantity discount'=>'Create new quantity discount',
'Quantity'=>'Quantity',
'Discount'=>'Discount',
'MULTIPLE EDITION'=>'MULTIPLE EDITION',
'Load all products'=>'Load all products',
'Delete selected item'=>'Delete selected item',
'Place selected products in selected categories'=>'Place selected products in selected categories',
'Remove selected products from selected categories (if not default category)'=>'Remove selected products from selected categories (if not default category)',
'Display only categories used by selected products'=>'Display only categories used by selected products',
'Select all combinations'=>'Select all combinations',
'Create the new category and close this window to refresh the tree'=>'Create the new category and close this window to refresh the tree',
'Create the new product and close this window to refresh the grid'=>'Create the new product and close this window to refresh the grid',
'You need to select a category'=>'You need to select a category',
'Do you want to copy images?'=>'Do you want to copy images?',
'You can now close this window to refresh the grid'=>'You can now close this window to refresh the grid',
'You can close this window when you get the confirmation of the duplication, don\'t forget to refresh the grid!'=>'You can close this window when you get the confirmation of the duplication, don\'t forget to refresh the grid!',
'Modify the category and close this window to refresh the tree'=>'Modify the category and close this window to refresh the tree',
'Modify the product and close this window to refresh the grid'=>'Modify the product and close this window to refresh the grid',
'The database will be refresh soon to reset products details, this operation takes few seconds. We will give you information about the license during this process.'=>'The database will be refresh soon to reset products details, this operation takes few seconds. We will give you information about the license during this process.',
'History'=>'History',
'Browse history'=>'Browse history',
'Delete all history'=>'Delete all history',
'This action will delete all history, do you confirm this action?'=>'This action will delete all history, do you confirm this action?',
'Section'=>'Section',
'Action'=>'Action',
'Object'=>'Object',
'Object ID'=>'Object ID',
'Old value'=>'Old value',
'New value'=>'New value',
'Land ID'=>'Land ID',
'Table'=>'Table',
'Date'=>'Date',
'Create a new group'=>'Create a new group',
'Delete group, all attributes and all combinations using this group'=>'Delete group, all attributes and all combinations using this group',
'Create new attributes for the selected group'=>'Create new attributes for the selected group',
'Number of attributes to create when clicking on the Create button'=>'Number of attributes to create when clicking on the Create button',
'Delete attribute(s) and all combinations using this attribute'=>'Delete attribute(s) and all combinations using this attribute',
'Create new attributes'=>'Create new attributes',
'Create new combination with the selected groups'=>'Create new combination with the selected groups',
'To create a new combination, check that:'=>'To create a new combination, check that:',
'The combination panel is displayed.'=>'The combination panel is displayed.',
'A product is selected.'=>'A product is selected.',
'No combinations already exist for the selected product.'=>'No combinations already exist for the selected product.',
'At least one group is selected.'=>'At least one group is selected.',
'Duplicate selected groups and their attributes'=>'Duplicate selected groups and their attributes',
'Are you sure to duplicate the selected groups and their attributes?'=>'Are you sure to duplicate the selected groups and their attributes?',
'You must select one item'=>'You must select one item',
'Delete selected features and their values'=>'Delete selected features and their values',
'Duplicate selected features'=>'Duplicate selected features',
'Create a new feature'=>'Create a new feature',
'Are you sure to duplicate the selected features and their values?'=>'Are you sure to duplicate the selected features and their values?',
'Delete selected values'=>'Delete selected values',
'Create new feature value'=>'Create new feature value',
'Number of values to create when clicking on the Create button'=>'Number of values to create when clicking on the Create button',
'Import CSV'=>'Import CSV',
'Upload CSV files'=>'Upload CSV files',
'Import - Backup your base before any mass update!'=>'Import - Backup your base before any mass update!',
'File date'=>'File date',
'File size'=>'File size',
'Mapping'=>'Mapping',
'Field separator'=>'Field separator',
'Value separator'=>'Value separator',
'Force UTF8'=>'Force UTF8',
'Products are identified by'=>'Products are identified by',
'If product with same identifier found in database'=>'If product with same identifier found in database',
'Load'=>'Load',
'Save mapping as'=>'Save mapping as',
'Use'=>'Use',
'File field'=>'File field',
'Database field'=>'Database field',
'Options'=>'Options',
'Process'=>'Process',
'Refresh'=>'Refresh',
'Upload CSV file'=>'Upload CSV file',
'Delete marked files'=>'Delete marked files',
'Load mapping'=>'Load mapping',
'Save mapping'=>'Save mapping',
'Delete mapping and reset grid'=>'Delete mapping and reset grid',
'Import data'=>'Import data',
'Product name'=>'Product name',
'Product reference'=>'Product reference',
'Product and supplier reference'=>'Product and supplier reference',
'Product and supplier name'=>'Product and supplier name',
'Prod. ref THEN prod. name'=>'Prod. ref THEN prod. name',
'Sup. ref THEN prod. name'=>'Sup. ref THEN prod. name',
'id_product'=>'id_product',
'Skip'=>'Skip',
'Replace product values'=>'Replace product values',
'First line content'=>'First line content',
'End of import process.'=>'End of import process.',
'Stats:'=>'Stats:',
'New products:'=>'New products:',
'Modified products:'=>'Modified products:',
'Skipped lines:'=>'Skipped lines:',
'You have to select a file and a mapping.'=>'You have to select a file and a mapping.',
'Lines to import'=>'Lines to import',
'Some options are missing'=>'Some options are missing',
'deleted'=>'deleted',
'Data saved!'=>'Data saved!',
'Supplier(s) created!'=>'Supplier(s) created!',
'Manufacturer(s) created!'=>'Manufacturer(s) created!',
'Feature(s) created!'=>'Feature(s) created!',
'Attribute(s) created!'=>'Attribute(s) created!',
'Attribute group(s) created!'=>'Attribute group(s) created!',
'You need to refresh the page, click here:'=>'You need to refresh the page, click here:',
'All products have been imported. The TODO file is deleted.'=>'All products have been imported. The TODO file is deleted.',
'Download selected file'=>'Download selected file',
'Check and create categories'=>'Check and create categories',
'Auto-import tool'=>'Auto-import tool',
'Interval'=>'Interval',
'Launch import every X seconds if possible'=>'Launch import every X seconds if possible',
'Create categories (auto)'=>'Create categories (auto)',
'Create elements (auto)'=>'Create elements (auto)',
'active'=>'active',
'name'=>'name',
'description'=>'description',
'description_short'=>'description_short',
'meta_title'=>'meta_title',
'meta_description'=>'meta_description',
'meta_keywords'=>'meta_keywords',
'available_now'=>'available_now',
'available_later'=>'available_later',
'out_of_stock'=>'out_of_stock',
'supplier_reference'=>'supplier_reference',
'supplier'=>'supplier',
'manufacturer'=>'manufacturer',
'wholesale_price'=>'wholesale_price',
'ecotax'=>'ecotax',
'priceinctax'=>'priceinctax',
'priceexctax'=>'priceexctax',
'vat'=>'vat',
'ean13'=>'ean13',
'weight'=>'weight',
'on_sale'=>'on_sale',
'reduction_price'=>'reduction_price',
'reduction_percent'=>'reduction_percent',
'reduction_from'=>'reduction_from',
'reduction_to'=>'reduction_to',
'location'=>'location',
'reference'=>'reference',
'combination - value(s) in the column'=>'combination - value(s) in the column',
'combination - value(s) in several lines'=>'combination - value(s) in several lines',
'category by default'=>'category by default',
'categories'=>'categories',
'quantity'=>'quantity',
'attribute of combination - multiple values'=>'attribute of combination - multiple values',
'attribute of combination'=>'attribute of combination',
'The process has been stopped before any modification in the database. You need to fix these errors first.'=>'The process has been stopped before any modification in the database. You need to fix these errors first.',
'Unable to delete this file, please check write permissions:'=>'Unable to delete this file, please check write permissions:',
'You should mark at least one file to delete'=>'You should mark at least one file to delete',
'Wrong mapping, mapping should contain the field reference'=>'Wrong mapping, mapping should contain the field reference',
'Wrong mapping, mapping should contain the field supplier_reference'=>'Wrong mapping, mapping should contain the field supplier_reference',
'Wrong mapping, mapping should contain the fields supplier_reference and name in '=>'Wrong mapping, mapping should contain the fields supplier_reference and name in ',
'Wrong mapping, mapping should contain the fields reference and supplier_reference'=>'Wrong mapping, mapping should contain the fields reference and supplier_reference',
'Wrong mapping, mapping should contain the field name in '=>'Wrong mapping, mapping should contain the field name in ',
'For each line of the mapping you need to double click in the Database field column to fill the mapping.'=>'For each line of the mapping you need to double click in the Database field column to fill the mapping.',
'If the cell in the Options column becomes blue, you need to edit this cell and complete the mapping.'=>'If the cell in the Options column becomes blue, you need to edit this cell and complete the mapping.',
'Wrong mapping, mapping should contain the field id_product'=>'Wrong mapping, mapping should contain the field id_product',
'Wrong mapping, mapping should contain the field id_product_attribute'=>'Wrong mapping, mapping should contain the field id_product_attribute',
'Cannot read the csv file'=>'Cannot read the csv file',
'Error in mapping: too much field to import'=>'Error in mapping: too much field to import',
'Error in mapping: the fields are not in the CSV file'=>'Error in mapping: the fields are not in the CSV file',
'Error in mapping: price including VAT found in CSV columns but no VAT colmun found. You need to indicate the VAT or use only price excluding VAT.'=>'Error in mapping: price including VAT found in CSV columns but no VAT colmun found. You need to indicate the VAT or use only price excluding VAT.',
'Feature not found: '=>'Feature not found: ',
'Error on line '=>'Error on line ',
': wrong column count: '=>': wrong column count: ',
'Error: the VAT value should be between 0 and 100 on line(s) '=>'Error: the VAT value should be between 0 and 100 on line(s) ',
'Error: tax doesn\'t exist: '=>'Error: tax doesn\'t exist: ',
'This supplier doesn\'t exist: '=>'This supplier doesn\'t exist: ',
'This manufacturer doesn\'t exist: '=>'This manufacturer doesn\'t exist: ',
'This feature doesn\'t exist: '=>'This feature doesn\'t exist: ',
'This attribute doesn\'t exist: '=>'This attribute doesn\'t exist: ',
'This attribute group doesn\'t exist: '=>'This attribute group doesn\'t exist: ',
'Click here to fix the problem'=>'Click here to fix the problem',
'Click here to create these suppliers'=>'Click here to create these suppliers',
'Click here to create these manufacturers'=>'Click here to create these manufacturers',
'Click here to create these features'=>'Click here to create these features',
'Click here to create these attributes'=>'Click here to create these attributes',
'Click here to create these attribute groups'=>'Click here to create these attribute groups',
'The TODO file has been deleted, please select the original CSV file.'=>'The TODO file has been deleted, please select the original CSV file.',
'There are still products to be imported in the working file. It can mean errors you need to correct or lines which have been ignored on purpose. Once corrections have been made, click again on the import icon to proceed further.'=>'There are still products to be imported in the working file. It can mean errors you need to correct or lines which have been ignored on purpose. Once corrections have been made, click again on the import icon to proceed further.',
'Mapping name should not be empty'=>'Mapping name should not be empty',
'Image not found:'=>'Image not found:',
'Impossible to copy image:'=>'Impossible to copy image:',
'You should use the tool "check and fix the level_depth field" from the Catalog > Tools menu to fix your categories.'=>'You should use the tool "check and fix the level_depth field" from the Catalog > Tools menu to fix your categories.',
'A category cannot be parent of itself, you must fix this error for category ID'=>'A category cannot be parent of itself, you must fix this error for category ID',
'Error : at least 2 columns have the same name in CSV file. You must use a unique name by column in the first line of your CSV file.'=>'Error : at least 2 columns have the same name in CSV file. You must use a unique name by column in the first line of your CSV file.',
'Enclosed by'=>'Enclosed by',
'Export filename'=>'Export filename',
'Categories to export'=>'Categories to export',
'Export combinations'=>'Export combinations',
'Free shipping fee from'=>'Free shipping fee from',
'Shipping fee'=>'Shipping fee',
'Create new export script'=>'Create new export script',
'Duplicate selected file'=>'Duplicate selected file',
'New script name:'=>'New script name:',
'ISO encoded'=>'ISO encoded',
'All enabled categories'=>'All enabled categories',
'All disabled categories'=>'All disabled categories',
'All categories'=>'All categories',
'Last export date'=>'Last export date',
'No map available'=>'No map available',
'Settings'=>'Settings',
'Tool'=>'Tool',
'Item'=>'Item',
'Default value'=>'Default value',
'Description'=>'Description',
'Database'=>'Database',
'Interface'=>'Interface',
'Notice'=>'Notice',
'Notice when descriptions are not saved'=>'Notice when descriptions are not saved',
'meta title field size'=>'meta title field size',
'meta description field size'=>'meta description field size',
'meta keywords field size'=>'meta keywords field size',
'link rewrite field size'=>'link rewrite field size',
'default product grid view'=>'default product grid view',
'Set product grid view displayed when you launch SC. (grid_light, grid_large, grid_delivery, grid_price, grid_discount, grid_seo, grid_reference)'=>'Set product grid view displayed when you launch SC. (grid_light, grid_large, grid_delivery, grid_price, grid_discount, grid_seo, grid_reference)',
'default product properties panel'=>'default product properties panel',
'Set product properties panel displayed when you launch SC. (combinations, descriptions, images, categories, features, discounts, accessories, tags, specificprices)'=>'Set product properties panel displayed when you launch SC. (combinations, descriptions, images, categories, features, discounts, accessories, tags, specificprices)',
'product drag&drop default behavior'=>'product drag&drop default behavior',
'Set the product drag&drop on category default behavior when you launch SC. (move, copy)'=>'Set the product drag&drop on category default behavior when you launch SC. (move, copy)',
'margin operation defintion'=>'margin operation defintion',
'Set the margin operation for the column [margin] in the price grid in [Prices] view. Available values:<br/>0: priceExcTax - wholesale_price<br/>1: (priceExcTax - wholesale_price)*100 / wholesale_price<br/>2: priceExcTax / wholesale_price<br/>3: priceIncTax / wholesale_price<br/>4: (priceIncTax - wholesale_price)*100 / wholesale_price<br/>5: (priceExcTax - wholesale_price)*100 / priceExcTax'=>'Set the margin operation for the column [margin] in the price grid in [Prices] view. Available values:<br/>0: priceExcTax - wholesale_price<br/>1: (priceExcTax - wholesale_price)*100 / wholesale_price<br/>2: priceExcTax / wholesale_price<br/>3: priceIncTax / wholesale_price<br/>4: (priceIncTax - wholesale_price)*100 / wholesale_price<br/>5: (priceExcTax - wholesale_price)*100 / priceExcTax',
'import images already imported'=>'import images already imported',
'Possible values:<br/>0: The image found in the CSV file is imported only the first time<br/>1: The image found in the CSV file is always imported'=>'Possible values:<br/>0: The image found in the CSV file is imported only the first time<br/>1: The image found in the CSV file is always imported',
'JPG compression level'=>'JPG compression level',
'Set compression level for uploaded product images. Possible values: 20 to 100 (100 is highest)'=>'Set compression level for uploaded product images. Possible values: 20 to 100 (100 is highest)',
'PNG compression level'=>'PNG compression level',
'Set compression level for uploaded product images in PNG format. Possible values: 0 to 9 (0 is highest)'=>'Set compression level for uploaded product images in PNG format. Possible values: 0 to 9 (0 is highest)',
'save image filename in database'=>'save image filename in database',
'Possible values:<br/>0: The image name is not saved, this is the Prestashop standard behavior.<br/>1: The filename is saved to skip the import process and display of the filename in the grid of images.'=>'Possible values:<br/>0: The image name is not saved, this is the Prestashop standard behavior.<br/>1: The filename is saved to skip the import process and display of the filename in the grid of images.',
'display image filename in grid'=>'display image filename in grid',
'Possible values:<br/>0: The image name is not displayed.<br/>1: The filename is displayed in the grid of images if the name has been saved previously.'=>'Possible values:<br/>0: The image name is not displayed.<br/>1: The filename is displayed in the grid of images if the name has been saved previously.',
'background color of resized images'=>'background color of resized images',
'Set the background color of resized images when you upload new images. (R,G,B format)'=>'Set the background color of resized images when you upload new images. (R,G,B format)',
'force update'=>'force update',
'Allow to display the update tool and force the update of Store Commander. You need to reload SC.'=>'Allow to display the update tool and force the update of Store Commander. You need to reload SC.',
'enable descriptions grid'=>'enable descriptions grid',
'Enable descriptions grid. Note: the product html code can create defects in Store Commander. You should use it with small text descriptions only.'=>'Enable descriptions grid. Note: the product html code can create defects in Store Commander. You should use it with small text descriptions only.',
'use PNG format'=>'use PNG format',
'Enable PNG format support in Store Commander and Prestashop.<br/>Possible values:<br/>0: No PNG support (Prestashop standard)<br/>1: PNG file is renamed with JPG file extension<br/>2: Both PNG and JPG format are used.<br/>See documentation'=>'Enable PNG format support in Store Commander and Prestashop.<br/>Possible values:<br/>0: No PNG support (Prestashop standard)<br/>1: PNG file is renamed with JPG file extension<br/>2: Both PNG and JPG format are used.<br/>See documentation',
'disable images in grids'=>'disable images in grids',
'Disable product images in grids to improve performance.<br/>Possible values: 0: images are present in the grids<br/>1: images are not present and the grid is loaded faster.'=>'Disable product images in grids to improve performance.<br/>Possible values: 0: images are present in the grids<br/>1: images are not present and the grid is loaded faster.',
'disable change history'=>'disable change history',
'Do not save modifications in database. This option hides the Tools > Change history menu.'=>'Do not save modifications in database. This option hides the Tools > Change history menu.',
'reset product categories before import'=>'reset product categories before import',
'Possible values:<br/>0: The products\' categories are not modified<br/>1: The product affectation to categories is deleted before import. It allows you to move product from an old category to another one.'=>'Possible values:<br/>0: The products\' categories are not modified<br/>1: The product affectation to categories is deleted before import. It allows you to move product from an old category to another one.',
'product price'=>'product price',
'Possible values:<br/>0: The product price is set to 0 and each combination has its own price.<br/>1: The product price is set to the first combination price found in your CSV and other combinations prices are set by subtraction from this price in the database.(Prestashop standard)'=>'Possible values:<br/>0: The product price is set to 0 and each combination has its own price.<br/>1: The product price is set to the first combination price found in your CSV and other combinations prices are set by subtraction from this price in the database.(Prestashop standard)',
'auto create reference for multiple attr.'=>'auto create reference for multiple attr.',
'If enabled, the attribute name is added to the combination reference. (SOURCEREF_ATTRNAME)'=>'If enabled, the attribute name is added to the combination reference. (SOURCEREF_ATTRNAME)',
'combinations grid format'=>'combinations grid format',
'Possible values:<br/>0: 1 combination = 1 unique physical product (standard)<br/>1: product combinations are composed of several disparate attributes (used for special configurators)'=>'Possible values:<br/>0: 1 combination = 1 unique physical product (standard)<br/>1: product combinations are composed of several disparate attributes (used for special configurators)',
'size of images in grids'=>'size of images in grids',
'Set the size of the images displayed in the grids. The possible values are the name of the image format in PrestaShop (in Tab Preferences > Image:small, medium,...)'=>'Set the size of the images displayed in the grids. The possible values are the name of the image format in PrestaShop (in Tab Preferences > Image:small, medium,...)',
'display all languages'=>'display all languages',
'Possible values:<br/>0: Only enabled languages are available in the interface.<br/>1: All languages are available in the interface.'=>'Possible values:<br/>0: Only enabled languages are available in the interface.<br/>1: All languages are available in the interface.',
'tabulation direction'=>'tabulation direction',
'When the tabulation key is pressed, the next element to edit is:<br/>0: the next column<br/>1: the next line<br/>(you need to restart Store Commander)'=>'When the tabulation key is pressed, the next element to edit is:<br/>0: the next column<br/>1: the next line<br/>(you need to restart Store Commander)',
'wholesale price > sell price'=>'wholesale price > sell price',
'Possible values:<br/>0: don\'t trigger alert<br/>1: display alert message after cell edition'=>'Possible values:<br/>0: don\'t trigger alert<br/>1: display alert message after cell edition',
'File created in'=>'File created in',
'File NOT created'=>'File NOT created',
'lines'=>'lines',
'Save selection'=>'Save selection',
'Save selection as'=>'Save selection as',
'No selection available'=>'No selection available',
'Load selection'=>'Load selection',
'Categories selection'=>'Categories selection',
'Selection name should not be empty'=>'Selection name should not be empty',
'Delete selection'=>'Delete selection',
'You have to define a filename for the export.'=>'You have to define a filename for the export.',
'_fixed_value'=>'_fixed_value',
'You have to set the mapping for the script.'=>'You have to set the mapping for the script.',
'Number of lines to create'=>'Number of lines to create',
'The CSV files have been installed. You can use them in the Export CSV tool.'=>'The CSV files have been installed. You can use them in the Export CSV tool.',
'Create script files example for CSV export'=>'Create script files example for CSV export',
'You need to install the tool from the menu Tools > Install > Create script files example for CSV export'=>'You need to install the tool from the menu Tools > Install > Create script files example for CSV export',
'stock_value'=>'stock_value',
'stock_value_wholesale'=>'stock_value_wholesale',
'Export out of stock products'=>'Export out of stock products',
'productshippingcost'=>'productshippingcost',
'availability_message'=>'availability_message',
'priceinctaxwithshipping'=>'priceinctaxwithshipping',
'Create # lines'=>'Create # lines',
'product reference'=>'product reference',
'When importing a product with combinations:<br/>Possible values:<br/>0: The product reference is not altered and each combination has its own reference.<br/>1: The product reference becomes the first combination reference.<br/>2: The product reference becomes the first combination reference + "P".'=>'When importing a product with combinations:<br/>Possible values:<br/>0: The product reference is not altered and each combination has its own reference.<br/>1: The product reference becomes the first combination reference.<br/>2: The product reference becomes the first combination reference + "P".',
'The Store Commander version you try to install is too recent for your support package. Please use an older version or renew your support on http://www.storecommander.com to use this version.'=>'The Store Commander version you try to install is too recent for your support package. Please use an older version or renew your support on http://www.storecommander.com to use this version.',
'Check and fix categories'=>'Check and fix categories',
'use old image path'=>'use old image path',
'Force the image file path to the old system [id_product]-[id_image]-[size].jpg. Usefull for servers with "safemode".'=>'Force the image file path to the old system [id_product]-[id_image]-[size].jpg. Usefull for servers with "safemode".',
'allow external default category'=>'allow external default category',
'Allow you to set a default category for a product even if the product is not present in this category.'=>'Allow you to set a default category for a product even if the product is not present in this category.',
'Do you want to delete the current mapping?'=>'Do you want to delete the current mapping?',
'Specific prices'=>'Specific prices',
'Create new specific price'=>'Create new specific price',
'Price'=>'Price',
'Unit price'=>'Unit price',
'Unity'=>'Unity',
'<p>Example 1:with default language of the shop<br />blue<br />black</p><p>Example 2:<br />blue,fr<br />black,en</p>'=>'<p>Example 1:with default language of the shop<br />blue<br />black</p><p>Example 2:<br />blue,fr<br />black,en</p>',
'Add tags'=>'Add tags',
'Lang'=>'Lang',
'Fixed price'=>'Fixed price',
'Create tags'=>'Create tags',
'Delete selected tags'=>'Delete selected tags',
'Link these tags to selected products when created'=>'Link these tags to selected products when created',
'Delete link between selected tags and selected products'=>'Delete link between selected tags and selected products',
'Add link between selected tags and selected products'=>'Add link between selected tags and selected products',
'View products with the selected tag on front office'=>'View products with the selected tag on front office',
'View only used tags in the same category'=>'View only used tags in the same category',
'Product quantity used when the product is created in SC.'=>'Product quantity used when the product is created in SC.',
'new product quantity default'=>'new product quantity default',
'Upgrade your account!'=>'Upgrade your account!',
'Attachments'=>'Attachments',
'attachments'=>'attachments',
'attachment'=>'attachment',
'Add attachments'=>'Add attachments',
'Delete link between selected attachments and selected products'=>'Delete link between selected attachments and selected products',
'Add link between selected attachments and selected products'=>'Add link between selected attachments and selected products',
'Delete selected attachments'=>'Delete selected attachments',
'Link these attachments to selected products when created'=>'Link these attachments to selected products when created',
'View only attachments used in the same category'=>'View only attachments used in the same category',
'Filters'=>'Filters',
'Search'=>'Search',
'Supplier Reference'=>'Supplier Reference',
'Customer group'=>'Customer group',
'All'=>'All',
'Country'=>'Country',
'Currency'=>'Currency',
'Start your SC FREE account'=>'Start your SC FREE account',
'Start your SC TRIAL account'=>'Start your SC TRIAL account',
'You must validate your license on your online shop. The free version cannot be launched on local websites.'=>'You must validate your license on your online shop. The free version cannot be launched on local websites.',
'Feature in "read-only" mode with your SC FREE Account.'=>'Feature in "read-only" mode with your SC FREE Account.',
'This action cannot be performed with your Free Account.'=>'This action cannot be performed with your Free Account.',
'Upgrade Now!'=>'Upgrade Now!',
'CSV Import'=>'CSV Import',
'CSV Export'=>'CSV Export',
'Are you sure?'=>'Are you sure?',
'always use product name as link rewrite'=>'always use product name as link rewrite',
'Possible values:<br/>0: SC will NOT modifiy the link_rewrite of the product: you should set it yourself.<br/>1: SC always set the link_rewrite url to the name of the product.'=>'Possible values:<br/>0: SC will NOT modifiy the link_rewrite of the product: you should set it yourself.<br/>1: SC always set the link_rewrite url to the name of the product.',
'price_inctax_without_reduction'=>'price_inctax_without_reduction',
'price_exctax_without_reduction'=>'price_exctax_without_reduction',
'accessory'=>'accessory',
'Export by default category'=>'Export by default category',
'priceinctax including ecotax'=>'priceinctax including ecotax',
'new product reference default'=>'new product reference default',
'new product supplier reference default'=>'new product supplier reference default',
'Product reference used when the product is created in SC.'=>'Product reference used when the product is created in SC.',
'Product supplier reference used when the product is created in SC.'=>'Product supplier reference used when the product is created in SC.',
'Your country'=>'Your country',
'Compatibility'=>'Compatibility',
'chars'=>'chars',
'eBay module'=>'eBay module',
'Set this option to 0 if you don\'t want SC to use the Prestashop hook system.'=>'Set this option to 0 if you don\'t want SC to use the Prestashop hook system.',
'Set a cover image for products without cover image'=>'Set a cover image for products without cover image',
'max elements in change history'=>'max elements in change history',
'Set the maximum of elements to store in database'=>'Set the maximum of elements to store in database',
'License to create:'=>'License to create:',
'Set product prices to their default combination prices'=>'Set product prices to their default combination prices',
'Please select 1 to'=>'Please select 1 to',
'max products to duplicate'=>'max products to duplicate',
'Set the maximum number of duplicate products to create in one click.'=>'Set the maximum number of duplicate products to create in one click.',
'Duplicate 1 to'=>'Duplicate 1 to',
'new product active state default'=>'new product active state default',
'Active state used when the product is created in SC. The active column must be present in the grid.'=>'Active state used when the product is created in SC. The active column must be present in the grid.',
'new product manufacturer default'=>'new product manufacturer default',
'id_manufacturer used when the product is created in SC. The manufacturer column must be present in the grid.'=>'id_manufacturer used when the product is created in SC. The manufacturer column must be present in the grid.',
'new product supplier default'=>'new product supplier default',
'id_supplier used when the product is created in SC. The supplier column must be present in the grid.'=>'id_supplier used when the product is created in SC. The supplier column must be present in the grid.',
'max products to open in browser'=>'max products to open in browser',
'Set the maximum number of new browser tabs to open when you do a right click on products > See on shop'=>'Set the maximum number of new browser tabs to open when you do a right click on products > See on shop',
'Action: Delete images'=>'Action: Delete images',
'Action: Delete tags'=>'Action: Delete tags',
'You have to set the language in the mapping for the field:'=>'You have to set the language in the mapping for the field:',
'Export disabled products'=>'Export disabled products',
'available for order'=>'available for order',
'show price'=>'show price',
'online only (not sold in store)'=>'online only (not sold in store)',
'Are you sure you want to rebuild product prices?'=>'Are you sure you want to rebuild product prices?',
'Creation date'=>'Creation date',
'Modified date'=>'Modified date',
'Unit'=>'Unit',
'Minimum quantity'=>'Minimum quantity',
'minimum quantity'=>'minimum quantity',
'Price excl. Tax'=>'Price excl. Tax',
'Price incl. Tax'=>'Price incl. Tax',
'Prod. price excl tax'=>'Prod. price excl tax',
'Attr. price excl tax'=>'Attr. price excl tax',
'Expand all'=>'Expand all',
'Collapse all'=>'Collapse all',
'Are you sure you want to change the legend of the selected image(s) with the product name?'=>'Are you sure you want to change the legend of the selected image(s) with the product name?',
'Assign product name to selected image(s) legend'=>'Assign product name to selected image(s) legend',
'Reduction amount'=>'Reduction amount',
'Reduction %'=>'Reduction %',
'Discount starts'=>'Discount starts',
'Discount ends'=>'Discount ends',
'Price incl reduction'=>'Price incl reduction',
'Price incl % reduction'=>'Price incl % reduction',
'Pages'=>'Pages',
'Page'=>'Page',
'pages'=>'pages',
'page'=>'page',
'Sort'=>'Sort',
'Please select an item'=>'Please select an item',
'Mark / Unmark'=>'Mark / Unmark',
'Category sorted, click on the Refresh icon to allow reorder (drag and drop) on the categories tree.'=>'Category sorted, click on the Refresh icon to allow reorder (drag and drop) on the categories tree.',
'price impact of combination'=>'price impact of combination',
'weight impact of combination'=>'weight impact of combination',
'default status of created categories'=>'default status of created categories',
'Available values:<br/>0: created categories by the import process are disabled<br/>1: created categories by the import process are enabled'=>'Available values:<br/>0: created categories by the import process are disabled<br/>1: created categories by the import process are enabled',
'Feature in "read-only" mode with your SC Lite license.'=>'Feature in "read-only" mode with your SC Lite license.',
'This action cannot be performed with your SC Lite license.'=>'This action cannot be performed with your SC Lite license.',
'Short description charset'=>'Short description charset',
'Short description charset must be < '=>'Short description charset must be < ',
'max charset in short description field'=>'max charset in short description field',
'Set the maximum character set SC checks before saving it in the database. This does NOT modify the database.'=>'Set the maximum character set SC checks before saving it in the database. This does NOT modify the database.',
'Set this option to 1 if you use the eBay module developped by Prestashop. You need to reload SC. <a target="_blank" href="http://support.storecommander.com/categories/search?query=ebay&locale=1">Read more</a>'=>'Set this option to 1 if you use the eBay module developped by Prestashop. You need to reload SC. <a target="_blank" href="http://support.storecommander.com/categories/search?query=ebay&locale=1">Read more</a>',
'You have to use the standard combinations grid format to create new combinations in Store Commander. Please change the settings of SC.'=>'You have to use the standard combinations grid format to create new combinations in Store Commander. Please change the settings of SC.',
'Please select an export script.'=>'Please select an export script.',
'Short description'=>'Short description',
'The field separator and the value separator could not be the same character.'=>'The field separator and the value separator could not be the same character.',
'You need to refresh Store Commander to use the new settings.'=>'You need to refresh Store Commander to use the new settings.',
'name_with_attributes'=>'name_with_attributes',
'width'=>'width',
'height'=>'height',
'depth'=>'depth',
'category by default (full path)'=>'category by default (full path)',
'feature (custom)'=>'feature (custom)',
'Used by'=>'Used by',
'Are you sure you want to merge the selected items?'=>'Are you sure you want to merge the selected items?',
'minimal quantity'=>'minimal quantity',
'unit'=>'unit',
'categories (full path)'=>'categories (full path)',
'Modify and create products'=>'Modify and create products',
'Modify products'=>'Modify products',
'unit price'=>'unit price',
'unit price (combination impact)'=>'unit price (combination impact)',
'unit (for unit price)'=>'unit (for unit price)',
'Set this option to "128M" for example if you want to set the php memory limit (ini_set "memory_limit"). Set to "0" to use system default value.'=>'Set this option to "128M" for example if you want to set the php memory limit (ini_set "memory_limit"). Set to "0" to use system default value.',
'Note: you will need to use the menu "Catalog > Tools > Check and fix categories" after your moves operation.'=>'Note: you will need to use the menu "Catalog > Tools > Check and fix categories" after your moves operation.',
'Note: Using the Prestashop cache system may interfere with the proper use of Store Commander as well as modules. To prevent this, go to the Preferences > Performance tab to disable the option at the end of the page.'=>'Note: Using the Prestashop cache system may interfere with the proper use of Store Commander as well as modules. To prevent this, go to the Preferences > Performance tab to disable the option at the end of the page.',
'use 4 decimals for wholesale price'=>'use 4 decimals for wholesale price',
'Possible values:<br/>0: wholesale price format with 2 decimals (standard)<br/>1: wholesale price format with 4 decimals'=>'Possible values:<br/>0: wholesale price format with 2 decimals (standard)<br/>1: wholesale price format with 4 decimals',
'Image'=>'Image',
'Pos.'=>'Pos.',
'modification'=>'modification',
'move_categ'=>'move_categ',
'ID employee'=>'ID employee',
'File'=>'File',
'Text'=>'Text',
'Required'=>'Required',
'Customization fields'=>'Customization fields',
'customization fields'=>'customization fields',
'customization field'=>'customization field',
'Add fields'=>'Add fields',
'Delete selected fields'=>'Delete selected fields',
'Enable recursive selection'=>'Enable recursive selection',
'Disable recursive selection'=>'Disable recursive selection',
'File name'=>'File name',
'Action: Delete attachements'=>'Action: Delete attachements',
'customization field: type'=>'customization field: type',
'customization field: required'=>'customization field: required',
'customization field: name'=>'customization field: name',
'export: field separator = value separator'=>'export: field separator = value separator',
'colors of margin cells'=>'colors of margin cells',
'Set the rules for the background color of the margin cells.<br/>Format: Value:Color;Value:Color;...<br/>Exemple: 20:#BA2329;50:#E3772B;1000:#34841F<br/>If the margin is < Value then the cell will be Color.<br/><a target="_blank" href="http://support.storecommander.com/entries/22049593-how-to-manage-the-margin-of-your-products?locale=1">Read more</a>'=>'Set the rules for the background color of the margin cells.<br/>Format: Value:Color;Value:Color;...<br/>Exemple: 20:#BA2329;50:#E3772B;1000:#34841F<br/>If the margin is < Value then the cell will be Color.<br/><a target="_blank" href="http://support.storecommander.com/entries/22049593-how-to-manage-the-margin-of-your-products?locale=1">Read more</a>',
'Selection'=>'Selection',
'Filter'=>'Filter',
'Read more'=>'Read more',
'You need to create some folders by FTP:'=>'You need to create some folders by FTP:',
'You have to set the category selection for the script'=>'You have to set the category selection for the script',
'File created'=>'File created',
'This will set the cheapest combination as default combination for each seleted product. Continue?'=>'This will set the cheapest combination as default combination for each seleted product. Continue?',
'attribute of combination - color value'=>'attribute of combination - color value',
'attribute of combination - texture'=>'attribute of combination - texture',
'Action: Delete specific price'=>'Action: Delete specific price',
'Quick export window'=>'Quick export window',
'specific price'=>'specific price',
'from quantity'=>'from quantity',
'price'=>'price',
'reduction'=>'reduction',
'reduction type'=>'reduction type',
'from date'=>'from date',
'to date'=>'to date',
'CRON task name'=>'CRON task name',
'Update products older than'=>'Update products older than',
'Import'=>'Import',
'Modifications'=>'Modifications',
'Export process'=>'Export process',
'Customers'=>'Customers',
'View only used accessories in the same category'=>'View only used accessories in the same category',
'Add link between selected accessories and selected products'=>'Add link between selected accessories and selected products',
'Delete link between selected accessories and selected products'=>'Delete link between selected accessories and selected products',
'Export grid to clipboard in CSV format for MSExcel with tab delimiter.'=>'Export grid to clipboard in CSV format for MSExcel with tab delimiter.',
'Type'=>'Type',
'Available date'=>'Available date',
'Visibility'=>'Visibility',
'Both'=>'Both',
'None'=>'None',
'You need to upgrade your license to SC MultiStores then you will be able to manage several shops in Store Commander.'=>'You need to upgrade your license to SC MultiStores then you will be able to manage several shops in Store Commander.',
'We are currently in the process of developing multistore management tools, which will accelerate your multistores management with Store Commander. For now, changes you are making will only modify your default store.'=>'We are currently in the process of developing multistore management tools, which will accelerate your multistores management with Store Commander. For now, changes you are making will only modify your default store.',
'attribute of combination - default combination'=>'attribute of combination - default combination',
'ignored lines'=>'ignored lines',
'A supplier needs to be associated to the product to set a supplier\'s reference to the product'=>'A supplier needs to be associated to the product to set a supplier\'s reference to the product',
'When lines are ignored during import:<br/>0: keep the line in the working file .TODO.csv<br/>1: delete this line in .TODO.csv'=>'When lines are ignored during import:<br/>0: keep the line in the working file .TODO.csv<br/>1: delete this line in .TODO.csv',
'Shop'=>'Shop',
'Store'=>'Store',
'Stores'=>'Stores',
'Select all'=>'Select all',
'Multistore sharing manager'=>'Multistore sharing manager',
'Add all the products selected to all the selected shops'=>'Add all the products selected to all the selected shops',
'Delete all the products selected to all the selected shops'=>'Delete all the products selected to all the selected shops',
'shops'=>'shops',
'shop'=>'shop',
'Present'=>'Present',
'Multistore : products information'=>'Multistore : products information',
'Shop:'=>'Shop:',
'Multistore : combinations'=>'Multistore : combinations',
'Attribute:'=>'Attribute:',
'Tax rate'=>'Tax rate',
'ID prod.'=>'ID prod.',
'Attribute'=>'Attribute',
'All shops'=>'All shops',
'Display:'=>'Display:',
'Update:'=>'Update:',
'default combination'=>'default combination',
'export root category'=>'export root category',
'Export root category for full paths: Home > ...'=>'Export root category for full paths: Home > ...',
'Shop group'=>'Shop group',
'Only display categories associated to the selected shop'=>'Only display categories associated to the selected shop',
'Activate all selected products for the selected shops'=>'Activate all selected products for the selected shops',
'Desactivate all selected products for the selected shops'=>'Desactivate all selected products for the selected shops',
'product weight'=>'product weight',
'When importing a product with combinations:<br/>Possible values:<br/>0: The product weight is not altered and each combination has its own weight.<br/>1: The product weight becomes the first combination weight.'=>'When importing a product with combinations:<br/>Possible values:<br/>0: The product weight is not altered and each combination has its own weight.<br/>1: The product weight becomes the first combination weight.',
'Action: Delete all combinations'=>'Action: Delete all combinations',
'Shops'=>'Shops',
'Current shop'=>'Current shop',
'The quantity of products below was not modified because they possess combinations'=>'The quantity of products below was not modified because they possess combinations',
'Manage user permissions'=>'Manage user permissions',
'Access'=>'Access',
'Profile access'=>'Profile access',
'User permissions'=>'User permissions',
'Reset permissions'=>'Reset permissions',
'Profiles'=>'Profiles',
'Do you want to duplicate these permissions?'=>'Do you want to duplicate these permissions?',
'Are you sure that you want reset this permissions?'=>'Are you sure that you want reset this permissions?',
'Update StoreCommander'=>'Update StoreCommander',
'StoreCommander licence'=>'StoreCommander licence',
'See in Prestashop'=>'See in Prestashop',
'Delete products and combinations'=>'Delete products and combinations',
'Empty recycle bin'=>'Empty recycle bin',
'Add new products and combinations'=>'Add new products and combinations',
'Add new categories'=>'Add new categories',
'Fast exports'=>'Fast exports',
'Product grid: Light view'=>'Product grid: Light view',
'Product grid: Large view'=>'Product grid: Large view',
'Product grid: Delivery view'=>'Product grid: Delivery view',
'Product grid: Prices view'=>'Product grid: Prices view',
'Product grid: Discounts view'=>'Product grid: Discounts view',
'Product grid: Discounts view 2'=>'Product grid: Discounts view 2',
'Product grid: SEO view'=>'Product grid: SEO view',
'Product grid: References view'=>'Product grid: References view',
'Product grid: Descriptions view'=>'Product grid: Descriptions view',
'Grid'=>'Grid',
'View'=>'View',
'Properties grid: combinations'=>'Properties grid: combinations',
'Properties grid: descriptions'=>'Properties grid: descriptions',
'Properties grid: images'=>'Properties grid: images',
'Properties grid: accessories'=>'Properties grid: accessories',
'Properties grid: attachments'=>'Properties grid: attachments',
'Properties grid: specific prices'=>'Properties grid: specific prices',
'Properties grid: features'=>'Properties grid: features',
'Properties grid: tags'=>'Properties grid: tags',
'Properties grid: categories'=>'Properties grid: categories',
'Properties grid: customized fields'=>'Properties grid: customized fields',
'Properties grid: multishops share'=>'Properties grid: multishops share',
'Properties grid: multishops product information'=>'Properties grid: multishops product information',
'Properties grid: multishops combinations'=>'Properties grid: multishops combinations',
'Orders'=>'Orders',
'See the profile in Prestashop'=>'See the profile in Prestashop',
'Profile access:'=>'Profile access:',
'Employee access'=>'Employee access',
'Different from profile:'=>'Different from profile:',
'Delete access'=>'Delete access',
'Give access'=>'Give access',
'Calculate total stock of products with combinations'=>'Calculate total stock of products with combinations',
'price including taxes contains ecotax'=>'price including taxes contains ecotax',
'Possible values:<br/>0: Ecotax is not included in the Incl. taxes price but purely in the Ecotax column<br/>1: Price including taxes contains ecotax'=>'Possible values:<br/>0: Ecotax is not included in the Incl. taxes price but purely in the Ecotax column<br/>1: Price including taxes contains ecotax',
'Add/Move products in categories'=>'Add/Move products in categories',
'Contextual menu : mass update products'=>'Contextual menu : mass update products',
'Move categories'=>'Move categories',
'Contextual menu : show/hide categories'=>'Contextual menu : show/hide categories',
'Properties grid: Discount'=>'Properties grid: Discount',
'Stock: Warehouses'=>'Stock: Warehouses',
'Warehouse management in Store Commander is out next month!'=>'Warehouse management in Store Commander is out next month!',
'The period entitling you to download Store Commander updates and benefit from support has expired.'=>'The period entitling you to download Store Commander updates and benefit from support has expired.',
'If you wish to benefit from future updates and new features, please log onto your account here:'=>'If you wish to benefit from future updates and new features, please log onto your account here:',
'and click on Updates & support 6 or 12 months, or upgrade to a higher license plan.'=>'and click on Updates & support 6 or 12 months, or upgrade to a higher license plan.',
'Please contact us for any sales inquiries you may have.'=>'Please contact us for any sales inquiries you may have.',
'Click here to satisfy your curiosity!'=>'Click here to satisfy your curiosity!',
'Bulk Orders Management shall be released end of June in Store Commander.'=>'Bulk Orders Management shall be released end of June in Store Commander.',
'Bulk Clients Management shall be released end of June in Store Commander.'=>'Bulk Clients Management shall be released end of June in Store Commander.',
'Bulk Stocks and Warehouses Management shall be released end of June in Store Commander.'=>'Bulk Stocks and Warehouses Management shall be released end of June in Store Commander.',
'Clear cookies data: grid preferences for orders'=>'Clear cookies data: grid preferences for orders',
'Clear cookies data: grid preferences for customers'=>'Clear cookies data: grid preferences for customers',
'Period'=>'Period',
'Order status'=>'Order status',
'30 days'=>'30 days',
'15 days'=>'15 days',
'3 months'=>'3 months',
'6 months'=>'6 months',
'1 year'=>'1 year',
'View selected orders in Prestashop'=>'View selected orders in Prestashop',
'View selected customers in Prestashop'=>'View selected customers in Prestashop',
'Print grid'=>'Print grid',
'Login as selected user on the front office'=>'Login as selected user on the front office',
'Alert: You need to select only one order'=>'Alert: You need to select only one order',
'Customer'=>'Customer',
'Lastname'=>'Lastname',
'Total paid'=>'Total paid',
'Invoice No'=>'Invoice No',
'Delivery No'=>'Delivery No',
'In stock'=>'In stock',
'Message'=>'Message',
'Order history'=>'Order history',
'Properties of order'=>'Properties of order',
'Employee'=>'Employee',
'Author'=>'Author',
'Messages'=>'Messages',
'Prestashop: order page'=>'Prestashop: order page',
'Warning: Store Commander doesn\'t recalculate order\'s totals.'=>'Warning: Store Commander doesn\'t recalculate order\'s totals.',
'Address'=>'Address',
'Postcode'=>'Postcode',
'State'=>'State',
'Other'=>'Other',
'Phone'=>'Phone',
'Mobile'=>'Mobile',
'Warehouses'=>'Warehouses',
'warehouses'=>'warehouses',
'Advanced Stock Mgmt.'=>'Advanced Stock Mgmt.',
'Physical stock'=>'Physical stock',
'Available stock'=>'Available stock',
'Live stock'=>'Live stock',
'Warehouse'=>'Warehouse',
'warehouse'=>'warehouse',
'Images and warehouses'=>'Images and warehouses',
'Add all selected products to all selected warehouses'=>'Add all selected products to all selected warehouses',
'Delete all selected products from all selected warehouses'=>'Delete all selected products from all selected warehouses',
'Advanced stocks & Stock movements'=>'Advanced stocks & Stock movements',
'Manage warehouses'=>'Manage warehouses',
'Valuation'=>'Valuation',
'Advanced stocks'=>'Advanced stocks',
'Stock movements'=>'Stock movements',
'Reason'=>'Reason',
'Disabled'=>'Disabled',
'Enabled'=>'Enabled',
'Enabled + Manual Mgmt'=>'Enabled + Manual Mgmt',
'The selected products have no Advanced Stock Management activated nor possess combinations'=>'The selected products have no Advanced Stock Management activated nor possess combinations',
'The selected product do not have the Advanced Stock Management option activated nor possess combinations'=>'The selected product do not have the Advanced Stock Management option activated nor possess combinations',
'The selected combinations do not have thé Advanced Stock Management option activated'=>'The selected combinations do not have thé Advanced Stock Management option activated',
'The selected combination do not have the Advanced Stock Management option activated'=>'The selected combination do not have the Advanced Stock Management option activated',
'Show help for color codes used for quantities/warehouses management'=>'Show help for color codes used for quantities/warehouses management',
'Create a new stock movement'=>'Create a new stock movement',
'Create a new stock movement : Add product to stock'=>'Create a new stock movement : Add product to stock',
'Create a new stock movement : Remove product from stock'=>'Create a new stock movement : Remove product from stock',
'You have to select a row in the "Advanced Stock" grid, to be able to delete stock from this reference'=>'You have to select a row in the "Advanced Stock" grid, to be able to delete stock from this reference',
'You have to select a row in the "Advanced Stock" grid, to be able to add stock to this reference'=>'You have to select a row in the "Advanced Stock" grid, to be able to add stock to this reference',
'Stock movements history'=>'Stock movements history',
'To update quantities, you need to create a stock movement.'=>'To update quantities, you need to create a stock movement.',
'OR'=>'OR',
'Add to stock'=>'Add to stock',
'Remove stock'=>'Remove stock',
'Add product to stock'=>'Add product to stock',
'Product reference:'=>'Product reference:',
'Name:'=>'Name:',
'Quantity to add:'=>'Quantity to add:',
'Available for sale?'=>'Available for sale?',
'Warehouse:'=>'Warehouse:',
'Wholesale price:'=>'Wholesale price:',
'Currency:'=>'Currency:',
'Label:'=>'Label:',
'Remove product from stock'=>'Remove product from stock',
'Quantity to remove:'=>'Quantity to remove:',
'Available for sale:'=>'Available for sale:',
'The quantity value is invalid.'=>'The quantity value is invalid.',
'The selected warehouse is invalid.'=>'The selected warehouse is invalid.',
'The reason is invalid.'=>'The reason is invalid.',
'You have to specify if the product quantity is available for sale on the store.'=>'You have to specify if the product quantity is available for sale on the store.',
'The wholesale price is invalid.'=>'The wholesale price is invalid.',
'The selected currency is invalid.'=>'The selected currency is invalid.',
'An error occurred. No stock was added.'=>'An error occurred. No stock was added.',
'You do not have enough available quantity.'=>'You do not have enough available quantity.',
'You do not have enough quantity (not available).'=>'You do not have enough quantity (not available).',
'It is not possible to remove the specified quantity or an error occurred. No stock was removed.'=>'It is not possible to remove the specified quantity or an error occurred. No stock was removed.',
'The source warehouse is not valid.'=>'The source warehouse is not valid.',
'The destination warehouse is not valid.'=>'The destination warehouse is not valid.',
'You have to specify if the product quantity is available for sale on the store in the source warehouse.'=>'You have to specify if the product quantity is available for sale on the store in the source warehouse.',
'You have to specify if the product quantity is available for sale on the store in the destination warehouse.'=>'You have to specify if the product quantity is available for sale on the store in the destination warehouse.',
'It is not possible to transfer the specified quantity, or an error occurred. No stock was transferred.'=>'It is not possible to transfer the specified quantity, or an error occurred. No stock was transferred.',
'An error occurred. Please try again later.'=>'An error occurred. Please try again later.',
'Caution!!!'=>'Caution!!!',
'You should select combinations'=>'You should select combinations',
'You should select products'=>'You should select products',
'Not Advanced stock'=>'Not Advanced stock',
'Transfer product from one warehouse to another'=>'Transfer product from one warehouse to another',
'Quantity to transfer:'=>'Quantity to transfer:',
'Available for sale in the source warehouse?'=>'Available for sale in the source warehouse?',
'Is this a quantity available for sale?'=>'Is this a quantity available for sale?',
'Source Warehouse:'=>'Source Warehouse:',
'Select the warehouse from which you want to transfer the product.'=>'Select the warehouse from which you want to transfer the product.',
'Destination Warehouse:'=>'Destination Warehouse:',
'Select the destination warehouse.'=>'Select the destination warehouse.',
'Available for sale in the destination warehouse?'=>'Available for sale in the destination warehouse?',
'Do you want this quantity to be available for sale?'=>'Do you want this quantity to be available for sale?',
'Transfer'=>'Transfer',
'Help for quantities / warehouses management'=>'Help for quantities / warehouses management',
'Product with combination(s), whatever the Advanced Stock Mgmt mode is.'=>'Product with combination(s), whatever the Advanced Stock Mgmt mode is.',
'Product not associated to any warehouse'=>'Product not associated to any warehouse',
'Product with at least one warehouse, but not the warehouse selected on the left col'=>'Product with at least one warehouse, but not the warehouse selected on the left col',
'Product associated to at least one warehouse, including the warehouse selected on the left col'=>'Product associated to at least one warehouse, including the warehouse selected on the left col',
'Payment'=>'Payment',
'order'=>'order',
'orders'=>'orders',
'Last connection'=>'Last connection',
'DOB'=>'DOB',
'Default group'=>'Default group',
'Private note'=>'Private note',
'Last order date'=>'Last order date',
'Nb products in cart'=>'Nb products in cart',
'Total cart'=>'Total cart',
'Confirmed orders'=>'Confirmed orders',
'Total confirmed orders'=>'Total confirmed orders',
'The selected products have no Advanced Stock Management or have Manual mgmt activated'=>'The selected products have no Advanced Stock Management or have Manual mgmt activated',
'The selected product do not have the Advanced Stock Management option activated or have Manual mgmt activated'=>'The selected product do not have the Advanced Stock Management option activated or have Manual mgmt activated',
'The product is associated with no warehouse'=>'The product is associated with no warehouse',
'The products are associated with no warehouse'=>'The products are associated with no warehouse',
'Associate only products using Advanced Stocks (Associate only AS)'=>'Associate only products using Advanced Stocks (Associate only AS)',
'Enable Advanced Stocks and Associate (Activate AS & Associate)'=>'Enable Advanced Stocks and Associate (Activate AS & Associate)',
'Enable Advanced Stocks + Manual Management & Associate (Activate AS + GMM & Associate)'=>'Enable Advanced Stocks + Manual Management & Associate (Activate AS + GMM & Associate)',
'Advanced stocks settings'=>'Advanced stocks settings',
'The selected products possess combinations'=>'The selected products possess combinations',
'The selected product possess combinations'=>'The selected product possess combinations',
'Some of the selected products possess combinations'=>'Some of the selected products possess combinations',
'Some of the selected products do not have the Advanced Stock Management option activated.'=>'Some of the selected products do not have the Advanced Stock Management option activated.',
'Read help more explanations.'=>'Read help more explanations.',
'If enabled: associate/dissociate products and combinations in warehouse'=>'If enabled: associate/dissociate products and combinations in warehouse',
'Action: Associate only AS'=>'Action: Associate only AS',
'Action: Activate AS & Associate'=>'Action: Activate AS & Associate',
'Action: Activate AS + MM & Associate'=>'Action: Activate AS + MM & Associate',
'Only the selected products using the Advanced Stocks Management (Advanced Stocks + Manual Management) will be associated to the selected warehouses.'=>'Only the selected products using the Advanced Stocks Management (Advanced Stocks + Manual Management) will be associated to the selected warehouses.',
'Advanced Stocks mode (AS) only will be enabled onto all selected products where Advanced Stocks Management is disabled. They will then be associated to the selected warehouses.'=>'Advanced Stocks mode (AS) only will be enabled onto all selected products where Advanced Stocks Management is disabled. They will then be associated to the selected warehouses.',
'Advanced Stocks + Manual Management (AS+MM) only will be enabled onto all selected products where Advanced Stocks Management is disabled. They will then be associated to the selected warehouses.'=>'Advanced Stocks + Manual Management (AS+MM) only will be enabled onto all selected products where Advanced Stocks Management is disabled. They will then be associated to the selected warehouses.',
'Show help for Advanced stocks default activation'=>'Show help for Advanced stocks default activation',
'Help for Advanced stocks default activation'=>'Help for Advanced stocks default activation',
'Discount codes'=>'Discount codes',
'Title'=>'Title',
'Last delivery address'=>'Last delivery address',
'Convert'=>'Convert',
'Addresses'=>'Addresses',
'City'=>'City',
'Invoice?'=>'Invoice?',
'Delivery?'=>'Delivery?',
'Show Company, Reg. and SIC cols'=>'Show Company, Reg. and SIC cols',
'Dou you want to display these 3 cols in the customers grids?'=>'Dou you want to display these 3 cols in the customers grids?',
'Add all selected customers to all selected groups'=>'Add all selected customers to all selected groups',
'Remove all selected customers from all selected groups'=>'Remove all selected customers from all selected groups',
'Create a group:'=>'Create a group:',
'Create a new discount code'=>'Create a new discount code',
'customers'=>'customers',
'customer'=>'customer',
'Create discount code'=>'Create discount code',
'optimized grid loading'=>'optimized grid loading',
'Use an optimized grid display method for grids with more than 500 lines (by default). Set to 0 to disable the optimized display method.'=>'Use an optimized grid display method for grids with more than 500 lines (by default). Set to 0 to disable the optimized display method.',
'Orders status changed:'=>'Orders status changed:',
'staying.'=>'staying.',
'Dont\'t reload page.'=>'Dont\'t reload page.',
'Customer service'=>'Customer service',
'Global statistics'=>'Global statistics',
'By contact'=>'By contact',
'By status'=>'By status',
'Open'=>'Open',
'Waiting 1'=>'Waiting 1',
'Waiting 2'=>'Waiting 2',
'Closed'=>'Closed',
'By lang'=>'By lang',
'Total discussions'=>'Total discussions',
'Discussions pending'=>'Discussions pending',
'Total customer messages'=>'Total customer messages',
'Total employee messages'=>'Total employee messages',
'Unread discussions'=>'Unread discussions',
'Closed discussions'=>'Closed discussions',
'Advisor'=>'Advisor',
'Last Messages'=>'Last Messages',
'Answer'=>'Answer',
'Sender'=>'Sender',
'Private'=>'Private',
'Message forwarded to'=>'Message forwarded to',
'Comment:'=>'Comment:',
'You must write a message to send an answer'=>'You must write a message to send an answer',
'An error occured during file upload. Please try again.'=>'An error occured during file upload. Please try again.',
'Send'=>'Send',
'The message was successfully sent'=>'The message was successfully sent',
'Transfer to...'=>'Transfer to...',
'Please select a discussion'=>'Please select a discussion',
'Are you sure you want to delete the selected discussions?'=>'Are you sure you want to delete the selected discussions?',
'This discussion doesn\'t concern an order.'=>'This discussion doesn\'t concern an order.',
'Discussion history'=>'Discussion history',
'Validated Orders'=>'Validated Orders',
'Customer turnover'=>'Customer turnover',
'Read'=>'Read',
'Delete'=>'Delete',
'Last advisor'=>'Last advisor',
'Open discussions'=>'Open discussions',
'Order weight'=>'Order weight',
'Modify the order and close this window to refresh the grid'=>'Modify the order and close this window to refresh the grid',
'Total number of products'=>'Total number of products',
'Current stock'=>'Current stock',
'Your shops'=>'Your shops',
'images creation mode'=>'images creation mode',
'Possible values:<br/>0: the whole image is resized to fit the destination size with colored background (PS standard)<br/>1: the image is cropped to get a better resolution but you lose the image borders<br/>2: the whole image is resized to fit the destination size without colored background'=>'Possible values:<br/>0: the whole image is resized to fit the destination size with colored background (PS standard)<br/>1: the image is cropped to get a better resolution but you lose the image borders<br/>2: the whole image is resized to fit the destination size without colored background',
'share and activate product'=>'share and activate product',
'Set to 0 if you don\'t want to activate the product you are sharing in a new shop'=>'Set to 0 if you don\'t want to activate the product you are sharing in a new shop',
'MultiStores'=>'MultiStores',
'Update product?'=>'Update product?',
'Carriers'=>'Carriers',
'carriers'=>'carriers',
'carrier'=>'carrier',
'Synchronize the categories positions on multiple shops'=>'Synchronize the categories positions on multiple shops',
'You must select one shop to synchronize the positions'=>'You must select one shop to synchronize the positions',
'You must tick shops to synchronize the positions'=>'You must tick shops to synchronize the positions',
'Are you sure that you want to synchronize the positions of these categories:'=>'Are you sure that you want to synchronize the positions of these categories:',
'with'=>'with',
'Synchronize'=>'Synchronize',
'Error:'=>'Error:',
'It must have category'=>'It must have category',
'as parent category.'=>'as parent category.',
'Warning:'=>'Warning:',
'This category is not shared with the shop'=>'This category is not shared with the shop',
'Ready to synchronize positions.'=>'Ready to synchronize positions.',
'Current qty in stock'=>'Current qty in stock',
'Stock Transfert'=>'Stock Transfert',
'Stock Transfer'=>'Stock Transfer',
'Downloadable product'=>'Downloadable product',
'Display filename'=>'Display filename',
'Filename on the server'=>'Filename on the server',
'Upload date'=>'Upload date',
'Expiration date'=>'Expiration date',
'Nb days accessible'=>'Nb days accessible',
'Nb of authorized downloads'=>'Nb of authorized downloads',
'To add a downloadable file, you must select one product only.'=>'To add a downloadable file, you must select one product only.',
'This product already has a downloadable file.'=>'This product already has a downloadable file.',
'Upload file'=>'Upload file',
'Upload product file'=>'Upload product file',
'You must sélect a file to upload.'=>'You must sélect a file to upload.',
'To edit a downloadable file, you must select one row only.'=>'To edit a downloadable file, you must select one row only.',
'Edit product file'=>'Edit product file',
'Properties grid: Download product'=>'Properties grid: Download product',
'You want to synchronize the categories positions on the Shop'=>'You want to synchronize the categories positions on the Shop',
'Delete selected products from the list'=>'Delete selected products from the list',
'Save features positions'=>'Save features positions',
'Save positions'=>'Save positions',
'A virtual product cannot have combinations.'=>'A virtual product cannot have combinations.',
'A virtual product cannot use the advanced stock management.'=>'A virtual product cannot use the advanced stock management.',
'Add file'=>'Add file',
'Delete file'=>'Delete file',
'Edit file'=>'Edit file',
'Download file'=>'Download file',
'Add all the selected products to all the selected carriers'=>'Add all the selected products to all the selected carriers',
'Delete all the selected products to all the selected carriers'=>'Delete all the selected products to all the selected carriers',
'Column name'=>'Column name',
'margin'=>'margin',
'Are you sure that you want load all the fields in this mapping?'=>'Are you sure that you want load all the fields in this mapping?',
'Load all fields'=>'Load all fields',
'You can\'t manage the positions when there is a filter on a column'=>'You can\'t manage the positions when there is a filter on a column',
'Image\'s format to export'=>'Image\'s format to export',
'Put the image\'s format to export: default, large, small,.... Leave space to put the original format.'=>'Put the image\'s format to export: default, large, small,.... Leave space to put the original format.',
'Export data'=>'Export data',
'Lines to export'=>'Lines to export',
'Complete export of all products'=>'Complete export of all products',
'Use auto export'=>'Use auto export',
'This export is already in progress.'=>'This export is already in progress.',
'Export finished.'=>'Export finished.',
'Error during sc_export creation.'=>'Error during sc_export creation.',
'Exported files'=>'Exported files',
'Launch export every X seconds if possible'=>'Launch export every X seconds if possible',
'Number of the first lines to export into the CSV file'=>'Number of the first lines to export into the CSV file',
'An error occured during export. Please try again.'=>'An error occured during export. Please try again.',
'An error occured during last export with product'=>'An error occured during last export with product',
'Check this product before to try again.'=>'Check this product before to try again.',
'Check'=>'Check',
'Uncheck'=>'Uncheck',
'An error occured during last export with combination'=>'An error occured during last export with combination',
'from product'=>'from product',
'Check this combination before to try again.'=>'Check this combination before to try again.',
'An error occured during export.'=>'An error occured during export.',
'Insert item'=>'Insert item',
'Delete item'=>'Delete item',
'Selected shop'=>'Selected shop',
'Export in one step the products corresponding to the selected script'=>'Export in one step the products corresponding to the selected script',
'Export in several times the products corresponding to the selected script'=>'Export in several times the products corresponding to the selected script',
'Categories management'=>'Categories management',
'Products nb'=>'Products nb',
'SEO products nb'=>'SEO products nb',
'Are you sure you want to delete the selected images?'=>'Are you sure you want to delete the selected images?',
'You must upload an image using format png, gif, jpg or jpeg'=>'You must upload an image using format png, gif, jpg or jpeg',
'Update'=>'Update',
'Description length'=>'Description length',
'Name & description'=>'Name & description',
'Lang:'=>'Lang:',
'Edit description'=>'Edit description',
'To update description, you must select one row only.'=>'To update description, you must select one row only.',
'META title length'=>'META title length',
'META description length'=>'META description length',
'META keywords length'=>'META keywords length',
'Add all selected categories to all selected shops'=>'Add all selected categories to all selected shops',
'Delete all selected categories from all selected shops'=>'Delete all selected categories from all selected shops',
'Update all shops'=>'Update all shops',
'If enabled: update all shops when you edit a category'=>'If enabled: update all shops when you edit a category',
'Open this category in SC'=>'Open this category in SC',
'Put in the bin/trash'=>'Put in the bin/trash',
'Activate all products for the selected categories'=>'Activate all products for the selected categories',
'Deactivate all products for the selected categories'=>'Deactivate all products for the selected categories',
'and'=>'and',
'The products has been updated correctly.'=>'The products has been updated correctly.',
'Phone mobile'=>'Phone mobile',
'Advanced stock'=>'Advanced stock',
'Default type for Advanced Stock Management'=>'Default type for Advanced Stock Management',
'When Advanced Stock Management activated, define default type for a new produit.<br/>1: Disabled<br/>2: Enabled<br/>3: Enabled + Manual Mgmt'=>'When Advanced Stock Management activated, define default type for a new produit.<br/>1: Disabled<br/>2: Enabled<br/>3: Enabled + Manual Mgmt',
'specific prices'=>'specific prices',
'With Shop(s)'=>'With Shop(s)',
'Next step'=>'Next step',
'Last step'=>'Last step',
'Rounding prices up'=>'Rounding prices up',
'Rounding prices, possible values:<br/>0: Rounding up or down to the nearest 5 cents<br/>1:  Rounding up<br/>2: Rounding down<br/><a href="http://support.storecommander.com/entries/29662223-How-do-price-rounding-options-work-" target="_blank">Read more</a>'=>'Rounding prices, possible values:<br/>0: Rounding up or down to the nearest 5 cents<br/>1:  Rounding up<br/>2: Rounding down<br/><a href="http://support.storecommander.com/entries/29662223-How-do-price-rounding-options-work-" target="_blank">Read more</a>',
'Are you sure that you want to round all these prices up?'=>'Are you sure that you want to round all these prices up?',
'Rounding up the'=>'Rounding up the',
'Rounding the price up'=>'Rounding the price up',
'http://www.storecommander.com/redir.php?dest=20131127'=>'http://www.storecommander.com/redir.php?dest=20131127',
'Open this product in SC Catalog'=>'Open this product in SC Catalog',
'or'=>'or',
'No warehouse'=>'No warehouse',
'products ignored because no wholesale price is associated'=>'products ignored because no wholesale price is associated',
'stock - advanced stock mgmt.'=>'stock - advanced stock mgmt.',
'Product ean'=>'Product ean',
'Product upc'=>'Product upc',
'When importing a product with combinations:<br/>Possible values:<br/>0: The product EAN is not altered and each combination has its own EAN.<br/>1: The product EAN becomes the first combination EAN.'=>'When importing a product with combinations:<br/>Possible values:<br/>0: The product EAN is not altered and each combination has its own EAN.<br/>1: The product EAN becomes the first combination EAN.',
'When importing a product with combinations:<br/>Possible values:<br/>0: The product UPC is not altered and each combination has its own UPC.<br/>1: The product UPC becomes the first combination UPC.'=>'When importing a product with combinations:<br/>Possible values:<br/>0: The product UPC is not altered and each combination has its own UPC.<br/>1: The product UPC becomes the first combination UPC.',
'Customers grid: minimum order date'=>'Customers grid: minimum order date',
'Allows to display the orders spent from this date.<br/>Required format : YYYY-MM-DD'=>'Allows to display the orders spent from this date.<br/>Required format : YYYY-MM-DD',
'Action: Delete all product features'=>'Action: Delete all product features',
'An error occured during reset. Please try again.'=>'An error occured during reset. Please try again.',
'The export was successfully reset.'=>'The export was successfully reset.',
'Load fields by name'=>'Load fields by name',
'Reset'=>'Reset',
'From'=>'From',
'to'=>'to',
'Select the date interval to filter'=>'Select the date interval to filter',
'From:'=>'From:',
'To:'=>'To:',
'Close'=>'Close',
'You must write the two dates.'=>'You must write the two dates.',
'Your dates are wrong.'=>'Your dates are wrong.',
'Clear all grids preferences for orders'=>'Clear all grids preferences for orders',
'Clear all grids preferences for customers'=>'Clear all grids preferences for customers',
'Truncate'=>'Truncate',
'Enter "ok" and click "Validate" (?) to empty the warehouse.'=>'Enter "ok" and click "Validate" (?) to empty the warehouse.',
'This warehouse was successfully truncated'=>'This warehouse was successfully truncated',
'An error occured during truncate'=>'An error occured during truncate',
'Modify and create customers/addresses'=>'Modify and create customers/addresses',
'Modify customers/addresses'=>'Modify customers/addresses',
'Create new customer'=>'Create new customer',
'If customer with same identifier found in database'=>'If customer with same identifier found in database',
'Customers are identified by'=>'Customers are identified by',
'id_customer+address title'=>'id_customer+address title',
'Wrong mapping, mapping should contain the id_customer field'=>'Wrong mapping, mapping should contain the id_customer field',
'Wrong mapping, mapping should contain the email field'=>'Wrong mapping, mapping should contain the email field',
'Wrong mapping, mapping should contain the address title field'=>'Wrong mapping, mapping should contain the address title field',
'Wrong mapping, mapping should contain the id_customer and the address title fields'=>'Wrong mapping, mapping should contain the id_customer and the address title fields',
'Email can\'t be empty to create a customer: line n°'=>'Email can\'t be empty to create a customer: line n°',
'New customers:'=>'New customers:',
'Modified customers:'=>'Modified customers:',
'All customers have been imported. The TODO file is deleted.'=>'All customers have been imported. The TODO file is deleted.',
'All customers have been imported.'=>'All customers have been imported.',
'There are still customers to be imported in the working file. It can mean errors you need to correct or lines which have been ignored on purpose. Once corrections have been made, click again on the import icon to proceed further.'=>'There are still customers to be imported in the working file. It can mean errors you need to correct or lines which have been ignored on purpose. Once corrections have been made, click again on the import icon to proceed further.',
'Update customers older than'=>'Update customers older than',
'Wrong mapping, mapping should contain the email and the address title fields'=>'Wrong mapping, mapping should contain the email and the address title fields',
'Wrong mapping, mapping should contain the id_customer and the id_address fields'=>'Wrong mapping, mapping should contain the id_customer and the id_address fields',
'Wrong mapping, mapping should contain the id_address field'=>'Wrong mapping, mapping should contain the id_address field',
'Wrong mapping, mapping should contain the email and the id_address fields'=>'Wrong mapping, mapping should contain the email and the id_address fields',
'Transfert stock between two warehouses'=>'Transfert stock between two warehouses',
'Transfert'=>'Transfert',
'You want to transfert all stock from warehouse'=>'You want to transfert all stock from warehouse',
'to warehouse :'=>'to warehouse :',
'The stock was successfully transfered'=>'The stock was successfully transfered',
'An error occured during transfer'=>'An error occured during transfer',
'My address'=>'My address',
'Modified customers'=>'Modified customers',
'The customer has been imported but there is a problem with his address'=>'The customer has been imported but there is a problem with his address',
'Gender'=>'Gender',
'Password'=>'Password',
'Birthday'=>'Birthday',
'Partner offers'=>'Partner offers',
'Private notes'=>'Private notes',
'Website'=>'Website',
'title'=>'title',
'country'=>'country',
'state'=>'state',
'company'=>'company',
'lastname'=>'lastname',
'firstname'=>'firstname',
'address 1'=>'address 1',
'address 2'=>'address 2',
'postcode'=>'postcode',
'city'=>'city',
'other'=>'other',
'phone'=>'phone',
'phone mobile'=>'phone mobile',
'id_customer + address title'=>'id_customer + address title',
'email + address title'=>'email + address title',
'Caution! This action will delete all stock and movements for destination warehouse!'=>'Caution! This action will delete all stock and movements for destination warehouse!',
'Action: Delete all customers'=>'Action: Delete all customers',
'Action: Delete all addresses'=>'Action: Delete all addresses',
'Action: Regenerate all passwords'=>'Action: Regenerate all passwords',
'Opt-in (Partners offers)'=>'Opt-in (Partners offers)',
'New groups:'=>'New groups:',
'Last order'=>'Last order',
'last order'=>'last order',
'Combinations (copy/paste)'=>'Combinations (copy/paste)',
'Product ref'=>'Product ref',
'This product cannot be duplicated because of a Prestashop malfunction.'=>'This product cannot be duplicated because of a Prestashop malfunction.',
'The product must exist in the default store to be duplicated.'=>'The product must exist in the default store to be duplicated.',
'Short description can\'t include an iframe or is invalid'=>'Short description can\'t include an iframe or is invalid',
'Description can\'t include an iframe or is invalid'=>'Description can\'t include an iframe or is invalid',
'Short description is invalid'=>'Short description is invalid',
'Description is invalid'=>'Description is invalid',
'Always use category name as link rewrite'=>'Always use category name as link rewrite',
'Possible values:<br/>0: SC will NOT modifiy the link_rewrite of the category: you should set it yourself.<br/>1: SC always set the link_rewrite url to the name of the category.'=>'Possible values:<br/>0: SC will NOT modifiy the link_rewrite of the category: you should set it yourself.<br/>1: SC always set the link_rewrite url to the name of the category.',
'Associate only products in Advanced stocks'=>'Associate only products in Advanced stocks',
'Activate Advanced stocks and Associate'=>'Activate Advanced stocks and Associate',
'Activate Advanced stocks + manual mgmt and Associate'=>'Activate Advanced stocks + manual mgmt and Associate',
'login as selected customer on the front office'=>'login as selected customer on the front office',
'Color code'=>'Color code',
'Add all selected combinations to all selected shop'=>'Add all selected combinations to all selected shop',
'Delete all selected combinations from all selected shop'=>'Delete all selected combinations from all selected shop',
'To run exports, you need to install samples files from Tools > Installation'=>'To run exports, you need to install samples files from Tools > Installation',
'Notice for the automatic modification of products link_rewrite'=>'Notice for the automatic modification of products link_rewrite',
'Possible values:<br/>0: don\'t trigger alert<br/>1: display alert message after name edition'=>'Possible values:<br/>0: don\'t trigger alert<br/>1: display alert message after name edition',
'Caution: The option located in Prestashop > Products > Force update of friendly URL is set to NO: friendly url will not be saved automatically. To stop this alert: SC  > Tools > Settings > Alert.'=>'Caution: The option located in Prestashop > Products > Force update of friendly URL is set to NO: friendly url will not be saved automatically. To stop this alert: SC  > Tools > Settings > Alert.',
'Notice when Advanced stock preference in PS is different to SC preference'=>'Notice when Advanced stock preference in PS is different to SC preference',
'Possible values:<br/>0: don\'t trigger alert<br/>1: display alert message at SC load.'=>'Possible values:<br/>0: don\'t trigger alert<br/>1: display alert message at SC load.',
'Caution: The option located in Prestashop > Products > New products use advanced stock management is set to Yes but in SC the preference Default type for Advanced Stock Management is set to disabled. To stop this alert: SC  > Tools > Settings > Alert.'=>'Caution: The option located in Prestashop > Products > New products use advanced stock management is set to Yes but in SC the preference Default type for Advanced Stock Management is set to disabled. To stop this alert: SC  > Tools > Settings > Alert.',
'Segment:'=>'Segment:',
'of segment'=>'of segment',
'& segments'=>'& segments',
'Use the Prestashop hook when quantity updated'=>'Use the Prestashop hook when quantity updated',
'Suppliers'=>'Suppliers',
'Add all selected products to all selected suppliers'=>'Add all selected products to all selected suppliers',
'Remove all selected products from all selected suppliers'=>'Remove all selected products from all selected suppliers',
'suppliers'=>'suppliers',
'Price excl. tax'=>'Price excl. tax',
'Product/combination'=>'Product/combination',
'Default values display products/combinations grids'=>'Default values display products/combinations grids',
'currency (supplier price)'=>'currency (supplier price)',
'quantity - add'=>'quantity - add',
'quantity - remove'=>'quantity - remove',
'products ignored (no wholesale price is associated, incorrect quantity, etc)'=>'products ignored (no wholesale price is associated, incorrect quantity, etc)',
'See shop'=>'See shop',
'Color the combinations in red with same attributes'=>'Color the combinations in red with same attributes',
'Color the row in red when some combinations have the same attributes values.'=>'Color the row in red when some combinations have the same attributes values.',
'Add all selected categories to all selected groups'=>'Add all selected categories to all selected groups',
'Delete all selected categories from all selected groups'=>'Delete all selected categories from all selected groups',
'Customer groups'=>'Customer groups',
'Copy structure'=>'Copy structure',
'Paste categories'=>'Paste categories',
'Discounts and margins'=>'Discounts and margins',
'supplier - default'=>'supplier - default',
'Margin amount after reduction'=>'Margin amount after reduction',
'Margin % after reduction'=>'Margin % after reduction',
'Margin after reduction'=>'Margin after reduction',
'Price tax excl after reduction'=>'Price tax excl after reduction',
'Price tax incl after reduction'=>'Price tax incl after reduction',
'Notice when create first combi'=>'Notice when create first combi',
'Possible values:<br/>0: don\'t trigger alert<br/>1: display alert.'=>'Possible values:<br/>0: don\'t trigger alert<br/>1: display alert.',
'Caution: The first combination needs to be created by SC so that you can then add more in bulk. To stop this alert: SC  > Tools > Settings > Alert.'=>'Caution: The first combination needs to be created by SC so that you can then add more in bulk. To stop this alert: SC  > Tools > Settings > Alert.',
'Product price excl. tax'=>'Product price excl. tax',
'Quantity orded'=>'Quantity orded',
'Qty in stock at time of order'=>'Qty in stock at time of order',
'Insufficient current total stock'=>'Insufficient current total stock',
'Partial'=>'Partial',
'If enabled: Images will be automatically uploaded once selected'=>'If enabled: Images will be automatically uploaded once selected',
'Display deleted accounts'=>'Display deleted accounts',
'Set to 1 if you want to display deleted accounts in customer views'=>'Set to 1 if you want to display deleted accounts in customer views',
'Remove link between selected accessories and selected products'=>'Remove link between selected accessories and selected products',
'Action for new products'=>'Action for new products',
'Action for existing products'=>'Action for existing products',
'Modify product'=>'Modify product',
'Create duplication'=>'Create duplication',
'Skipped'=>'Skipped',
'Modified'=>'Modified',
'Created as duplication'=>'Created as duplication',
'!!! WARNING !!!'=>'!!! WARNING !!!',
'visibility'=>'visibility',
'Inv.'=>'Inv.',
'Last invoice address'=>'Last invoice address',
'Shipping n°'=>'Shipping n°',
'Picking'=>'Picking',
'Orders and products'=>'Orders and products',
'Alert: You need to select only one customer'=>'Alert: You need to select only one customer',
'Customer grid: Light view'=>'Customer grid: Light view',
'Customer grid: Large view'=>'Customer grid: Large view',
'Customer grid: Address view'=>'Customer grid: Address view',
'Customer grid: Convert view'=>'Customer grid: Convert view',
'default order grid view'=>'default order grid view',
'default customer grid view'=>'default customer grid view',
'Set order grid view displayed when you launch SC. (grid_light, grid_large, grid_picking, grid_delivery)'=>'Set order grid view displayed when you launch SC. (grid_light, grid_large, grid_picking, grid_delivery)',
'Set customer grid view displayed when you launch SC. (grid_light, grid_large, grid_address, grid_convert)'=>'Set customer grid view displayed when you launch SC. (grid_light, grid_large, grid_address, grid_convert)',
'for'=>'for',
'Newsletter'=>'Newsletter',
'Create new customer with the PrestaShop form'=>'Create new customer with the PrestaShop form',
'Create the new customer and close this window to refresh the grid'=>'Create the new customer and close this window to refresh the grid',
'Download PDF invoices from selected orders'=>'Download PDF invoices from selected orders',
'Download PDF delivery slips from selected orders'=>'Download PDF delivery slips from selected orders',
'Download'=>'Download',
'Valid'=>'Valid',
'Invoice'=>'Invoice',
'Gift'=>'Gift',
'Gift message'=>'Gift message',
'Recyclable'=>'Recyclable',
'Convertion rate'=>'Convertion rate',
'Total discounts'=>'Total discounts',
'Total discounts inc. tax'=>'Total discounts inc. tax',
'Total discounts exc. tax'=>'Total discounts exc. tax',
'Total paid  real'=>'Total paid  real',
'Total paid inc. tax'=>'Total paid inc. tax',
'Total paid exc. tax'=>'Total paid exc. tax',
'Total products'=>'Total products',
'Total products inc. tax'=>'Total products inc. tax',
'Total shipping'=>'Total shipping',
'Total shipping inc. tax'=>'Total shipping inc. tax',
'Total shipping exc. tax'=>'Total shipping exc. tax',
'Total wrapping'=>'Total wrapping',
'Total wrapping inc. tax'=>'Total wrapping inc. tax',
'Total wrapping exc. tax'=>'Total wrapping exc. tax',
'Carrier tax rate'=>'Carrier tax rate',
'Delivery slip'=>'Delivery slip',
'Conversion rate'=>'Conversion rate',
'Qty in stock'=>'Qty in stock',
'Qty refunded'=>'Qty refunded',
'Qty returned'=>'Qty returned',
'Customer not enabled on shop.'=>'Customer not enabled on shop.',
'3 days'=>'3 days',
'Customers groups are available from Prestashop 1.2'=>'Customers groups are available from Prestashop 1.2',
'Properties grid: Groups'=>'Properties grid: Groups',
'Properties grid: Messages'=>'Properties grid: Messages',
'Properties grid: Orders'=>'Properties grid: Orders',
'Properties grid: Addresses'=>'Properties grid: Addresses',
'Properties grid: Customers'=>'Properties grid: Customers',
'Order grid: Light view'=>'Order grid: Light view',
'Order grid: Large view'=>'Order grid: Large view',
'Order grid: Delivery view'=>'Order grid: Delivery view',
'Order grid: Picking'=>'Order grid: Picking',
'Properties grid: Order history'=>'Properties grid: Order history',
'Properties grid: Products'=>'Properties grid: Products',
'maximum customers displayed'=>'maximum customers displayed',
'Mr.'=>'Mr.',
'Ms.'=>'Ms.',
'Miss'=>'Miss',
'Unk.'=>'Unk.',
'Set the maximum number of customers displayed in the main grid. You can increase this value if your server\'s ressources are sufficient.'=>'Set the maximum number of customers displayed in the main grid. You can increase this value if your server\'s ressources are sufficient.',
'Do you want to delete all items?'=>'Do you want to delete all items?',
'Del.'=>'Del.',
'Cart language'=>'Cart language',
'Total paid (module)'=>'Total paid (module)',
'Invoice date'=>'Invoice date',
'Delivery slip date'=>'Delivery slip date',
'The image is created as a progressive JPEG.'=>'The image is created as a progressive JPEG.',
'JPG progressive'=>'JPG progressive',
'Wrong mapping, mapping should contain the EAN field'=>'Wrong mapping, mapping should contain the EAN field',
'Wrong mapping, mapping should contain the UPC field'=>'Wrong mapping, mapping should contain the UPC field',
'EAN'=>'EAN',
'UPC'=>'UPC',
'login as selected customer'=>'login as selected customer',
'Use this compatibility mode if "Login as selected customer" does not work on the front office'=>'Use this compatibility mode if "Login as selected customer" does not work on the front office',
'Accounting'=>'Accounting',
'Quick Accounting'=>'Quick Accounting',
'available date'=>'available date',
'VAT Scheduler'=>'VAT Scheduler',
'Export orders'=>'Export orders',
'attribute of combination - color'=>'attribute of combination - color',
'Grids editor'=>'Grids editor',
'Date of account creation'=>'Date of account creation',
'The customer has been imported but there is a problem with his address.'=>'The customer has been imported but there is a problem with his address.',
'You need to check these fields on the first line of'=>'You need to check these fields on the first line of',
'More information about the address'=>'More information about the address',
'Use SC extensions'=>'Use SC extensions',
'Enable/Disable SC Extensions'=>'Enable/Disable SC Extensions',
'additional shipping cost'=>'additional shipping cost',
'Use global.css in descriptions'=>'Use global.css in descriptions',
'Use the global.css stylesheet of the shop in the editors. You can set it to 0 if the background of your shop is displayed in the text editors.'=>'Use the global.css stylesheet of the shop in the editors. You can set it to 0 if the background of your shop is displayed in the text editors.',
'additional_shipping_cost'=>'additional_shipping_cost',
'Only products associated to supplier <strong>%s</strong> will be updated.'=>'Only products associated to supplier <strong>%s</strong> will be updated.',
'<strong>%s</strong> lines of file <strong>"%s"</strong> will be imported.'=>'<strong>%s</strong> lines of file <strong>"%s"</strong> will be imported.',
'The mapping <strong>"%s"</strong> will be used.'=>'The mapping <strong>"%s"</strong> will be used.',
'Products will be identified by <strong>%s</strong>.'=>'Products will be identified by <strong>%s</strong>.',
'Action for new products: <strong>%s</strong>.'=>'Action for new products: <strong>%s</strong>.',
'Action for existing products: <strong>%s</strong>.'=>'Action for existing products: <strong>%s</strong>.',
'The categories will be created automatically and products associated to them.'=>'The categories will be created automatically and products associated to them.',
'Elements found in the CSV file will be created automatically: features, combination attributes, manufacturers, suppliers, tags.'=>'Elements found in the CSV file will be created automatically: features, combination attributes, manufacturers, suppliers, tags.',
'Is your import ready? See the Checklist!'=>'Is your import ready? See the Checklist!',
'Field/Value separators selected in your configuation do not seem to match your CSV file. Check your settings.'=>'Field/Value separators selected in your configuation do not seem to match your CSV file. Check your settings.',
'An action needs to be selected before importing.'=>'An action needs to be selected before importing.',
'If this import will create combinations, only the first combination of each product will be created. To avoid this, select "modify product" in "Action for existing products".'=>'If this import will create combinations, only the first combination of each product will be created. To avoid this, select "modify product" in "Action for existing products".',
'If this import will add new combinations, a product will be created for each line corresponding to a combination. To avoid this select "modify product" in "Action for existing products".'=>'If this import will add new combinations, a product will be created for each line corresponding to a combination. To avoid this select "modify product" in "Action for existing products".',
'Lines of your CSV file do not use the correct number of columns, please check your file. Alternatively, this can be caused by descriptions spread on multiple lines.'=>'Lines of your CSV file do not use the correct number of columns, please check your file. Alternatively, this can be caused by descriptions spread on multiple lines.',
'You should use the tool "check and fix the level_depth field" from the Catalog > Tools menu to fix your categories.'=>'You should use the tool "check and fix the level_depth field" from the Catalog > Tools menu to fix your categories.',
'Sort by position'=>'Sort by position',
'condition (new, used, refurbished)'=>'condition (new, used, refurbished)',
'Columns'=>'Columns',
'Properties of selected combinations'=>'Properties of selected combinations',
'Before'=>'Before',
'On'=>'On',
'After'=>'After',
'Unlimited'=>'Unlimited',
'Manufacturers'=>'Manufacturers',
'Countries'=>'Countries',
'If enabled: display specific prices from all subcategories'=>'If enabled: display specific prices from all subcategories',
'Validate'=>'Validate',
'Specific prices on this date'=>'Specific prices on this date',
'Display specific prices for combinations'=>'Display specific prices for combinations',
'You must select only one line.'=>'You must select only one line.',
'Permanently delete the selected special prices ?'=>'Permanently delete the selected special prices ?',
'Multiple actions are currently running. Do you really want to close?'=>'Multiple actions are currently running. Do you really want to close?',
'Tasks'=>'Tasks',
'Tasks error logs'=>'Tasks error logs',
'An error has occured:'=>'An error has occured:',
'Check the logs to see the modification that triggered the error as well as other that could not be applied'=>'Check the logs to see the modification that triggered the error as well as other that could not be applied',
'See logs'=>'See logs',
'An error has occured when inserting'=>'An error has occured when inserting',
'Row'=>'Row',
'Delete all history'=>'Delete all history',
'Run the task again'=>'Run the task again',
'Are you sure you want to delete these rows?'=>'Are you sure you want to delete these rows?',
'Are you sure you want to run these tasks again?'=>'Are you sure you want to run these tasks again?',
'actions'=>'actions',
'Your import includes \'description\' and/or \'short description\', please check carriage returns.'=>'Your import includes \'description\' and/or \'short description\', please check carriage returns.',
'http://support.storecommander.com/entries/22130421-Importing-your-description-efficiently'=>'http://support.storecommander.com/entries/22130421-Importing-your-description-efficiently',
'Number of tasks sent to the server'=>'Number of tasks sent to the server',
'Number of tasks sent similtaneously to be actioned by the server'=>'Number of tasks sent similtaneously to be actioned by the server',
'Open in SC catalog'=>'Open in SC catalog',
'Update date'=>'Update date',
'This will set the cheapest combination in stock as default combination for each seleted product. Continue?'=>'This will set the cheapest combination in stock as default combination for each seleted product. Continue?',
'Set the cheapest combination in stock as default combination'=>'Set the cheapest combination in stock as default combination',
'Display all descriptions'=>'Display all descriptions',
'Automatically share the products images'=>'Automatically share the products images',
'Automatically share the products images when the product is shared with a new shop. Set the option to 0 if you do not wish to share images automatically.'=>'Automatically share the products images when the product is shared with a new shop. Set the option to 0 if you do not wish to share images automatically.',
'Use TinyMCE as text editor'=>'Use TinyMCE as text editor',
'Set this option to 1 if you want to use TinyMCE rather than CKeditor (default).'=>'Set this option to 1 if you want to use TinyMCE rather than CKeditor (default).',
'default order properties panel'=>'default order properties panel',
'Set order properties panel displayed when you launch SC. (orderproduct,message,orderhistory,orderpsorderpage)'=>'Set order properties panel displayed when you launch SC. (orderproduct,message,orderhistory,orderpsorderpage)',
'default customer properties panel'=>'default customer properties panel',
'Set customer properties panel displayed when you launch SC. (customerorder,message,customergroup,customeraddress)'=>'Set customer properties panel displayed when you launch SC. (customerorder,message,customergroup,customeraddress)',
'open browser tab'=>'open browser tab',
'Set to 1 if you wish to open PS windows in a new browser tab instead of SC window. (forced to 1 if SC is run on iPad)'=>'Set to 1 if you wish to open PS windows in a new browser tab instead of SC window. (forced to 1 if SC is run on iPad)',
'Store Commander cannot create the table %s, please contact your hosting support and ask: Can you please confirm that the MySQL user has the necessary permission to execute these commands: SHOW and CREATE TABLE. Upon confirmation, you can restart Store Commander.'=>'Store Commander cannot create the table %s, please contact your hosting support and ask: Can you please confirm that the MySQL user has the necessary permission to execute these commands: SHOW and CREATE TABLE. Upon confirmation, you can restart Store Commander.',
'price'=>'price',
'prices'=>'prices',
'Status update'=>'Status update',
'Ord from'=>'Ord from',
'Inv from'=>'Inv from',
'Action: Dissociate carriers'=>'Action: Dissociate carriers',
'New products default status'=>'New products default status',
'Active status used when the product is created by CSV import'=>'Active status used when the product is created by CSV import',
'Export EAN13'=>'Export EAN13',
'When exporting EAN13 for combinations:<br/>Possible values:<br/>0: the exported EAN13 is associated to the combinations.<br/>1: the exported EAN13 is associated to the product if EAN13 is empty for combinations.'=>'When exporting EAN13 for combinations:<br/>Possible values:<br/>0: the exported EAN13 is associated to the combinations.<br/>1: the exported EAN13 is associated to the product if EAN13 is empty for combinations.',
'Choose a standard message'=>'Choose a standard message',
'Do you want to overwrite your existing message?'=>'Do you want to overwrite your existing message?',
'Products position'=>'Products position',
'Vat number'=>'Vat number',
'Delivery date'=>'Delivery date',
'Delivery date - Standard'=>'Delivery date - Standard',
'Delivery date - Limit'=>'Delivery date - Limit',
'Clear warehouse (keep history)'=>'Clear warehouse (keep history)',
'Clear warehouse'=>'Clear warehouse',
'physical stock'=>'physical stock',
'available stock'=>'available stock',
'live stock'=>'live stock',
'Number of decimal to export prices'=>'Number of decimal to export prices',
'Possible values: 0 to 6'=>'Possible values: 0 to 6',
'At least one shop needs to be ticked'=>'At least one shop needs to be ticked',
'Properties grid: Warehouses'=>'Properties grid: Warehouses',
'Group rows by product'=>'Group rows by product',
'Includes access to \'Advanced Stocks\' and \'Warehouses\' grids, and actions on quantities fields and in the \'Warehouses\' panel'=>'Includes access to \'Advanced Stocks\' and \'Warehouses\' grids, and actions on quantities fields and in the \'Warehouses\' panel',
'Would you like to dissociate all products from warehouse'=>'Would you like to dissociate all products from warehouse',
'Upload images'=>'Upload images',
'display subcategories count'=>'display subcategories count',
'Possible values:<br/>0: no count displayed (best display performance)<br/>1: count displayed (best user experience)'=>'Possible values:<br/>0: no count displayed (best display performance)<br/>1: count displayed (best user experience)',
'Tips'=>'Tips',
'Open Tips'=>'Open Tips',
'Find all tips in Help &gt; Tips'=>'Find all tips in Help &gt; Tips',
'Action: Dissociate warehouses'=>'Action: Dissociate warehouses',
'You are trying to add quantities to a product with combinations: product ID '=>'You are trying to add quantities to a product with combinations: product ID ',
'Product grid:'=>'Product grid:',
'Customer grid:'=>'Customer grid:',
'Order grid:'=>'Order grid:',
'Are you sure you want to dissociate the selected items?'=>'Are you sure you want to dissociate the selected items?'
);
?>