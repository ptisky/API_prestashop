<?php 

if (!defined('_PS_VERSION_'))
	exit;

class PFUpSell extends Module{
	
	public $upsell_on;
	public $upsell_left_col;
	public $upsell_right_col;

	public function __construct()
	{
		$this->name = 'pfupsell';
		$this->tab  = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'PrestaFan';
		$this->need_instance = 0;
		if (floatval(Tools::substr(_PS_VERSION_, 0, 3)) >= 1.6)
			$this->bootstrap = true;
		parent::__construct();

		$this->_globalVars();
		$this->displayName = $this->l('UpSell');
		$this->description = $this->l('Sell related products easily.');	 
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
	}
	
	public function install()
	{   
		if (!parent::install() 
			|| !$this->registerHook('header')
			|| !Configuration::updateValue('PF_UPSELL_ON', '1')								
			|| !Configuration::updateValue('PF_UPSELL_LEFT_COL', '1')								
			|| !Configuration::updateValue('PF_UPSELL_RIGHT_COL', '0')								
			|| !Configuration::updateValue('PF_UPSELL_PRODUCTS', serialize(array()))				
				
		)
			return false;		 
		return true;
	}
	
	public function uninstall()
	{
		if (!parent::uninstall())
			return false;		 
	   return true;
	}
	
	private function _globalVars()
	{
		$this->upsell_on       = Configuration::get('PF_UPSELL_ON');	
		$this->upsell_left_col = Configuration::get('PF_UPSELL_LEFT_COL');	
		$this->upsell_right_col = Configuration::get('PF_UPSELL_RIGHT_COL');	
	}
	
	private function isSelectedShopGroup()
	{
		if (Shop::getContext() == Shop::CONTEXT_GROUP || Shop::getContext() == Shop::CONTEXT_ALL)
			return true;
		else
			return false;
	}	
		
	public function getVersion()
	{
		return floatval(Tools::substr(_PS_VERSION_, 0, 3));
	}
	
	public function _displayForm()
	{	
		$html = '';
		$psv = $this->getVersion();
		if (!$this->isSelectedShopGroup())
		{
			$html .= ' 
				<form class = "form-horizontal defaultForm" method = "post" action = "'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" '.($psv < 1.6?'id="product_form"':'').'>';
					if ($psv < 1.6)
						include (_PS_MODULE_DIR_.$this->name.'/admin/settings1.5.php');										
					else
						include (_PS_MODULE_DIR_.$this->name.'/admin/settings.php');							
			$html .= '	
				</form>';
				if ($psv <= 1.6)
						$this->context->controller->addCSS($this->_path.'views/css/admin.css', 'all');
				$this->context->controller->addJS(($this->_path).'views/js/upsell.js');
			return $html;
		}else
		{
			$html .= '<p class="alert alert-warning">'.
						$this->l('You cannot manage the module from a "All Shops" or a "Group Shop" context, select directly the shop you want to edit').
					'</p>';
			return $html;
			
		}
	}
	
	public function _postProcess()
	{	
		if (Tools::isSubmit('submit_upsell'))
		{
			Configuration::updateValue('PF_UPSELL_ON', (bool)(Tools::getValue('show_upsell_info')));
			Configuration::updateValue('PF_UPSELL_LEFT_COL', (bool)(Tools::getValue('left_col_info')));
			Configuration::updateValue('PF_UPSELL_RIGHT_COL', (bool)(Tools::getValue('right_col_info')));
		}
	}
	
	public function getContent()
	{
		if (Tools::isSubmit('submit_upsell'))
			$this->_postProcess();               					
		$this->_globalVars();			
		return $this->_displayForm();		 
	}
	
	public function hookHeader()
	{	
		$controller = Dispatcher::getInstance()->getController();
		if ($controller != "order" && $controller != "orderopc" && $controller != "upsell" && $this->upsell_on)
		{
			return $this->display(__FILE__, 'header.tpl');
		}
	}	
}	
?>