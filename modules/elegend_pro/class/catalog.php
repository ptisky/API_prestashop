<?php
/**
*
*  @package e-legend pro
*  @author    Abouloula Taoufik - E-LEGEND PRO
*  @version    1.0
*  @copyright Copyright (c) Abouloula Taoufik - E-LEGEND PRO
*
*/
include_once('product.php');
include_once('state.php');

class Catalog
{
	
   
	public function getApi()
	{
		return Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'pro_elegend_api');

	}
	
	public function getTax()
	{
		
		$products= new Product();
		$upc = $products->getParamEl('EL_UPC');
		$all_tax = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('SELECT `id_tax_rules_group`, `name`
																	FROM  `'._DB_PREFIX_.'tax_rules_group`');
																	
		$tax_default_name = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT trg.`name`  FROM  `'._DB_PREFIX_.'tax_rules_group` trg
																	  INNER JOIN `'._DB_PREFIX_.'product` p ON trg.id_tax_rules_group = p.id_tax_rules_group
																	  WHERE p.upc='.$upc);
		$response = '';
		//$tax_PS = TaxRulesGroup::getTaxRulesGroups();
		$response .= '<p>Taxe affectée sur les produits E-LEGEND : <span style="color:blue">'.$tax_default_name.'</span></p>';
		$i = 0;
		$response .= '<select name="tax_ps" style="width: 34%;">';
		foreach ($all_tax as $tax)
		{
			
			$response .= '<option value="'.$tax['id_tax_rules_group'].'"> '.$tax['name'].' </option>';
			
			$i++;
		}
		$response .= '</select>';
		return $response;
	}
	
	public function updateTax()
	{
		
		$products= new Product();
		$upc = $products->getParamEl('EL_UPC');
		$id_tax_rules_group = Tools::getValue('tax_ps');
		if ($id_tax_rules_group != '')
		{
			
					$sql = Db::getInstance()->ExecuteS('UPDATE `'._DB_PREFIX_.'product`
												SET `id_tax_rules_group` = '.(int)$id_tax_rules_group.'
												WHERE `upc` = '.$upc);
												
					$sql = Db::getInstance()->ExecuteS('UPDATE `'._DB_PREFIX_.'product_shop`
												SET `id_tax_rules_group` = '.(int)$id_tax_rules_group.'
												WHERE id_product IN ( SELECT id_product FROM  `'._DB_PREFIX_.'product` WHERE `upc` = '.$upc.')');
												
												
				
			
		}
	}
	
	
	public function getNewProduct($condition)
	{
		
		
		$result = '';

	   try
	    {	

		
		$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); 
		$opt = array('resource' => 'products/?display=full&filter[condition]='.$condition.'&filter[active]=1');
		$xml = $webService->get($opt);      	
		//$opt = array('url' => PS_SHOP_PATH . '/api/products/?display=full&filter[condition]='.$condition.'&filter[active]=1');			
		//$xml = $webService->get($opt);	
		$resources = $xml->children()->children();	
		
		
		if (isset($resources) && count($resources) > 0)
		{


			$result .= '<table id="list_order" class="table" style="margin-bottom: 20px;">';
			$result .= '<tr>';
			$result .= '<th>ID</th>';
			$result .= '<th>Image</th>';
			$result .= '<th>Nom</th>';
			$result .= '<th>Description</th>';
			$result .= '<th>ean13</th>';
			$result .= '<th>Fabricant</th>';
			$result .= '<th>Date d&acute;ajout</th>';
			
			if($condition == "used")
			$result .= '<th>Action</th>';
			$result .= '</tr>';
           
			$id = 1; 
			foreach ($resources as $resource)
		    {			

			
				$sql = Db::getInstance()->getValue('SELECT count(reference) FROM '._DB_PREFIX_.'product where reference LIKE "%'.$resource->reference.'%"');
				$url = PS_SHOP_PATH.'api/images/products/'.$resource->id.'/'.$resource->id_default_image;
				if($sql == 0){

				$result .= '<tr id="tr_id_product_'.$resource->id.'">';
				$result .= '<td>'.$id.'</td>';
				$result .= '<td><img src="'.$url.'" width="50px" height="50px" /></td>';
				$result .= '<td>'.pSQL($resource->name->language[0][0]).'</td>';
				$result .= '<td>'.pSQL($resource->description_short->language[0][0]).'</td>';
				$result .= '<td>'.$resource->ean13.'</td>';
				$result .= '<td>'.$resource->manufacturer_name.'</td>';
				$result .= '<td>'.date("d/m/Y",strtotime($resource->date_upd)).'</td>';
				if($condition == "used")
				$result .= '<td><a href="#" onclick="addNewProduct'.$resource->id.'();">Ajouter</a></td>';				
				$result .= '<input type="hidden" name="id_product" id="id_product'.$resource->id.'" value="'.$resource->id.'" />';
				$result .= '<input type="hidden" name="id_category" id="id_category'.$resource->id.'" value="'.$resource->id_category_default.'" />';
				$result .= '<input type="hidden" name="reference" id="reference'.$resource->id.'" value="'.$resource->reference.'" />';
				$result .= '</tr>';
				$result .= '<script type="text/javascript">
			           function addNewProduct'.$resource->id.'() {
					  
					   var id_product = $("#id_product'.$resource->id.'").val();
					   var id_category = $("#id_category'.$resource->id.'").val();
					    var reference = $("#reference'.$resource->id.'").val();
                       $.ajax({
                       type: "POST",
                       url: "../modules/elegend_pro/class/add_product.php",
                       data: { registration: "success", id_product: id_product, id_category: id_category, reference: reference },
					   dataType: "json",
                       beforeSend: function(data) { 
                       $(".theform").html("Importation en cours");

                      },
                        success: function(data) {					
                        $(".theform").html(data[0]);						
						if(data[1] == "1"){
						$("#tr_id_product_'.$resource->id.'").hide();
						}else{
						$("#tr_id_product_'.$resource->id.'").show();
						}
                        },
						error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                        }
                         });
                       }
					   </script>';
				  
				}
				$id++;
			}
			$result .= '</table>';
			$result .= '<div class="theform"></div>';
			
			
		}
		else
			$result .= 'Aucun nouveau produit';
				 
		 return $result;
		
	   }
	  catch (PrestaShopWebserviceException $e)
	  {
					
			
	  }			
		
		
	}
	
	
	public function getFinProduct($condition)
	{
		
		
		$result = '';

	   try
	    {	

		
		$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); 
		$opt = array('resource' => 'products/?display=full&filter[condition]='.$condition.'&filter[active]=0');
		$xml = $webService->get($opt);      	
		//$opt = array('url' => PS_SHOP_PATH . '/api/products/?display=full&filter[condition]='.$condition.'&filter[active]=1');			
		//$xml = $webService->get($opt);	
		$resources = $xml->children()->children();	
		
		
		if (isset($resources) && count($resources) > 0)
		{


			$result .= '<table id="list_order" class="table" style="margin-bottom: 20px;">';
			$result .= '<tr>';
			$result .= '<th>ID</th>';
			$result .= '<th>Image</th>';
			$result .= '<th>Nom</th>';
			$result .= '<th>Description</th>';
			$result .= '<th>ean13</th>';
			$result .= '<th>Fabricant</th>';
			$result .= '<th>Date de fin</th>';
			
			if($condition == "used")
			$result .= '<th>Action</th>';
			$result .= '</tr>';
           
			$id = 1; 
			foreach ($resources as $resource)
		    {			

			
				$sql = Db::getInstance()->getValue('SELECT count(reference) FROM '._DB_PREFIX_.'product where reference LIKE "%'.$resource->reference.'%"');
				$url = PS_SHOP_PATH.'api/images/products/'.$resource->id.'/'.$resource->id_default_image;
				if($sql == 0){

				$result .= '<tr id="tr_id_product_'.$resource->id.'">';
				$result .= '<td>'.$id.'</td>';
				$result .= '<td><img src="'.$url.'" width="50px" height="50px" /></td>';
				$result .= '<td>'.pSQL($resource->name->language[0][0]).'</td>';
				$result .= '<td>'.pSQL($resource->description_short->language[0][0]).'</td>';
				$result .= '<td>'.$resource->ean13.'</td>';
				$result .= '<td>'.$resource->manufacturer_name.'</td>';
				$result .= '<td>'.date("d/m/Y",strtotime($resource->date_upd)).'</td>';
				if($condition == "used")
				$result .= '<td><a href="#" onclick="addNewProduct'.$resource->id.'();">Ajouter</a></td>';				
				$result .= '<input type="hidden" name="id_product" id="id_product'.$resource->id.'" value="'.$resource->id.'" />';
				$result .= '<input type="hidden" name="id_category" id="id_category'.$resource->id.'" value="'.$resource->id_category_default.'" />';
				$result .= '<input type="hidden" name="reference" id="reference'.$resource->id.'" value="'.$resource->reference.'" />';
				$result .= '</tr>';
				$result .= '<script type="text/javascript">
			           function addNewProduct'.$resource->id.'() {
					  
					   var id_product = $("#id_product'.$resource->id.'").val();
					   var id_category = $("#id_category'.$resource->id.'").val();
					    var reference = $("#reference'.$resource->id.'").val();
                       $.ajax({
                       type: "POST",
                       url: "../modules/elegend_pro/class/add_product.php",
                       data: { registration: "success", id_product: id_product, id_category: id_category, reference: reference },
					   dataType: "json",
                       beforeSend: function(data) { 
                       $(".theform").html("Importation en cours");

                      },
                        success: function(data) {
					
                        $(".theform").html(data[0]);						
						if(data[1] == "yes")
						$("#tr_id_product_'.$resource->id.'").hide();
						else
						$("#tr_id_product_'.$resource->id.'").show();
                        },
						error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                        }
                         });
                       }
					   </script>';
				  
				}
				$id++;
			}
			$result .= '</table>';
			$result .= '<div class="theform"></div>';
			
			
		}
		else
			$result .= 'Aucun nouveau produit';
				 
		 return $result;
		
	   }
	  catch (PrestaShopWebserviceException $e)
	  {
					
			
	  }			
		
		
	}
	
	
	
	public function getMaxIdCategoty()
	{
		$sql = Db::getInstance()->getValue('SELECT max(`id_category`) FROM '._DB_PREFIX_.'category');
		return $sql + 1;

	}
	
	public function getMaxPositionCategoty()
	{
		$sql = Db::getInstance()->getValue('SELECT max(`position`) FROM '._DB_PREFIX_.'category');
        return $sql + 1;
	}
	
	public function getMaxNleftCategoty()
	{
		$sql = Db::getInstance()->getValue('SELECT max(`nleft`) FROM '._DB_PREFIX_.'category');
        return $sql + 1;
	}
	
	public function setGroupCategory($id_category)
	{
		 $sql = Db::getInstance()->ExecuteS('INSERT INTO '._DB_PREFIX_.'category_group SELECT '.pSQL($id_category).', id_group FROM '._DB_PREFIX_.'group');
         return $sql;
	}
	
	public function getNameCategory($name_category)
	{
		$sql = Db::getInstance()->ExecuteS('SELECT name FROM '._DB_PREFIX_.'pro_elegend_param WHERE name LIKE "%'.$name_category.'"');
		return $sql;

	}
	
	public function getIdCategory($name_category)
	{
		$sql = Db::getInstance()->getValue('SELECT value FROM '._DB_PREFIX_.'pro_elegend_param  WHERE name LIKE "%'.$name_category.'"');
		return $sql;

	}
	
	public function showNameCategory($name)
	{
		$sql = Db::getInstance()->ExecuteS('SELECT name FROM '._DB_PREFIX_.'pro_elegend_param');
		
		foreach ( $sql as $row ) 
		    if($row["name"] == $name)
		return $row["name"];

	}
	
	
	
	public function uploadImage($path,$id,$new_id){		
		

		$url = $path.'/img/c/'.$id.'.jpg';
		$name = basename($url);
        list($txt, $ext) = explode(".", $name);
        $name = $txt;
        $name = $name.".".$ext;
        $upload = file_put_contents("../img/c/".$new_id.".jpg",file_get_contents($url));
		
		if($upload){
		    // creation du logo medium_default de la catégorie 
			$sourcejpeg = imagecreatefromjpeg("$url");
			list($width, $height) = getimagesize($url);
			$newwidth = 125; $newheight = 125;
			$suffixe = "-medium_default";
			$destine = "../img/c/".$new_id.$suffixe.".jpg";
			$image_thumb = imagecreatetruecolor($newwidth,$newheight);
			imagecopyresampled($image_thumb, $sourcejpeg, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
			imagejpeg( $image_thumb, $destine );
			
			// creation du logo medium_default de la catégorie 
			$sourcejpeg = imagecreatefromjpeg("$url");
			list($width, $height) = getimagesize($url);
			$newwidth = 870; $newheight = 217;
			$suffixe = "-category_default";
			$destine = "../img/c/".$new_id.$suffixe.".jpg";
			$image_thumb = imagecreatetruecolor($newwidth,$newheight);
			imagecopyresampled($image_thumb, $sourcejpeg, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
			imagejpeg( $image_thumb, $destine );	
		}
		
		return true;
	}
	
	/**
	 * mettreajourCatalog Insertion catalogue
	 *
	 * @param string $id_category id de la categorie	 *
	 * @return true
	 */ 
	

   public function _catalog($id_category)
	{		
		
		$html = '';
		
		$products= new Product();
		$state = new State();
		


	try
	{	

		$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);        	
		$opt = array('resource' => 'categories');
		$opt['id'] = (int)$id_category; 
		$xml = $webService->get($opt);
		$c = $xml->children()->children();
		

		$name_category = pSQL($c->name->language[0][0]);
		
		$name_id_category = $this->getNameCategory($name_category);

		if(!$name_id_category){	
		// insertion  table category 
		$id_category = 	$this->getMaxIdCategoty();
		$id_parent = $this->getMaxIdCategoty();
		$data['id_category'] = $id_category;
        $data['id_parent'] = $c->id_parent;
		$data['level_depth'] = 2;
		$data['nleft'] = $this->getMaxNleftCategoty();
		$data['nright'] = 1;
        $data['id_shop_default'] = 1;
        $data['active'] = $c->active;
        $data['position'] = $this->getMaxPositionCategoty();
		$data['date_add'] = pSQL(date('Y-m-d H:i:s'));
		$data['date_upd'] = pSQL(date('Y-m-d H:i:s'));

        // insertion  table category_lang 
		$datal['id_category'] = $this->getMaxIdCategoty();
        $datal['id_shop'] = 1;
        $datal['id_lang'] = 1;
        $datal['name'] = $c->name->language[0][0];
        $datal['description'] = $c->description->language[0][0];
        $datal['link_rewrite'] =$c->link_rewrite->language[0][0]; 
        $datal['meta_title'] = $c->meta_title->language[0][0];
        $datal['meta_keywords'] = $c->meta_keywords->language[0][0];
        $datal['meta_description'] = $c->meta_description->language[0][0];
		
		// insertion table category_shop 
        $dataShop['id_category'] = $this->getMaxIdCategoty();
        $dataShop['id_shop'] = 1;
        $dataShop['position'] = $this->getMaxPositionCategoty();
		
		// insertion  table pro_elegend_param 
	    $dataParam['name'] = $c->name->language[0][0];
        $dataParam['value'] = $this->getMaxIdCategoty();

        if(!DB::getInstance()->insert('category', $data)) die('Error in category insert : '.$c->id);
        if(!DB::getInstance()->insert('category_lang', $datal)) die('Error in category lang insert : '.$c->id);
        if(!DB::getInstance()->insert('category_shop', $dataShop)) die('Error in category shop insert : '.$c->id);
		if(!DB::getInstance()->insert('pro_elegend_param', $dataParam)) echo('Error in pro_elegend_param insert : '.$c->id);
		
		// insertion  table category_group 
        $this->setGroupCategory($data['id_category']);
		
		// telechargement des images
		$this->uploadImage(PS_SHOP_PATH,$c->id,$data['id_category']);
			
	    // insertion  sous categorie		
	    foreach($c->associations->categories->category  as $rc){					
			
		//$optc = array('url' => PS_SHOP_PATH . 'api/categories/'.$rc->id);	
		$optc = array('resource' => 'categories');
		$optc['id'] = (int)$rc->id; 		
		$xmlc = $webService->get($optc);	
		$ra = $xmlc->children()->children();
		
		// insertion  table category 			
		$data['id_category'] = $this->getMaxIdCategoty();
        $data['id_parent'] = $id_parent;
		$data['level_depth'] = 3;
		$data['nleft'] = $this->getMaxNleftCategoty();
		$data['nright'] = 1;
        $data['id_shop_default'] = 1;
        $data['active'] = $ra->active;
        $data['position'] = $this->getMaxPositionCategoty();
		$data['date_add'] = pSQL(date('Y-m-d H:i:s'));
		$data['date_upd'] = pSQL(date('Y-m-d H:i:s'));
        
		 // insertion  table category_lang 
        $datal['id_category'] = $this->getMaxIdCategoty();
        $datal['id_shop'] = 1;
        $datal['id_lang'] = 1;
        $datal['name'] = $ra->name->language[0][0];
        $datal['description'] = $ra->description->language[0][0];
        $datal['link_rewrite'] =$ra->link_rewrite->language[0][0]; 
        $datal['meta_title'] = $ra->meta_title->language[0][0];
        $datal['meta_keywords'] = $ra->meta_keywords->language[0][0];
        $datal['meta_description'] = $ra->meta_description->language[0][0];

        // insertion  table category_lang 
	    $dataShop['id_category'] = $this->getMaxIdCategoty();
        $dataShop['id_shop'] = 1;
        $dataShop['position'] = $this->getMaxPositionCategoty();
		
		 // insertion  table category_lang 
	    $dataParam['name'] = $ra->name->language[0][0];
        $dataParam['value'] = $this->getMaxIdCategoty();
	

        if(!DB::getInstance()->insert('category', $data)) echo('Error in category insert : '.$id);
        if(!DB::getInstance()->insert('category_lang', $datal)) echo('Error in category lang insert : '.$id);
        if(!DB::getInstance()->insert('category_shop', $dataShop)) echo('Error in category shop insert : '.$id);
		if(!DB::getInstance()->insert('pro_elegend_param', $dataParam)) echo('Error in pro_elegend_param insert : '.$id);
		 // insertion  table category_group 

		 $this->setGroupCategory($data['id_category']);
		
		// telechargement des images
		$this->uploadImage(PS_SHOP_PATH,$ra->id,$data['id_category']);
		foreach($ra->associations->products->product  as $rp){
	      $products->_products($rp->id,$data['id_category']);		
	   }
	   }
	   $sous_category = $c->associations->categories->category;
       if (!count($sous_category)) {
	   foreach($c->associations->products->product  as $rp)
	      $products->_products($rp->id,$id_category);		
	   
	   }
	    
		
		

	    $state->orderState();
		
	   $html .=  "<div>Importation catalogue réussie</div>";
		}else{
		$html .=  "<div>catalogue existe</div>";	
		}
	   
	}
	catch (PrestaShopWebserviceException $e)
	{
					
			$trace = $e->getTrace();
			if ($trace[0]['args'][0] == 404) echo 'Bad ID Catalog';
			else if ($trace[0]['args'][0] == 401) echo 'Bad auth key Catalog';
			else echo 'Other error'.$e->getMessage().'</b>';
	}			
		
		return true;
	}
	
	
} //Fin class Catalog
?>