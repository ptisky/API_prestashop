{if isset($setting) && $setting}
    <div class="panel content-setting-list" id="fieldset_1">
        <div class="panel-heading"> <i class="icon-cogs"></i>{l s='  Export saved settings'  mod='updateproducts'}</div>
        <div class="form-wrapper form-wrapper-list-settings">
            {foreach $setting as $key => $set}
                <li {if $set['id'] == $id} class="active_setting" {/if}>
                    <span class="settings_key">{$set['id']|escape:'htmlall':'UTF-8'}.</span>
                    <a href="{$base_url|escape:'htmlall':'UTF-8'}&settings={$set['id']|escape:'htmlall':'UTF-8'}" class="one_setting">{$set['name']|escape:'htmlall':'UTF-8'}</a>
                    <a id-setting="{$set['id']|escape:'htmlall':'UTF-8'}" class="delete_setting btn btn-default"><i class="icon-trash"></i></a>
                </li>
            {/foreach}
        </div>
    </div>
{/if}


{if isset($setting_update) && $setting_update}
    <div class="panel content-setting-list-update" id="fieldset_2">
        <div class="panel-heading"> <i class="icon-cogs"></i>{l s='  Update saved settings '  mod='updateproducts'}</div>
        <div class="form-wrapper form-wrapper-list-settings-update">
            {foreach $setting_update as $key => $set}
                <li {if $set['id'] == $idUpdate} class="active_setting_update" {/if}>
                    <span class="settings_key_update">{$set['id']|escape:'htmlall':'UTF-8'}.</span>
                    <a href="{$base_url|escape:'htmlall':'UTF-8'}&settingsUpdate={$set['id']|escape:'htmlall':'UTF-8'}" class="one_setting_update">{$set['name']|escape:'htmlall':'UTF-8'}</a>
                    <a id-setting-update="{$set['id']|escape:'htmlall':'UTF-8'}" class="delete_setting_update btn btn-default"><i class="icon-trash"></i></a>
                </li>
            {/foreach}
        </div>
    </div>
{/if}

<script type="text/javascript">

    {if isset($idUpdate) && $idUpdate}
        setTimeout(function(){
           $('.updateproducts .nav-tabs li:last-child a').click();
        }, 100);
    {else}
        setTimeout(function(){
           $('.updateproducts .nav-tabs li:first-child a').click();
        }, 100);
    {/if}

</script>