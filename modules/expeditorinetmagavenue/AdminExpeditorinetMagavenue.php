<?php
/**
 * AdminExpeditorinetMagavenue class, AdminExpeditorinetMagavenue.php, Order export for Expeditor Inet
 *
 * @category modules
 *
 * @author    Magavenue <contact@magavenue.com>
 * @copyright Magavenue
 * @license   http://www.opensource.org/licenses/osl-3.0.php Open-source licence 3.0
 * @version 2.5 compatible with Prestashop >= 1.5
 */

//include_once(_PS_CLASS_DIR_.'/AdminTab.php');
include_once(_PS_MODULE_DIR_.'expeditorinetmagavenue/expeditorinetmagavenue.php');

class AdminExpeditorinetMagavenue extends AdminTab
{
    public function display()
    {
        $module = new expeditorinetmagavenue;
        echo $module->displayTab();
    }
}
