<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{simpleimportproduct}prestashop>import_3d6b3733b53a9cadba2118164df82e48'] = 'Con éxito importada %1s productos de: %2s';
$_MODULE['<{simpleimportproduct}prestashop>import_80ecac9d1ab80db67673eae894df8c32'] = 'Con éxito importado %s productos!';
$_MODULE['<{simpleimportproduct}prestashop>send_87810c4404993af3601717276d095026'] = 'Se produjo un error al subir, archivo debe estar en formato XLSX';
$_MODULE['<{simpleimportproduct}prestashop>send_022435517a77e0c5f0ce07cad32cb3a6'] = 'Se produjo un error al subir, archivo debe estar en formato CSV';
$_MODULE['<{simpleimportproduct}prestashop>send_668c37239cc66c665e4b42851dad354b'] = 'Se produjo un error al subir, archivo debe ser';
$_MODULE['<{simpleimportproduct}prestashop>send_1ddcb92ade31c8fbd370001f9b29a7d9'] = 'formato';
$_MODULE['<{simpleimportproduct}prestashop>send_e86a5b2cb041c3c0a9eb88c6ec6a95f9'] = 'Seleccionar archivo para importar';
$_MODULE['<{simpleimportproduct}prestashop>send_dbf808ea93023749d20f80a0e998155f'] = 'Los datos guardados con éxito!';
$_MODULE['<{simpleimportproduct}prestashop>send_858a2b552953bc182248fe9374646398'] = 'Importado';
$_MODULE['<{simpleimportproduct}prestashop>send_86024cad1e83101d97359d7351051156'] = 'productos';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c6d34dd6fa87e5922e13131aad9f302a'] = 'Productos de Importación';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_f2bbdf9f72c085adc4d0404e370f0f4c'] = 'Atributo';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_ed3f8939becb4f60258f84fad6774671'] = 'Attribute (Nombre:Tipo, Nombre:Tipo)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_689202409e48743b914713f96d93947c'] = 'Valor';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_b5ee3655098360fcfc5ccdcef04eb85d'] = 'Atributo valor (Valor,Valor)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_448aec4f94026678585f09d528ba417c'] = 'Código de referencia';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_60d2f5b05dc2e92d8167bb71809def2f'] = 'Referencia interna para este combinación. Los caracteres especiales están permitidos .-_#';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c8effe37e0fefd95ff0cc4873de6e6a6'] = 'EAN-13 o código de barra JAN';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_edf436af97a6b969ff164d338d0814c3'] = 'Este tipo de código de combinación es específico de Europa y Japón, pero es ampliamente utilizado a nivel internacional. Es más utilizado que el código UPC: todos los productos con un EAN serán aceptados en Norteamérica.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_5bb4529a7441854e00e00f8ed0096756'] = 'Código de barra UPC';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_84c5eb656a5a234fd029d3d4f9ab113d'] = 'Este tipo de código de combinación es ampliamente usado en Estados Unidos, Canadá, Reino Unido, Australia, Nueva Zelanda y en otros países.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c804723ccdde3d7a46933b208c6f928d'] = 'Precio al por mayor';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_85bed720d784e4cfca7944e96579f23b'] = 'Reemplaza el precio mayor de la pestaña \"Precios\".';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_6ebc48c8cea15d33e2f95bf63c330e3c'] = 'Impacto en el precio (importe)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1182810eb2ab365892590f1c4bb0402d'] = 'Impacto en el peso (importe)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_4fc9f49cb85af9ba5caab7747fe35846'] = 'Ecotasa (impuestos excl.)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_108a2296d474989de3b95c5431002f73'] = 'Reemplaza la ecotasa de la pestaña \"Precios\".';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1b72aed8e84b91d73896efd48cf1a4e0'] = 'La cantidad mínima ';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1c5f0be6c267baf1982f51ba25dd850f'] = 'La cantidad mínima para pedir este producto (indique 1 para desactivar esta opción)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_694e8d1f2ee056f98ee488bdc4982d73'] = 'Cantidad';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_79d3a9336845a659b537a14862522b52'] = 'Cantidades disponibles para la venta';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_7c52393eaa879dd8c3081f4c3c847846'] = 'Combinación por defecto';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_f86b4cc6b57bcabf6eb47816d56f0185'] = 'Hacer esta combinación como predeterminada para este producto. (0 = No, 1 = Si)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_7b28d39c479ba659159b3356616b0a04'] = 'Tipo de descuento';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c1fd556728f5475359edd7d92162033d'] = 'Tipo de descuento (importe o el porcentaje)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_9e834f13e35e4edf64863ab414a6217a'] = 'Descuento';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_14759772455cb478f99133cb419f3e2f'] = 'Importe de la reducción';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0e1e48c5a509fcb7cb904aa2418c68a6'] = 'Disponible a partir de (fecha)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_139fd814d0da2c69756467fb52a4ac00'] = 'Disponible (fecha)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_6bc323cd5254af4efd7b138cc9de277b'] = 'Precio fijo (imp. excl.)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_9b69fa8e03a5e5a8d949a85f9692ef01'] = 'A partir de (cantidad)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_89357c5634fc97255d5c5b0170970915'] = 'Disponible a partir de (cantidad)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_120f8d2cc7f58179b8a7a48753300464'] = 'Características del nombre';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1145fa97c0885a434db7e371f5a07c6c'] = 'Características del nombre. Caracteres no válidos ;=#{}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_cef09b40844ebc72dcaff9813e0504da'] = 'Características valor';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_b4942eb197fbadcc2bd8c269a2f46d44'] = 'Valor de las características del producto';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_fa1d72220129e2192a7568e1fa4df71c'] = 'Mostrar más...';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_b9c139957b9394c7d4a9fd0aaa999c3d'] = 'Esconder...';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_6f4a225b29c0975663d0e4b9e1f4547f'] = 'Paso 1';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_e31ad1284b7618a9c10777ed99b12e64'] = 'Paso 2';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1c97e434cef50560c03877bd8177d692'] = 'Ejemplo de archivo de importación (XLSX)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0b4c0dfdc51829024e115056fe1500b2'] = 'Sus ajustes guardados';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_3f191d96d6c2c6bc91e4f271801221a0'] = 'XLSX';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_cc8d68c551c4a9a6d5313e07de4deafd'] = 'CSV';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_9eecb7db59d16c80417c72d1e1f4fbf1'] = ';';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_853ae90f0351324bd73ea615e6487517'] = ':';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c0cb5f0fcf239ab3d9c1fcd31fff1efc'] = ',';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_5058f1af8388633f609cadb75a75dc9d'] = '.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_6666cd76f96956469e7be39d750cc7d9'] = '/';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_b99834bc19bbad24580b3adfa04fb947'] = '|';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_ec211f7c20af43e742bf2570c3cb84f9'] = 'Añadir';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_976ac75c2985c988956385ba30a17e4a'] = 'Añadir/actualizar';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_df644ae155e79abf54175bd15d75f363'] = 'Nombre de producto';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1dec4f55522b828fe5dacf8478021a9e'] = 'URL amigable';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_39080ffac9ef208828d1d5fc4aa434e3'] = 'Seleccione el formato de archivo';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_4994a8ffeba4ac3140beb89e8d41f174'] = 'Idioma';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_abb968fcf23cfb09b36e4d92bbbe7500'] = 'Delimitador';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_b5a528f642ee412523c909fe6c0e4235'] = 'Tipo de importación';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_f0468b8afcc44a24054c525bedbeb98a'] = 'Clave para la actualización';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0b27918290ff5323bea1e3b78a9cf04e'] = 'Archivo';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'Próximo';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_630f6dc397fe74e52d5189e2c80f282b'] = 'Volver a la lista';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_4ee7906cfc80deaf7c5a4742b14cd16d'] = 'Nombre público para este producto. Caracteres inválidos ;=#{}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_59d4e474aa07c59634473dd3618ba3af'] = 'Referencia interna para este producto. Los caracteres especiales están permitidos .-_#';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_9e11e4b371570340ca07913bc4783a7a'] = 'Meta-título';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_ef53513cd86526414bec442e1a044e28'] = 'Título público para la página del producto, y para los motores de búsqueda. Dejar en blanco para usar el nombre del producto.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_7d7559ccac6bc30a4d985db11cb34a3a'] = 'Meta palabras clave';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_f7fd5f0f9bacf6c76bfbe496658c8447'] = 'Palabras clave para la cabecera HTML, separados por comas.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_3f64b2beede1082fd32ddb0bf11a641f'] = 'Meta descripción';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_352ceee04b365bba230157c960b1412d'] = 'Esta descripción aparecerá en los buscadores. Utilice una sola frase, más corta de 160 caracteres (incluyendo espacios).';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c1069a480848e06782b81b8bea9c0c94'] = 'Descripción corta';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_765a5021c58a220cb2a0bec7d0e83b17'] = 'Aparece en los listados de productos y en la parte superior de la página del producto.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Descripción';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_9a2d3323b05a18f9723ea91b05dd9988'] = 'Aparece en la información de la página del producto.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_4c32794a96aec43624f14839274174bf'] = 'Precio de venta sin IVA';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_90cc6a03b5edaf79e8c3fb8f3451fe31'] = 'El precio de venta antes de impuestos es el precio por el que pretende vender este producto a sus clientes. Este debería ser más alto que el precio del mayorista sin IVA: la diferencia entre los dos será su margen.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_20a34c4e30c5bbe1d3f870ac55f0d831'] = 'Grado de tasa';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_620d66a334f161bdb5c766601fe6824f'] = 'El valor debe estar entre 0 y 100';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_d84d5a260e0501f0c1c01c912396bd1b'] = 'Imágenes urls';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_414180f1919858ce9f11df12192de6d5'] = 'Cada imágenes tiene que estar seguida por una coma. Formato JPG, PNG.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0f60ea0e9d979de5ac9d37c94b207869'] = 'Esta es la URL legible, generada por el nombre del producto. Puedes cambiarla si quieres.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_a17f5bc22553d897f6363867c0165256'] = 'Categoría por defecto';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_be696601fc51ce3f87b06f0d124a5afb'] = 'La categoría por defecto es la principal categoría de su producto, es mostrado por defecto.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1814d65a76028fdfbadab64a5a8076df'] = 'Proveedores';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_70af813064e91d72cad1a8e7fc130393'] = 'Cada proveedor tiene que estar seguida por una coma. Caracteres inválidos ;=#{}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0b5c5a9176b94b7577e998f7440228a7'] = 'Proveedor predeterminado';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_96d36948c94caa13500290e783528bf7'] = 'Predeterminado nombre del proveedor. Caracteres inválidos ;=#{}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Fabricante';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_d4daa70843924b7f4ae259388c095cfa'] = 'Nombre del fabricante. Caracteres inválidos ;=#{}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_189f63f277cd73395561651753563065'] = 'Etiquetas (x,y,z...)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_18302b5faa19bb432b8331c6402ec77f'] = 'Se mostrará en el bloque de etiquetas cuando esté activo. Las etiquetas ayudan a los clientes a encontrar fácilmente sus productos.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0041a5c39ca241f883f4e0516a32d290'] = 'Este tipo de código de producto es específico de Europa y Japón, pero es ampliamente utilizado a nivel internacional. Es más utilizado que el código UPC: todos los productos con un EAN serán aceptados en Norteamérica.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_39bd7eba8aa2033579b5ae60bf0c774d'] = 'Este tipo de código de producto es ampliamente usado en Estados Unidos, Canadá, Reino Unido, Australia, Nueva Zelanda y en otros países.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_a171a9b7de01d023bad650e778493ea6'] = 'Anchura del paquete';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_895a9b117143b191564e634c2fa67cef'] = 'Altura del paquete';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_22759d4c7ba574eebd9faf5310857d1f'] = 'Profundidad del paquete';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0829bbc64dd88308c23d94fecc3c7e88'] = 'Peso del paquete';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_0147896b0a27d9c02a92ae0d4fab2791'] = 'Imágenes subtítulo';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_3dcbf8cf3cb0bbd8b648fc7a5f889104'] = 'Cada imágenes subtítulo tiene que estar seguida por una coma. Caracteres inválidos ;=#{}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_f2149c422ab7577f063b69a2884d17f0'] = 'Disponible para pedidos';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_19b3596c48d5493544e722037fb567cf'] = '(0 = No, 1 = Si)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activado';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_9e2941b3c81256fac10392aaca4ccfde'] = 'Condición';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_feeec928c30160cc983d4461ca0289c2'] = 'Valor new, used o refurbished';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_1729a56cfc89021478498fe0c89a843a'] = 'Visible en';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_3824ef587e10d871a6a2b813c32d92a3'] = 'Valor both, catalog, search o none';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_5a915a9e984b5f42d941be9d18072a9c'] = 'Mostrar el icono \"Rebajas\"';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_ef270c5b5ab343ad251f5ab86cc3ea23'] = 'Mostrar el icono \"Rebajas\" y el texto: \"En descuento\" en la página del producto y en el catálogo de productos. (0 = No, 1 = Si)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_e1a5e653bc356ed6745d6814d50213eb'] = 'Muestra el precio';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_cddb91d856108b4431ebc2a9f79d4769'] = 'Precio unitario (sin IVA)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_a00cc83ae38424bb93f5179e1ab50850'] = 'Al vender un pack de artículos, puede indicar el precio unitario de cada artículo del pack. Por ejemplo, \"por botella\" o \"por libra\".';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_cc790edb31f5984a5754e4bc6aadf7bb'] = 'Precio mayorista sin IVA';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_7a2215a4f812433c5e171eff2f95834b'] = 'El precio al por mayor es el precio que pagó por el producto. No incluya las tasas.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c265e7bd60b9bf0470d39a8ef87e0727'] = 'Ecotasa (Impuestos incluidos)';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_4b8c01975c469777b0e0bffe373e5a0c'] = 'La ecotasa es un conjunto local de impuestos destinados a \"promover actividades ecológicamente sostenibles a través de incentivos económicos\". Esta ya está incluida en el precio de venta al público: cuanto mayor sea esta ecotasa, menor será el margen.';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c40f15cd6deedb63c8c555b22c9b2f30'] = 'Texto mostrado cuando hay existencias';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_5373df5f8f5dc40cd7a4a4ae7a9671bf'] = 'Texto mostrado cuando hay existencias. Caracteres inválidos ;=#{}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_69dc68c6a765187d6450467f42f994f2'] = 'Texto mostrado cuando son permitidos los pedidos pendientes';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c5e753d76d950b20fb84e358fb6285df'] = 'Si está vacío, se mostrará el mensaje \"con stock\". Caracteres inválidos ;=#{}';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_c24c379ede5dfa7b82a91bfcc5c3deef'] = 'Asociar categorías';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_a5e2a36615cbcae27d5dfca282a2c719'] = 'Todas las categorías y subcategorías de productos';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_ee42125205cfb2f312f62dab1eebfd9f'] = 'Tiene combinación';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_93cba07454f06a4a960172bbd6e2a435'] = 'Sí';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_75e017cc992f7a6090803efcb6ac1c53'] = 'agregar combinación';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_15bde11773b7bc84e7ef3aa6d98ca5ee'] = 'Tiene descuento';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_addec426932e71323700afa1911f8f1c'] = 'más';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_572dda2167773d211243ecfeec52555e'] = 'Tiene características';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_64b644ce026182d76faca40823c3b372'] = 'añadir características';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_d4dccb8ca2dac4e53c01bd9954755332'] = 'Guardar ajustes';
$_MODULE['<{simpleimportproduct}prestashop>simpleimportproduct_72d6d7a1885885bb55a565fd1070581a'] = 'Importación';
$_MODULE['<{simpleimportproduct}prestashop>form_fb32534540c610809da070e9cca8d90e'] = 'Añadir subcategoría';
$_MODULE['<{simpleimportproduct}prestashop>form_cabb8270408172ee3c7bb9ba7391fac0'] = 'Añadir categoría';
$_MODULE['<{simpleimportproduct}prestashop>form_fa1d72220129e2192a7568e1fa4df71c'] = 'Mostrar más...';
$_MODULE['<{simpleimportproduct}prestashop>form_b9c139957b9394c7d4a9fd0aaa999c3d'] = 'Esconder...';
$_MODULE['<{simpleimportproduct}prestashop>addcategories_fb32534540c610809da070e9cca8d90e'] = 'añadir subcategoría';
$_MODULE['<{simpleimportproduct}prestashop>addcategories_cabb8270408172ee3c7bb9ba7391fac0'] = 'añadir categoría';
$_MODULE['<{simpleimportproduct}prestashop>addcombinations_75e017cc992f7a6090803efcb6ac1c53'] = 'agregar combinación';
$_MODULE['<{simpleimportproduct}prestashop>addcombinations_099af53f601532dbd31e0ea99ffdeb64'] = 'eliminar';
$_MODULE['<{simpleimportproduct}prestashop>addfeatures_64b644ce026182d76faca40823c3b372'] = 'añadir características';
$_MODULE['<{simpleimportproduct}prestashop>addfeatures_099af53f601532dbd31e0ea99ffdeb64'] = 'eliminar';
$_MODULE['<{simpleimportproduct}prestashop>morecategory_fb32534540c610809da070e9cca8d90e'] = 'añadir subcategoría';
$_MODULE['<{simpleimportproduct}prestashop>morecombination_75e017cc992f7a6090803efcb6ac1c53'] = 'agregar combinación';
$_MODULE['<{simpleimportproduct}prestashop>morecombination_099af53f601532dbd31e0ea99ffdeb64'] = 'eliminar';
$_MODULE['<{simpleimportproduct}prestashop>morediscount_addec426932e71323700afa1911f8f1c'] = 'más';
$_MODULE['<{simpleimportproduct}prestashop>morediscount_099af53f601532dbd31e0ea99ffdeb64'] = 'eliminar';
$_MODULE['<{simpleimportproduct}prestashop>morefeatured_64b644ce026182d76faca40823c3b372'] = 'añadir características';
$_MODULE['<{simpleimportproduct}prestashop>morefeatured_099af53f601532dbd31e0ea99ffdeb64'] = 'eliminar';
