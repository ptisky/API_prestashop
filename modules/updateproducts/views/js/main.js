$(document).ready(function(){

  $("body").on("mouseenter", "#update .content_fields li",
    function(){
      if($(this).hasClass("isset_hint")){
        $("body").append("<div class='hint_content'>"+$(this).attr('data-hint')+"</div>");
        var top = $(this).offset().top;
        var left = $(this).offset().left;
        $('.hint_content').css({'top':top-15, 'left':left-190});
        $('.hint_content').fadeIn();
      }
    }
  ).on("mouseleave", ".content_fields li",
    function(){
      if($(this).hasClass("isset_hint")){
        $('.hint_content').remove()

      }
    }
  );
  $(document).on('click', '#update .block_base_fields li', function(e){
    if(e.ctrlKey) {
      $(this).addClass('checked');
    }
    else{
      $('#update .block_base_fields li').removeClass('checked');
      $(this).addClass('checked');
    }
  });
  $(document).on('click', '#update .block_combinations_fields li', function(e){
    if(e.ctrlKey) {
      $(this).addClass('checked');
    }
    else{
      $('#update .block_combinations_fields li').removeClass('checked');
      $(this).addClass('checked');
    }
  });
  $(document).on('click', '#update .block_specificPrice_fields li', function(e){
    if(e.ctrlKey) {
      $(this).addClass('checked');
    }
    else{
      $('#update .block_specificPrice_fields li').removeClass('checked');
      $(this).addClass('checked');
    }
  });
  $(document).on('click', '#update .selected_fields li', function(e){
    if(e.ctrlKey) {
      $(this).addClass('checked');
    }
    else{
      $('#update .selected_fields li').removeClass('checked');
      $(this).addClass('checked');
    }
  });







  $(document).on('click', '#update .add_base_filds', function(e){
    $('#update .block_base_fields li.checked').each(function(e) {
      var el = $(this).clone().removeClass('checked').append('<i class="icon-arrows icon-arrows-select-fields"></i>');
      $('#update .block_selected_fields .selected_fields').append(el[0]);
      $(this).remove();
    });
  });
  $(document).on('click', '#update .add_base_filds_all', function(e){
    $('#update .block_base_fields li').each(function(e) {
      var el = $(this).clone().removeClass('checked').append('<i class="icon-arrows icon-arrows-select-fields"></i>');
      $('#update .block_selected_fields .selected_fields').append(el[0]);
      $(this).remove();
    });
  });
  $(document).on('click', '#update .remove_base_filds', function(e){
    $('#update .selected_fields li.checked').each(function(e) {
      if(!$(this).hasClass('disable_fields')){
        var el = $(this).clone().removeClass('checked');
        $('#update .block_base_fields').append(el[0]);
        $(this).remove();
      }
    });
    $('#update .block_base_fields li .icon-arrows-select-fields').remove();
  });
  $(document).on('click', '#update .remove_base_filds_all', function(e){
    $('#update .selected_fields li').each(function(e) {
      if(!$(this).hasClass('disable_fields')) {
        var el = $(this).clone().removeClass('checked');
        $('#update .block_base_fields').append(el[0]);
        $(this).remove();
      }
    });
    $('#update .block_base_fields li .icon-arrows-select-fields').remove();
  });


  $(document).on('click', '#update .add_combinations_filds', function(e){
    $('#update .block_combinations_fields li.checked').each(function(e) {
      var el = $(this).clone().removeClass('checked').append('<i class="icon-arrows icon-arrows-select-fields"></i>');
      $('#update .block_selected_fields .selected_fields').append(el[0]);
      $(this).remove();
    });
  });
  $(document).on('click', '#update .add_combinations_filds_all', function(e){
    $('#update .block_combinations_fields li').each(function(e) {
      var el = $(this).clone().removeClass('checked').append('<i class="icon-arrows icon-arrows-select-fields"></i>');
      $('#update .block_selected_fields .selected_fields').append(el[0]);
      $(this).remove();
    });
  });
  $(document).on('click', '#update .remove_combinations_filds', function(e){
    $('#update .selected_fields li.checked').each(function(e) {
      if(!$(this).hasClass('disable_fields')) {
        var el = $(this).clone().removeClass('checked');
        $('#update .block_combinations_fields').append(el[0]);
        $(this).remove();
      }
    });
    $('#update .block_combinations_fields li .icon-arrows-select-fields').remove();
  });
  $(document).on('click', '#update .remove_combinations_filds_all', function(e){
    $('.selected_fields li').each(function(e) {
      if(!$(this).hasClass('disable_fields')) {
        var el = $(this).clone().removeClass('checked');
        $('#update .block_combinations_fields').append(el[0]);
        $(this).remove();
      }
    });
    $('#update .block_combinations_fields li .icon-arrows-select-fields').remove();
  });


  $(document).on('click', '#update .add_specificPrice_filds', function(e){
    $('#update .block_specificPrice_fields li.checked').each(function(e) {
      var el = $(this).clone().removeClass('checked').append('<i class="icon-arrows icon-arrows-select-fields"></i>');
      $('#update .block_selected_fields .selected_fields').append(el[0]);
      $(this).remove();
    });
  });
  $(document).on('click', '#update .add_specificPrice_filds_all', function(e){
    $('#update .block_specificPrice_fields li').each(function(e) {
      var el = $(this).clone().removeClass('checked').append('<i class="icon-arrows icon-arrows-select-fields"></i>');
      $('#update .block_selected_fields .selected_fields').append(el[0]);
      $(this).remove();
    });
  });
  $(document).on('click', '#update .remove_specificPrice_filds', function(e){
    $('#update .selected_fields li.checked').each(function(e) {
      if(!$(this).hasClass('disable_fields')) {
        var el = $(this).clone().removeClass('checked');
        $('#update .block_specificPrice_fields').append(el[0]);
        $(this).remove();
      }
    });
    $('#update .block_specificPrice_fields li .icon-arrows-select-fields').remove();
  });
  $(document).on('click', '#update .remove_specificPrice_filds_all', function(e){
    $('#update .selected_fields li').each(function(e) {
      if(!$(this).hasClass('disable_fields')) {
        var el = $(this).clone().removeClass('checked');
        $('#update .block_specificPrice_fields').append(el[0]);
        $(this).remove();
      }
    });
    $('#update .block_specificPrice_fields li .icon-arrows-select-fields').remove();
  });

  $(document).on('keyup', '#update .search_base_fields', function(){
    var self = $(this);
    $('#update .block_base_fields li').each(function(){

      if( $(this).text().toLowerCase().indexOf(self.val().toLowerCase()) >= 0 ){
        $(this).show();
      }
      else{
        $(this).hide();
      }
    });
  });
  $(document).on('keyup', '#update .search_combinations_fields', function(){
    var self = $(this);
    $('#update .block_combinations_fields li').each(function(){

      if( $(this).text().toLowerCase().indexOf(self.val().toLowerCase()) >= 0 ){
        $(this).show();
      }
      else{
        $(this).hide();
      }
    });
  });
  $(document).on('keyup', '#update .search_specificPrice_fields', function(){
    var self = $(this);
    $('#update .block_specificPrice_fields li').each(function(){

      if( $(this).text().toLowerCase().indexOf(self.val().toLowerCase()) >= 0 ){
        $(this).show();
      }
      else{
        $(this).hide();
      }
    });
  });
  $(document).on('keyup', '#update .search_selected_fields', function(){
    var self = $(this);
    $('#update .selected_fields li').each(function(){

      if( $(this).text().toLowerCase().indexOf(self.val().toLowerCase()) >= 0 ){
        $(this).show();
      }
      else{
        $(this).hide();
      }
    });
  });


  $("body").on("mouseenter", "#export .content_fields li",
    function(){
      if($(this).hasClass("isset_hint")){
        $("body").append("<div class='hint_content'>"+$(this).attr('data-hint')+"</div>");
        var top = $(this).offset().top;
        var left = $(this).offset().left;
        $('.hint_content').css({'top':top-15, 'left':left-190});
        $('.hint_content').fadeIn();
      }
    }
  ).on("mouseleave", ".content_fields li",
    function(){
      if($(this).hasClass("isset_hint")){
        $('.hint_content').remove()

      }
    }
  );

  $('.selected_fields').sortable({
    revert:false,
    axis: "y"
  });
  $(document).on('click', '#export .block_base_fields li', function(e){
    if(e.ctrlKey) {
      $(this).addClass('checked');
    }
    else{
      $('#export .block_base_fields li').removeClass('checked');
      $(this).addClass('checked');
    }
  });
  $(document).on('click', '#export .block_combinations_fields li', function(e){
    if(e.ctrlKey) {
      $(this).addClass('checked');
    }
    else{
      $('#export .block_combinations_fields li').removeClass('checked');
      $(this).addClass('checked');
    }
  });
  $(document).on('click', '#export .block_specificPrice_fields li', function(e){
    if(e.ctrlKey) {
      $(this).addClass('checked');
    }
    else{
      $('#export .block_specificPrice_fields li').removeClass('checked');
      $(this).addClass('checked');
    }
  });
  $(document).on('click', '#export .selected_fields li', function(e){
    if(e.ctrlKey) {
      $(this).addClass('checked');
    }
    else{
      $('#export .selected_fields li').removeClass('checked');
      $(this).addClass('checked');
    }
  });







  $(document).on('click', '#export .add_base_filds', function(e){
    $('#export .block_base_fields li.checked').each(function(e) {
      var el = $(this).clone().removeClass('checked').append('<i class="icon-arrows icon-arrows-select-fields"></i>');
      $('#export .block_selected_fields .selected_fields').append(el[0]);
      $(this).remove();
    });
  });
  $(document).on('click', '#export .add_base_filds_all', function(e){
    $('#export .block_base_fields li').each(function(e) {
      var el = $(this).clone().removeClass('checked').append('<i class="icon-arrows icon-arrows-select-fields"></i>');
      $('#export .block_selected_fields .selected_fields').append(el[0]);
      $(this).remove();
    });
  });
  $(document).on('click', '#export .remove_base_filds', function(e){
    $('#export .selected_fields li.checked').each(function(e) {
      if(!$(this).hasClass('disable_fields')){
        var el = $(this).clone().removeClass('checked');
        $('#export .block_base_fields').append(el[0]);
        $(this).remove();
      }
    });
    $('#export .block_base_fields li .icon-arrows-select-fields').remove();
  });
  $(document).on('click', '#export .remove_base_filds_all', function(e){
    $('#export .selected_fields li').each(function(e) {
      if(!$(this).hasClass('disable_fields')) {
        var el = $(this).clone().removeClass('checked');
        $('#export .block_base_fields').append(el[0]);
        $(this).remove();
      }
    });
    $('#export .block_base_fields li .icon-arrows-select-fields').remove();
  });


  $(document).on('click', '#export .add_combinations_filds', function(e){
    $('#export .block_combinations_fields li.checked').each(function(e) {
      var el = $(this).clone().removeClass('checked').append('<i class="icon-arrows icon-arrows-select-fields"></i>');
      $('#export .block_selected_fields .selected_fields').append(el[0]);
      $(this).remove();
    });
  });
  $(document).on('click', '#export .add_combinations_filds_all', function(e){
    $('#export .block_combinations_fields li').each(function(e) {
      var el = $(this).clone().removeClass('checked').append('<i class="icon-arrows icon-arrows-select-fields"></i>');
      $('#export .block_selected_fields .selected_fields').append(el[0]);
      $(this).remove();
    });
  });
  $(document).on('click', '#export .remove_combinations_filds', function(e){
    $('#export .selected_fields li.checked').each(function(e) {
      if(!$(this).hasClass('disable_fields')) {
        var el = $(this).clone().removeClass('checked');
        $('#export .block_combinations_fields').append(el[0]);
        $(this).remove();
      }
    });
    $('#export .block_combinations_fields li .icon-arrows-select-fields').remove();
  });
  $(document).on('click', '#export .remove_combinations_filds_all', function(e){
    $('.selected_fields li').each(function(e) {
      if(!$(this).hasClass('disable_fields')) {
        var el = $(this).clone().removeClass('checked');
        $('#export .block_combinations_fields').append(el[0]);
        $(this).remove();
      }
    });
    $('#export .block_combinations_fields li .icon-arrows-select-fields').remove();
  });


  $(document).on('click', '#export .add_specificPrice_filds', function(e){
    $('#export .block_specificPrice_fields li.checked').each(function(e) {
      var el = $(this).clone().removeClass('checked').append('<i class="icon-arrows icon-arrows-select-fields"></i>');
      $('#export .block_selected_fields .selected_fields').append(el[0]);
      $(this).remove();
    });
  });
  $(document).on('click', '#export .add_specificPrice_filds_all', function(e){
    $('#export .block_specificPrice_fields li').each(function(e) {
      var el = $(this).clone().removeClass('checked').append('<i class="icon-arrows icon-arrows-select-fields"></i>');
      $('#export .block_selected_fields .selected_fields').append(el[0]);
      $(this).remove();
    });
  });
  $(document).on('click', '#export .remove_specificPrice_filds', function(e){
    $('#export .selected_fields li.checked').each(function(e) {
      if(!$(this).hasClass('disable_fields')) {
        var el = $(this).clone().removeClass('checked');
        $('#export .block_specificPrice_fields').append(el[0]);
        $(this).remove();
      }
    });
    $('#export .block_specificPrice_fields li .icon-arrows-select-fields').remove();
  });
  $(document).on('click', '#export .remove_specificPrice_filds_all', function(e){
    $('#export .selected_fields li').each(function(e) {
      if(!$(this).hasClass('disable_fields')) {
        var el = $(this).clone().removeClass('checked');
        $('#export .block_specificPrice_fields').append(el[0]);
        $(this).remove();
      }
    });
    $('#export .block_specificPrice_fields li .icon-arrows-select-fields').remove();
  });

  $(document).on('keyup', '#export .search_base_fields', function(){
    var self = $(this);
    $('#export .block_base_fields li').each(function(){

      if( $(this).text().toLowerCase().indexOf(self.val().toLowerCase()) >= 0 ){
        $(this).show();
      }
      else{
        $(this).hide();
      }
    });
  });
  $(document).on('keyup', '#export .search_combinations_fields', function(){
    var self = $(this);
    $('#export .block_combinations_fields li').each(function(){

      if( $(this).text().toLowerCase().indexOf(self.val().toLowerCase()) >= 0 ){
        $(this).show();
      }
      else{
        $(this).hide();
      }
    });
  });
  $(document).on('keyup', '#export .search_specificPrice_fields', function(){
    var self = $(this);
    $('#export .block_specificPrice_fields li').each(function(){

      if( $(this).text().toLowerCase().indexOf(self.val().toLowerCase()) >= 0 ){
        $(this).show();
      }
      else{
        $(this).hide();
      }
    });
  });
  $(document).on('keyup', '#export .search_selected_fields', function(){
    var self = $(this);
    $('#export .selected_fields li').each(function(){

      if( $(this).text().toLowerCase().indexOf(self.val().toLowerCase()) >= 0 ){
        $(this).show();
      }
      else{
        $(this).hide();
      }
    });
  });


  $(document).on('change', '.select_products', function(){
    var id_shop = $("input[name=id_shop]").val();
    var shopGroupId = $("input[name=shopGroupId]").val();
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'add_product=true&id_product='+$(this).val() + '&id_shop=' + id_shop + '&shopGroupId=' + shopGroupId,
      dataType: 'json'
    });
  });

  $(document).on('click', '.updateproducts .nav-tabs li a', function(e){
    var tab = $(this).attr('href');
    tabActive(tab);
  });


  $(document).on('change', '.select_manufacturers', function(){
    var id_shop = $("input[name=id_shop]").val();
    var shopGroupId = $("input[name=shopGroupId]").val();
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'add_manufacturer=true&id_manufacturer='+$(this).val() + '&id_shop=' + id_shop + '&shopGroupId=' + shopGroupId,
      dataType: 'json'
    });
  });

  $(document).on('change', '.select_suppliers', function(){
    var id_shop = $("input[name=id_shop]").val();
    var shopGroupId = $("input[name=shopGroupId]").val();
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'add_supplier=true&id_supplier='+$(this).val() + '&id_shop=' + id_shop + '&shopGroupId=' + shopGroupId,
      dataType: 'json'
    });
  });

  $(document).on('change', '.selection_all', function(){
    $('#export .export_fields input').prop('checked', this.checked);
    $('#export .export_fields input[value=id_product]').prop('checked', true);
    $('#export .export_fields input[value=id_product_attribute]').prop('checked', true);
    $('#export .export_fields input[value=id_specific_price]').prop('checked', true);
  });

  $(document).on('change', '.selection_all_update', function(){
    $('#update .export_fields input').prop('checked', this.checked);
  });

  $(document).on('keyup', '#update #search_field', function(){
    var self = $(this);
    $('#update .export_fields label').each(function(){
      $(this).parent().css('opacity', '0.5');
      if( $(this).text().indexOf(self.val()) >= 0 ){
        $(this).parent().css('opacity', '1');
      }
    });
  });

  $(document).on('keyup', '#export #search_field', function(){

    var self = $(this);
    $('#export .export_fields label').each(function(){
      $(this).parent().css('opacity', '0.5');
      if( $(this).text().indexOf(self.val()) >= 0 ){
        $(this).parent().css('opacity', '1');
      }
    });
  });

  $(document).on('click', '.product_list #show_checked', function(e){
    e.preventDefault();
    $(".product_list .col-lg-6 .search_checkbox_table").val("");
    var id_lang = $("select[name=id_lang]").val();
    var id_shop = $("input[name=id_shop]").val();
    var shopGroupId = $("input[name=shopGroupId]").val();
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'show_checked_products=true' +'&id_shop='+id_shop+'&id_lang='+id_lang + '&shopGroupId=' + shopGroupId,
      dataType: 'json',
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
//        if (json['products']) {
          $(".product_list .col-lg-6 tbody").replaceWith(json['products']);
//        }
      }
    });
  });

  $(document).on('click', '.manufacturer_list #show_checked', function(e){
    e.preventDefault();
    var id_shop = $("input[name=id_shop]").val();
    var shopGroupId = $("input[name=shopGroupId]").val();
    $(".manufacturer_list .col-lg-6 .search_checkbox_table").val("");
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'show_checked_manufacturers=true&id_shop=' + id_shop + '&shopGroupId=' + shopGroupId,
      dataType: 'json', 
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
//        if (json['products']) {
        $(".manufacturer_list .col-lg-6 tbody").replaceWith(json['manufacturers']);
//        }
      }
    });
  });

  $(document).on('click', '.supplier_list #show_checked', function(e){
    e.preventDefault();
    var id_shop = $("input[name=id_shop]").val();
    var shopGroupId = $("input[name=shopGroupId]").val();
    $(".supplier_list .col-lg-6 .search_checkbox_table").val("");
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'show_checked_suppliers=true&id_shop=' + id_shop + '&shopGroupId=' + shopGroupId,
      dataType: 'json',
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
//        if (json['products']) {
        $(".supplier_list .col-lg-6 tbody").replaceWith(json['suppliers']);
//        }
      }
    });
  });

  $(document).on('click', '.product_list #show_all', function(e){
    e.preventDefault();
    var id_shop = $("input[name=id_shop]").val();
    var shopGroupId = $("input[name=shopGroupId]").val();
    $(".product_list .col-lg-6 .search_checkbox_table").val("");
    var id_lang = $("select[name=id_lang]").val();
    var id_shop = $("input[name=id_shop]").val();
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'show_all_products=true' + '&id_shop='+id_shop+'&id_lang='+id_lang + '&shopGroupId=' + shopGroupId,
      dataType: 'json',
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
//        if (json['products']) {
        $(".product_list .col-lg-6 tbody").replaceWith(json['products']);
//        }
      }
    });
  });

  $(document).on('click', '.manufacturer_list #show_all', function(e){
    e.preventDefault();
    var id_shop = $("input[name=id_shop]").val();
    var shopGroupId = $("input[name=shopGroupId]").val();
    $(".manufacturer_list .col-lg-6 .search_checkbox_table").val("");
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'show_all_manufacturers=true&id_shop=' + id_shop + '&shopGroupId=' + shopGroupId,
      dataType: 'json',
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
//        if (json['products']) {
        $(".manufacturer_list .col-lg-6 tbody").replaceWith(json['manufacturers']);
//        }
      }
    });
  });

  $(document).on('click', '.supplier_list #show_all', function(e){
    e.preventDefault();
    var id_shop = $("input[name=id_shop]").val();
    var shopGroupId = $("input[name=shopGroupId]").val();
    $(".supplier_list .col-lg-6 .search_checkbox_table").val("");
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'show_all_suppliers=true&id_shop=' + id_shop + '&shopGroupId=' + shopGroupId,
      dataType: 'json',
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
//        if (json['products']) {
        $(".supplier_list .col-lg-6 tbody").replaceWith(json['suppliers']);
//        }
      }
    });
  });

  $(document).on('keyup', '.product_list .search_checkbox_table', function(e){
    var id_lang = $("select[name=id_lang]").val();
    var id_shop = $("input[name=id_shop]").val();
    var shopGroupId = $("input[name=shopGroupId]").val();
    var self = $(this);

    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'search_product=' + $(this).val() +'&id_shop='+id_shop+'&id_lang='+id_lang + '&shopGroupId=' + shopGroupId,
      dataType: 'json',
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
        if (json['products']) {
          self.parents('table').find('tbody').replaceWith(json['products']);
        }
      }
    });
  })

  $(document).on('keyup', '.manufacturer_list .search_checkbox_table', function(e){
    var self = $(this);
    var id_shop = $("input[name=id_shop]").val();
    var shopGroupId = $("input[name=shopGroupId]").val();
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'search_manufacturer=' + $(this).val() + '&id_shop=' + id_shop + '&shopGroupId=' + shopGroupId,
      dataType: 'json',
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
        if (json['manufacturers']) {
          self.parents('table').find('tbody').replaceWith(json['manufacturers']);
        }
      }
    });
  })

  $(document).on('keyup', '.supplier_list .search_checkbox_table', function(e){
    var self = $(this);
    var id_shop = $("input[name=id_shop]").val();
    var shopGroupId = $("input[name=shopGroupId]").val();
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'search_supplier=' + $(this).val() + '&id_shop=' + id_shop + '&shopGroupId=' + shopGroupId,
      dataType: 'json',
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
        if (json['suppliers']) {
          self.parents('table').find('tbody').replaceWith(json['suppliers']);
        }
      }
    });
  })

  $(document).on('click', '.delete_setting', function(e){
    var id = $(this).attr('id-setting');
    var id_shop = $("input[name=id_shop]").val();
    var shopGroupId = $("input[name=shopGroupId]").val();
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'removeSetting=true&id=' + id + '&id_shop=' + id_shop + '&shopGroupId=' + shopGroupId,
      dataType: 'json',
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
        if (json['success']) {
          location.href = $("input[name=base_url]").val();
        }
      }
    });
  });


  $(document).on('click', '.delete_setting_update', function(e){
    var id = $(this).attr('id-setting-update');
    var id_shop = $("input[name=id_shop]").val();
    var shopGroupId = $("input[name=shopGroupId]").val();
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'removeSettingUpdate=true&id=' + id + '&id_shop=' + id_shop + '&shopGroupId=' + shopGroupId,
      dataType: 'json',
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
        if (json['success']) {
          location.href = $("input[name=base_url]").val();
        }
      }
    });
  });




  $(document).on('click', 'button.export', function(e){
    var data = '';
    $.each($('#export .selected_fields li'), function(i){
      data += '&field['+$(this).attr('data-value')+']='+ $(this).attr('data-name');
    });
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'export=true&' + $('form.updateproducts').serialize()+data,
      dataType: 'json',
      beforeSend: function(){
        $(".progres_bar_ex").show();
      },
      complete: function(){
        $(".progres_bar_ex").hide();
      },
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
        if( json.file ){
          location.href = json.file;
        }
      }
    });
  })

  $(document).on('click', '.saveSettingsExport', function(e){
    var data = '';
    $.each($('#export .selected_fields li'), function(i){
      data += '&field['+$(this).attr('data-value')+']='+ $(this).attr('data-name');
    });
    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'saveSettings=true' + $('form.updateproducts').serialize()+data,
      dataType: 'json',
      beforeSend: function(){
        $(".progres_bar_ex").show();
      },
      complete: function(){
        $(".progres_bar_ex").hide();
      },
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
        if(json['id']){
          location.href = $("input[name=base_url]").val() + '&settings='+json['id']
        }
        else{
          $(document).scrollTop(0);
          $('#content').prepend('<div class="alert alert-danger">' + json['error'] + '</div>');
        }
      }
    });
  });


  $(document).on('click', '.saveSettingsUpdate', function(e){

    var data = '';
    $.each($('#update .selected_fields li'), function(i){
      data += '&field_update['+$(this).attr('data-value')+']='+ $(this).attr('data-name');
    });

    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: 'saveSettingsUpdate=true' + $('form.updateproducts').serialize()+data,
      dataType: 'json',
      beforeSend: function(){
        $(".progres_bar_ex").show();
      },
      complete: function(){
        $(".progres_bar_ex").hide();
      },
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
        if(json['id']){
          location.href = $("input[name=base_url]").val() + '&settingsUpdate='+json['id'];
        }
        else{
          $(document).scrollTop(0);
          $('#content').prepend('<div class="alert alert-danger">' + json['error'] + '</div>');
        }
      }
    });
  });


  $(document).on('click', 'button.update', function(e){



    var xlsxData = new FormData();
    xlsxData.append('file', $('input[name=file]')[0].files[0]);
    xlsxData.append('id_shop', $("input[name=id_shop]").val());
    xlsxData.append('id_lang', $("select[name=id_lang_update]").val());
    xlsxData.append('format_file', $("input[name=format_file_update]:checked").val());
    xlsxData.append('update', true);

    $.each( $('#update .selected_fields li'), function() {
      xlsxData.append('field_update['+$(this).attr('data-value')+']',$(this).attr('data-name'));
    });

    $.ajax({
      url: '../modules/updateproducts/send.php',
      type: 'post',
      data: xlsxData,
      dataType: 'json',
      processData: false,
      contentType: false,
      beforeSend: function(){
        $(".progres_bar_ex").show();
      },
      complete: function(){
        $(".progres_bar_ex").hide();
      },
      success: function(json) {
        $('.alert, .alert-danger, .alert-success').remove();
        if( !json.error  ){

          var success = json['success'];
          var error_logs = success.error_logs;

          if(error_logs){
            var url = error_logs;
            url = '<a class="error_logs_import" href="'+url+'">error_logs.csv<a>';
          }
          else{
            var url = '';
          }

          $('#content').prepend('<div class="alert alert-success">' + success.message + url + '</div>');
        }
        else{
          $('#content').prepend('<div class="alert alert-danger">' + json['error'] + '</div>');
        }
        $(document).scrollTop(0);
      }
    });
  })

})

function tabActive(tab){
  if(tab == '#export'){
    $('.content-setting-list').show()
    $('.content-setting-list-update').hide()
  }
  else if(tab == '#update'){
    $('.content-setting-list').hide()
    $('.content-setting-list-update').show()
  }
}