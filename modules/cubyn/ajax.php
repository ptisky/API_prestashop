<?php
/**
 * 2015-2016 Cubyn
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Cubyn <modules@cubyn.com>
 * @copyright 2015-2016 Cubyn
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once dirname(__FILE__).'/../../config/config.inc.php';
require_once dirname(__FILE__).'/../../init.php';
require_once dirname(__FILE__).'/classes/CubynApi.php';
require_once dirname(__FILE__).'/Model/CubynOrderModel.php';
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

/****************************************************
 * securize webhooks by allowing only cubyn key
 ****************************************************/

$cubynKey = Configuration::get('CUBYN_API_KEY');

/****************************************************
 * errors
 ****************************************************/

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    header('HTTP/1.0 404 Not Found');
    die(Tools::jsonEncode(array(
        'message' => 'Method '.$_SERVER['REQUEST_METHOD'].' is not allowed',
        'type'    => 'MethodError',
    )));
}

function unauthorized()
{
    header('HTTP/1.0 401 Unauthorized');
    die(Tools::jsonEncode(array(
        'message' => 'You are not elegible for this call',
        'type'    => 'UnauthorizedError',
    )));
}

function badRequest($message)
{
    header('HTTP/1.0 400 Bad Request');
    die(Tools::jsonEncode(array(
        'message' => $message,
        'type'    => 'BadRequest',
    )));
}

function alreadyInThatState($toState)
{
    header('HTTP/1.0 400 Bad Request');
    die(Tools::jsonEncode(array(
        'message' => "Order already in $toState state",
        'type'    => 'BadRequest',
    )));
}

function webhookDisabled($toState)
{
    die(Tools::jsonEncode(array(
        'message' => "Webhooks disabled for state $toState",
        'type'    => 'WebhookDisabled',
    )));
}

/****************************************************
 * check inputs
 ****************************************************/

try {

    $data   = Tools::jsonDecode(Tools::file_get_contents('php://input'));
    $key    = $data->key;
    $event  = $data->event;
    $parcel = $data->parcel;

    if (!$key || $key !== $cubynKey) {
        unauthorized();
    }

    if (!$event || !$parcel) {
        badRequest("Body should contain 'event', 'parcel' and 'key'");
    }

    $orderRef = $parcel->orderRef;
    $status   = $parcel->status;

    if (!$orderRef || !$status) {
        badRequest("Parcel should contain 'orderRef' and 'status'");
    }

    $sql      = 'SELECT id_order FROM `'._DB_PREFIX_.'orders` o WHERE o.`reference` = "'.$orderRef.'"';
    $order_id = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
    if (!$order_id) {
        badRequest("Order $orderRef was not found");
    }

    $order              = new Order($order_id);
    $currentState       = $order->getCurrentState();
    $configPickedState  = Configuration::get('CUBYN_CRT_PICKED_STATE_NAME_ID');
    $configShippedState = Configuration::get('CUBYN_CRT_SHIPPED_STATE_NAME_ID');

    if ($event === 'parcel:deleted') {
        $exists = CubynOrderModel::findByOrderId($order_id);
        if (!$exists) {
            alreadyInThatState();
        }
        $cubynOrder           = new CubynOrderModel($exists['id_cubyn']);
        $cubynOrder->exported = 0;
        $cubynOrder->shipped  = 0;
        $cubynOrder->save();
        die(Tools::jsonEncode(array(
            'message' => 'Order export reset',
            'type'    => 'OrderExportReset',
        )));
    }

    switch ($status) {
        /****************************************************
         * pop statuses
         ****************************************************/

        case "PICKED":
            $toState = $configPickedState;
            // no webhook for that one
            if ($toState == CubynApi::DISABLED_STATE) {
                webhookDisabled($status);
            }
            // already shipped
            if ($currentState === $toState || $currentState === $configShippedState) {
                alreadyInThatState($status);
            }
            $order->setCurrentState($toState);
            $order->save();
            die(Tools::jsonEncode(array(
                'message' => 'Order state updated',
                'type'    => 'OrderPicked',
            )));
            break;

        case "SHIPPED":
            $toState = $configShippedState;
            // no webhook for that one
            if ($toState == CubynApi::DISABLED_STATE) {
                webhookDisabled($status);
            }
            if ($currentState === $toState) {
                alreadyInThatState($status);
            }
            // update tracking ID
            $sql              = 'SELECT id_order_carrier FROM `'._DB_PREFIX_.'order_carrier` oc WHERE oc.`id_order` = "'.$order_id.'"';
            $order_carrier_id = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
            if (!$order_carrier_id) {
                badRequest("Carrier record was not found for $orderRef");
            }
            $orderCarrier                  = new OrderCarrier($order_carrier_id);
            $orderCarrier->tracking_number = $parcel->trackingId;
            $orderCarrier->save();
            // update order status
            $order->setCurrentState($toState);
            $order->save();
            die(Tools::jsonEncode(array(
                'message' => 'Order state + tracking updated',
                'type'    => 'OrderShipped',
            )));
            break;
    }

    die(Tools::jsonEncode(array(
        'message' => 'No change',
        'type'    => 'NoChange',
    )));
} catch (Exception $e) {

    header('HTTP/1.0 500 Internal Server Error');
    die(Tools::jsonEncode(array(
        'message' => $e->getMessage(),
        'type'    => 'Error',
        'trace'   => $e->getTraceAsString(),
    )));
}
