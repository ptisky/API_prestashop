<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 *         DISCLAIMER   *
 * *************************************** */
/* Do not edit or add to this file if you wish to upgrade Prestashop to newer
* versions in the future.
* ****************************************************
*
*  @author     BEST-KIT.COM (contact@best-kit.com)
*  @copyright  http://best-kit.com
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class bestkit_lazyloading extends Module
{
    const PREFIX = 'bestkit_ll_';

    protected $_hooks = array(
        'header',
    );

    protected $_patterns = array(
        array(
            'type' => 'theme',//TODO: module
            'config_key' => 'product-list',
            'filename' => 'product-list.tpl',
            'search_pattern' => '<img class="replace-2x img-responsive" src="',
            'replace_pattern' => '<img class="replace-2x img-responsive lazy" src="{$base_uri}/modules/bestkit_lazyloading/views/img/loader.gif" data-original="',
            'end' => '<script type="text/javascript">bestkitLazyLoading.loadImages();</script>',
        ),
    );

    public function __construct()
    {
        $this->name = 'bestkit_lazyloading';
        $this->tab = 'front_office_features';
        $this->version = '1.6.1';
        $this->author = 'best-kit.com';
        $this->need_instance = 0;
        $this->module_key = 'ba702b0ba204392c2a979a4c743eec55';
        $this->bootstrap = TRUE;

        parent::__construct();
		
        $this->displayName = $this->l('Lazy loading / Load your images on demand');
        $this->description = $this->l('A very good solution to this is to load your images on demand, or what is most commonly known as lazy loading. Package for PrestaShop v.1.6.x');
    }

    public function install()
    {
        foreach ($this->_patterns as $_rule) {
            if ($_rule['type'] == 'theme') {
                $tpl = _PS_ALL_THEMES_DIR_ . _THEME_NAME_ . '/' . $_rule['filename'];
            }

            $time = time();
            if (copy($tpl, $tpl . $time)) {
                Configuration::updateValue(self::PREFIX . $_rule['config_key'], $time);
                $tpl_content = Tools::file_get_contents($tpl);
                $replace_result = str_replace($_rule['search_pattern'], $_rule['replace_pattern'], $tpl_content);
                $replace_result .= $_rule['end'];

                $fp = fopen($tpl, 'w');
                if(flock($fp, LOCK_EX)) { //Lock File
                    fwrite($fp, $replace_result);
                    flock($fp, LOCK_UN); //Unlock File
                    fclose($fp);
                } else {
                    $this->_errors[] = sprintf($this->l('Could not Lock File: `%s`.'), $_rule['filename']);
                    return false;
                }
            } else {
                $this->_errors[] = sprintf($this->l('Unable to install the module (Can not make backup for `%s`, please check permissions and try again).'), $_rule['filename']);
                return false;
            }
        }
        unset($_rule);

        $return = parent::install();
        foreach ($this->_hooks as $hook) {
            if (!$this->registerHook($hook)) {
                return FALSE;
            }
        }

        return $return;
    }

    public function uninstall()
    {
        foreach ($this->_patterns as $_rule) {
            if ($_rule['type'] == 'theme') {
                $tpl = _PS_ALL_THEMES_DIR_ . _THEME_NAME_ . '/' . $_rule['filename'];
            }

            $time = Configuration::get(self::PREFIX . $_rule['config_key']);
            $replace_result = Tools::file_get_contents($tpl . $time);

            $fp = fopen($tpl, 'w');
            if(flock($fp, LOCK_EX)) { //Lock File
                fwrite($fp, $replace_result);
                flock($fp, LOCK_UN); //Unlock File
                fclose($fp);
            } else {
                $this->_errors[] = sprintf($this->l('Could not Lock File: `%s`.'), $_rule['filename']);
                return false;
            }
        }

        return parent::uninstall();
    }

    public function hookHeader($params) {
        $this->context->controller->addJS(($this->_path) . 'views/js/jquery.lazyload.js');
        $this->context->controller->addJS(($this->_path) . 'views/js/bestkit_lazyload.js');
    }

    /*public function getContent() {
        $redirect = $this->context->link->getAdminLink('AdminImport');
        header('Location: ' . $redirect);
        exit;
    }*/
}