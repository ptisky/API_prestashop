{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="{$lang_iso}"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="{$lang_iso}"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="{$lang_iso}"> <![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="{$lang_iso}"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$lang_iso}">
	<head>
		<title>{$meta_title|escape:'htmlall':'UTF-8'}</title>
{if isset($meta_description) AND $meta_description}
		<meta name="description" content="{$meta_description|escape:html:'UTF-8'}" />
{/if}
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
{if isset($meta_keywords) AND $meta_keywords}
		<meta name="keywords" content="{$meta_keywords|escape:html:'UTF-8'}" />
{/if}

		<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
		<meta http-equiv="content-language" content="{$meta_language}" />
		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
        <script src="http://wholesale.euvapors.com/modules/blocktopmenu/js/superfish-modified.js" type="text/javascript"></script>
		<script type="text/javascript">
			var baseDir = '{$content_dir|addslashes}';
			var baseUri = '{$base_uri|addslashes}';
			var static_token = '{$static_token|addslashes}';
			var token = '{$token|addslashes}';
			var priceDisplayPrecision = {$priceDisplayPrecision*$currency->decimals};
			var priceDisplayMethod = {$priceDisplay};
			var roundMode = {$roundMode};
			  $.noConflict();
			  jQuery( document ).ready(function($) {
			  	if (($("#bg-promo").length > 0)){
   $('#tit').css('margin-top','110px');
  
}
 var widthdevice = $(window).width();
if(widthdevice<1024)
{
	$('.layered_close').addClass('closed');
	$('#layered_form div div ul').css('display','none')
}
if (($("#leftcol").length > 0)&&$(window).width() >1000){
   $('#product_list').css('width','100%');
   }
var supportsOrientationChange = "onorientationchange" in window,
    orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";
window.addEventListener(orientationEvent, function() {
$(".paireprod").css('margin-left', "0" );
 
}, false);

			  	$("#s-menumob").hide();
			  /**	var number_of_items = $('#menumobb').children().size();
			  	var i =1;
$('#menumobb').children().each(function() {
	$('#menumobb li').addClass("chedren"+i);
i++;
});**/

			 /** 	$(".sf-menu li").click(function() {
			  			this.addClass("sfHoverdd");
			  		//alert("test");
			  	});**/

		
			  
			 $(".btn-navbar").click(function() {

$("#s-menumob").toggle();


});
			$(".menumob").click(function() {
				var v = this.id;
				
               $(this).toggleClass( "activ" );
 });

			// $(".sf-with-ul").removeAttr("href").css("cursor","pointer");
			
			});
		</script>

		{if isset($css_files)}
	{foreach from=$css_files key=css_uri item=media}
	<link href="{$css_uri}" rel="stylesheet" type="text/css" media="{$media}" />
	{/foreach}
{/if}
{if isset($js_files)}
	{foreach from=$js_files item=js_uri}
	<script type="text/javascript" src="{$js_uri}"></script>
	{/foreach}
{/if}
<link href="{$content_dir}themes/euvapor/css/phone.css" rel="stylesheet" type="text/css" media="{$media}" />
<link href="{$content_dir}themes/euvapor/css/caroo.css" rel="stylesheet" type="text/css" media="{$media}" />
<link href="{$content_dir}themes/euvapor/css/modules/blocksearch/blocksearch.css" rel="stylesheet" type="text/css" media="{$media}" />
<link href="{$content_dir}themes/euvapor/css/product_list.css" rel="stylesheet" type="text/css" media="{$media}" />

		{$HOOK_HEADER}
	</head>
	
	<body {if isset($page_name)}id="{$page_name|escape:'htmlall':'UTF-8'}" class=""{/if}>
	{if !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
		<div id="restricted-country">
			<p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country}</span></p>
		</div>
		{/if}
		<div id="page" class="container_9 clearfix">

			<!-- Header -->
			<div id="header" class="">
				<div id="header_left">
				<a id="header_logo" href="{$base_dir}" title="{$shop_name|escape:'htmlall':'UTF-8'}">
					<img class="logo" src="{$logo_url}" alt="{$shop_name|escape:'htmlall':'UTF-8'}" {if $logo_image_width}width="{$logo_image_width}"{/if} {if $logo_image_height}height="{$logo_image_height}" {/if}/>
				</a></div>
				<div id="header_center" class="">
					 {hook h='displayTop'}
				</div>
              <div id="header_right" class="">
               {if $logged}
				<a href="{$link->getPageLink('my-account', true)}" title="{l s='Your Account'}"><div id="cmt"><div id="vtext">{l s='Mon compte'}</div></div></a>
				{else}
					<a href="{$link->getPageLink('my-account', true)}" title="{l s='Your Account'}"><div id="pro"><div id="vtext">{l s='Espace professionnel'}</div></div></a>
				{/if}
				{if !$logged}
                <a href="{$link->getPageLink('my-account', true)}"><div id="sub"><div id="vtext">{l s='SE CONNECTER'}</div></div></a>
                {else}
               <a href="{$base_dir}index.php?mylogout"><div id="sub"><div id="vtext">{l s='DECONNECTER'}</div></div></a>
                {/if}
                <a href="{$link->getPageLink('order', true)}"><div id="pan">

                <div id="vtext">{l s='Panier'}</div>
</div>
               </a>
                <a class="btn btn-navbar">

    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>

</a>

			  </div>

			</div>
			<div id="headermob" class="">
			<div id="header_leftmob">
				<a id="header_logo" href="{$base_dir}" title="{$shop_name|escape:'htmlall':'UTF-8'}">
					<img class="logo1" src="{$img_dir}logomob.jpg" alt="{$shop_name|escape:'htmlall':'UTF-8'}" {if $logo_image_width}width="{$logo_image_width}"{/if} {if $logo_image_height}height="{$logo_image_height}" {/if}/>
				</a></div>
				<div class="iconn">
				 {if $cookie->isLogged()}
				<a href="{$link->getPageLink('my-account', true)}" title="{l s='Your Account'}"><i class="flaticon-user58"></i> </a>
				{else}
					<a href="{$link->getPageLink('my-account', true)}" title="{l s='Your Account'}"><i class="flaticon-approve"></i>  </a>
				{/if}
{if !$cookie->isLogged()}
                
                <a href="{$link->getPageLink('my-account', true)}" title="{l s='Your Account'}"><i class="flaticon-lock7"></i>  </a>
                {else}
               
                 <a href="{$base_dir}index.php?mylogout" title="{l s='Your Account'}"><i class="flaticon-lock7"></i>  </a>
                {/if}

<a href="{$link->getPageLink('order', true)}" title="{l s='Your Account'}"><i class="flaticon-grocery6 flaticonpan"></i>  </a>
</div>
				<a class="btn btn-navbar">

    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>

</a>

			</div>
			<div id="s-menumob" class="">
					 {hook h='displayTop'}
				</div>
            {if $page_name == 'index'}
            <div id="sliderd"> {hook h='displaySlider'}</div>
            {hook h='displayHome'}
         <div id="box-vert">
              <div id="boxes">
              <a href="{$base_dir}160-grossiste-cigarette-electronique">
                <div id="box1">
                
                <img src="{$img_dir}/cigare.png" class="imgbox1" >
                <div class="supar"></div>
                <div class="text-box1">{l s="GROSSISTE E-CIGARETTE"}</div>
               
                </div> </a>
   <a href="{$base_dir}168-grossiste-vaporisateur">
                <div id="box2">
                
                <img src="{$img_dir}/vapoteur.png" class="imgbox2">
                <div class="supar"></div>
                <div class="text-box2">{l s="GROSSISTE VAPORISATEUR"}</div>
                
                </div></a>
              </div>
        </div> 

        {/if}
			<div id="columns" class="grid_9 alpha omega clearfix">
				<!-- Left -->

            
				<!--div id="left_column" class="column grid_2 alpha">
					{* $HOOK_LEFT_COLUMN *}
				</div-->

				<!-- Center -->
				{if $page_name=='index'}
				<div id="center_column" class=""> 
				{hook h='displayCol'}
				</div>
				{/if}
	{/if}
