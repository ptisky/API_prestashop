<?php
	include(dirname(__FILE__).'/../../../config/config.inc.php');
	include(dirname(__FILE__).'/../../../init.php');
	include(dirname(__FILE__).'/../pfupsell.php');
	
	$upsell_obj = new PFUpSell();
	
	if (Tools::getValue('action') AND $value = urldecode(Tools::getValue('q')) AND !is_array($value))
	{
		$search = Search::find(intval(Tools::getValue('id_lang')), $value, 1, 10, 'position', 'desc', true);	
		foreach ($search as $product)
			echo $product['id_product'].'|'.$product['pname'].'|'.$product['cname']."\n";
	}
				
	if (Tools::getIsset('action') && Tools::getValue('action') == 'add_product')
	{	
		$error = '';
		$content = '';	
		$psv = Tools::getValue('psv');
		$products = unserialize(Configuration::get('PF_UPSELL_PRODUCTS'));
		$product_id = Tools::getValue('product_id');
		if (!Validate::isLoadedObject($product_obj = new Product($product_id)))
			$error = $upsell_obj->l('The product ID is invalid');
		else
		{
			if (isset($product_id) && $product_id != '' && !in_array($product_id, $products))
				array_push($products, $product_id);	
			else
				$error = $upsell_obj->l('You already have this product');
		}	
			
			Configuration::updateValue('PF_UPSELL_PRODUCTS', serialize($products));
			$product_ids = unserialize(Configuration::get('PF_UPSELL_PRODUCTS'));
			$id_language = Configuration::get('PS_LANG_DEFAULT');
			$link = new Link();
			
			$content .= '
				<div class="form-group listOfPack">';
				if ($psv >= 1.6)
					$content .= '
						<div class="col-lg-12">';
							$content .= '
								<ul id="divPackItems" class="'.($psv >= 1.6 ? 'list-unstyled':'upsell_pr_list').'">';
							foreach($product_ids as $id)
							{
								$product = new Product($id, true, $id_language, Shop::getContextShopID());
								$cover_image = $product->getCover($id);
								$avalibale_image = Image::getImages($id_language, $id);
								if(isset($avalibale_image) && !empty($avalibale_image))
									$img_link = $link->getImageLink($product->link_rewrite, $cover_image['id_image'], 'home_default');
								else
									$img_link = $link->getImageLink($product->link_rewrite, $product->defineProductImage($product->getImages($id_language), $id_language), 'home_default');
								$content .= '
										<li class="product-pack-item media-product-pack" '.($psv >= 1.6 ?'style="width:180px"':'').'>
											<img class="media-product-pack-img" src="'.Tools::getProtocol().$img_link.'"/>
											<span class="media-product-pack-title"> '.$upsell_obj->l('Name').': '.(strlen($product->name) >= 14?substr($product->name, 0, 14)."...":$product->name).'</span>
											<span class="media-product-pack-ref"> '.$upsell_obj->l('REF:').' '.$product->reference.'</span>';
											if($psv >= 1.6)
												$content .= '<button type="button" class="btn btn-default media-product-pack-action delete_upsell_pr" data-delete="'.$id.'"><i class="icon-trash"></i></button>';
											else
												$content .= '<a href="#" class="media-product-pack-action delete_upsell_pr" data-delete="'.$id.'"><img src="../img/admin/delete.gif" alt="'.$upsell_obj->l('Delete').'"></a>';
								$content .= 
										'</li>';
							}
							$content .= 
								'</ul>';
			if ($psv >= 1.6)
				$content .=
						'</div>';
			$content .= '
				</div>
				<input type="hidden" name="upsell_basedir" id="basedir" value="'._MODULE_DIR_.'pfupsell">
				<script type="text/javascript">
					$(document).ready(function(){
						$(".delete_upsell_pr").click(function()
							{
								var product_id = $(this).attr("data-delete");
								var upSellDir = $("[name=upsell_basedir]").val();;
								$.ajax({
									type: "POST",
									url: upSellDir+"/ajax/delete_product.php",
									data: {
										action : "delete",
										product_id : product_id
									},
									success: function(response){
										location.reload();
									}
								});	
								return false;
							}
						);
					});
				</script>
			';
		die(Tools::jsonEncode(array('content' => $content, 'error' => $error)));														
	}
?>