<?php

try{
$bdd_addresses = new PDO('mysql:host=localhost;dbname=prestashop;charset=utf8', 'root', '');
}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}
	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	   //																																			  ///////
	  //-----------------------------------------------------------// PARTIE PRESTASHOP //-----------------------------------------------------------///////
	 //																																				///////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				//connection de l'api au webservice
				// include("../connection.php");

	//on recupere le fichier xml
	$get_xml_addresses = simplexml_load_file("addresses.xml");
	
	//début de la boucle pour envoyer les customers
	foreach ($get_xml_addresses->children()->children() as $ressources_addresses)
	{
		try {
				$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); //connection
				$xml_addresses = $webService->get(array('url' => PS_SHOP_PATH.'/api/addresses?schema=synopsis'));//on recupere un shemas blanc xml

				//autres données sans l'id dans mon xml
				$id_addresses  			     						= $ressources_addresses->id;
				$id_customer_addresses 						    	= $ressources_addresses->id_customer;
				$id_manufacturer_addresses  			  			= $ressources_addresses->id_manufacturer;
				$id_supplier_addresses  							= $ressources_addresses->id_supplier;
				$id_warehouse_addresses 		    		 		= $ressources_addresses->id_warehouse;
				$id_country_addresses      	     	   				= $ressources_addresses->id_country;
				$id_state_addresses  			      	   			= $ressources_addresses->id_state;
				$alias_addresses 					           		= $ressources_addresses->alias;
				$company_addresses     		     	 		   		= $ressources_addresses->company;
				$lastname_addresses  				      	 		= $ressources_addresses->lastname;
				$firstname_addresses 			     	 			= $ressources_addresses->firstname;
				$vat_number_addresses     					  		= $ressources_addresses->vat_number;
				$address1_addresses  					 	     	= $ressources_addresses->address1;
				$address2_addresses 						        = $ressources_addresses->address2;
				$postcode_addresses     						    = $ressources_addresses->postcode;
				$city_addresses  							        = $ressources_addresses->city;
				$phone_addresses	  		 						= $ressources_addresses->phone;
				$phone_mobile_addresses    		  					= $ressources_addresses->phone_mobile;
				$dni_addresses  						     		= $ressources_addresses->dni;
				$deleted_addresses 		   			    			= $ressources_addresses->deleted;
				$date_add_addresses     					   	    = $ressources_addresses->date_add;
				$date_upd_addresses     					   	    = $ressources_addresses->date_upd;

				//on associe les données de mon xml au xml blank créer au debut
				$xml_addresses->address->id_customer 					       		= $id_customer_addresses;
				$xml_addresses->address->id_manufacturer  					    	= $id_manufacturer_addresses;
				$xml_addresses->address->id_supplier 						    	= $id_supplier_addresses;
				$xml_addresses->address->id_warehouse 			        			= $id_warehouse_addresses;
				$xml_addresses->address->id_country  					        	= $id_country_addresses;
				$xml_addresses->address->id_state 		      						= $id_state_addresses;
				$xml_addresses->address->alias 	  									= $alias_addresses;
				$xml_addresses->address->company  			        				= $company_addresses;
				$xml_addresses->address->lastname 					          		= $lastname_addresses;
				$xml_addresses->address->firstname 					           	 	= $firstname_addresses;
				$xml_addresses->address->vat_number  					        	= $vat_number_addresses;
				$xml_addresses->address->address1 					          		= $address1_addresses;
				$xml_addresses->address->address2 				            		= $address2_addresses;
				$xml_addresses->address->postcode  				       				= $postcode_addresses;
				$xml_addresses->address->city 						            	= $city_addresses;
				$xml_addresses->address->phone 					            		= $phone_addresses;
				$xml_addresses->address->phone_mobile  				            	= $phone_mobile_addresses;
				$xml_addresses->address->dni 					            		= $dni_addresses;
				$xml_addresses->address->deleted 					            	= $deleted_addresses;
				$xml_addresses->address->date_add     								= $date_add_addresses;
				$xml_addresses->address->date_upd 	       							= $date_upd_addresses;					
				
				$opt_addresses = array('resource' => 'addresses');
				$opt_addresses['postXml'] = $xml_addresses->asXML();

				// var_dump($opt);
				
				//envoie le xml a prestashop
				$xml_addresses = $webService->add($opt_addresses);

				$monid_addresses = $xml_addresses->addresses->id;//récupere l'id inséré dans prestashop par auto increment
				$bdd_addresses->query('UPDATE `prestashop`.`ps_address` SET `id_address` = '.$id_addresses.' WHERE `ps_address`.`id_address` = '.$monid_addresses.'');//envoie la nouvelle id				
			}	
			catch (PrestaShopWebserviceException $e)
			{
			  // Here we are dealing with errors
			  $trace = $e->getTrace();
			  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
			  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
			  else echo 'Other error<br />'.$e->getMessage();
			}
	}
	echo 'adresse ok','<br>';
	?>