
$(document).ready(function()
	{
		var upSellDir = $("[name=upsell_basedir]").val();
		var psv = $("[name=psv]").val();	
		$('#reductionProductFilter').autocomplete(upSellDir + "/ajax/ajax.php", 
			{
				minChars: 2,
				max: 50,
				width: 500,
				formatItem: function (data) {
					return data[0]+ '('+data[2] + '>' + data[1]+')';
				},
				scroll: false,
				multiple: false,
				extraParams: {
					action : 'productFilter',
					id_lang : $("#product_language").val()
				}
			}
		);
	
	
	$("#add_upsell_item").click(function()
		{
			var product_id = $("#reductionProductFilter").val();
			
			$.ajax({
				type: 'POST',
				url: upSellDir + "/ajax/ajax.php",
				data: {
					action : "add_product",
					product_id : product_id,	
					psv : psv	
				},
				success: function(response){
					var post_type = $.parseJSON(response);
					if(post_type.error){
						$(".error").removeClass("hide");
						$(".error").html(post_type.error);
					}else
						$(".error").addClass("hide");
					$(".upsell_products_list").html(post_type.content);
					$("#reductionProductFilter").val("");
				}
			});	
		}
	);
	
	$(".delete_upsell_pr").click(function()
		{
			var product_id = $(this).attr("data-delete");
			$.ajax({
				type: "POST",
				url: upSellDir + "/ajax/delete_product.php",
				data: {
					action : "delete",
					product_id : product_id
				},
				success: function(response){
					location.reload();
				}
			});	
			return false;
		}
	);	

});
