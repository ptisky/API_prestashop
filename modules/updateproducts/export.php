<?php

class exportProducts
{
  private $_context;
  private $_idShop;
  private $_shopGroupId;
  private $_idLang;
  private $_format;
  private $_model;
  private $_PHPExcel;
  private $_alphabet;
  private $_head;

  public function __construct( $idShop, $idLang, $format, $shopGroupId ){
    include_once(dirname(__FILE__).'/../../config/config.inc.php');
    include_once(dirname(__FILE__).'/../../init.php');
    include_once(_PS_MODULE_DIR_ . 'updateproducts/libraries/PHPExcel_1.7.9/Classes/PHPExcel.php');
    include_once(_PS_MODULE_DIR_ . 'updateproducts/libraries/PHPExcel_1.7.9/Classes/PHPExcel/IOFactory.php');
    include_once('datamodel.php');
    $this->_context = Context::getContext();
    $this->_idShop = $idShop;
    $this->_shopGroupId = $shopGroupId;
    $this->_idLang = $idLang;
    $this->_format = $format;
    $this->_model = new productsUpdateModel();
    $this->_PHPExcel = new PHPExcel();
    $this->_alphabet = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
      'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
      'BA','BB','BC','BD','BE','BF','BG','BH', 'BI','BJ','BK','BL','BM','BN','BO','BP','BQ', 'BR','BS','BT','BU','BV','BW','BX','BY','BZ',
      'CA','CB','CC','CD','CE','CF','CG','CH', 'CI','CJ','CK','CL','CM','CN','CO','CP','CQ', 'CR','CS','CT','CU','CV','CW','CX','CY','CZ',
      'DA','DB','DC','DD','DE','DF','DG','DH', 'DI','DJ','DK','DL','DM','DN','DO','DP','DQ', 'DR','DS','DT','DU','DV','DW','DX','DY','DZ',
      'EA','ED','EC'
    );
  }

  public function export()
  {
    $productIds = $this->_model->getExportIds( $this->_idShop, $this->_idLang, $this->_shopGroupId  );
    return $this->_getProductsData($productIds);
  }

  private function _getProductsData( $productIds )
  {
    $line = 2;
    $this->_createHead();
    foreach( $productIds as $productId ){
      $productId = $productId['id_product'];
      $this->_setProductInFile($this->_getProductById($productId), $line);
      $line++;
    }

    $this->_setStyle($line);
    $fileName = $this->_saveFile();

    return $fileName;
  }
  
  private function _setStyle( $line )
  {
    $i = $line;
    $j = count($this->_head);

    $style_wrap = array(
      'borders'=>array(
        'outline' => array(
          'style'=>PHPExcel_Style_Border::BORDER_THICK
        ),
        'allborders'=>array(
          'style'=>PHPExcel_Style_Border::BORDER_THIN,
          'color' => array(
            'rgb'=>'696969'
          )
        )
      )
    );
    $this->_PHPExcel->getActiveSheet()->getStyle('A1:'.$this->_alphabet[$j-1].($i-1))->applyFromArray($style_wrap);

    $style_hprice = array(
      //выравнивание
      'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
      ),
      //заполнение цветом
      'fill' => array(
        'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
        'color'=>array(
          'rgb' => 'CFCFCF'
        )
      ),
      //Шрифт
      'font'=>array(
        'bold' => true,
        'italic' => true,
        'name' => 'Times New Roman',
        'size' => 13
      ),
    );
    $this->_PHPExcel->getActiveSheet()->getStyle('A1:'.$this->_alphabet[$j-1].'1')->applyFromArray($style_hprice);

    $style_price = array(
      'alignment' => array(
        'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_LEFT,
      )
    );
    $this->_PHPExcel->getActiveSheet()->getStyle('A2:'.$this->_alphabet[$j-1].($i-1))->applyFromArray($style_price);

    $style_background1 = array(
      //заполнение цветом
      'fill' => array(
        'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
        'color'=>array(
          'rgb' => 'F2F2F5'
        )
      ),
    );
    $this->_PHPExcel->getActiveSheet()->getStyle('A2:'.$this->_alphabet[$j-1].($i-1))->applyFromArray($style_background1);
  }

  private function _setProductInFile( $product, $line )
  {
    $i = 0;
    foreach($this->_head as $field => $name){
      $this->_PHPExcel->setActiveSheetIndex(0)
        ->setCellValue($this->_alphabet[$i].$line, isset($product[$field]) ? $product[$field] : '');
      $i++;
    }
  }

  private function _saveFile()
  {   
    $date = date('Y.m.d_G-i-s');
    if ($this->_format == 'xlsx'){
      $objWriter = PHPExcel_IOFactory::createWriter($this->_PHPExcel, 'Excel2007');
      $objWriter->save('files/export_products_' . $date . '.xlsx');
    }
    elseif ($this->_format == 'csv'){
      $objWriter = PHPExcel_IOFactory::createWriter($this->_PHPExcel, 'CSV');
      $objWriter->save('files/export_products_' . $date . '.csv');
    }

    return _PS_BASE_URL_.__PS_BASE_URI__.'modules/updateproducts/files/export_products_' . $date . '.' . $this->_format;
  }

  private function _createHead()
  {
    $this->_head = $this->_getHeadFields();
    $this->_PHPExcel->getProperties()->setCreator("PHP")
      ->setLastModifiedBy("Admin")
      ->setTitle("Office 2007 XLSX")
      ->setSubject("Office 2007 XLSX")
      ->setDescription(" Office 2007 XLSX, PHPExcel.")
      ->setKeywords("office 2007 openxml php")
      ->setCategory("File");
    $this->_PHPExcel->getActiveSheet()->setTitle('Export');

    $i = 0;
    foreach($this->_head as $field => $name) {
      $this->_PHPExcel->setActiveSheetIndex(0)
        ->setCellValue($this->_alphabet[$i].'1', $name);
      if( $field == "product_link" ){
        $this->_PHPExcel->getActiveSheet()->getColumnDimension($this->_alphabet[$i])->setWidth(80);
      }
      elseif( $field == "images" ){
        $this->_PHPExcel->getActiveSheet()->getColumnDimension($this->_alphabet[$i])->setWidth(80);
      }
      elseif( $field == "name" ){
        $this->_PHPExcel->getActiveSheet()->getColumnDimension($this->_alphabet[$i])->setWidth(80);
      }
      elseif( $field == "description" ){
        $this->_PHPExcel->getActiveSheet()->getColumnDimension($this->_alphabet[$i])->setWidth(80);
      }
      elseif( $field == "description_short" ){
        $this->_PHPExcel->getActiveSheet()->getColumnDimension($this->_alphabet[$i])->setWidth(80);
      }
      else{
        $this->_PHPExcel->getActiveSheet()->getColumnDimension($this->_alphabet[$i])->setWidth(30);
      }
      $i++;
    }

    $this->_PHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
  }

  private function _getHeadFields()
  {
    $selected_fields = Tools::unserialize(Configuration::get('GOMAKOIL_FIELDS_CHECKED','',$this->_shopGroupId,$this->_idShop));

    if( isset($selected_fields['features']) ){
      unset($selected_fields['features']);
      foreach( Feature::getFeatures($this->_idLang) as $feature ){
        $selected_fields[$feature['id_feature'] . "_FEATURE_" . $feature['name']] = $feature['id_feature'] . "_FEATURE_" . $feature['name'];
      }
    }

    return $selected_fields;
  }

  private function _getProductById( $productId )
  {
    $productInfo = array();
    $selected_fields = Tools::unserialize(Configuration::get('GOMAKOIL_FIELDS_CHECKED','',$this->_shopGroupId,$this->_idShop));
    $product = new Product($productId, true, $this->_idLang, $this->_idShop);
    $combinations = array();
    foreach( $product->getWsCombinations() as $attribute ){
      $combination = new Combination($attribute['id']);
      $combinations[$attribute['id']] = $combination;
    }
    foreach( $selected_fields as $field => $value ){
      if( $field == "id_product" ){
        $productInfo[$field] = $productId;
      }
      elseif( $field == "id_product_attribute" ){
        $productInfo[$field] = "";
        foreach( $combinations as $key=>$attribute ){
          $productInfo[$field] .= $key . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "categories_ids" ){
        $productInfo[$field] = "";
        foreach( $product->getWsCategories() as $category ){
          $productInfo[$field] .= $category['id'] . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "categories_names" ){
        $productInfo[$field] = "";
        foreach( $product->getWsCategories() as $category ){
          $cat_obj = new Category($category['id'], $this->_idLang, $this->_idShop);
          $productInfo[$field] .=   $cat_obj->name. ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif($field == 'suppliers_ids'){
        $product_supplier = $this->_model->getProductSuppliersID( $productId  );
        if($product_supplier){
          $productInfo[$field] = $product_supplier[0]['suppliers_ids'];
        }
      }
      elseif($field == 'suppliers_name'){
        $product_supplier = $this->_model->getProductSuppliersID( $productId  );
        if($product_supplier){
          $productInfo[$field] = $product_supplier[0]['suppliers_name'];
        }
      }
      elseif( $field == "base_price_with_tax" ){
        $taxPrice = $product->base_price;
        if( $product->tax_rate ){
          $taxPrice = $taxPrice + ($taxPrice * ($product->tax_rate/100));
        }
        $productInfo[$field] = $taxPrice;
      }
      elseif( $field == "price" ){
        $taxPrice = $product->getPrice(false, 0, 2);
        $productInfo[$field] = $taxPrice;
      }
      elseif( $field == "final_price_with_tax" ){
        $productInfo[$field] = $product->getPrice(true, 0, 2);
      }
      elseif( $field == "combinations_name" ){
        $productInfo[$field] = "";
        foreach( $combinations as $combination ){
          $productInfo[$field] .= str_replace($product->name . " : ",'',Product::getProductName( $product->id, $combination->id )). ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "combinations_price" ){
        $productInfo[$field] = "";
        foreach( $combinations as $combination ){
          $productInfo[$field] .= $combination->price . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "combinations_price_with_tax" ){
        $productInfo[$field] = "";
        foreach( $combinations as $combination ){
          $taxPrice = $combination->price;
          $productInfo[$field] .= ( $taxPrice + ($taxPrice * ($product->tax_rate/100)) ) . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "id_specific_price" ){
        $productInfo[$field] = "";
        foreach( SpecificPrice::getByProductId($productId) as $specific ){
          $productInfo[$field] .= $specific['id_specific_price'] . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "specific_price_from_quantity" ){
        $productInfo[$field] = "";
        foreach( SpecificPrice::getByProductId($productId) as $specific ){
          $productInfo[$field] .= $specific['from_quantity'] . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "specific_price_reduction" ){
        $productInfo[$field] = "";
        foreach( SpecificPrice::getByProductId($productId) as $specific ){
          $productInfo[$field] .= $specific['reduction'] . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "specific_price_reduction_type" ){
        $productInfo[$field] = "";
        foreach( SpecificPrice::getByProductId($productId) as $specific ){
          $productInfo[$field] .= $specific['reduction_type'] . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "specific_price_from" ){
        $productInfo[$field] = "";
        foreach( SpecificPrice::getByProductId($productId) as $specific ){
          $productInfo[$field] .= $specific['from'] . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "specific_price_to" ){
        $productInfo[$field] = "";
        foreach( SpecificPrice::getByProductId($productId) as $specific ){
          $productInfo[$field] .= $specific['to'] . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "combinations_quantity" ){
        $productInfo[$field] = "";
        foreach( $combinations as $key=>$combination ){
          $productInfo[$field] .=  $product->getQuantity( $productId, $key ) . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "combinations_wholesale_price" ){
        $productInfo[$field] = "";
        foreach( $combinations as $combination ){
          $productInfo[$field] .=  $combination->wholesale_price . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "combinations_unit_price_impact" ){
        $productInfo[$field] = "";
        foreach( $combinations as $combination ){
          $productInfo[$field] .=  $combination->unit_price_impact . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "combinations_minimal_quantity" ){
        $productInfo[$field] = "";
        foreach( $combinations as $combination ){
          $productInfo[$field] .=  $combination->minimal_quantity . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "combinations_reference" ){
        $productInfo[$field] = "";
        foreach( $combinations as $combination ){
          $productInfo[$field] .=  $combination->reference . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "combinations_location" ){
        $productInfo[$field] = "";
        foreach( $combinations as $combination ){
          $productInfo[$field] .=  $combination->location . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "combinations_weight" ){
        $productInfo[$field] = "";
        foreach( $combinations as $combination ){
          $productInfo[$field] .=  $combination->weight . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "combinations_ecotax" ){
        $productInfo[$field] = "";
        foreach( $combinations as $combination ){
          $productInfo[$field] .=  $combination->ecotax . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "combinations_ean13" ){
        $productInfo[$field] = "";
        foreach( $combinations as $combination ){
          $productInfo[$field] .=  $combination->ean13 . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "combinations_upc" ){
        $productInfo[$field] = "";
        foreach( $combinations as $combination ){
          $productInfo[$field] .=  $combination->upc . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "tags" ){
        $productInfo[$field] = $product->getTags( $this->_idLang );
      }
      elseif( $field == "images" ){
        $productInfo[$field] = "";
        $link = new Link(null, 'http://');
        foreach( $product->getWsImages() as $image ){
          $productInfo[$field] .= $link->getImageLink( $product->link_rewrite, $image['id'] ) . ";";
        }
        $productInfo[$field] = rtrim($productInfo[$field], ";");
      }
      elseif( $field == "features" ){
        $productInfo[$field] = "";
        foreach( $product->getFrontFeatures( $this->_idLang ) as $feature ){
          $productInfo[$feature['id_feature'] . "_FEATURE_" . $feature['name']] = $feature['value'];
        }
      }
      elseif( $field == "product_link" ){
        $productInfo[$field] = "";
        $link = new Link(null, 'http://');
        $productInfo[$field] = $link->getProductLink($productId);
      }
      else{
        $productInfo[$field] = $product->$field;
      }
    }
    return $productInfo;
  }

  private function  _getCombinationName(){
    ProductCore::getProductName();
  }

}
