{extends file="helpers/form/form.tpl"}
{block name="legend"}
  <div class="panel-heading">
    {if isset($input.image) && isset($input.title)}<img src="{$input.image|escape:'htmlall':'UTF-8'}" alt="{$input.title|escape:'htmlall':'UTF-8'}" />{/if}
    {$input.label|escape:'htmlall':'UTF-8'}
  </div>
{/block}
{block name="input_row"}

  {if $input.type == 'html_categories'}
    {assign var=options value=$input.options}
    <div class="form-group form-group-categories" >
      <label class="control-label col-lg-3">
        <span class="{if $input.hint}label-tooltip{else}control-label{/if}" data-toggle="tooltip" data-html="true" title="" data-original-title="{$input.hint|escape:'htmlall':'UTF-8'}">
          {$input.label|escape:'htmlall':'UTF-8'}
        </span>
      </label>
      <div class="col-lg-9" style="padding-left: 15px;">
        <input type="hidden" value="1" class="hidden_count_category">
        <div class="one_category one_category_block  one_category_block_1"><span class="count_cat">1</span>
          <input type="hidden" value="1" class="hidden_count_subcategory_1">
          <div class="one_subcategory one_subcategory_1" style="width: 260px;">
            <select name="category" class="chosen fixed-width-xl" id="category"  style="display: none; width:350px;">
              {foreach from=$input.value key=key item=field}
                <option value="{$field['name']|escape:'htmlall':'UTF-8'}">{$field['name']|escape:'htmlall':'UTF-8'}</option>
              {/foreach}
            </select>
          </div>
          <div style="clear: both"></div>
          <div class="more_subcategory more_subcategory_1" category="1"><button type="button" class="btn btn-default">{l s='add subcategory' mod='simpleimportproduct'}</button></div>
        </div>
        <div class="more_category more_category_1"><button type="button" class="btn btn-default">{l s='add category' mod='simpleimportproduct'}</button></div>
      </div>
    </div>
  {else}
    {$smarty.block.parent}
  {/if}
{/block}
{block name="script"}
    var import_show_more = '{l s='Show more...' mod='simpleimportproduct'}';
    var import_hide = '{l s='Hide...' mod='simpleimportproduct'}';
{/block}
<script type="text/javascript">
  $('.chosen').chosen();
</script>

