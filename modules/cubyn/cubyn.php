<?php
/**
 * 2015-2016 Cubyn
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Cubyn <modules@cubyn.com>
 * @copyright 2015-2016 Cubyn
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
require_once _PS_MODULE_DIR_.'/cubyn/classes/CubynApi.php';
require_once _PS_MODULE_DIR_.'/cubyn/Model/CubynOrderModel.php';
require_once _PS_MODULE_DIR_.DIRECTORY_SEPARATOR.'cubyn'.DIRECTORY_SEPARATOR.'Model'.DIRECTORY_SEPARATOR.'CubynRelayPoint.php';

class Cubyn extends Module
{

    protected $context;

    public function __construct()
    {
        $this->initContext();

        $this->name                   = 'cubyn';
        $this->tab                    = 'shipping_logistics';
        $this->version                = '1.1.0';
        $this->author                 = 'Cubyn';
        $this->need_instance          = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '2.0');
        $this->bootstrap              = true;

        parent::__construct();

        $this->displayName      = $this->l('Cubyn');
        $this->description      = $this->l('La logistique pour tous à 1€.');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    /*Install Method*/
    public function install()
    {
        // the module not compatible w PS1.5
        if (version_compare(_PS_VERSION_, '1.5', '<')) {
            $this->_errors[] = $this->l('CUBYN is not compatible with PrestaShop lower than 1.5.');
            Logger::addLog('CUBYN is not compatible with PrestaShop lower than 1.5.');
            return false;
        }

        // if curl's not avaliable, the module wont work
        if (!extension_loaded('curl')) {
            $this->_errors[] = $this->l('CUBYN requires curl php extension');
            Logger::addLog('CUBYN requires curl php extension');
            return false;
        }

        if (parent::install()
            && $this->installDb()
            && $this->hookRegistration()
            && $this->createCubynCarriers()
            && $this->installTab()) {
            return true;
        }
        return false;
    }

    private function installDb()
    {
        $sql = "CREATE TABLE IF NOT EXISTS "._DB_PREFIX_."cubyn (
        id_cubyn int(11) NOT NULL AUTO_INCREMENT,
        id_order int(11) NOT NULL,
        id_carrier int(11) NOT NULL,
        id_state int(11) NOT NULL,
        exported tinyint(1) NOT NULL DEFAULT 0,
        shipped tinyint(1) NOT NULL DEFAULT 0,
        PRIMARY KEY (id_cubyn)
        ) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";

        $result = Db::getInstance()->execute($sql);
        if (!$result) {
            d('DB ERROR');
        }

        return $result && CubynRelayPoint::install();
    }

    public function hookRegistration()
    {
        if (version_compare(_PS_VERSION_, '1.6.0.1', '<')) {
            $this->registerHook('actionAdminControllerSetMedia');
        };
        return $this->registerHook('backOfficeHeader')
        && $this->registerHook('displayHeader')
        && $this->registerHook('displayCarrierList')
        && $this->registerHook('actionCarrierProcess')
        && $this->registerHook('actionValidateOrder')
        && $this->registerHook('updateCarrier')
        && $this->registerHook('newOrder');
    }

    public function createCubynCarriers()
    {
        $zoneList  = Zone::getZones();
        $groupList = Group::getGroups($this->context->language->id);

        foreach (CubynApi::getCarriers() as $cubynCarrier) {
            if (!Configuration::get($cubynCarrier['reference'])) {
                $carrier          = new Carrier();
                $carrier->name    = $cubynCarrier['name'];
                $carrier->url     = $cubynCarrier['url'];
                $carrier->delay   = array_fill(0, 10, $cubynCarrier['delay']);
                $carrier->active  = true;
                $carrier->is_free = true;
                $carrier->save();
                foreach ($zoneList as $zone) {
                    $carrier->addZone($zone['id_zone']);
                }
                $carrier->save();
                foreach ($groupList as $group) {
                    Db::getInstance()->insert('carrier_group', array(
                        'id_carrier' => (int) $carrier->id,
                        'id_group'   => (int) $group['id_group'],
                    ));
                }
                Configuration::updateValue($cubynCarrier['reference'], (int) $carrier->id);
            }
        }
        return true;
    }

    private function installTab()
    {
        $idTab = Tab::getIdFromClassName('AdminCubyn');
        if (!$idTab) {
            $tab                                     = new Tab();
            $tab->name[$this->context->language->id] = $this->l("Cubyn");
            $tab->class_name                         = 'AdminCubyn';
            $tab->module                             = $this->name;
            if (version_compare(_PS_VERSION_, '1.6.0.1', '>=')) {
                $tab->id_parent = 0;
            } else {
                $tab->id_parent = 13; //transport
            }
            $languages = Language::getLanguages(false);
            foreach ($languages as $lang) {
                $tab->name[$lang['id_lang']] = Tools::strtoupper($this->name);
            }
            return $tab->add();
        }
        return true;
    }

    public function uninstall()
    {
        if (parent::uninstall()
            && $this->deleteCubynCarrier()
            && $this->deleteCubynCurrentConfig()
            && $this->uninstallTab()
            && $this->uninstallDb()
        ) {
            return true;
        }

        return false;
    }

    public function uninstallDb()
    {
        return Db::getInstance()->execute("DROP TABLE IF EXISTS "._DB_PREFIX_."cubyn") && CubynRelayPoint::uninstall();
    }

    private function deleteCubynCarrier()
    {
        foreach (CubynApi::getCarriers() as $cubynCarrier) {
            $reference        = $cubynCarrier['reference'];
            $carrier          = new Carrier((int) Configuration::get($reference));
            $carrier->deleted = true;
            $carrier->save();
            Configuration::deleteByName($reference);
        }
        return true;
    }

    private function uninstallTab()
    {
        // AdminFlowcoding is Admin module controller
        $idTab = Tab::getIdFromClassName('AdminCubyn');
        if ($idTab != 0) {
            $tab = new Tab($idTab);
            $tab->delete(); // Remove tab from admin
            return true;
        }
        return false;
    }

    private function deleteCubynCurrentConfig()
    {
        Configuration::deleteByName('CUBYN_API_KEY');
        return true;
    }

    /*Hook Handling */
    public function hookActionAdminControllerSetMedia()
    {
        $this->context->controller->addJS(_MODULE_DIR_.'cubyn/views/js/cubyn.js');
    }

    public function hookBackOfficeHeader()
    {
        if (version_compare(_PS_VERSION_, '1.6.0.1', '>=')) {
            $this->context->controller->addJS(_MODULE_DIR_.'cubyn/views/js/cubyn.js');
            $this->context->controller->addCSS(_MODULE_DIR_.'cubyn/views/css/tab.css');
        }
    }

    public function hookUpdateCarrier($params)
    {
        $id_carrier_old = (int) $params['id_carrier'];
        $id_carrier_new = (int) ($params['carrier']->id);
        foreach (CubynApi::getCarriers() as $cubynCarrier) {
            $reference = $cubynCarrier['reference'];
            if ($id_carrier_old == Configuration::get($reference)) {
                Configuration::updateValue($reference, $id_carrier_new);
                CubynOrderModel::updateCarrier($id_carrier_new, $id_carrier_old);
                return;
            }
        }
    }

    public function hookDisplayHeader()
    {
        if (($relay_point = Tools::jsonDecode(Tools::getValue('cubyn_point_relai'))) && Configuration::get('PS_ORDER_PROCESS_TYPE')) {
            $this->setCubynRelayPoint($relay_point);

            if (!count($this->context->controller->errors)) {
                $datas = array('hasError' => false);
            } else {
                $datas = array('hasError' => true, 'errors' => $this->context->controller->errors);
            }

            die(Tools::jsonEncode($datas));
        }

        if (($this->context->controller instanceof OrderController && $this->context->controller->step == 2)
            || $this->context->controller instanceof OrderOpcController) {
            if ($error = Tools::getValue('cubyn_error')) {
                switch ($error) {
                    case 'no_selected':
                        $this->context->controller->errors[] = $this->l('Please choose relay point.');
                        break;
                }
            }

            $this->context->controller->addCss($this->getPathUri().'views/css/'.$this->name.'.css');

            if ($this->context->controller instanceof OrderOpcController) {
                $this->context->controller->addJS($this->getPathUri().'views/js/cubyn-opc.js');
            }

            $this->context->smarty->assign('cubyn_relai', (int) Configuration::get('CUBYN_CARRIER_RELAIS'));

            return $this->display(__FILE__, 'order-carrier.tpl');
        }
    }

    public function hookDisplayCarrierList($params)
    {
        if ($params['cart']->id_carrier == Configuration::get('CUBYN_CARRIER_RELAIS')) {
            $relay_exists = CubynRelayPoint::getByCart((int) $params['cart']->id);

            $this->context->smarty->assign('address', $params['address']);

            if ($relay_exists) {
                $this->context->smarty->assign('relay_point', $relay_exists['datas']);
                $this->context->smarty->assign('relay_point_decode', Tools::jsonDecode($relay_exists['datas']));
            }

            return $this->display(__FILE__, 'order-carrier-relai.tpl');
        }
    }

    /**
     * @param $params
     */
    public function hookactionCarrierProcess($params)
    {
        if ($params['cart']->id_carrier == Configuration::get('CUBYN_CARRIER_RELAIS') && !Configuration::get('PS_ORDER_PROCESS_TYPE')) {
            if (!$relay_point = Tools::jsonDecode(Tools::getValue('cubyn_point_relai'))) {
                if (Configuration::get('PS_ORDER_PROCESS_TYPE')) {
                    Tools::redirect($this->context->link->getPageLink('order-opc', null, null, array('cubyn_error' => 'no_selected')));
                } else {
                    Tools::redirect($this->context->link->getPageLink('order', null, null, array('step' => 2, 'cubyn_error' => 'no_selected')));
                }
            } else {
                $this->setCubynRelayPoint($relay_point);
            }
        }
    }

    public function hookActionValidateOrder($params)
    {
        if ($params['cart']->id_carrier == Configuration::get('CUBYN_CARRIER_RELAIS'))
        {
            $relay_point = CubynRelayPoint::getByCart($params['cart']->id);

            if ($relay_point)
            {
                $address = $this->setCubynRelayPoint(Tools::jsonDecode($relay_point['datas']));

                if ($this->context->cart->id_address_delivery != $address->id) {
                    foreach ($this->context->cart->getProducts() as $product) {
                        $this->context->cart->setProductAddressDelivery($product['id_product'], $product['id_product_attribute'], $this->context->cart->id_address_delivery, $address->id);
                    }

                    $options_list = $this->context->cart->getDeliveryOption(null, false, false);

                    $delivery_option = array($address->id => $options_list[$this->context->cart->id_address_delivery]);

                    $this->context->cart->id_address_delivery = $address->id;
                    $this->context->cart->save();

                    $this->context->cart->setDeliveryOption($delivery_option);
                }

                $params['order']->id_address_delivery = $this->context->cart->id_address_delivery;
                $params['order']->save();
                $address = new Address($this->context->cart->id_address_delivery);
                $address->id_customer = 0;
                $address->save();
            }
        }
    }

    private function setCubynRelayPoint($relay_point)
    {
        // Check if association exists
        $relay_exists = CubynRelayPoint::getByCart((int) $this->context->cart->id);

        $customer = new Customer((int) $this->context->cart->id_customer);
        if (!$id_address = $this->getAddressByAlias('Cubyn '.$relay_point->id.' '.$this->context->cart->id_customer)) {

            $address              = new Address();
            $address->id_country  = Country::getByIso('fr');
            $address->postcode    = $relay_point->zip;
            $address->city        = $relay_point->city;
            $address->address1    = $relay_point->street;
            $address->company     = $relay_point->name;
            $address->lastname    = $customer->lastname;
            $address->firstname   = $customer->firstname;
            $address->alias       = 'Cubyn '.$relay_point->id.' '.$this->context->cart->id_customer;

        } else {
            $address = new Address($id_address);
        }

        $address->id_customer = $customer->id;
        $address->save();
        $id_address = $address->id;

        if ($relay_exists) {
            $relay = new CubynRelayPoint((int) $relay_exists['id_cubyn_relay_point']);
        } else {
            $relay = new CubynRelayPoint();
        }

        $relay->id_relay_point = (string) $relay_point->id;
        $relay->id_cart        = (int) $this->context->cart->id;
        $relay->datas          = Tools::jsonEncode($relay_point);

        if (!$relay->save()) {
            $this->context->controller->errors[] = $this->l('One error was encountered during save Cubyn.');
        }

        return $address;
    }

    private function getAddressByAlias($alias)
    {
        $query = new DbQuery();
        $query->select('id_address');
        $query->from('address');
        $query->where('alias = "'.$alias.'" ');

        return Db::getInstance()->getValue($query);
    }

    /*Display Method*/
    public function displayForm()
    {

        // Get default language
        $default_lang = $this->context->language->id;

        // Init Fields form array
        $fields_form            = array();
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Configuration générale'),
            ),
            'input'  => array(
                array(
                    'type'     => 'text',
                    'label'    => $this->l("Veuillez saisir votre clef API Cubyn"),
                    'name'     => 'cubyn_api_key',
                    'size'     => 20,
                    'class'    => 'input-md',
                    'required' => true,
                ),
                array(
                    'type'     => 'select',
                    'label'    => $this->l('Exporter vers Cubyn les commandes dont le statut est :'),
                    'desc'     => $this->l("Note : Seules les commandes associées à un transporteur Cubyn seront exportées. Une commande ne peut être exportée plusieurs fois."),
                    'name'     => 'cubyn_EXPORT_state',
                    'required' => true,
                    'options'  => array(
                        'query' => $this->getStateOptions($default_lang, true),
                        'id'    => 'id_order_state',
                        'name'  => 'name',
                    ),
                ),
            ),
            'submit' => array(
                'name'  => 'general_settings',
                'title' => $this->l('Save'),
                'class' => 'button',
                'id'    => 'btn_key',
            ),
        );

        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('SYNCHRONISATION DES STATUTS ET NUMÉROS DE SUIVI AVEC CUBYN'),
            ),
            'input'  => array(
                array(
                    'type'     => 'select',
                    'label'    => $this->l('Une fois les commandes collectées par nos coursiers, appliquer le statut :'),
                    'name'     => 'cubyn_picked_state',
                    'desc'     => $this->l("Exemple : En cours de préparation"),
                    'required' => false,
                    'options'  => array(
                        'query' => $this->getStateOptions($default_lang),
                        'id'    => 'id_order_state',
                        'name'  => 'name',
                    ),
                ),
                array(
                    'type'     => 'select',
                    'label'    => $this->l('Une fois les commandes expédiées, appliquer le statut :'),
                    'class'    => 'lg',
                    'name'     => 'cubyn_shipped_state',
                    'desc'     => $this->l("Exemple : Expédié"),
                    'required' => false,
                    'options'  => array(
                        'query' => $this->getStateOptions($default_lang),
                        'id'    => 'id_order_state',
                        'name'  => 'name',
                    ),
                ),
            ),
            'submit' => array(
                'name'  => 'webhook_settings',
                'title' => $this->l('Save'),
                'class' => 'button',
                'id'    => 'btn_webhook',
            ),
        );

        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module          = $this;
        $helper->name_controller = $this->name;
        $helper->token           = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex    = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $helper->default_form_language    = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        // Title and toolbar
        $helper->title          = $this->displayName;
        $helper->show_toolbar   = true; // false -> remove toolbar
        $helper->toolbar_scroll = true; // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action  = 'submit'.$this->name;

        // Load current value

        $currentCES = Configuration::get('CUBYN_CRT_EXPORT_STATE_NAME_ID');
        $currentCPS = Configuration::get('CUBYN_CRT_PICKED_STATE_NAME_ID');
        $currentCSS = Configuration::get('CUBYN_CRT_SHIPPED_STATE_NAME_ID');

        $helper->fields_value['cubyn_api_key']      = Configuration::get('CUBYN_API_KEY');
        $helper->fields_value['cubyn_EXPORT_state'] = $currentCES != false ? $currentCES : Configuration::get(
            'CUBYN_EXPORT_STATE_NAME_ID'
        );
        $helper->fields_value['cubyn_picked_state'] = $currentCPS != false ? $currentCPS : Configuration::get(
            'CUBYN_PICKED_STATE_NAME_ID'
        );
        $helper->fields_value['cubyn_shipped_state'] = $currentCSS != false ? $currentCSS : Configuration::get(
            'CUBYN_SHIPPED_STATE_NAME_ID'
        );

        return $helper->generateForm($fields_form);
    }

    private function getStateOptions($default_lang, $required = false)
    {
        $states = OrderState::getOrderStates($default_lang);
        if (!$required) {
            array_unshift($states, array(
                "id_order_state" => CubynApi::DISABLED_STATE,
                "name"           => $this->l('-- Désactivé --'),
            ));
        }
        // ddd($states);
        return $states;
    }

    public function getContent()
    {
        $output     = null;
        $currentKey = Configuration::get('CUBYN_API_KEY');
        if ($currentKey == '' || is_null($currentKey) || empty($currentKey)) {
            $output .= $this->displayError($this->l('Vous devez configurer une clef API Cubyn'));
        }
        if (Tools::isSubmit('general_settings')) {
            $output = $this->handleGeneralSettingsSubmit();
        }
        if (Tools::isSubmit('webhook_settings')) {
            $output = $this->handleWebhookSettingsSubmit();
        }
        $output .= $this->displayForm();
        $tpl = 'views/templates/admin/cubyn/';
        $tpl .= version_compare(_PS_VERSION_, '1.6.0.1', '>=') ? 'assurance_bt.tpl' : 'assurance.tpl';
        return $output;
    }

    /*Configuration handler Method */

    private function handleGeneralSettingsSubmit()
    {
        $output = '';
        // api key
        $lastApiKey = Configuration::get('CUBYN_API_KEY');
        $apikey     = (string) Tools::getValue('cubyn_api_key');
        if ($apikey == '') {
            $output .= $this->displayError($this->l('Votre clef API Cubyn est obligatoire'));
        } elseif (!CubynApi::checkApiKey($apikey)) {
            $output .= $this->displayError($this->l('Votre clef API cubyn est incorrecte'));
        } elseif ($lastApiKey !== $apikey) {
            Configuration::updateValue('CUBYN_API_KEY', $apikey);
            $output .= $this->displayConfirmation($this->l('Votre clef Api Cubyn est sauvegardée'));
        }
        // state
        $lastExportState = Configuration::get('CUBYN_CRT_EXPORT_STATE_NAME_ID');
        $exportState     = (string) Tools::getValue('cubyn_EXPORT_state');
        if ($exportState == '') {
            $output .= $this->displayError($this->l('Vous devez configurer le statut à exporter'));
        } elseif ($lastExportState !== $exportState) {
            Configuration::updateValue('CUBYN_CRT_EXPORT_STATE_NAME_ID', $exportState);
            $output .= $this->displayConfirmation($this->l('Configuration du statut à exporter mis à jour'));
        }
        return $output;
    }

    private function handleWebhookSettingsSubmit()
    {
        $pickedState  = (string) Tools::getValue('cubyn_picked_state');
        $shippedState = (string) Tools::getValue('cubyn_shipped_state');
        Configuration::updateValue('CUBYN_CRT_PICKED_STATE_NAME_ID', $pickedState);
        Configuration::updateValue('CUBYN_CRT_SHIPPED_STATE_NAME_ID', $shippedState);
        return $this->displayConfirmation($this->l('Configuration de la synchronisation mise à jour. Attention : veuillez consulter http://help.cubyn.com/hc/fr/articles/207488955#synchro-suivi pour que le changement soit effectif.'));
    }

    /* Utility Method for retro-compatibility */

    private function initContext()
    {
        $this->ajax    = true;
        $this->context = Context::getContext();
    }
}
