<?php
/**
 * Store Commander
 *
 * @category administration
 * @author Store Commander - support@storecommander.com
 * @version 2015-09-15
 * @uses Prestashop modules
 * @since 2009
 * @copyright Copyright &copy; 2009-2015, Store Commander
 * @license commercial
 * All rights reserved! Copying, duplication strictly prohibited
 *
 * *****************************************
 * *           STORE COMMANDER             *
 * *   http://www.StoreCommander.com       *
 * *            V 2015-09-15               *
 * *****************************************
 *
 * Compatibility: PS version: 1.1 to 1.6.1
 *
 **/

$LANG=array(

'You must be logged to use Store Commander.'=>'Vous devez être identifié pour pouvoir utiliser Store Commander.',
':'=>' :',
'of'=>'de',
'Home'=>'Accueil',
'You have to register your license key in the [Help > Register your license] menu to update Store Commander.'=>'Vous devez renseigner votre numéro de licence fourni par email dans le menu [Aide > Enregistrer votre licence] pour continuer.',
'Server IP address or application path has changed, you need to reset your license'=>'Le serveur a changé d\'adresse IP ou le chemin de l\'application a changé, vous devez réinitialiser votre licence',
'Welcome to Store Commander!'=>'Bienvenue !',
'Your shop URL'=>'Adresse de votre boutique',
'Company'=>'Société',
'Name'=>'Nom',
'Firstname'=>'Prénom',
'Email'=>'Email',
'Number of products in your e-shop'=>'Nombre de produits sur votre boutique',
'Subscribe to our newsletter'=>'S\'inscrire à la newsletter',
'Receive special offers from our partners'=>'Recevoir les offres de nos partenaires',
'M.'=>'M.',
'Ms'=>'Mme',
'Error with archive (filesize = 0 Ko)'=>'Erreur avec le fichier archive (filesize = 0 Ko)',
'Visit storecommander.com and purchase your license'=>'Visiter le site StoreCommander.com et acquérir votre licence',
'Register your license'=>'Enregistrer votre licence',
'Start now!'=>'Démarrer !',
'You can use the 14-day free trial on your own shop with your own products. However, this free trial is limited to the first 1/4 of the products of your shop being displayed.'=>'Vous souhaitez utiliser la version d\'essai de Store Commander limitée à 1/4 de vos produits (produits les plus anciens) pendant 14 jours',
'Start your trial'=>'Démarrer votre version d\'essai',
'Purchase your license'=>'Acheter votre licence',
'More info'=>'En savoir +',
'You must validate your license on your online shop. The trial version cannot be launched on local websites.'=>'Vous devez valider votre licence sur votre boutique en ligne. La version trial ne peut pas être utilisée sur une boutique locale.',
'Categories and products'=>'Catégories et produits',
'Products list'=>'Liste de tous les produits',
'Configuration'=>'Configuration',
'Links'=>'Liens',
'Create link in the PrestaShop Quick access menu'=>'Créer un lien dans le menu d\'accès rapide de PrestaShop',
'Create files example for CSV import'=>'Créer des fichiers CSV exemples pour l\'importation de produits dans le catalogue',
'Your Shop'=>'Votre boutique',
'PrestaShop BackOffice'=>'BackOffice PrestaShop',
'Visit PrestaShop.com'=>'Visiter PrestaShop.com',
'Visit StoreCommander.com'=>'Visiter StoreCommander.com',
'Help'=>'Aide',
'Update Store Commander'=>'Mettre &amp;agrave; jour Store Commander',
'Buy or extend your license'=>'Acheter ou &amp;eacute;tendre votre licence',
'Your version is up to date!'=>'Vous utilisez la derni\\350re version !',
'NEW VERSION AVAILABLE!'=>'NOUVELLE VERSION DISPONIBLE !',
'Wrong license key'=>'Mauvaise saisie de la licence',
'Installation'=>'Installation',
'Catalog language'=>'Langue du catalogue',
'Tools'=>'Outils',
'Clear grid preferences for products'=>'Effacer les préférences d\'affichage de la grille des produits',
'Clear all grids preferences for products'=>'Effacer les préférences d\'affichage de toutes les grilles des produits',
'Clear grid preferences for combinations'=>'Effacer les préférences d\'affichage de la grille des déclinaisons',
'Send a comment, a bug, a request'=>'Envoyer une remarque, un bug, une demande',
'Change history'=>'Historique des modifications',
'Reset prices drop dates'=>'Réinitialiser toutes les dates des promotions',
'Reset prices drop reductions'=>'Réinitialiser toutes les reductions des promotions',
'Discounts'=>'Promotions',
'Are you sure you want to reset prices drop dates?'=>'Etes-vous certain de vouloir réinitialiser les dates des promotions ?',
'Are you sure you want to reset prices drop reductions?'=>'Etes-vous certain de vouloir réinitialiser les montants et les pourcentages des promotions ?',
'Are you sure you want to reset all prices drop?'=>'Etes-vous certain de vouloir supprimer les promotions ?',
'Delete all prices drop'=>'Supprimer toutes les promotions',
'Update history'=>'Historique des mises à jour',
'Documentation'=>'Documentation',
'Attributes and groups'=>'Attributs et groupes',
'You need to refresh the whole page (F5 or Apple+R) to reset the application.'=>'Vous devez recharger la page (F5 ou Pomme+R) pour réinitialiser l\'application',
'Check and fix language fields of products for all languages'=>'Vérifier et corriger les champs de langue des produits pour toutes les langues',
'Extend your support and automatic updates'=>'Etendre le support et les mises à jour automatiques',
'LICENSE SOON LIMITED!'=>'LICENCE BIENTÔT LIMITÉE !',
'LICENSE LIMITED!'=>'LICENCE LIMITÉE !',
'This license is active.'=>'Cette licence est active.',
'Error: This license has been suspended.'=>'La licence a été suspendue.',
'Error: This license has expired.'=>'La licence de démonstration a expiré. Vous pouvez acquérir votre licence sur notre site ou créer une nouvelle démontration sur notre serveur. Vous ne pouvez pas recréer de démonstration sur votre boutique.',
'Error: This license has now expired.'=>'La licence de démonstration a expiré. Vous pouvez acquérir votre licence sur notre site ou créer une nouvelle démontration sur notre serveur. Vous ne pouvez pas recréer de démonstration sur votre boutique.',
'Error: This license is pending review.'=>'La licence est en attente de traitement.',
'Error: The license key did not match any in the database.'=>'Cette clé de licence n\'a pas été trouvée dans la base.',
'Error: The local license key is invalid for this location.'=>'Cette clé de licence n\'est pas valable pour le chemin de l\'application.',
'Server'=>'Serveur',
'Laboratory'=>'Laboratoire',
'Empty Smarty cache'=>'Vider le cache Smarty',
'The shortcut has been created. The installation is finished you can now use Store Commander!'=>'Le raccourci a été créé dans le panneau d\'administration de PrestaShop. L\'installation est terminée, vous pouvez maintenant profiter de Store Commander !',
'The CSV files have been installed. You can use them in the Import CSV tool.'=>'Les fichiers ont été installés. Vous pouvez les utiliser dans l\'outil d\'import CSV.',
'The CSV files have not been installed. Check write permissions on '=>'Les fichiers n\'ont pas été installés. Vérifiez les permissions en écriture sur le dossier ',
'This folder cannot be created by Store Commander, you need to create it by FTP with writing permission.'=>'Ce dossier ne peut pas être créé sur votre boutique par Store Commander. Vous devez créer ce répertoire par FTP avec les droits en écriture.',
'License key:'=>'Numéro de licence :',
'License key saved! Thank you!'=>'Licence enregistrée ! Merci !',
'The application will be reloaded.'=>'L\'application va être redémarrée.',
'Enter your license key'=>'Saisissez votre numéro de licence',
'Wrong support date.'=>'Format de date incorrect',
'Click here to refresh the application'=>'Cliquez ici pour relancer l\'application',
'Store Commander update'=>'Mise à jour de Store Commander',
'Some files are not writable, please change the permission of these files:'=>'Ces fichiers et répertoires doivent avoir les droits en écriture pour pouvoir continuer :',
'No update found.'=>'Pas de mise à jour trouvée.',
'Updating...'=>'Mise à jour en cours...',
'Downloading pack'=>'Téléchargement du pack',
'Opening zip archive...'=>'Ouverture de l\'archive...',
'Extracting zip archive...'=>'Décompressin de l\'archive...',
'End of extraction'=>'Archive décompressée',
'Copying files...'=>'Copie des fichiers...',
'The installation of ionCube is required to use the automatic updater.'=>'L\'installation de ionCube est nécessaire pour utiliser la mise à jour automatique.',
'Click here to check your installation.'=>'Cliquez ici pour vérifier votre installation. (en anglais)',
'Your support period has expired, please renew your support package'=>'Votre période de support a expiré, vous devez renouveler votre support pour bénéficier des nouveautés.',
'Update finished!'=>'Mise à jour terminée !',
'See on shop'=>'Voir sur la boutique',
'Edit in PrestaShop BackOffice'=>'Modifier dans le BackOffice PrestaShop',
'Set selected fields to'=>'Modifier les champs sélectionnés',
'Expand'=>'Déplier',
'Collapse'=>'Replier',
'Copy'=>'Copier',
'Paste'=>'Coller',
'Permanently delete the selected products everywhere in the shop.'=>'Supprimer définitivement toute trace des produits sélectionnés sur la boutique.',
'Mass update'=>'Edition en masse',
'Sell price exc. tax'=>'Prix de vente HT',
'Sell price inc. tax'=>'Prix de vente TTC',
'Modify sell price exc. tax, possible values: -10.50%, +5.0, -5.25,...'=>'Modifier le prix de vente HT, valeurs possibles : -10.50%, +5.0, -5.25,...',
'Modify sell price inc. tax, possible values: -10.50%, +5.0, -5.25,...'=>'Modifier le prix de vente TTC, valeurs possibles : -10.50%, +5.0, -5.25,...',
'Modify quantity, possible values: -10%, +5, -5, 5.25,...'=>'Modifier le stock, valeurs possibles : -10%, +5, -5, 5.25,...',
'Apply margin (use the math formula choosen in Settings for modify sale price):'=>'Appliquer la marge (utilise la formule de calcul choisie dans les Préférences pour modifier le prix de vente) :',
'Set the cheapest combination as default combination'=>'Définir la déclinaison la moins chère comme déclinaison par défaut',
'Enable / Disable'=>'Afficher / Cacher',
'Margin'=>'Marge',
'Catalog'=>'Catalogue',
'Category'=>'Catégorie',
'Categories'=>'Catégories',
'Stock available'=>'Qté dispo.',
'Wholesale price'=>'Prix d\'achat',
'PriceLTaxes'=>'Prix HT',
'Price+Taxes'=>'Prix TTC',
'Weight'=>'Poids',
'Width'=>'Largeur',
'Height'=>'Hauteur',
'Depth'=>'Profond.',
'Add. shipping cost'=>'Frais de port supp.',
'Tax rule'=>'Taxe',
'Show price'=>'Aff. prix',
'New'=>'Neuf',
'Refurbished'=>'Reconditionné',
'Online only'=>'En ligne seul.',
'Condition'=>'Condition.',
'Available for order'=>'Dispo. a la vente',
'Minimal quantity'=>'Qté minimum',
'Supplier Ref.'=>'Réf. four.',
'Manufacturer'=>'Fabricant',
'Supplier'=>'Fournisseur',
'Tax'=>'Taxe',
'Location'=>'Emplacement',
'Reduction price'=>'Montant réduction',
'Reduction percent'=>'Pourcentage réduction',
'Reduction from'=>'Réduction à partir du',
'Reduction to'=>'Réduction jusqu\'au',
'Price with reduction'=>'Prix avec réduction',
'Price with reduction percent'=>'Prix avec pourcentage réduction',
'On sale'=>'En solde',
'Active'=>'Activé',
'Products'=>'Produits',
'Product'=>'Produit',
'Product:'=>'Produit :',
'Category:'=>'Catégorie :',
'Refresh grid'=>'Mettre à jour la grille',
'Refresh tree'=>'Mettre à jour l\'arbre',
'Stock available +/-'=>'Qté dispo. +/-',
'Light navigation (simple click on grid)'=>'Navigation rapide (simple clic sur la grille)',
'Expand all items'=>'Ouvrir l\'arbre des catégories',
'Collapse all items'=>'Fermer l\'arbre des catégories',
'Yes'=>'Oui',
'No'=>'Non',
'Msg available now'=>'Msg expédition si prod. en stock',
'Msg available later'=>'Msg expédition si prod. hors stock',
'If enabled: display products from all subcategories'=>'Si activé : afficher les produits des souscatégories',
'If enabled: display products only from their default category'=>'Si activé : afficher les produits uniquement dans leur catégories par défaut',
'products'=>'produits',
'product'=>'produit',
'in this category and all subcategories'=>'dans cette catégorie et toutes les souscatégories',
'in this category'=>'dans cette catégorie',
'Loading in progress, please wait...'=>'Chargement en cours, merci de patienter...',
'Create new category with the PrestaShop form'=>'Créer une nouvelle catégorie avec le formulaire de PrestaShop',
'Create new category'=>'Créer une nouvelle catégorie',
'Create new product'=>'Créer un nouveau produit',
'Select all products'=>'Sélectionner tous les produits',
'If enabled: link products in the target category when you drag and drop products. Not enabled: move products'=>'Si icone activée (sur fond orange) : ajoute les produits dans la catégorie cible. Si désactivée : déplace les produits. Voir Aide',
'Select catalog language'=>'Sélectionner la langue du catalogue',
'Language'=>'Langue',
'Alert: wholesale price higher than sell price!'=>'Attention ! Le prix d\'achat est supérieur au prix de vente !',
'Filter options'=>'Options des filtres',
'Reset filters'=>'Réinitialiser les filtres',
'To Translate:'=>'A Traduire :',
'Create new product with the PrestaShop form'=>'Créer un nouveau produit avec le formulaire de PrestaShop',
'Save product positions in the grid as category positions'=>'Enregistre les positions des produits comme positions dans la catégorie',
'You need to select a parent category before creating a category'=>'Vous devez sélectionner une catégorie parente avant de créer une catégorie',
'If out of stock'=>'Si hors stock',
'Deny orders'=>'Com. refusées',
'Allow orders'=>'Com. permises',
'Default(Pref)'=>'Defaut(Préf.)',
'Show all columns'=>'Afficher toutes les colonnes',
'Hide all columns'=>'Cacher toutes les colonnes',
'SC Recycle Bin'=>'SC Corbeille',
'Empty bin'=>'Vider la corbeille',
'Are you sure to delete all categories and products placed in the recycled bin?'=>'Etes vous certain de vouloir supprimer le contenu de la corbeille ?',
'Light view'=>'Vue rapide',
'Large view'=>'Vue d\'ensemble',
'Delivery'=>'Expédition',
'Prices'=>'Prix',
'SEO'=>'SEO',
'References'=>'Références',
'Combination prices'=>'Prix des déclinaisons',
'Date add'=>'Créé le',
'Date update'=>'Modifié le',
'Public price'=>'Prix public',
'public price'=>'prix public',
'Grid view settings'=>'Modifier le type de grille à afficher',
'You must to select a product.'=>'Vous devez sélectionner un produit.',
'You must to select a customizable product.'=>'Vous devez sélectionner un produit personnalisable.',
'Create a category:'=>'Créer une catégorie :',
'Default color group'=>'Groupe couleur par défaut (décli.)',
'Do not display'=>'Ne pas afficher',
'Discount price'=>'Prix dégressif',
'Margin/Coef'=>'Marge/Coef',
'This will permanently delete the selected products everywhere in the shop.'=>'Supprime définitivement de la boutique les produits sélectionnés',
'Properties'=>'Propriétés',
'Combinations'=>'Déclinaisons',
'Descriptions'=>'Descriptions',
'combinations'=>'déclinaisons',
'combination'=>'déclinaison',
'Select properties panel'=>'Choix des propriétés à afficher dans ce cadre',
'Save'=>'Enregistrer',
'ID'=>'ID',
'Reference'=>'Référence',
'Supplier reference'=>'Référence fournisseur',
'Qty'=>'Qté',
'Qty +/-'=>'Qté +/-',
'Prod. price'=>'Prix prod. TTC',
'Attr. price'=>'Prix attr. TTC',
'Prod. price ex. tax'=>'Prix prod. HT',
'Attr. price ex. tax'=>'Prix attr. HT',
'Prod. weight'=>'Poids prod.',
'Att. weight'=>'Poids attr.',
'Please select a category'=>'Vous devez sélectionner une catégorie',
'Please select a product'=>'Vous devez sélectionner un produit',
'Please select a combination'=>'Vous devez sélectionner une déclinaison',
'Delete category relation'=>'Enlever le produit de la catégorie sélectionnée',
'Open and select category'=>'Ouvrir et sélectionner la catégorie',
'Save descriptions'=>'Enregistrer les descriptions',
'Do you want to save the descriptions?'=>'Voulez-vous enregistrer les descriptions ?',
'Delete combination'=>'Supprimer une déclinaison',
'Groups'=>'Groupes',
'groups'=>'Groupes',
'group'=>'groupe',
'Attributes'=>'Attributs',
'attributes'=>'attributs',
'attribute'=>'attribut',
'Color group?'=>'Groupe couleur?',
'Public name'=>'Nom publique',
'Color'=>'Couleur',
'Open PrestaShop combination creation form'=>'Ouvre le formulaire de création de déclinaisons de PrestaShop',
'Modify the combinations and close this window to refresh the grid'=>'Modifiez les déclinaisons et fermez cette fenêtre pour mettre à jour la grille',
'Are you sure you want to delete the selected items?'=>'Etes-vous certain de vouloir supprimer les éléments sélectionnés ?',
'Create new combination'=>'Créer une nouvelle déclinaison',
'The product price should not be equal to 0. You have to enter the product price before the combination price.'=>'Le prix du produit ne devrait pas être égal à 0. Vous devez renseigner le prix produit avant les prix des déclinaisons.',
'Open attributes and groups window'=>'Ouvre la fenêtre des attributs et groupes',
'Number of combinations to create when clicking on the Create button'=>'Nombre de déclinaisons à créer en cliquant sur le bouton vert Créer une nouvelle déclinaison',
'Default'=>'Défaut',
'Combination:'=>'Déclinaison :',
'The combinations grid should be displayed before using this tool.'=>'La grille des déclinaisons doit être affichée pour pouvoir utiliser cet outil.',
'Images'=>'Images',
'images'=>'images',
'image'=>'image',
'Delete selected images'=>'Supprime les images sélectionnées',
'Legend'=>'Légende',
'Please select an image'=>'Veuillez sélectionner une image',
'Save image positions'=>'Enregistrer les positions des images',
'Are you sure you want to reset the legend of the selected images?'=>'Etes-vous certain de vouloir utiliser le nom du produit comme légende ?',
'Set legend of selected images to product name'=>'Ecrit le nom du produit comme légende pour les images sélectionnées',
'Select all images'=>'Sélectionner toutes les images',
'Upload new images'=>'Ajouter de nouvelles images',
'Used'=>'Utilisée',
'Features'=>'Caractéristiques',
'Feature values'=>'Valeurs',
'feature values'=>'valeurs',
'features'=>'caractéristiques',
'Feature'=>'Caractéristique',
'feature'=>'caractéristique',
'Value'=>'Valeur',
'Custom'=>'Personnalisée',
'Display only features used by products in the same category'=>'Affiche uniquement les caractéristiques utilisées par les produits de la même catégorie',
'Quantity discounts'=>'Prix dégressifs',
'Accessories'=>'Accessoires',
'accessories'=>'accessoires',
'accessorie'=>'accessoire',
'category'=>'catégorie',
'Create new quantity discount'=>'Créer un nouveau prix dégressif',
'Quantity'=>'Quantité',
'Discount'=>'Réduction',
'MULTIPLE EDITION'=>'EDITION MULTIPLE',
'Load all products'=>'Charger tous les produits',
'Delete selected item'=>'Supprimer l\'élément sélectionné',
'Place selected products in selected categories'=>'Placer les produits sélectionnés dans les catégories sélectionnées',
'Remove selected products from selected categories (if not default category)'=>'Enlever les produits sélectionnés des catégories sélectionnées (si ce n\'est pas la catégorie par défaut du produit)',
'Display only categories used by selected products'=>'Afficher seulement les catégories utilisées par les produits sélectionnés',
'Select all combinations'=>'Sélectionner toutes les déclinaisons',
'Create the new category and close this window to refresh the tree'=>'Créez la nouvelle catégorie et fermez cette fenêtre pour mettre à jour l\'arbre des catégories',
'Create the new product and close this window to refresh the grid'=>'Créez le nouveau produit et fermez cette fenêtre pour mettre à jour la grille des produits',
'You need to select a category'=>'Vous devez sélectionner une catégorie.',
'Do you want to copy images?'=>'Voulez-vous copier les images ?',
'You can now close this window to refresh the grid'=>'Vous pouvez maintenant fermer cette fenêtre pour mettre à jour la grille',
'You can close this window when you get the confirmation of the duplication, don\'t forget to refresh the grid!'=>'Vous pouvez fermer cette fenêtre lorsque vous recevez confirmation de la duplication, n\'oubliez pas de mettre à jour la grille !',
'Modify the category and close this window to refresh the tree'=>'Modifier la categorie et fermer cette fenêtre pour mettre à jour l\'arbre',
'Modify the product and close this window to refresh the grid'=>'Modifier le produit et fermer cette fenêtre pour mettre à jour la grille',
'The database will be refresh soon to reset products details, this operation takes few seconds. We will give you information about the license during this process.'=>'La base de données va se rafraîchir pour remettre les produits d\'origine, l\'opération prend quelques secondes. En attendant, nous vous proposons des informations sur la licence.',
'History'=>'Historique des modifications',
'Browse history'=>'Parcourir l\'historique',
'Delete all history'=>'Supprimer tout l\'historique',
'This action will delete all history, do you confirm this action?'=>'Cette action va supprimer tout l\'historique, confirmez-vous cette action ?',
'Section'=>'Section',
'Action'=>'Action',
'Object'=>'Objet',
'Object ID'=>'ID Objet',
'Old value'=>'Ancienne valeur',
'New value'=>'Nouvelle valeur',
'Land ID'=>'Lang ID',
'Table'=>'Table',
'Date'=>'Date',
'Create a new group'=>'Créer un groupe d\'attributs',
'Delete group, all attributes and all combinations using this group'=>'Supprime le groupe, tous ses attributs et toutes les déclinaisons utilisant ce groupe',
'Create new attributes for the selected group'=>'Créer des attributs pour le groupe sélectionné',
'Number of attributes to create when clicking on the Create button'=>'Nombre d\'attributs à créer lors d\'un clic sur le bouton Créer des attributs',
'Delete attribute(s) and all combinations using this attribute'=>'Supprime les attributs sélectionnés et toutes les déclinaisons utilisant ces attributs',
'Create new attributes'=>'Créer des attributs',
'Create new combination with the selected groups'=>'Créer une nouvelle déclinaison avec les groupes sélectionnés',
'To create a new combination, check that:'=>'Pour créer une nouvelle déclinaison, vérifiez que :',
'The combination panel is displayed.'=>'Le panneau des déclinaisons est visible.',
'A product is selected.'=>'Un produit est sélectionné.',
'No combinations already exist for the selected product.'=>'Aucune déclinaison n\'existe déjà pour ce produit.',
'At least one group is selected.'=>'Au moins un groupe est sélectionné.',
'Duplicate selected groups and their attributes'=>'Dupliquer les groupes sélectionnés et leurs attributs',
'Are you sure to duplicate the selected groups and their attributes?'=>'Etes-vous certain de vouloir dupliquer les groupes sélectionnés et leurs attributs ?',
'You must select one item'=>'Vous devez sélectionner un élément',
'Delete selected features and their values'=>'Supprimer les caractéristiques sélectionnées et leurs valeurs',
'Duplicate selected features'=>'Dupliquer les caractéristiques sélectionnées',
'Create a new feature'=>'Créer une nouvelle caractéristique',
'Are you sure to duplicate the selected features and their values?'=>'Etes-vous certain de vouloir dupliquer les caractéristiques sélectionnées et leurs valeurs ?',
'Delete selected values'=>'Supprimer les valeurs sélectionnées',
'Create new feature value'=>'Créer une nouvelle valeur',
'Number of values to create when clicking on the Create button'=>'Nombre de valeurs à créer en cliquant sur le bouton vert Créer une nouvelle valeur',
'Import CSV'=>'Import CSV',
'Upload CSV files'=>'Charger un fichier CSV',
'Import - Backup your base before any mass update!'=>'Import - Sauvegardez votre base avant toute modification !',
'File date'=>'Date',
'File size'=>'Taille',
'Mapping'=>'Mapping',
'Field separator'=>'Séparateur de champ',
'Value separator'=>'Séparateur de valeur',
'Force UTF8'=>'Force UTF8',
'Products are identified by'=>'Produits identifiés par',
'If product with same identifier found in database'=>'Si un produit avec même identifiant est trouvé dans la base',
'Load'=>'Charger',
'Save mapping as'=>'Sauver mapping sous',
'Use'=>'Utiliser',
'File field'=>'Champ du fichier',
'Database field'=>'Champ de la base',
'Options'=>'Options',
'Process'=>'Import des données',
'Refresh'=>'Rafraîchir',
'Upload CSV file'=>'Charger un fichier CSV',
'Delete marked files'=>'Supprimer les fichiers sélectionnés',
'Load mapping'=>'Charger un mapping',
'Save mapping'=>'Enregistrer le mapping',
'Delete mapping and reset grid'=>'Supprimer le mapping et réinitialiser la grille',
'Import data'=>'Importer les données',
'Product name'=>'Nom produit',
'Product reference'=>'Référence du produit',
'Product and supplier reference'=>'Référence du produit et du fournisseur',
'Product and supplier name'=>'Nom du produit et du fournisseur',
'Prod. ref THEN prod. name'=>'Ref. prod. PUIS nom prod.',
'Sup. ref THEN prod. name'=>'Ref. four. PUIS nom prod.',
'id_product'=>'id_product',
'Skip'=>'Ignorer',
'Replace product values'=>'Remplacer les propriétés du produit',
'First line content'=>'Contenu de la première ligne',
'End of import process.'=>'Fin de l\'importation des données.',
'Stats:'=>'Stats :',
'New products:'=>'Nouveaux produits :',
'Modified products:'=>'Produits modifiés :',
'Skipped lines:'=>'Lignes ignorées :',
'You have to select a file and a mapping.'=>'Vous devez sélectionner un fichier et un mapping',
'Lines to import'=>'Lignes à importer',
'Some options are missing'=>'Certaines options du mapping doivent être saisies',
'deleted'=>'supprimé',
'Data saved!'=>'Enregistré !',
'Supplier(s) created!'=>'Fournisseurs créés !',
'Manufacturer(s) created!'=>'Fabricants créés !',
'Feature(s) created!'=>'Caractéristiques créées !',
'Attribute(s) created!'=>'Attributs créés !',
'Attribute group(s) created!'=>'Groupes d\'attributs créés !',
'You need to refresh the page, click here:'=>'Vous devez mettre à jour la page, cliquez ici :',
'All products have been imported. The TODO file is deleted.'=>'Tous les produits ont été importés. Le fichier TODO a été supprimé.',
'Download selected file'=>'Télécharger le fichier sélectionné',
'Check and create categories'=>'Activez cet icone pour créer les catégories',
'Auto-import tool'=>'Auto-import',
'Interval'=>'Intervalle',
'Launch import every X seconds if possible'=>'Lance l\'import toutes les X secondes si possible',
'Create categories (auto)'=>'Créer catégories (auto)',
'Create elements (auto)'=>'Créer éléments (auto)',
'active'=>'actif',
'name'=>'nom',
'description'=>'description',
'description_short'=>'description courte',
'meta_title'=>'meta_title',
'meta_description'=>'meta_description',
'meta_keywords'=>'meta_keywords',
'available_now'=>'disponibilité: message si en stock',
'available_later'=>'disponibilité: message si hors stock',
'out_of_stock'=>'disponibilité si hors stock: accepter/refuser commandes',
'supplier_reference'=>'référence fournisseur',
'supplier'=>'fournisseur',
'manufacturer'=>'fabricant',
'wholesale_price'=>'prix d\'achat',
'ecotax'=>'ecotax',
'priceinctax'=>'prix TTC',
'priceexctax'=>'prix HT',
'vat'=>'tva',
'ean13'=>'ean13',
'weight'=>'poids',
'on_sale'=>'soldé',
'reduction_price'=>'réduction (montant)',
'reduction_percent'=>'réduction (pourcentage)',
'reduction_from'=>'réduction depuis le',
'reduction_to'=>'réduction jusqu\'au',
'location'=>'emplacement',
'reference'=>'référence',
'combination - value(s) in the column'=>'déclinaison - valeur(s) dans la colonne',
'combination - value(s) in several lines'=>'déclinaison - valeur(s) en lignes',
'category by default'=>'catégorie par défaut',
'categories'=>'catégories',
'quantity'=>'quantité',
'attribute of combination - multiple values'=>'attribut de déclinaison - valeurs multiples',
'attribute of combination'=>'attribut de déclinaison',
'The process has been stopped before any modification in the database. You need to fix these errors first.'=>'La procédure a été arrêtée avant de modifier la base de données, vous devez corriger les erreurs ci-dessus avant de reprendre l\'importation.',
'Unable to delete this file, please check write permissions:'=>'Impossible de supprimer ce fichier, vérifiez les droits en écriture :',
'You should mark at least one file to delete'=>'Vous devez sélectionner un fichier à supprimer',
'Wrong mapping, mapping should contain the field reference'=>'Mapping incorrect, le mapping doit contenir le champ référence',
'Wrong mapping, mapping should contain the field supplier_reference'=>'Mapping incorrect, le mapping doit contenir le champ référence fournisseur',
'Wrong mapping, mapping should contain the fields supplier_reference and name in '=>'Mapping incorrect, le mapping doit contenir les champs référence fournisseur et nom du produit en ',
'Wrong mapping, mapping should contain the fields reference and supplier_reference'=>'Mapping incorrect, le mapping doit contenir les champs référence et référence fournisseur',
'Wrong mapping, mapping should contain the field name in '=>'Mapping incorrect, le mapping doit contenir le champ nom du produit en ',
'For each line of the mapping you need to double click in the Database field column to fill the mapping.'=>'Pour chaque ligne du mapping, vous devez double cliquer dans la colonne Champ de la base pour compléter le mapping.',
'If the cell in the Options column becomes blue, you need to edit this cell and complete the mapping.'=>'Si la cellule de la colonne Options devient bleue, vous devez également modifier et compléter cette cellule.',
'Wrong mapping, mapping should contain the field id_product'=>'Mapping incorrect, le mapping doit contenir le champ id_product',
'Wrong mapping, mapping should contain the field id_product_attribute'=>'Mapping incorrect, le mapping doit contenir le champ id_product_attribute',
'Cannot read the csv file'=>'Impossible de lire le fichier CSV',
'Error in mapping: too much field to import'=>'Erreur dans le mapping : trop de champs à importer',
'Error in mapping: the fields are not in the CSV file'=>'Erreur dans le mapping : les champs ne sont pas trouvés dans le fichier CSV',
'Error in mapping: price including VAT found in CSV columns but no VAT colmun found. You need to indicate the VAT or use only price excluding VAT.'=>'Mapping incorrect, un prix TTC a été trouvé mais aucune taxe n\'a été trouvée. Vous devez indiquer la taxe ou utiliser des prix HT.',
'Feature not found: '=>'Caractéristique non trouvée : ',
'Error on line '=>'Erreur sur la ligne ',
': wrong column count: '=>' : nombre de colonnes incorrect : ',
'Error: the VAT value should be between 0 and 100 on line(s) '=>'Erreur : la taxe doit être un nombre entre 0 et 100 sur les lignes ',
'Error: tax doesn\'t exist: '=>'Erreur : cette taxe n\'existe pas : ',
'This supplier doesn\'t exist: '=>'Ce fournisseur n\'existe pas : ',
'This manufacturer doesn\'t exist: '=>'Ce fabricant n\'existe pas : ',
'This feature doesn\'t exist: '=>'Cette caractéristique n\'existe pas : ',
'This attribute doesn\'t exist: '=>'Cet attribut n\'existe pas : ',
'This attribute group doesn\'t exist: '=>'Ce groupe d\'attributs n\'existe pas : ',
'Click here to fix the problem'=>'Cliquez ici pour corriger le problème',
'Click here to create these suppliers'=>'Cliquez ici pour créer ces fournisseurs',
'Click here to create these manufacturers'=>'Cliquez ici pour créer ces fabricants',
'Click here to create these features'=>'Cliquez ici pour créer ces caractéristiques',
'Click here to create these attributes'=>'Cliquez ici pour créer ces attributs',
'Click here to create these attribute groups'=>'Cliquez ici pour créer ces groupes d\'attributs',
'The TODO file has been deleted, please select the original CSV file.'=>'Le fichier de travail TODO a été supprimé, veuillez sélectionner le fichier CSV d\'origine',
'There are still products to be imported in the working file. It can mean errors you need to correct or lines which have been ignored on purpose. Once corrections have been made, click again on the import icon to proceed further.'=>'Il reste des produits à traiter dans le fichier de travail, il peut s\'agir d\'erreurs à corriger ou de lignes qui ont été ignorées à juste titre. Après vos corrections éventuelles, recliquez sur l\'icône Importer pour poursuivre la procédure.',
'Mapping name should not be empty'=>'Le nom du mapping ne doit pas être vide',
'Image not found:'=>'Image non trouv&eacute;e :',
'Impossible to copy image:'=>'Impossible de copier l\'image :',
'You should use the tool "check and fix the level_depth field" from the Catalog > Tools menu to fix your categories.'=>'Vous devez utiliser l\'outil "Vérifier et corriger le champ level_depth" depuis le menu Catalogue > Outils pour corriger vos catégories',
'A category cannot be parent of itself, you must fix this error for category ID'=>'Une catégorie ne peut pas être parente d\'elle-même, vous devez corriger ce problème pour la catégorie ID',
'Error : at least 2 columns have the same name in CSV file. You must use a unique name by column in the first line of your CSV file.'=>'Erreur : au moins 2 colonnes du fichier CSV portent le même nom. Vous devez utiliser des noms uniques pour la première ligne de votre fichier CSV.',
'Enclosed by'=>'Entourées par',
'Export filename'=>'Nom du fichier exporté',
'Categories to export'=>'Catégories à exporter',
'Export combinations'=>'Exporter les déclinaisons',
'Free shipping fee from'=>'Frais de port gratuit à partir de',
'Shipping fee'=>'Frais de port',
'Create new export script'=>'Créer un nouveau script d\'export',
'Duplicate selected file'=>'Dupliquer le fichier sélectionné',
'New script name:'=>'Nom du nouveau script :',
'ISO encoded'=>'Encodage ISO',
'All enabled categories'=>'Catégories actives',
'All disabled categories'=>'Catégories désactivées',
'All categories'=>'Toutes les catégories',
'Last export date'=>'Date du dernier export',
'No map available'=>'Aucun mapping disponible',
'Settings'=>'Préférences',
'Tool'=>'Outil',
'Item'=>'Item',
'Default value'=>'Valeur par défaut',
'Description'=>'Description',
'Database'=>'Base de données',
'Interface'=>'Interface',
'Notice'=>'Alerte',
'Notice when descriptions are not saved'=>'Alerte quand les descriptions ne sont pas enregistrées',
'meta title field size'=>'taille du champ meta title',
'meta description field size'=>'taille du champ meta description',
'meta keywords field size'=>'taille du champ meta keywords',
'link rewrite field size'=>'taille du champ link rewrite',
'default product grid view'=>'vue par défaut de la grille des produits',
'Set product grid view displayed when you launch SC. (grid_light, grid_large, grid_delivery, grid_price, grid_discount, grid_seo, grid_reference)'=>'Affiche la grille choisie lors du lancement de SC. (grid_light, grid_large, grid_delivery, grid_price, grid_discount, grid_seo, grid_reference)',
'default product properties panel'=>'vue par défaut du panneau de propriétés des produits',
'Set product properties panel displayed when you launch SC. (combinations, descriptions, images, categories, features, discounts, accessories, tags, specificprices)'=>'Affiche le panneau choisi lors du lancement de SC. (combinations, descriptions, images, categories, features, discounts, accessories, tags, specificprices)',
'product drag&drop default behavior'=>'action par d&eacute;faut lors d\'un gliss&eacute;-d&eacute;pos&eacute; de produit',
'Set the product drag&drop on category default behavior when you launch SC. (move, copy)'=>'Action par d&eacute;faut lors d\'un gliss&eacute;-d&eacute;pos&eacute; de produit sur une cat&eacute;gorie (move, copy)',
'margin operation defintion'=>'calcul de la marge',
'Set the margin operation for the column [margin] in the price grid in [Prices] view. Available values:<br/>0: priceExcTax - wholesale_price<br/>1: (priceExcTax - wholesale_price)*100 / wholesale_price<br/>2: priceExcTax / wholesale_price<br/>3: priceIncTax / wholesale_price<br/>4: (priceIncTax - wholesale_price)*100 / wholesale_price<br/>5: (priceExcTax - wholesale_price)*100 / priceExcTax'=>'D&eacute;termine l\'op&eacute;ration qui sert &agrave; calculer la marge pour la colonne [Marge] de la grille des produits. Valeurs possibles :<br/>0: prix HT - prix achat<br/>1: (prix HT - prix achat)*100 / prix achat<br/>2: prix HT / prix achat<br/>3: prix TTC / prix achat<br/>4: (prix TTC - prix achat)*100 / prix achat<br/>5: (prix HT - prix achat)*100 / prix HT',
'import images already imported'=>'importer les images déjà importées',
'Possible values:<br/>0: The image found in the CSV file is imported only the first time<br/>1: The image found in the CSV file is always imported'=>'Valeurs possibles :<br/>0 : L\'image trouvée est importée une seule fois<br/>1 : L\'image trouvée est toujours importée',
'JPG compression level'=>'niveau de compression JPG',
'Set compression level for uploaded product images. Possible values: 20 to 100 (100 is highest)'=>'Détermine le taux de compression de l\'image d\'un produit suite à son téléchargement. Valeurs possibles : 20 à 100 (100 est meilleur)',
'PNG compression level'=>'niveau de compression PNG',
'Set compression level for uploaded product images in PNG format. Possible values: 0 to 9 (0 is highest)'=>'Détermine le taux de compression de l\'image d\'un produit suite à son téléchargement au format PNG. Valeurs possibles : 0 à 9 (0 est meilleur)',
'save image filename in database'=>'enregistrer le nom du fichier image dans la base de données',
'Possible values:<br/>0: The image name is not saved, this is the Prestashop standard behavior.<br/>1: The filename is saved to skip the import process and display of the filename in the grid of images.'=>'Valeurs possibles :<br/>0: Le nom de l\'image n\'est pas enregistré, c\'est le comportement standard de Prestashop.<br/>1: Le nom du fichier est enregistré pour éviter sa réimportation et pour son affichage dans la grille des images.',
'display image filename in grid'=>'afficher le nom de l\'image dans la grille',
'Possible values:<br/>0: The image name is not displayed.<br/>1: The filename is displayed in the grid of images if the name has been saved previously.'=>'Valeurs possibles :<br/>0: Le nom n\'est pas affiché dans la grille.<br/>1: Le nom est affiché dans la grille si le nom a été préalablement enregistré.',
'background color of resized images'=>'couleur de fond des images téléchargées',
'Set the background color of resized images when you upload new images. (R,G,B format)'=>'Détermine la couleur de fond des images retaillées lorsque  vous chargez de nouvelles images. (format R,G,B)',
'force update'=>'forcer les mises à jour',
'Allow to display the update tool and force the update of Store Commander. You need to reload SC.'=>'Permet d\'afficher l\'outil de mise à jour de SC dans le menu et forcer une mise à jour. Vous devrez relancer SC pour voir le menu.',
'enable descriptions grid'=>'activer la vue \'Descriptions\' de la grille des produits',
'Enable descriptions grid. Note: the product html code can create defects in Store Commander. You should use it with small text descriptions only.'=>'Active la vue \'Descriptions\' de la grille des produits. Note : le code html des descriptions peut créer des erreurs dans SC. A utiliser si les descriptions des produits sont courtes pour éviter un chargement trop lourd dans la grille.',
'use PNG format'=>'utiliser le format PNG',
'Enable PNG format support in Store Commander and Prestashop.<br/>Possible values:<br/>0: No PNG support (Prestashop standard)<br/>1: PNG file is renamed with JPG file extension<br/>2: Both PNG and JPG format are used.<br/>See documentation'=>'Activer le format PNG dans Store Commander et PrestaShop.<br/>Valeurs possibles :<br/>0 : Pas de support PNG (standard PrestaShop)<br/>1 : le fichier PNG est renommé avec une extension JPG<br/>2 : les deux formats PNG et JPG sont enregistrés et utilisés<br/>Voir documentation',
'disable images in grids'=>'désactiver l\'affichage des images',
'Disable product images in grids to improve performance.<br/>Possible values: 0: images are present in the grids<br/>1: images are not present and the grid is loaded faster.'=>'Désactiver l\'affichage des images dans les grilles pour améliorer les performances.<br/>Valeurs possibles :<br/>0 : images présentes dans les grilles<br/>1 : images non affichées pour améliorer la vitesse de chargement.',
'disable change history'=>'désactiver l\'historique des modifications',
'Do not save modifications in database. This option hides the Tools > Change history menu.'=>'Ne pas enregistrer les modifications dans l\'historique des modifications. Cette option cache le menu Outils > Historique des modifications.',
'reset product categories before import'=>'réinitialiser les emplacements des produits avant import',
'Possible values:<br/>0: The products\' categories are not modified<br/>1: The product affectation to categories is deleted before import. It allows you to move product from an old category to another one.'=>'Valeurs possibles :<br/>0 : Les produits restent dans leurs catégories<br/>1 : Les emplacements des produits dans les catégories sont supprimés avant import pour permettre de déplacer les produits pendant l\'import.',
'product price'=>'prix du produit',
'Possible values:<br/>0: The product price is set to 0 and each combination has its own price.<br/>1: The product price is set to the first combination price found in your CSV and other combinations prices are set by subtraction from this price in the database.(Prestashop standard)'=>'Valeurs possibles :<br/>0 : Le prix du produit est fixé à 0 et chaque déclinaison a son propre prix.<br/>1 : Le prix du produit est celui de la première déclinaison trouvée dans votre CSV et les autres déclinaisons sont déterminées par soustraction depuis ce prix dans la base de données.(standard Prestashop)',
'auto create reference for multiple attr.'=>'création auto des réf. pour attr. multiples',
'If enabled, the attribute name is added to the combination reference. (SOURCEREF_ATTRNAME)'=>'Si activé, le nom de l\'attribut est ajouté à la référence de la déclinaison. (SOURCEREF_NOMATTR)',
'combinations grid format'=>'format grille des déclinaisons',
'Possible values:<br/>0: 1 combination = 1 unique physical product (standard)<br/>1: product combinations are composed of several disparate attributes (used for special configurators)'=>'Valeurs possibles :<br/>0 : 1 déclinaison = 1 produit physique unique (standard)<br/>1 : les déclinaisons sont composées d\'attributs séparés (utilisé pour des configurateurs spéciaux par exemple)',
'size of images in grids'=>'taille des images',
'Set the size of the images displayed in the grids. The possible values are the name of the image format in PrestaShop (in Tab Preferences > Image:small, medium,...)'=>'Taille des images à afficher dans les grilles. Les valeurs possibles sont les formats d\'images de PrestaShop (dans l\'onglet Préférences > Image : small, medium,...)',
'display all languages'=>'afficher toutes les langues',
'Possible values:<br/>0: Only enabled languages are available in the interface.<br/>1: All languages are available in the interface.'=>'Valeurs possibles :<br/>0 : Seules les langues activées dans Prestashop sont disponibles.<br/>1 : Toutes les langues sont disponibles.',
'tabulation direction'=>'direction des tabulations',
'When the tabulation key is pressed, the next element to edit is:<br/>0: the next column<br/>1: the next line<br/>(you need to restart Store Commander)'=>'Lorsque la touche tabulation est pressée, le prochain élément à éditer est :<br/>0 : la prochaine colonne<br/>1 : la prochaine ligne<br/>(vous devez redémarrer Store Commander)',
'wholesale price > sell price'=>'prix d\'achat > prix de vente',
'Possible values:<br/>0: don\'t trigger alert<br/>1: display alert message after cell edition'=>'Valeurs possibles :<br/>0 : pas d\'alerte<br/>1 : affiche une alerte après l\'édition d\'un prix',
'File created in'=>'Fichier créé dans',
'File NOT created'=>'Fichier NON créé',
'lines'=>'lignes',
'Save selection'=>'Enregistrer la sélection',
'Save selection as'=>'Sauver sélection sous',
'No selection available'=>'Pas de sélection disponible',
'Load selection'=>'Charger la sélection',
'Categories selection'=>'Sélection des catégories',
'Selection name should not be empty'=>'Le nom de la sélection ne doit pas être vide',
'Delete selection'=>'Supprimer la sélection',
'You have to define a filename for the export.'=>'Vous devez définir le nom du fichier à exporter',
'_fixed_value'=>'_valeur_fixe',
'You have to set the mapping for the script.'=>'Vous devez renseigner le mapping à utiliser pour le script sélectionné.',
'Number of lines to create'=>'Nombre de lignes à créer',
'The CSV files have been installed. You can use them in the Export CSV tool.'=>'Les fichiers ont été installés. Vous pouvez les utiliser dans l\'outil d\'export CSV.',
'Create script files example for CSV export'=>'Créer des exemples de script pour l\'outil d\'export CSV',
'You need to install the tool from the menu Tools > Install > Create script files example for CSV export'=>'Vous devez installer l\'outil d\'export depuis le menu Outils > Installation > Créer des exemples de script pour l\'outil d\'export CSV',
'stock_value'=>'valeur du stock',
'stock_value_wholesale'=>'valeur du stock achat',
'Export out of stock products'=>'Exporter les produits hors stock',
'productshippingcost'=>'frais de transport produit',
'availability_message'=>'disponibilité (message)',
'priceinctaxwithshipping'=>'prix avec frais de port',
'Create # lines'=>'Nombre de lignes à créer',
'product reference'=>'Référence du produit',
'When importing a product with combinations:<br/>Possible values:<br/>0: The product reference is not altered and each combination has its own reference.<br/>1: The product reference becomes the first combination reference.<br/>2: The product reference becomes the first combination reference + "P".'=>'Lors d\'un import de produit avec déclinaison :<br/>Valeurs possibles :<br/>0 : La référence produit n\'est pas modifiée et chaque déclinaison a sa propre référence.<br/>1 : La référence produit devient la référence de la première déclinaison.<br/>2 : La référence produit devient la référence de la première déclinaison + "P".',
'The Store Commander version you try to install is too recent for your support package. Please use an older version or renew your support on http://www.storecommander.com to use this version.'=>'La version de Store Commander que vous essayez d\'utiliser est trop récente pour votre pack support actuel. Merci d\'utiliser une ancienne version de Store Commander ou de renouveler votre support sur http://www.storecommander.com pour utiliser les dernières mises à jour.',
'Check and fix categories'=>'Vérifier et corriger les catégories',
'use old image path'=>'utiliser les anciens chemins de fichier',
'Force the image file path to the old system [id_product]-[id_image]-[size].jpg. Usefull for servers with "safemode".'=>'Utiliser uniquement l\'ancien système de fichier image [id_product]-[id_image]-[taille].jpg. Utile pour les serveurs en "safemode".',
'allow external default category'=>'permettre une catégorie par défaut extérieure',
'Allow you to set a default category for a product even if the product is not present in this category.'=>'Vous permet de définir une catégorie par défaut pour un produit même si le produit n\'est pas présent dans cette catégorie.',
'Do you want to delete the current mapping?'=>'Voulez-vous supprimer le mapping définitivement?',
'Specific prices'=>'Prix spéciaux',
'Create new specific price'=>'Créer un nouveau prix spécial',
'Price'=>'Prix',
'Unit price'=>'Prix unitaire',
'Unity'=>'Unité',
'<p>Example 1:with default language of the shop<br />blue<br />black</p><p>Example 2:<br />blue,fr<br />black,en</p>'=>'<p>Exemple 1 : avec langue par défaut de la boutique<br />blue<br />black</p><p>Exemple 2 :<br />noir,fr<br />black,en</p>',
'Add tags'=>'Ajouter des tags',
'Lang'=>'Lang',
'Fixed price'=>'Prix fixe',
'Create tags'=>'Créer les tags',
'Delete selected tags'=>'Supprimer les tags sélectionnés',
'Link these tags to selected products when created'=>'Lier ces tags aux produits sélectionnés lors de leur création',
'Delete link between selected tags and selected products'=>'Supprimer le lien entre les tags sélectionnés et les produits sélectionnés',
'Add link between selected tags and selected products'=>'Ajouter un lien entre les tags sélectionnés et les produits sélectionnés',
'View products with the selected tag on front office'=>'Voir les produits liés au tag sélectionné',
'View only used tags in the same category'=>'Voir uniquement les tags de la même catégorie',
'Product quantity used when the product is created in SC.'=>'Quantité qui sera renseignée sur les produits créés dans SC',
'new product quantity default'=>'quantité par défaut des nouveaux produits',
'Upgrade your account!'=>'Upgradez au compte supérieur !',
'Attachments'=>'Documents joints',
'attachments'=>'documents',
'attachment'=>'document',
'Add attachments'=>'Ajouter un document',
'Delete link between selected attachments and selected products'=>'Retirer le lien entre les documents sélectionnés et les produits sélectionnés',
'Add link between selected attachments and selected products'=>'Ajouter un lien entre les documents sélectionnés et les produits sélectionnés',
'Delete selected attachments'=>'Supprimer les documents joints sélectionnés',
'Link these attachments to selected products when created'=>'Lier ces documents aux produits sélectionnés lors de leurs créations',
'View only attachments used in the same category'=>'Afficher uniquement les documents utilisés dans la catégorie sélectionnée',
'Filters'=>'Filtres',
'Search'=>'Rechercher',
'Supplier Reference'=>'Référence Fournisseur',
'Customer group'=>'Groupe clients',
'All'=>'Tous',
'Country'=>'Pays',
'Currency'=>'Devise',
'Start your SC FREE account'=>'Démarrez votre compte SC FREE',
'Start your SC TRIAL account'=>'Démarrez votre compte SC TRIAL',
'You must validate your license on your online shop. The free version cannot be launched on local websites.'=>'Vous devez valider votre licence sur votre boutique en ligne. La version free ne peut pas être utilisée sur une boutique locale.',
'Feature in "read-only" mode with your SC FREE Account.'=>'Fonction en lecture seule avec votre compte SC FREE.',
'This action cannot be performed with your Free Account.'=>'Cette action ne pas être executée avec votre compte SC FREE.',
'Upgrade Now!'=>'Upgradez votre compte !',
'CSV Import'=>'Import CSV',
'CSV Export'=>'Export CSV',
'Are you sure?'=>'Confirmez-vous cette action ?',
'always use product name as link rewrite'=>'toujours utiliser le nom du produit comme link_rewrite',
'Possible values:<br/>0: SC will NOT modifiy the link_rewrite of the product: you should set it yourself.<br/>1: SC always set the link_rewrite url to the name of the product.'=>'Valeurs possibles :<br/>0 : SC ne modifiera jamais l\'url du produit : vous devrez l\'indiquer vous-même.<br/>1 : SC utilise le nom du produit pour créer son url à chaque fois que celui-ci est modifié.',
'price_inctax_without_reduction'=>'prix TTC sans réduction',
'price_exctax_without_reduction'=>'prix HT sans réduction',
'accessory'=>'accessoire',
'Export by default category'=>'Exporter par catégorie par défaut',
'priceinctax including ecotax'=>'prix TTC avec ecotax',
'new product reference default'=>'référence par défaut des nouveaux produits',
'new product supplier reference default'=>'référence fournisseur par défaut des nouveaux produits',
'Product reference used when the product is created in SC.'=>'Référence qui sera renseignée sur les produits créés dans SC',
'Product supplier reference used when the product is created in SC.'=>'Référence fournisseur qui sera renseignée sur les produits créés dans SC',
'Your country'=>'Votre pays',
'Compatibility'=>'Compatibilité',
'chars'=>'caractères',
'eBay module'=>'module eBay',
'Set this option to 0 if you don\'t want SC to use the Prestashop hook system.'=>'Mettre à 0 pour désactiver les appels aux hooks de Prestashop',
'Set a cover image for products without cover image'=>'Appliquer une image par défaut aux produits qui sont sans image par défaut',
'max elements in change history'=>'nombre d\'elements de l\'historique',
'Set the maximum of elements to store in database'=>'Définit le nombre d\'elements de l\'historique à conserver dans la base de données',
'License to create:'=>'Licence à créer :',
'Set product prices to their default combination prices'=>'Changer le prix des produits de base par le prix de la déclinaison par défaut',
'Please select 1 to'=>'Veuillez sélectionner 1 à',
'max products to duplicate'=>'Nombre de duplications maximum',
'Set the maximum number of duplicate products to create in one click.'=>'Définit le nombre maximum de duplications permises en une seule fois.',
'Duplicate 1 to'=>'Dupliquer de 1 à',
'new product active state default'=>'champ actif par défaut des nouveaux produits',
'Active state used when the product is created in SC. The active column must be present in the grid.'=>'Définit si les nouveaux produits créés dans SC sont activés par défaut. La colonne activé doit être présente dans la grille des produits.',
'default manufacturer for new products'=>'marque/fabricant par défaut des nouveaux produits',
'id_manufacturer used when the product is created in SC. The manufacturer column must be present in the grid.'=>'id_manufacturer par défaut des nouveaux produits. La colonne marque/fabricant doit être présente dans la grille des produits.',
'new product supplier default'=>'fournisseur par défaut des nouveaux produits',
'id_supplier used when the product is created in SC. The supplier column must be present in the grid.'=>'id_supplier par défaut des nouveaux produits. La colonne fournisseur doit être présente dans la grille des produits.',
'max products to open in browser'=>'nb max de produits à voir',
'Set the maximum number of new browser tabs to open when you do a right click on products > See on shop'=>'Nombre maximum de nouveaux onglets à ouvrir lors d\'un clic droit sur des produits > Voir sur la boutique',
'Action: Delete images'=>'Action: Supprimer les images',
'Action: Delete tags'=>'Action: Supprimer les tags',
'You have to set the language in the mapping for the field:'=>'Vous devez renseigner dans le mapping la langue pour le champ :',
'Export disabled products'=>'Exporter produits inactifs',
'available for order'=>'disponible à l\'achat',
'show price'=>'afficher le prix',
'online only (not sold in store)'=>'exclusivité web (non vendu en magasin)',
'Are you sure you want to rebuild product prices?'=>'Etes-vous certain de vouloir reconstruire les prix de vos produits ?',
'Creation date'=>'Créé le',
'Modified date'=>'Modifié le',
'Unit'=>'Unité',
'Minimum quantity'=>'Qté minimum',
'minimum quantity'=>'quantité minimum',
'Price excl. Tax'=>'Prix HT',
'Price incl. Tax'=>'Prix TTC',
'Prod. price excl tax'=>'Prix prod. HT',
'Attr. price excl tax'=>'Prix attr. HT',
'Expand all'=>'Déplier tout',
'Collapse all'=>'Replier tout',
'Are you sure you want to change the legend of the selected image(s) with the product name?'=>'Etes-vous certain de vouloir utiliser le nom du produit comme légende des images sélectionnées ?',
'Assign product name to selected image(s) legend'=>'Ecrit le nom du produit comme légende pour les images sélectionnées',
'Reduction amount'=>'Montant réduction',
'Reduction %'=>'Pourcentage réduction',
'Discount starts'=>'Réduction à partir du',
'Discount ends'=>'Réduction jusqu\'au',
'Price incl reduction'=>'Prix avec réduction',
'Price incl % reduction'=>'Prix avec pourcentage réduction',
'Pages'=>'Pages',
'Page'=>'Page',
'pages'=>'pages',
'page'=>'page',
'Sort'=>'Trier',
'Please select an item'=>'Vous devez faire votre sélection',
'Mark / Unmark'=>'Cocher / Décocher',
'Category sorted, click on the Refresh icon to allow reorder (drag and drop) on the categories tree.'=>'Catégorie triée, cliquez sur l\'icone Mettre à jour l\'arbre pour pouvoir trier les catégories manuellement.',
'price impact of combination'=>'prix : impact de déclinaison',
'weight impact of combination'=>'poids : impact de déclinaison',
'default status of created categories'=>'état des catégories créées',
'Available values:<br/>0: created categories by the import process are disabled<br/>1: created categories by the import process are enabled'=>'Valeurs possibles :<br/>0 : les catégories créées lors de l\'import ne sont pas actives<br/>1 : les catégories créées lors de l\'import sont actives',
'Feature in "read-only" mode with your SC Lite license.'=>'Fonction en lecture seule avec votre licence SC Lite.',
'This action cannot be performed with your SC Lite license.'=>'Cette action ne pas être executée avec votre licence SC Lite.',
'Short description charset'=>'Taille description courte',
'Short description charset must be < '=>'La taille de la description courte doit être < ',
'max charset in short description field'=>'taille du champ description courte',
'Set the maximum character set SC checks before saving it in the database. This does NOT modify the database.'=>'D&eacute;termine la taille du champ que SC contr&ocirc;le avant de l\'enregistrer dans la base de donn&eacute;es. Cela ne modifie PAS la base de donn&eacute;es.',
'Set this option to 1 if you use the eBay module developped by Prestashop. You need to reload SC. <a target="_blank" href="http://support.storecommander.com/categories/search?query=ebay&locale=1">Read more</a>'=>'Configurez cette option à 1 si vous utilisez le module eBay de Prestashop. Vous devez redémarrer SC. <a target="_blank" href="http://support.storecommander.com/categories/search?query=ebay&locale=16">Plus d\'infos</a>',
'You have to use the standard combinations grid format to create new combinations in Store Commander. Please change the settings of SC.'=>'Vous devez utiliser le format de grille standard des déclinaisons pour pouvoir ajouter de nouvelles déclinaisons dans Store Commander. Modifiez les préférences de SC.',
'Please select an export script.'=>'Vous devez sélectionner un script d\'export.',
'Short description'=>'Description courte',
'The field separator and the value separator could not be the same character.'=>'Les caractères séparateur de champ et séparateur de valeur ne doivent être identiques.',
'You need to refresh Store Commander to use the new settings.'=>'Vous devez relancer Store Commander pour prendre en compte les modifications.',
'name_with_attributes'=>'nom avec attributs',
'width'=>'largeur',
'height'=>'hauteur',
'depth'=>'profondeur',
'category by default (full path)'=>'catégorie par défaut (chemin complet)',
'feature (custom)'=>'caractéristique personnalisée',
'Used by'=>'Utilisé par',
'Are you sure you want to merge the selected items?'=>'Voulez-vous fusionner les éléments sélectionnés ?',
'minimal quantity'=>'quantité minimum',
'unit'=>'unité',
'categories (full path)'=>'catégories (chemin complet)',
'Modify and create products'=>'Modifier et créer les produits',
'Modify products'=>'Modifier les produits',
'unit price'=>'prix à l\'unité',
'unit price (combination impact)'=>'prix à l\'unité (impact de déclinaison)',
'unit (for unit price)'=>'unité (pour prix à l\'unité)',
'Set this option to "128M" for example if you want to set the php memory limit (ini_set "memory_limit"). Set to "0" to use system default value.'=>'Permet de définir la limite de mémoire utilisée par php pour Store Commander : "128M" par exemple (ini_set "memory_limit"). Indiquez "0" pour utiliser la valeur par défaut de php.',
'Note: you will need to use the menu "Catalog > Tools > Check and fix categories" after your moves operation.'=>'Note : vous devez utiliser le menu "Catalogue > Outils > Vérifier et corriger les catégories" après vos déplacements de catégories.',
'Note: Using the Prestashop cache system may interfere with the proper use of Store Commander as well as modules. To prevent this, go to the Preferences > Performance tab to disable the option at the end of the page.'=>'Note : L\'utilisation du cache Prestashop peut provoquer des problèmes dans Store Commander et dans d\'autres modules. Désactivez l\'option en bas de page dans l\'onglet Préférences > Performance de Prestashop.',
'use 4 decimals for wholesale price'=>'utiliser 4 décimales pour le prix d\'achat',
'Possible values:<br/>0: wholesale price format with 2 decimals (standard)<br/>1: wholesale price format with 4 decimals'=>'Valeurs possibles :<br/>0: prix d\'achat avec 2 décimales (standard)<br/>1: avec 4 décimales',
'Image'=>'Image',
'Pos.'=>'Pos.',
'modification'=>'modification',
'move_categ'=>'move_categ',
'ID employee'=>'ID employé',
'File'=>'Fichier',
'Text'=>'Texte',
'Required'=>'Requis',
'Customization fields'=>'Champs de personnalisation',
'customization fields'=>'champs de personnalisation',
'customization field'=>'champ de personnalisation',
'Add fields'=>'Ajouter un champ de personnalisation',
'Delete selected fields'=>'Supprimer les champs de personnalisation sélectionnés',
'Enable recursive selection'=>'Activer la sélection récursive',
'Disable recursive selection'=>'Désactiver la sélection récursive',
'File name'=>'Nom du fichier',
'Action: Delete attachements'=>'Action: Supprimer les documents joints',
'customization field: type'=>'champ de personnalisation : type',
'customization field: required'=>'champ de personnalisation : requis',
'customization field: name'=>'champ de personnalisation : nom',
'export: field separator = value separator'=>'export : séparateur de champs = séparateur de valeurs',
'colors of margin cells'=>'couleurs des cellules marge',
'Set the rules for the background color of the margin cells.<br/>Format: Value:Color;Value:Color;...<br/>Exemple: 20:#BA2329;50:#E3772B;1000:#34841F<br/>If the margin is < Value then the cell will be Color.<br/><a target="_blank" href="http://support.storecommander.com/entries/22049593-how-to-manage-the-margin-of-your-products?locale=1">Read more</a>'=>'Détermine la couleur de fond des cellules de marge.<br/>Format : Valeur:Couleur;Valeur:Couleur;...<br/>Exemple : 20:#BA2329;50:#E3772B;1000:#34841F<br/>Si la marge est < Valeur alors la cellule sera en Couleur.<br/><a target="_blank" href="http://support.storecommander.com/entries/22052986-comment-gerer-la-marge-de-vos-produits?locale=16">Plus d\'infos</a>',
'Selection'=>'Sélection',
'Filter'=>'Filtre',
'Read more'=>'Plus d\'infos',
'You need to create some folders by FTP:'=>'Vous devez créer des dossiers par FTP :',
'You have to set the category selection for the script'=>'Vous devez renseigner les catégories à utiliser pour le script sélectionné',
'File created'=>'Fichier créé',
'This will set the cheapest combination as default combination for each seleted product. Continue?'=>'Cette opération déterminera la déclinaison la moins chère comme déclinaison par défaut pour les produits sélectionnés. Continuer ?',
'attribute of combination - color value'=>'attribut de déclinaison - valeur couleur',
'attribute of combination - texture'=>'attribut de déclinaison - texture',
'Action: Delete specific price'=>'Action: Supprimer les prix spéciaux',
'Quick export window'=>'Fenêtre d\'export rapide',
'specific price'=>'prix spécial',
'from quantity'=>'quantité minimum',
'price'=>'prix',
'reduction'=>'réduction',
'reduction type'=>'type de réduction',
'from date'=>'date : depuis le',
'to date'=>'date : jusqu\'au',
'CRON task name'=>'Nom de la tâche CRON',
'Update products older than'=>'Mise à jour des produits plus vieux de',
'Import'=>'Import',
'Modifications'=>'Modifications',
'Export process'=>'Lancer l\'export',
'Customers'=>'Clients',
'View only used accessories in the same category'=>'Voir uniquement les accessoires utilisés dans la catégorie sélectionnée',
'Add link between selected accessories and selected products'=>'Ajouter un lien entre les accessoires sélectionnés et les produits sélectionnés',
'Delete link between selected accessories and selected products'=>'Supprimer le lien entre les accessoires sélectionnés et les produits sélectionnés',
'Export grid to clipboard in CSV format for MSExcel with tab delimiter.'=>'Exporte la grille dans le presse-papier au format CSV pour Excel avec tabulations.',
'Type'=>'Type',
'Available date'=>'Date dispo.',
'Visibility'=>'Visibilité',
'Both'=>'Partout',
'None'=>'Aucune',
'You need to upgrade your license to SC MultiStores then you will be able to manage several shops in Store Commander.'=>'Vous devez mettre à jour votre licence vers SC MultiStores pour gérer plusieurs boutiques avec Store Commander',
'We are currently in the process of developing multistore management tools, which will accelerate your multistores management with Store Commander. For now, changes you are making will only modify your default store.'=>'Nous développons actuellement les outils de gestion multiboutiques qui faciliterons votre gestion d\'une boutique à l\'autre avec Store Commander. Pour le moment, les modifications que vous effectuez dans Store Commander modifieront la boutique par défaut uniquement.',
'attribute of combination - default combination'=>'attribut de déclinaison - déclinaison par défaut',
'ignored lines'=>'lignes ignorées',
'A supplier needs to be associated to the product to set a supplier\'s reference to the product'=>'Un fournisseur doit être affecté au produit pour pouvoir renseigner une référence fournisseur',
'When lines are ignored during import:<br/>0: keep the line in the working file .TODO.csv<br/>1: delete this line in .TODO.csv'=>'Lorsque des lignes sont ignorées durant l\'import:<br/>0: garder ces lignes dans le fichier .TODO.csv<br/>1: supprimer ces lignes dans .TODO.csv',
'Shop'=>'Boutique',
'Store'=>'Boutique',
'Stores'=>'Boutiques',
'Select all'=>'Sélectionner tout',
'Multistore sharing manager'=>'Multi-boutiques : Partage',
'Add all the products selected to all the selected shops'=>'Associer les produits sélectionnés aux boutiques sélectionnées',
'Delete all the products selected to all the selected shops'=>'Dissocier les produits sélectionnés aux boutiques sélectionnées',
'shops'=>'boutiques',
'shop'=>'boutique',
'Present'=>'Présent',
'Multistore : products information'=>'Multi-boutiques : Informations produit',
'Shop:'=>'Boutique :',
'Multistore : combinations'=>'Multi-boutiques : Déclinaisons',
'Attribute:'=>'Attribut :',
'Tax rate'=>'Taxe (taux)',
'ID prod.'=>'ID prod.',
'Attribute'=>'Attribut',
'All shops'=>'Toutes les boutiques',
'Display:'=>'Affichage :',
'Update:'=>'Modification :',
'default combination'=>'déclinaison par défaut',
'Export root category (Prestashop  version < 1.5)'=>'Exporter la catégorie accueil (version Prestashop < 1.5)',
'Export root category for full paths: Home > ...'=>'Exporter la catégorie accueil pour les chemins complets des categories : Accueil > ...',
'Shop group'=>'Groupe de boutiques',
'Only display categories associated to the selected shop'=>'Afficher seulement les catégories associées à la boutique sélectionnée',
'Activate all selected products for the selected shops'=>'Activer les produits sélectionnés pour les boutiques sélectionnées',
'Desactivate all selected products for the selected shops'=>'Désactiver les produits sélectionnés pour les boutiques sélectionnées',
'product weight'=>'poids produit',
'When importing a product with combinations:<br/>Possible values:<br/>0: The product weight is not altered and each combination has its own weight.<br/>1: The product weight becomes the first combination weight.'=>'Lors d\'un import de produit avec déclinaisons :<br/>Valeurs possibles :<br/>0 : Le poids du produit n\'est pas modifiée et chaque déclinaison a son propre poids.<br/>1 : Le poids du produit devient le poids de la première déclinaison.',
'Action: Delete all combinations'=>'Action: Supprimer toutes les déclinaisons',
'Shops'=>'Boutiques',
'Current shop'=>'Boutique courante',
'The quantity of products below was not modified because they possess combinations'=>'La quantité des produits ci-dessous n\'a pu être modifiée car ils possèdent des déclinaisons',
'Manage user permissions'=>'Gestion des permissions utilisateurs',
'Access'=>'Accès',
'Profile access'=>'Accès du profil',
'User permissions'=>'Permissions des utilisateurs',
'Reset permissions'=>'Réinitialiser les permissions',
'Profiles'=>'Profils',
'Do you want to duplicate these permissions?'=>'Voulez-vous dupliquer ces permissions ?',
'Are you sure that you want reset this permissions?'=>'Êtes-vous bien sur de vouloir réinitialiser ces permissions ?',
'Update StoreCommander'=>'Mise-à-jour de StoreCommander',
'StoreCommander licence'=>'Gestion de la licence de StoreCommander',
'See in Prestashop'=>'Voir dans Prestashop',
'Delete products and combinations'=>'Supprimer les produits et les déclinaisons',
'Empty recycle bin'=>'Vider la corbeille',
'Add new products and combinations'=>'Ajouter de nouveaux produits et déclinaisons',
'Add new categories'=>'Ajouter de nouvelles catégories',
'Fast exports'=>'Exports rapides',
'Product grid: Light view'=>'Grille produit : vue rapide',
'Product grid: Large view'=>'Grille produit : vue d\'ensemble',
'Product grid: Delivery view'=>'Grille produit : vue d\'expédition',
'Product grid: Prices view'=>'Grille produit : vue des prix',
'Product grid: Discounts view'=>'Grille produit : vue des promotions',
'Product grid: SEO view'=>'Grille produit : vue SEO',
'Product grid: References view'=>'Grille produit : vue des références',
'Product grid: Descriptions view'=>'Grille produit : vue description',
'Grid'=>'Grille',
'View'=>'Vue',
'Properties grid: combinations'=>'Grilles propriétés : déclinaisons',
'Properties grid: descriptions'=>'Grilles propriétés : descriptions',
'Properties grid: images'=>'Grilles propriétés : images',
'Properties grid: accessories'=>'Grilles propriétés : accessoires',
'Properties grid: attachments'=>'Grilles propriétés : pièces jointes',
'Properties grid: specific prices'=>'Grilles propriétés : prix spécifiques',
'Properties grid: features'=>'Grilles propriétés : caractéristiques',
'Properties grid: tags'=>'Grilles propriétés : tags',
'Properties grid: categories'=>'Grilles propriétés : catégories',
'Properties grid: customized fields'=>'Grilles propriétés : champs de personnalisation',
'Properties grid: multishops share'=>'Grilles propriétés : Multi-boutiques partage',
'Properties grid: multishops product information'=>'Grilles propriétés : Multi-boutiques informations produit',
'Properties grid: multishops combinations'=>'Grilles propriétés : Multi-boutiques déclinaisons',
'Orders'=>'Commandes',
'See the profile in Prestashop'=>'Voir le profil sur Prestashop',
'Profile access:'=>'Accès du profil :',
'Employee access'=>'Accès de l\'employé',
'Different from profile:'=>'Différent du profil :',
'Delete access'=>'Supprimer l\'accès',
'Give access'=>'Donner l\'accès',
'Calculate total stock of products with combinations'=>'Recalculer le stock total des produits avec déclinaisons',
'price including taxes contains ecotax'=>'prix TTC contient l\'écotaxe',
'Possible values:<br/>0: Ecotax is not included in the Incl. taxes price but purely in the Ecotax column<br/>1: Price including taxes contains ecotax'=>'Valeurs possibles :<br/>0: L\'écotax n\'est pas incluse dans le prix TTC mais seulement dans la colonne écotaxe<br/>1: Le prix TTC contient l\'écotaxe',
'Add/Move products in categories'=>'Ajouter/Déplacer les produits dans les catégories',
'Contextual menu : mass update products'=>'Menu contextuel : édition en masse des produits',
'Move categories'=>'Déplacer les catégories',
'Contextual menu : show/hide categories'=>'Menu contextuel : afficher/cacher les catégories',
'Properties grid: Discount'=>'Grilles propriétés : Promotions',
'Stock: Warehouses'=>'Stock : Entrepôts',
'Warehouse management in Store Commander is out next month!'=>'La gestion des stocks avancée arrivent !',
'The period entitling you to download Store Commander updates and benefit from support has expired.'=>'Votre période de droits aux nouvelles mises à jour et support a expiré.',
'If you wish to benefit from future updates and new features, please log onto your account here:'=>'Pour bénéficier des prochaines mises à jour et des nouvelles fonctionnalités, merci de bien vouloir vous connecter sur votre compte ici :',
'and click on Updates & support 6 or 12 months, or upgrade to a higher license plan.'=>'Cliquez ensuite sur mise à jour et support 6 ou 12 mois ou bien upgradez votre licence vers un plan supérieur.',
'Please contact us for any sales inquiries you may have.'=>'Pour toute demande d\'informations commerciales, merci de bien vouloir nous contacter.',
'Click here to satisfy your curiosity!'=>'Cliquez ici pour satisfaire votre curiosité !',
'Bulk Orders Management shall be released end of June in Store Commander.'=>'La gestion en masse des commandes sera lancée fin juin.',
'Bulk Clients Management shall be released end of June in Store Commander.'=>'La gestion en masse des clients sera lancée fin juin.',
'Bulk Stocks and Warehouses Management shall be released end of June in Store Commander.'=>'La gestion en masse des stocks et entrepôts sera lancée fin juin.',
'Clear cookies data: grid preferences for orders'=>'Effacer les cookies : préférences d\'affichage de la grille des commandes',
'Clear cookies data: grid preferences for customers'=>'Effacer les cookies : préférences d\'affichage de la grille des clients',
'Period'=>'Période',
'Order status'=>'Etat de commande',
'30 days'=>'30 jours',
'15 days'=>'15 jours',
'3 months'=>'3 mois',
'6 months'=>'6 mois',
'1 year'=>'1 an',
'View selected orders in Prestashop'=>'Voir les commandes sélectionnées dans Prestashop',
'View selected customers in Prestashop'=>'Voir les clients sélectionnés dans Prestashop',
'Print grid'=>'Imprimer la grille',
'Login as selected user on the front office'=>'Se connecter en tant que cet utilisateur sur la boutique',
'Alert: You need to select only one order'=>'Vous ne devez sélectionner qu\'une commande',
'Customer'=>'Client',
'Lastname'=>'Nom',
'Total paid'=>'Total payé',
'Invoice No'=>'N° Facture',
'Delivery No'=>'N° Bon liv.',
'In stock'=>'En stock',
'Message'=>'Message',
'Order history'=>'Historique de commande',
'Properties of order'=>'Propriétés de commande',
'Employee'=>'Employé',
'Author'=>'Auteur',
'Messages'=>'Messages',
'Prestashop: order page'=>'Prestashop : page de commande',
'Warning: Store Commander doesn\'t recalculate order\'s totals.'=>'Attention : Store Commander ne recalcule pas les totaux de la commande',
'Address'=>'Adresse',
'Postcode'=>'Code postal',
'State'=>'Etat',
'Other'=>'Divers',
'Phone'=>'Téléphone',
'Mobile'=>'Mobile',
'Warehouses'=>'Entrepôts',
'warehouses'=>'entrepôts',
'Advanced Stock Mgmt.'=>'Gestion des stocks avancée',
'Physical stock'=>'Quantité physique',
'Available stock'=>'Quantité utilisable',
'Live stock'=>'Quantité réelle',
'Warehouse'=>'Entrepôt',
'warehouse'=>'entrepôt',
'Images and warehouses'=>'Images et Entrepôts',
'Add all selected products to all selected warehouses'=>'Ajouter tous les produits sélectionnés à tous les entrepôts sélectionnés',
'Delete all selected products from all selected warehouses'=>'Dissocier tous les produits sélectionnés à tous les entrepôts sélectionnés',
'Advanced stocks & Stock movements'=>'Stocks avancés et Mouvements de stock',
'Manage warehouses'=>'Gérer les entrepôts',
'Valuation'=>'Valorisation',
'Advanced stocks'=>'Stocks avancés',
'Stock movements'=>'Mouvements de stock',
'Reason'=>'Raison',
'Disabled'=>'Désactivée',
'Enabled'=>'Activée',
'Enabled + Manual Mgmt'=>'Activée + Gestion manuelle',
'The selected products have no Advanced Stock Management activated nor possess combinations'=>'Les produits sélectionnés n\'ont pas la gestion des stocks avancée d\'activée ou possèdent des déclinaisons',
'The selected product do not have the Advanced Stock Management option activated nor possess combinations'=>'Le produit sélectionné n\'a pas la gestion des stocks avancée d\'activée ou possède des déclinaisons',
'The selected combinations do not have thé Advanced Stock Management option activated'=>'Les déclinaisons sélectionnées n\'ont pas la gestion des stocks avancée d\'activée',
'The selected combination do not have the Advanced Stock Management option activated'=>'La déclinaison sélectionnée n\'a pas la gestion des stocks avancée d\'activée',
'Show help for color codes used for quantities/warehouses management'=>'Afficher l\'aide pour les codes couleurs utilisés pour la gestion des Quantités / Entrepôts',
'Create a new stock movement'=>'Créer un nouveau mouvement de stock',
'Create a new stock movement : Add product to stock'=>'Créer un nouveau mouvement de stock : Ajouter des produits au stock',
'Create a new stock movement : Remove product from stock'=>'Créer un nouveau mouvement de stock : Enlever du stock au produit',
'You have to select a row in the "Advanced Stock" grid, to be able to delete stock from this reference'=>'Vous devez sélectionner une ligne dans la grille "Stock avancé" pour pouvoir enlever du stock à cette référence',
'You have to select a row in the "Advanced Stock" grid, to be able to add stock to this reference'=>'Vous devez sélectionner une ligne dans la grille "Stock avancé" pour pouvoir ajouter du stock à cette référence',
'Stock movements history'=>'Historique des Mouvements de stock',
'To update quantities, you need to create a stock movement.'=>'Pour modifier les quantités, vous devez créer un mouvement de stock.',
'OR'=>'OU',
'Add to stock'=>'Ajouter du stock',
'Remove stock'=>'Retirer du stock',
'Add product to stock'=>'Ajouter des produits au stock',
'Product reference:'=>'Référence produit :',
'Name:'=>'Nom :',
'Quantity to add:'=>'Quantité à ajouter',
'Available for sale?'=>'Disponible à la vente ?',
'Warehouse:'=>'Entrepôt :',
'Wholesale price:'=>'Prix d\'achat :',
'Currency:'=>'Devise :',
'Label:'=>'Raison :',
'Remove product from stock'=>'Retirer des produits du stock',
'Quantity to remove:'=>'Quantité à retirer :',
'Available for sale:'=>'Disponible à la vente :',
'The quantity value is invalid.'=>'La quantité n\'est pas valide.',
'The selected warehouse is invalid.'=>'L\'entrepôt sélectionné n\'est pas valide.',
'The reason is invalid.'=>'La raison n\'est pas valide.',
'You have to specify if the product quantity is available for sale on the store.'=>'Vous devez spécifier si la quantité est disponible à la vente.',
'The wholesale price is invalid.'=>'Le prix d\'achat n\'est pas valide.',
'The selected currency is invalid.'=>'La devise sélectionnée n\'est pas valide.',
'An error occurred. No stock was added.'=>'Une erreur s\'est produite. Le stock n\'a pas pu être ajouté.',
'You do not have enough available quantity.'=>'Vous n\'avez pas assez de quantité disponible.',
'You do not have enough quantity (not available).'=>'Vous n\'avez pas assez de quantité (non disponible).',
'It is not possible to remove the specified quantity or an error occurred. No stock was removed.'=>'Il n\'est pas possible de retirer la quantité spécifiée ou une erreur s\'est produite. Le stock n\'a pas pu être retiré.',
'The source warehouse is not valid.'=>'L\'entrepôt source n\'est pas valide.',
'The destination warehouse is not valid.'=>'L\'entrepôt cible n\'est pas valide.',
'You have to specify if the product quantity is available for sale on the store in the source warehouse.'=>'Vous devez spécifier si la quantité est disponible à la vente dans l\'entrepôt source.',
'You have to specify if the product quantity is available for sale on the store in the destination warehouse.'=>'Vous devez spécifier si la quantité est disponible à la vente dans l\'entrepôt cible.',
'It is not possible to transfer the specified quantity, or an error occurred. No stock was transferred.'=>'Il n\'est pas possible de transférer la quantité spécifiée ou une erreur s\'est produite. Le stock n\'a pas pu être transféré.',
'An error occurred. Please try again later.'=>'Une erreur s\'est produite. Merci de réessayez plus tard.',
'Caution!!!'=>'Attention !!!',
'You should select combinations'=>'Vous devez sélectionner des déclinaisons',
'You should select products'=>'Vous devez sélectionner des produits',
'Not Advanced stock'=>'Hors Gestion des Stocks Avancée',
'Transfer product from one warehouse to another'=>'Transférer un produit d\'un entrepôt à un autre',
'Quantity to transfer:'=>'Quantité à transférer :',
'Available for sale in the source warehouse?'=>'Disponible à la vente dans l\'entrepôt d\'origine?',
'Is this a quantity available for sale?'=>'Est-ce une quantité disponible à la vente ?',
'Source Warehouse:'=>'Entrepôt d\'origine :',
'Select the warehouse from which you want to transfer the product.'=>'Sélectionnez l\'entrepôt depuis lequel vous souhaitez transférer le produit',
'Destination Warehouse:'=>'Entrepôt destinataire :',
'Select the destination warehouse.'=>'Sélectionnez l\'entrepôt destinataire.',
'Available for sale in the destination warehouse?'=>'Disponible à la vente dans l\'entrepôt destinataire ?',
'Do you want this quantity to be available for sale?'=>'Souhaitez-vous que cette quantité soit disponible à la vente ?',
'Transfer'=>'Transférer',
'Help for quantities / warehouses management'=>'Aide pour la gestion des quantités / entrepôts',
'Product with combination(s), whatever the Advanced Stock Mgmt mode is.'=>'Produit avec déclinaison(s), peu importe le mode utilisé pour la Gestion des Stocks Avancée',
'Product not associated to any warehouse'=>'Produit associé à aucun entrepôt',
'Product with at least one warehouse, but not the warehouse selected on the left col'=>'Produit associé à au moins un entrepôt, mais pas celui sélectionné en colonne de gauche',
'Product associated to at least one warehouse, including the warehouse selected on the left col'=>'Produit associé à au moins un entrepôt, dont celui sélectionné en colonne de gauche',
'Payment'=>'Paiement',
'order'=>'commande',
'orders'=>'commandes',
'Last connection'=>'Dernière connexion',
'DOB'=>'Date de naissance',
'Default group'=>'Groupe par défaut',
'Private note'=>'Note privée',
'Last order date'=>'Date dernière cmd.',
'Nb products in cart'=>'Nb. pdts. dans panier',
'Total cart'=>'Total panier',
'Confirmed orders'=>'Cmd. validées',
'Total confirmed orders'=>'Total cmd. validées',
'The selected products have no Advanced Stock Management or have Manual mgmt activated'=>'Les produits sélectionnés n\'ont pas la gestion des stocks avancée d\'activée ou a la Gestion manuelle d\'activée',
'The selected product do not have the Advanced Stock Management option activated or have Manual mgmt activated'=>'Le produit sélectionné n\'a pas la gestion des stocks avancée d\'activée ou a la Gestion manuelle d\'activée',
'The product is associated with no warehouse'=>'Le produit n\'est associé à aucun entrepôt',
'The products are associated with no warehouse'=>'Les produits ne sont associés à aucun entrepôt',
'Associate only products using Advanced Stocks (Associate only AS)'=>'Associer uniquement les produits en stocks avancés (Associer SA seul)',
'Enable Advanced Stocks and Associate (Activate AS & Associate)'=>'Activer en stocks avancés et Associer (Activer SA & Associer)',
'Enable Advanced Stocks + Manual Management & Associate (Activate AS + GMM & Associate)'=>'Activer en Stocks Avancés + Gestion Manuelle et Associer (Activer SA + GM & Associer)',
'Advanced stocks settings'=>'Paramètres des stocks avancés',
'The selected products possess combinations'=>'Les produits sélectionnés possèdent des déclinaisons',
'The selected product possess combinations'=>'Le produit sélectionné possède des déclinaisons',
'Some of the selected products possess combinations'=>'Certains des produits sélectionnés possèdent des déclinaisons',
'Some of the selected products do not have the Advanced Stock Management option activated.'=>'Certains des produits sélectionnés n\'ont pas la gestion des stocks avancés d\'activée.',
'Read help more explanations.'=>'Voir l\'aide pour plus d\'explications.',
'If enabled: associate/dissociate products and combinations in warehouse'=>'Si activé : associe/dissocie les produits et déclinaisons des entrepôts',
'Action: Associate only AS'=>'Action : Associer SA seul',
'Action: Activate AS & Associate'=>'Action : Activer SA & Associer',
'Action: Activate AS + MM & Associate'=>'Action : Activer SA + GM & Associer',
'Only the selected products using the Advanced Stocks Management (Advanced Stocks + Manual Management) will be associated to the selected warehouses.'=>'Seuls les produits sélectionnés qui utilisent le mode de gestion de stocks avancés (SA) ou le mode de gestion de stocks avancés + gestion manuelle (GM) seront associés à l’entrepôt choisi.',
'Advanced Stocks mode (AS) only will be enabled onto all selected products where Advanced Stocks Management is disabled. They will then be associated to the selected warehouses.'=>'Le mode de gestion de stocks avancés (SA) sera activé sur tous les produits sélectionnés sur lesquels la gestion de stocks avancés est désactivée. Ils seront ensuite associés à l’entrepôt choisi.',
'Advanced Stocks + Manual Management (AS+MM) only will be enabled onto all selected products where Advanced Stocks Management is disabled. They will then be associated to the selected warehouses.'=>'Le mode de gestion de stocks avancés + gestion manuelle (SA + GM) sera activé sur tous les produits sélectionnés sur lesquels la gestion de stocks avancés est désactivée. Ils seront ensuite associés à l’entrepôt choisi.',
'Show help for Advanced stocks default activation'=>'Afficher l\'aide sur l\'activation par défaut des Stocks avancés',
'Help for Advanced stocks default activation'=>'Aide sur l\'activation par défaut des Stocks avancés',
'Discount codes'=>'Bons de réduction',
'Title'=>'Civilité',
'Last delivery address'=>'Dernière adresse de livraison',
'Convert'=>'Convertir',
'Addresses'=>'Adresses',
'City'=>'Ville',
'Invoice?'=>'Facturation ?',
'Delivery?'=>'Livraison ?',
'Show Company, Reg. and SIC cols'=>'Afficher les colonnes Société, Siret et APE',
'Dou you want to display these 3 cols in the customers grids?'=>'Désirez-vous afficher ces 3 colonnes dans les grilles clients ?',
'Add all selected customers to all selected groups'=>'Ajouter tous les clients sélectionnés dans tous les groupes sélectionnés',
'Remove all selected customers from all selected groups'=>'Retirer tous les clients sélectionnés de tous les groupes sélectionnés',
'Create a group:'=>'Créer un groupe :',
'Create a new discount code'=>'Créer un bon de réduction',
'customers'=>'clients',
'customer'=>'client',
'Create discount code'=>'Créer un bon de réduction',
'optimized grid loading'=>'optimisation du chargement des grilles',
'Use an optimized grid display method for grids with more than 500 lines (by default). Set to 0 to disable the optimized display method.'=>'Utiliser un affichage optimisé des grilles à partir de 500 lignes (par défaut). Indiquez 0 pour ne pas utiliser cet affichage optimisé.',
'Orders status changed:'=>'Changement des statuts en cours :',
'staying.'=>'restant(s).',
'Dont\'t reload page.'=>'Ne pas recharger la page.',
'Customer service'=>'Service clients',
'Global statistics'=>'Statistiques globales',
'By contact'=>'Par contact',
'By status'=>'Par statut',
'Open'=>'Ouvert',
'Waiting 1'=>'En attente 1',
'Waiting 2'=>'En attente 2',
'Closed'=>'Clos',
'By lang'=>'Par langue',
'Total discussions'=>'Nombre total de discussions',
'Discussions pending'=>'Discussions en attente',
'Total customer messages'=>'Total de messages de clients',
'Total employee messages'=>'Total de messages d\'employés',
'Unread discussions'=>'Discussions non lues',
'Closed discussions'=>'Discussions closes',
'Advisor'=>'Conseiller',
'Last Messages'=>'Derniers messages',
'Answer'=>'Répondre',
'Sender'=>'Expéditeur',
'Private'=>'Privé',
'Message forwarded to'=>'Message transmis à',
'Comment:'=>'Commentaire :',
'You must write a message to send an answer'=>'Vous devez écrire un message pour envoyer la réponse',
'An error occured during file upload. Please try again.'=>'Une erreur s\'est produite durant l\'upload. Veuillez réessayer.',
'Send'=>'Envoyer',
'The message was successfully sent'=>'le message a été envoyé avec succès',
'Transfer to...'=>'Transférer à...',
'Please select a discussion'=>'Veuillez sélectionner une discussions',
'Are you sure you want to delete the selected discussions?'=>'Êtes-vous sur de vouloir supprimer les discussions sélectionnées ?',
'This discussion doesn\'t concern an order.'=>'Cette discussion ne concerne pas une commande.',
'Discussion history'=>'Historique de la discussion',
'Validated Orders'=>'Commandes validées',
'Customer turnover'=>'CA client',
'Read'=>'Lu',
'Delete'=>'Supprimer',
'Last advisor'=>'Dernier conseiller',
'Open discussions'=>'Discussions ouvertes',
'Order weight'=>'Poids Cmd.',
'Modify the order and close this window to refresh the grid'=>'Modifier la commande et fermer la fenêtre pour rafraîchir la grille',
'Total number of products'=>'Nombre total de produits',
'Current stock'=>'Stock actuel',
'Your shops'=>'Vos boutiques',
'images creation mode'=>'mode de création des images',
'Possible values:<br/>0: the whole image is resized to fit the destination size with colored background (PS standard)<br/>1: the image is cropped to get a better resolution but you lose the image borders<br/>2: the whole image is resized to fit the destination size without colored background'=>'Valeurs possibles :<br/>0 : l\'image complète est conservée et retaillée à la taille de destination avec fond coloré (standard PS)<br/>1 : l\'image est rognée pour obtenir une meilleure résolution mais vous perdrez les bords de l\'image<br/>2 : l\'image complète est conservée et retaillée à la taille de destination sans fond coloré',
'share and activate product'=>'partager et activer le produit',
'Set to 0 if you don\'t want to activate the product you are sharing in a new shop'=>'Indiquez 0 si vous ne souhaitez pas activer automatiquement le produit que vous partagez sur une nouvelle boutique',
'MultiStores'=>'Multi-boutiques',
'Update product?'=>'Mettre à jour le produit ?',
'Carriers'=>'Transporteurs',
'carriers'=>'transporteurs',
'carrier'=>'transporteur',
'Synchronize the categories positions on multiple shops'=>'Synchroniser les positions des catégories sur plusieurs boutiques',
'You must select one shop to synchronize the positions'=>'Vous devez selectionner une boutique pour synchroniser les positions',
'You must tick shops to synchronize the positions'=>'Vous devez cocher des boutiques pour synchroniser les positions',
'Are you sure that you want to synchronize the positions of these categories:'=>'Êtes-vous certains de vouloir synchroniser les positions de ces catégories :',
'with'=>'avec',
'Synchronize'=>'Synchroniser',
'Error:'=>'Erreur :',
'It must have category'=>'Elle doit posséder la catégorie',
'as parent category.'=>'en catégorie parente.',
'Warning:'=>'Attention :',
'This category is not shared with the shop'=>'Cette catégorie n\'est pas partagée avec la boutique',
'Ready to synchronize positions.'=>'C\'est prêt pour synchroniser les positions.',
'Current qty in stock'=>'Qté actuelle en stock',
'Stock Transfert'=>'Transfert de Stock',
'Stock Transfer'=>'Transfert de Stock Produit',
'Downloadable product'=>'Produit téléchargeable',
'Display filename'=>'Nom du fichier',
'Filename on the server'=>'Nom sur le serveur',
'Upload date'=>'Date d\'upload',
'Expiration date'=>'Date d\'expiration',
'Nb days accessible'=>'Nombre de jours accessibles',
'Nb of authorized downloads'=>'Nombre de téléchargements permis',
'To add a downloadable file, you must select one product only.'=>'Pour ajouter un fichier téléchargeable, vous devez sélectionner un seul produit.',
'This product already has a downloadable file.'=>'Ce produit a déjà un fichier téléchargeable',
'Upload file'=>'Charger un fichier',
'Upload product file'=>'Charger un fichier pour le produit',
'You must sélect a file to upload.'=>'Vous devez choisir un fichier à charger.',
'To edit a downloadable file, you must select one row only.'=>'Pour éditer un fichier téléchargeable, vous devez sélectionner une seule ligne.',
'Edit product file'=>'Modifier le fichier pour le produit',
'Properties grid: Download product'=>'Grilles des propriétés : Produit téléchargeable',
'You want to synchronize the categories positions on the Shop'=>'Vous voulez synchroniser les positions des catégories de la boutique',
'Delete selected products from the list'=>'Supprimer les produits sélectionnés de la liste',
'Save features positions'=>'Enregistrer les positions des caractéristiques',
'Save positions'=>'Enregistrer les positions',
'A virtual product cannot have combinations.'=>'Un produit virtuel ne peut pas avoir de déclinaisons.',
'A virtual product cannot use the advanced stock management.'=>'Un produit virtuel ne peut pas utiliser la gestion des stocks avancée.',
'Add file'=>'Ajouter un fichier',
'Delete file'=>'Supprimer le fichier',
'Edit file'=>'Editer le fichier',
'Download file'=>'Télécharger le fichier',
'Add all the selected products to all the selected carriers'=>'Ajouter tous les produits sélectionnés à tous les transporteurs sélectionnés',
'Delete all the selected products to all the selected carriers'=>'Supprimer tous les produits sélectionnés à tous les transporteurs sélectionnés',
'Column name'=>'Nom de la colonne',
'margin'=>'marge',
'Are you sure that you want load all the fields in this mapping?'=>'Êtes-vous certains de vouloir charger tous les champs dans ce mapping ?',
'Load all fields'=>'Charger tous les champs',
'You can\'t manage the positions when there is a filter on a column'=>'Vous ne pouvez gérer les positions quand il y a un filtre sur une colonne',
'Image\'s format to export'=>'Format d\'image à exporter',
'Put the image\'s format to export: default, large, small,.... Leave space to put the original format.'=>'Renseigner le format d\'image à exporter : default, large, small,... Laisser vide pour mettre le format original.',
'Export data'=>'Exporter',
'Lines to export'=>'Lignes à exporter',
'Complete export of all products'=>'Export complet de tous les produits',
'Use auto export'=>'Utiliser l\'export automatique',
'This export is already in progress.'=>'Cet export est déjà en cours.',
'Export finished.'=>'Export terminé.',
'Error during sc_export creation.'=>'Une erreur s\'est produite durant la création dans sc_export',
'Exported files'=>'Fichiers exportés',
'Launch export every X seconds if possible'=>'Lancer l\'export toutes les X secondes si possible',
'Number of the first lines to export into the CSV file'=>'Nombre de lignes à exporter dans le fichier CSV',
'An error occured during export. Please try again.'=>'Une erreur s\'est produite durant l\'export. Veuillez réessayer.',
'An error occured during last export with product'=>'Une erreur s\'est produite durant le dernier export avec le produit',
'Check this product before to try again.'=>'Vérifiez ce produit avant de réessayer.',
'Check'=>'Cocher',
'Uncheck'=>'Décocher',
'An error occured during last export with combination'=>'Une erreur s\'est produite durant le dernier export avec la déclination',
'from product'=>'du produit',
'Check this combination before to try again.'=>'Vérifiez cette déclinaison avant de réessayer.',
'An error occured during export.'=>'Une erreur s\'est produite durant l\'export.',
'Insert item'=>'Ajouter une ligne',
'Delete item'=>'Supprimer une ligne',
'Selected shop'=>'Boutique sélectionnée',
'Export in one step the products corresponding to the selected script'=>'Exporter en une fois les produits correspondants au script sélectionné',
'Export in several times the products corresponding to the selected script'=>'Exporter en plusieurs fois les produits correspondants au script sélectionné',
'Categories management'=>'Gestion des catégories',
'Products nb'=>'Nb produits',
'SEO products nb'=>'Nb produits SEO',
'Are you sure you want to delete the selected images?'=>'Êtes-vous certain de vouloir supprimer les images sélectionnées ?',
'You must upload an image using format png, gif, jpg or jpeg'=>'Vous devez uploader un image du type png, gif, jpg or jpeg',
'Update'=>'Mettre à jour',
'Description length'=>'Taille desc.',
'Name & description'=>'Nom & description',
'Lang:'=>'Langue :',
'Edit description'=>'Modifier la description',
'To update description, you must select one row only.'=>'Pour modifier la description, vous devez sélectionner une seule ligne.',
'META title length'=>'Taille META title',
'META description length'=>'Taille META description',
'META keywords length'=>'Taille META keywords',
'Add all selected categories to all selected shops'=>'Ajouter toutes les catégories sélectionnées à toutes les boutiques sélectionnées',
'Delete all selected categories from all selected shops'=>'Retirer toutes les catégories sélectionnées de toutes les boutiques sélectionnées',
'Update all shops'=>'Modifier toutes les boutiques',
'If enabled: update all shops when you edit a category'=>'Si activé : toutes les boutiques sont modifiées lorsque vous modifiez une catégorie',
'Open this category in SC'=>'Ouvrir la catégorie dans SC',
'Put in the bin/trash'=>'Mettre dans la corbeille',
'Activate all products for the selected categories'=>'Activer tous les produits appartenant aux catégories sélectionnées',
'Deactivate all products for the selected categories'=>'Désactiver tous les produits appartenant aux catégories sélectionnées',
'and'=>'et',
'The products has been updated correctly.'=>'Les produits ont été mis à jour avec succès.',
'Phone mobile'=>'Portable',
'Advanced stock'=>'Stock avancé',
'Default type for Advanced Stock Management'=>'Type par défaut pour la Gestion des stocks avancée',
'When Advanced Stock Management activated, define default type for a new produit.<br/>1: Disabled<br/>2: Enabled<br/>3: Enabled + Manual Mgmt'=>'Quand la Gestion des stocks avancée est activée, définissez le type par défaut pour tout nouveau produit.<br/>1: Désactivée<br/>2: Activée<br/>3: Activée + Gestion manuelle',
'specific prices'=>'prix spéciaux',
'With Shop(s)'=>'Avec la(les) boutique(s)',
'Next step'=>'Etape suivante',
'Last step'=>'Etape précédente',
'Rounding prices up'=>'Arrondir les prix',
'Rounding prices, possible values:<br/>0: Rounding up or down to the nearest 5 cents<br/>1:  Rounding up<br/>2: Rounding down<br/><a href="http://support.storecommander.com/entries/29662223-How-do-price-rounding-options-work-" target="_blank">Read more</a>'=>'Arrondir les prix, valeurs possibles :<br/>0: Arrondir au chiffre le plus proche<br/>1: Arrondir au nombre supérieur<br/>2: Arrondir au nombre inférieur<br/><a href="http://support.storecommander.com/entries/29662143-Comment-fonctionne-l-option-d-arrondis-de-prix" target="_blank">Plus d\'infos</a>',
'Are you sure that you want to round all these prices up?'=>'Êtes-vous certains de vouloir arrondir tous ces prix ?',
'Rounding up the'=>'Arrondir le',
'Rounding the price up'=>'Arrondir le prix',
'http://www.storecommander.com/redir.php?dest=20131127'=>'http://www.storecommander.com/redir.php?dest=201311272',
'Open this product in SC Catalog'=>'Ouvrir le produit dans SC Catalogue',
'or'=>'ou',
'No warehouse'=>'Hors entrepôt',
'products ignored because no wholesale price is associated'=>'produits ont été ignorés pour les mouvements de stock car ils ne possèdent pas de prix d\'achat',
'stock - advanced stock mgmt.'=>'stock - gestion des stocks avancée',
'Product ean'=>'Code EAN du produit',
'Product upc'=>'Code UPC du produit',
'When importing a product with combinations:<br/>Possible values:<br/>0: The product EAN is not altered and each combination has its own EAN.<br/>1: The product EAN becomes the first combination EAN.'=>'Lors d\'un import de produit avec déclinaison :<br/>Valeurs possibles :<br/>0 : Le code EAN produit n\'est pas modifiée et chaque déclinaison a son propre code EAN.<br/>1 : Le code EAN produit devient le code EAN de la première déclinaison.',
'When importing a product with combinations:<br/>Possible values:<br/>0: The product UPC is not altered and each combination has its own UPC.<br/>1: The product UPC becomes the first combination UPC.'=>'Lors d\'un import de produit avec déclinaison :<br/>Valeurs possibles :<br/>0 : Le code UPC produit n\'est pas modifiée et chaque déclinaison a son propre code UPC.<br/>1 : Le code UPC produit devient le code UPC de la première déclinaison.',
'Customers grid: minimum order date'=>'Grille clients : date de commande minimum',
'Allows to display the orders spent from this date.<br/>Required format : YYYY-MM-DD'=>'Permet d\'afficher les commandes passées à partir de cette date.<br/>Format requis : AAAA-MM-JJ',
'Action: Delete all product features'=>'Action: Supprimer les caractéristiques du produit',
'An error occured during reset. Please try again.'=>'Une erreur s\'est produite durant la réinitialisation. Veuillez réessayer.',
'The export was successfully reset.'=>'L\'export a été réinitialisé avec succès.',
'Load fields by name'=>'Charger les champs par leur nom',
'Reset'=>'Réinitialiser',
'From'=>'Du',
'to'=>'au',
'Select the date interval to filter'=>'Sélectionnez l\'interval de dates à filtrer',
'From:'=>'Du :',
'To:'=>'Au :',
'Close'=>'Fermer',
'You must write the two dates.'=>'Vous devez renseigner les deux dates.',
'Your dates are wrong.'=>'Vos dates sont erronées.',
'Clear all grids preferences for orders'=>'Effacer les préférences d\'affichage de toutes les grilles des commandes',
'Clear all grids preferences for customers'=>'Effacer les préférences d\'affichage de toutes les grilles des clients',
'Truncate'=>'Vider',
'Enter "ok" and click "Validate" (?) to empty the warehouse.'=>'Pour confirmer le vidage de l\'entrepôt, veuillez écrire "OK" puis valider.',
'This warehouse was successfully truncated'=>'Cette entrepôt a été vidé avec succès',
'An error occured during truncate'=>'Une erreur c\'est produite durant le vidage',
'Modify and create customers/addresses'=>'Modifier et créer les clients/adresses',
'Modify customers/addresses'=>'Modifier les clients/adresses',
'Create new customer'=>'Créer un nouveau client',
'If customer with same identifier found in database'=>'Si un client avec même identifiant est trouvé dans la base',
'Customers are identified by'=>'Clients identifiés par',
'id_customer+address title'=>'id_customer+titre adresse',
'Wrong mapping, mapping should contain the id_customer field'=>'Mapping incorrect, le mapping doit contenir le champ id_customer',
'Wrong mapping, mapping should contain the email field'=>'Mapping incorrect, le mapping doit contenir le champ email',
'Wrong mapping, mapping should contain the address title field'=>'Mapping incorrect, le mapping doit contenir le champ titre de l\'adresse',
'Wrong mapping, mapping should contain the id_customer and the address title fields'=>'Mapping incorrect, le mapping doit contenir les champs id_customer et titre de l\'adresse',
'Email can\'t be empty to create a customer: line n°'=>'L\'email ne peut être vide pour créer un client : ligne n°',
'New customers:'=>'Nouveaux clients :',
'Modified customers:'=>'Clients modifiés :',
'All customers have been imported. The TODO file is deleted.'=>'Tous les clients ont été importés. Le fichier TODO a été supprimé.',
'All customers have been imported.'=>'Tous les produits ont été importés.',
'There are still customers to be imported in the working file. It can mean errors you need to correct or lines which have been ignored on purpose. Once corrections have been made, click again on the import icon to proceed further.'=>'Il reste des clients à traiter dans le fichier de travail, il peut s\'agir d\'erreurs à corriger ou de lignes qui ont été ignorées à juste titre. Après vos corrections éventuelles, recliquez sur l\'icône Importer pour poursuivre la procédure.',
'Update customers older than'=>'Mise à jour des clients plus vieux de',
'Wrong mapping, mapping should contain the email and the address title fields'=>'Mapping incorrect, le mapping doit contenir les champs email et titre de l\'adresse',
'Wrong mapping, mapping should contain the id_customer and the id_address fields'=>'Mapping incorrect, le mapping doit contenir les champs email et id_address',
'Wrong mapping, mapping should contain the id_address field'=>'Mapping incorrect, le mapping doit contenir le champ id_address',
'Wrong mapping, mapping should contain the email and the id_address fields'=>'Mapping incorrect, le mapping doit contenir les champs id_customer et id_address',
'Transfert stock between two warehouses'=>'Transférer du stock entre deux entrepôts',
'Transfert'=>'Transférer',
'You want to transfert all stock from warehouse'=>'Vous voulez transférer tout le stock de l\'entrepôt',
'to warehouse :'=>'vers l\'entrepôt :',
'The stock was successfully transfered'=>'le stock a été transféré avec succès',
'An error occured during transfer'=>'Une erreur c\'est produite durant le transfert',
'My address'=>'Mon adresse',
'Modified customers'=>'Clients modifiés',
'The customer has been imported but there is a problem with his address'=>'Le client a été créé mais il y a un problème avec son adresse',
'Gender'=>'Genre',
'Password'=>'Mot de passe',
'Birthday'=>'Date de naissance',
'Partner offers'=>'Offres partenaires',
'Private notes'=>'Notes privées',
'Website'=>'Site web',
'title'=>'titre',
'country'=>'pays',
'state'=>'état/département',
'company'=>'société',
'lastname'=>'nom',
'firstname'=>'prénom',
'address 1'=>'adresse',
'address 2'=>'adresse comp.',
'postcode'=>'code postal',
'city'=>'ville',
'other'=>'autre',
'phone'=>'téléphone',
'phone mobile'=>'téléphone portable',
'id_customer + address title'=>'id_customer + titre adresse',
'email + address title'=>'email + titre adresse',
'Caution! This action will delete all stock and movements for destination warehouse!'=>'Attention ! Cela va vider le stock et les mouvements de l\'entrepôt de destination.',
'Action: Delete all customers'=>'Action : Supprimer tous les clients',
'Action: Delete all addresses'=>'Action : Supprimer toutes les adresses',
'Action: Regenerate all passwords'=>'Action : Régénérer tous les mots de passe',
'Opt-in (Partners offers)'=>'Opt-in (Offres partenaires)',
'New groups:'=>'Nouveaux groupes :',
'Last order'=>'Dernière cmd.',
'last order'=>'dernière commande',
'Combinations (copy/paste)'=>'Déclinaisons (copier/coller)',
'Product ref'=>'Ref produit',
'This product cannot be duplicated because of a Prestashop malfunction.'=>'Ce produit ne peut pas être dupliqué à cause d\'un dysfonctionnement de Prestashop.',
'The product must exist in the default store to be duplicated.'=>'Pour être duplicable, il faut ce que celui-ci soit présent sur la boutique par défaut.',
'Short description can\'t include an iframe or is invalid'=>'La description courte ne peut contenir d\'iframe ou est invalide',
'Description can\'t include an iframe or is invalid'=>'La description ne peut contenir d\'iframe ou est invalide',
'Short description is invalid'=>'La description courte est invalide',
'Description is invalid'=>'La description est invalide',
'Always use category name as link rewrite'=>'Toujours utiliser le nom de la catégorie comme link_rewrite',
'Possible values:<br/>0: SC will NOT modifiy the link_rewrite of the category: you should set it yourself.<br/>1: SC always set the link_rewrite url to the name of the category.'=>'Valeurs possibles :<br/>0 : SC ne modifiera jamais l\'url de la catégorie : vous devrez l\'indiquer vous-même.<br/>1 : SC utilise le nom de la catégorie pour créer son url à chaque fois que celui-ci est modifié.',
'Associate only products in Advanced stocks'=>'Associer uniquement les produits en stocks avancés',
'Activate Advanced stocks and Associate'=>'Activer en stocks avancés et Associer',
'Activate Advanced stocks + manual mgmt and Associate'=>'Activer en Stocks Avancés + Gestion Manuelle et Associer',
'login as selected customer on the front office'=>'se connecter sur le front office en tant que le client sélectionné',
'Color code'=>'Code couleur',
'Add all selected combinations to all selected shop'=>'Ajouter toutes les déclinaisons sélectionnées dans toutes les boutiques sélectionées',
'Delete all selected combinations from all selected shop'=>'Retirer toutes les déclinaisons sélectionnées de toutes les boutiques sélectionées',
'To run exports, you need to install samples files from Tools > Installation'=>'Pour pouvoir faire vos exports il vous faut installer les fichiers exemples à partir de Outils > installation',
'Notice for the automatic modification of products link_rewrite'=>'Alerte pour le changement automatique de l\'url_rewrite des produits',
'Possible values:<br/>0: don\'t trigger alert<br/>1: display alert message after name edition'=>'Valeurs possibles :<br/>0 : pas d\'alerte<br/>1 : affiche une alerte après l\'édition du nom',
'Caution: The option located in Prestashop > Products > Force update of friendly URL is set to NO: friendly url will not be saved automatically. To stop this alert: SC  > Tools > Settings > Alert.'=>'Attention l\'option Préférence de Prestashop > Produits > "Forcer la mise à jour des URL simplifiées" est sur "Non" : Les produits n\'auront pas l\'url réécrite automatiquement. Pour supprimer cette alerte : utilisez le menu de SC : Outils > Préférences > Section : Alerte.',
'Notice when Advanced stock preference in PS is different to SC preference'=>'Alerte quand la préférence des stock avancés de PS est différente de celle dans SC',
'Possible values:<br/>0: don\'t trigger alert<br/>1: display alert message at SC load.'=>'Valeurs possibles :<br/>0 : pas d\'alerte<br/>1 : affiche une alerte au chargement de SC',
'Caution: The option located in Prestashop > Products > New products use advanced stock management is set to Yes but in SC the preference Default type for Advanced Stock Management is set to disabled. To stop this alert: SC  > Tools > Settings > Alert.'=>'Attention l\'option Préférence de Prestashop > Produits > "Utiliser la gestion des stocks avancée pour les nouveaux produts" est sur "Oui" alors que la préférence SC "Type par défaut pour la Gestion des stocks avancée" est sur Désactivée. Pour supprimer cette alerte : utilisez le menu de SC : Outils > Préférences > Section : Alerte.',
'Segment:'=>'Segment :',
'of segment'=>'du segment',
'& segments'=>'& segments',
'Use the Prestashop hook when quantity updated'=>'Utiliser le hook Prestashop quand une quantité a été modifiée',
'Suppliers'=>'Fournisseurs',
'Add all selected products to all selected suppliers'=>'Associer tous les produits sélectionnés à tous les fournisseurs sélectionnés',
'Remove all selected products from all selected suppliers'=>'Retirer tous les produits sélectionnés de tous les fournisseurs sélectionnés',
'suppliers'=>'fournisseurs',
'Price excl. tax'=>'Prix HT',
'Product/combination'=>'Produit/déclinaison',
'Default values display products/combinations grids'=>'Valeur par défaut affichée dans les grilles produits/déclinaisons',
'currency (supplier price)'=>'devise (prix fournisseur)',
'quantity - add'=>'quantité - ajouter',
'quantity - remove'=>'quantité - retirer',
'products ignored (no wholesale price is associated, incorrect quantity, etc)'=>'produits ont été ignorés pour les mouvements de stock (ils ne possèdent pas de prix d\'achat, quantité invalide, etc)',
'See shop'=>'Voir la boutique',
'Color the combinations in red with same attributes'=>'Colorer en rouge les déclinaisons avec les mêmes attributs',
'Color the row in red when some combinations have the same attributes values.'=>'Colorer la ligne en rouge lorsque plusieurs déclinaisons possèdent les mêmes valeurs d\'attributs.',
'Add all selected categories to all selected groups'=>'Ajouter toutes les catégories sélectionnées dans tous les groupes sélectionnés',
'Delete all selected categories from all selected groups'=>'retirer toutes les catégories sélectionnées de tous les groupes sélectionnés',
'Customer groups'=>'Groupes clients',
'Copy structure'=>'Copier la structure',
'Paste categories'=>'Coller les catégories',
'Discounts and margins'=>'Promotions et marges',
'supplier - default'=>'fournisseur - défaut',
'Margin amount after reduction'=>'Montant marge après réduction',
'Margin % after reduction'=>'% marge après réduction',
'Margin after reduction'=>'Marge après réduction',
'Price tax excl after reduction'=>'Prix HT après réduction',
'Price tax incl after reduction'=>'Prix TTC après réduction',
'Notice when create first combi'=>'Alerte pour la création de la première déclinaison',
'Possible values:<br/>0: don\'t trigger alert<br/>1: display alert.'=>'Valeurs possibles :<br/>0 : pas d\'alerte<br/>1 : affiche l\'alerte.',
'Caution: The first combination needs to be created by SC so that you can then add more in bulk. To stop this alert: SC  > Tools > Settings > Alert.'=>'Attention : SC doit créer une première déclinaison pour que vous puissiez ensuite les créer en masse. Pour supprimer cette alerte : utilisez le menu de SC : Outils > Préférences > Section : Alerte.',
'Product price excl. tax'=>'Prix produit HT',
'Quantity orded'=>'Quantité commandée',
'Qty in stock at time of order'=>'Qté en stock à la commande',
'Insufficient current total stock'=>'Stock actuel global insuffisant',
'Partial'=>'Partiel',
'If enabled: Images will be automatically uploaded once selected'=>'Si sélectionné : Les images seront automatiquement uploadées après les avoir sélectionnées',
'Display deleted accounts'=>'Afficher les comptes supprimés',
'Set to 1 if you want to display deleted accounts in customer views'=>'Mettez à 1 si vous voulez afficher les comptes supprimés dans les vues clients',
'Remove link between selected accessories and selected products'=>'Dissocier les accessoires sélectionnés et les produits sélectionnés',
'Action for new products'=>'Action pour les nouveaux produits',
'Action for existing products'=>'Action pour les produits existants',
'Modify product'=>'Modifier le produit',
'Create duplication'=>'Créer un doublon',
'Skipped'=>'Ignorés',
'Modified'=>'Modifiés',
'Created as duplication'=>'Créés comme doublon',
'!!! WARNING !!!'=>'!!! ATTENTION !!!',
'visibility'=>'visibilité',
'Inv.'=>'Fact.',
'Last invoice address'=>'Dernière adresse de facturation',
'Shipping n°'=>'Numéro de suivi',
'Picking'=>'Préparation des commandes',
'Orders and products'=>'Commandes et produits',
'Alert: You need to select only one customer'=>'Attention : vous devez sélectionner un seul client',
'Customer grid: Light view'=>'Grille clients : Vue rapide',
'Customer grid: Large view'=>'Grille clients : Vue d\'ensemble',
'Customer grid: Address view'=>'Grille clients : Adresse',
'Customer grid: Convert view'=>'Grille clients : Convertir',
'default order grid view'=>'vue par défaut de la grille des commandes',
'default customer grid view'=>'vue par défaut de la grille des clients',
'Set order grid view displayed when you launch SC. (grid_light, grid_large, grid_picking, grid_delivery)'=>'Affiche la grille choisie lors du lancement de SC. (grid_light, grid_large, grid_picking, grid_delivery)',
'Set customer grid view displayed when you launch SC. (grid_light, grid_large, grid_address, grid_convert)'=>'Affiche la grille choisie lors du lancement de SC. (grid_light, grid_large, grid_address, grid_convert)',
'for'=>'pour',
'Newsletter'=>'Newsletter',
'Create new customer with the PrestaShop form'=>'Créer un nouveau client avec le formulaire de PrestaShop',
'Create the new customer and close this window to refresh the grid'=>'Créez un nouveau client et fermez cette fenêtre pour mettre à jour la grille',
'Download PDF invoices from selected orders'=>'Télécharger les factures en PDF des commandes sélectionnées',
'Download PDF delivery slips from selected orders'=>'Télécharger les bons de livraison en PDF des commandes sélectionnées',
'Download'=>'Télécharger',
'Valid'=>'Valide',
'Invoice'=>'Facture',
'Gift'=>'Cadeau',
'Gift message'=>'Message cadeau',
'Recyclable'=>'Recyclable',
'Convertion rate'=>'Taux conversion',
'Total discounts'=>'Total réductions',
'Total discounts inc. tax'=>'Total réductions TTC',
'Total discounts exc. tax'=>'Total réductions HT',
'Total paid  real'=>'Total encaissé',
'Total paid inc. tax'=>'Total payé TTC',
'Total paid exc. tax'=>'Total payé HT',
'Total products'=>'Total produits HT',
'Total products inc. tax'=>'Total produits TTC',
'Total shipping'=>'Total transport',
'Total shipping inc. tax'=>'Total transport TTC',
'Total shipping exc. tax'=>'Total transport HT',
'Total wrapping'=>'Total emballage',
'Total wrapping inc. tax'=>'Total emballage TTC',
'Total wrapping exc. tax'=>'Total emballage HT',
'Carrier tax rate'=>'Taux taxe transport',
'Delivery slip'=>'Bon de livraison',
'Conversion rate'=>'Taux de conversion',
'Qty in stock'=>'Qté en stock',
'Qty refunded'=>'Qté remboursée',
'Qty returned'=>'Qté retournée',
'Customer not enabled on shop.'=>'Client non activé sur la boutique',
'3 days'=>'3 jours',
'Customers groups are available from Prestashop 1.2'=>'Les groupes de clients sont disponibles à partir de Prestashop 1.2',
'Properties grid: Groups'=>'Grilles propriétés : Groupes',
'Properties grid: Messages'=>'Grilles propriétés : Messages',
'Properties grid: Orders'=>'Grilles propriétés : Commandes',
'Properties grid: Addresses'=>'Grilles propriétés : Adresses',
'Properties grid: Customers'=>'Grilles propriétés : Clients',
'Order grid: Light view'=>'Grille commandes : vue rapide',
'Order grid: Large view'=>'Grille commandes : vue d\'ensemble',
'Order grid: Delivery view'=>'Grille commandes : expédition',
'Order grid: Picking'=>'Grille commandes : Préparation des commandes',
'Properties grid: Order history'=>'Grilles propriétés : Historique de commande',
'Properties grid: Products'=>'Grilles propriétés : Produits',
'maximum customers displayed'=>'nombre max de clients affichés',
'Mr.'=>'M.',
'Ms.'=>'Mme',
'Miss'=>'Mlle',
'Unk.'=>'Inc.',
'Set the maximum number of customers displayed in the main grid. You can increase this value if your server\'s ressources are sufficient.'=>'Détermine le nombre maximum de clients affichés dans la grille principale de SC. Vous pouvez augmenter cette valeur si votre hébergement est assez puissant.',
'Do you want to delete all items?'=>'Voulez-vous supprimer tous les éléments ?',
'Del.'=>'Exp.',
'Cart language'=>'Langue panier',
'Total paid (module)'=>'Total payé (module)',
'Invoice date'=>'Date facture',
'Delivery slip date'=>'Date bon livraison',
'The image is created as a progressive JPEG.'=>'L\'image JPG est créée avec un chargement progressif.',
'JPG progressive'=>'JPG progressif',
'Wrong mapping, mapping should contain the EAN field'=>'Mapping incorrect, le mapping doit contenir le champ EAN',
'Wrong mapping, mapping should contain the UPC field'=>'Mapping incorrect, le mapping doit contenir le champ UPC',
'EAN'=>'EAN',
'UPC'=>'UPC',
'login as selected customer'=>'se connecter comme client',
'Use this compatibility mode if "Login as selected customer" does not work on the front office'=>'Activez ce mode de compatibilité si "Se connecter en tant que client sur cette boutique" ne fonctionne pas.',
'Accounting'=>'Comptabilité',
'Quick Accounting'=>'Quick Compta',
'available date'=>'date de disponibilité',
'VAT Scheduler'=>'Programmateur TVA',
'Export orders'=>'Export commandes',
'attribute of combination - color'=>'attribut de déclinaison - couleur',
'Grids editor'=>'Editeur de grilles',
'Date of account creation'=>'Date de création du compte',
'The customer has been imported but there is a problem with his address.'=>'Le client a bien été importé mais il y a un problème sur son adresse.',
'You need to check these fields on the first line of'=>'Vous devez contrôler ces champs sur la première ligne de',
'More information about the address'=>'Plus d\'informations sur cette adresse',
'Use SC extensions'=>'Utiliser les extensions SC',
'Enable/Disable SC Extensions'=>'Activer/Désactiver les extensions SC',
'additional shipping cost'=>'frais de port supplémentaires',
'Use global.css in descriptions'=>'Utiliser global.css dans les descriptions',
'Use the global.css stylesheet of the shop in the editors. You can set it to 0 if the background of your shop is displayed in the text editors.'=>'Utiliser le fichier global.css de la boutique dans les éditeurs. Vous pouvez indiquez 0 si le background de votre boutique est visible dans les éditeurs de texte.',
'additional_shipping_cost'=>'frais_de_port_supplémentaire',
'Only products associated to supplier <strong>%s</strong> will be updated.'=>'Seuls les produits du fournisseur <strong>%s</strong> seront pris en compte pour la mise à jour des produits.',
'<strong>%s</strong> lines of file <strong>"%s"</strong> will be imported.'=>'<strong>%s</strong> lignes du fichier <strong>"%s"</strong> vont être importées.',
'The mapping <strong>"%s"</strong> will be used.'=>'Le mapping <strong>"%s"</strong> sera utilisé.',
'Products will be identified by <strong>%s</strong>.'=>'Les produits seront identifiés par <strong>%s</strong>.',
'Action for new products: <strong>%s</strong>.'=>'Action pour les nouveaux produits : <strong>%s</strong>.',
'Action for existing products: <strong>%s</strong>.'=>'Action pour les produits existants : <strong>%s</strong>.',
'The categories will be created automatically and products associated to them.'=>'L\'arborescence des catégories sera créée automatiquement et les produits placés à l\'intérieur.',
'Elements found in the CSV file will be created automatically: features, combination attributes, manufacturers, suppliers, tags.'=>'Les éléments du fichier CSV seront créés automatiquement : caractéristiques, attributs de déclinaison, fabricants/marques, fournisseurs et tags.',
'Is your import ready? See the Checklist!'=>'Votre import est-il prêt ? Consultez la checklist !',
'Field/Value separators selected in your configuation do not seem to match your CSV file. Check your settings.'=>'Les séparateurs de champ et/ou valeur indiqués dans votre configuration ne semblent pas correspondre à votre fichier CSV. Vérifiez la configuration de votre fichier CSV.',
'An action needs to be selected before importing.'=>'Vous devez sélectionner une action à effectuer avant d\'importer.',
'If this import will create combinations, only the first combination of each product will be created. To avoid this, select "modify product" in "Action for existing products".'=>'Si cet import va vous servir pour créer des déclinaisons, seule la première déclinaison de chaque produit va être insérée. Pour éviter cela, il vous suffit de choisir "Modifier le produit" dans "Action pour les produits existants".',
'If this import will add new combinations, a product will be created for each line corresponding to a combination. To avoid this select "modify product" in "Action for existing products".'=>'Si cet import va vous servir pour ajouter de nouvelles déclinaisons, un produit va être créé pour chaque ligne de déclinaison. Pour éviter cela, il vous suffit de choisir "Modifier le produit" dans "Action pour les produits existants".',
'Lines of your CSV file do not use the correct number of columns, please check your file. Alternatively, this can be caused by descriptions spread on multiple lines.'=>'Veuillez vérifier votre fichier car il semblerait que toutes les lignes ne possèdent pas le bon nombre de colonnes. Cela peut également venir d\'une description sur plusieurs lignes.',
'You should use the tool "check and fix the level_depth field" from the Catalog > Tools menu to fix your categories.'=>'Vous devez utiliser l\'outil "Vérifier et corriger le champ level_depth" depuis le menu Catalogue > Outils pour corriger vos catégories',
'Sort by position'=>'Trier par position',
'condition (new, used, refurbished)'=>'etat (new, used, refurbished)',
'Columns'=>'Colonnes',
'Properties of selected combinations'=>'Propriétés des déclinaisons sélectionnées',
'Before'=>'Avant',
'On'=>'Le',
'After'=>'Après',
'Unlimited'=>'Illimité',
'Manufacturers'=>'Fabricants',
'Countries'=>'Pays',
'If enabled: display specific prices from all subcategories'=>'Si activé : affiche les prix spécifiques de toutes les sous-catégories',
'Validate'=>'Valider',
'Specific prices on this date'=>'Prix spécifiques à cette date',
'Display specific prices for combinations'=>'Afficher les prix spécifiques des déclinaisons',
'You must select only one line.'=>'Vous devez sélectionner une seule ligne.',
'Permanently delete the selected special prices ?'=>'Supprimer définitivement les prix spécifiques sélectionnés ?',
'Multiple actions are currently running. Do you really want to close?'=>'Plusieurs tâches sont en cours. Voulez-vous vraiment quitter ?',
'Tasks'=>'Tâches',
'Tasks error logs'=>'Tâches : log d\'erreurs',
'An error has occured:'=>'Une erreur est survenue :',
'Check the logs to see the modification that triggered the error as well as other that could not be applied'=>'Vérifiez les logs pour connaître la tâche qui a provoquée cette erreur',
'See logs'=>'Voir les logs',
'An error has occured when inserting'=>'Une erreur est survenue lors de la création',
'Row'=>'Ligne',
'Delete all history'=>'Supprimer tout l\'historique',
'Run the task again'=>'Ré-exécuter la tâche',
'Are you sure you want to delete these rows?'=>'Etes-vous sûr de supprimer ces lignes ?',
'Are you sure you want to run these tasks again?'=>'Etes-vous sûr d\'exécuter ces tâches ?',
'actions'=>'actions',
'Your import includes \'description\' and/or \'short description\', please check carriage returns.'=>'Votre import comporte les champs \'description\' et/ou \'description courte\', vérifiez s\'ils contiennent des retours à la ligne.',
'http://support.storecommander.com/entries/22130421-Importing-your-description-efficiently'=>'http://support.storecommander.com/entries/22143046-Importez-vos-descriptions-efficacement',
'Number of tasks sent to the server'=>'Nombre de tâches envoyées au serveur',
'Number of tasks sent similtaneously to be actioned by the server'=>'Nombre de tâches envoyées simultanément au serveur',
'Open in SC catalog'=>'Ouvrir dans le catalogue SC',
'Update date'=>'Date de mise à jour',
'This will set the cheapest combination in stock as default combination for each seleted product. Continue?'=>'Cette opération déterminera la déclinaison en stock la moins chère comme déclinaison par défaut pour les produits sélectionnés. Continuer ?',
'Set the cheapest combination in stock as default combination'=>'Définir la déclinaison en stock la moins chère comme déclinaison par défaut',
'Display all descriptions'=>'Afficher toutes les descriptions',
'Automatically share the products images'=>'Partager automatiquement les images produits',
'Automatically share the products images when the product is shared with a new shop. Set the option to 0 if you do not wish to share images automatically.'=>'Partager automatiquement toutes les images du produit lorsque le produit est partagé sur une nouvelle boutique. Indiquez 0 si vous ne souhaitez pas ce partage automatique.',
'Use TinyMCE as text editor'=>'Utiliser TinyMCE comme éditeur de texte',
'Set this option to 1 if you want to use TinyMCE rather than CKeditor (default).'=>'Mettez cette option à 1 si vous souhaitez utiliser TinyMCE à la place de CKEditor (défaut).',
'default order properties panel'=>'commandes : panneau de propriété par défaut',
'Set order properties panel displayed when you launch SC. (orderproduct,message,orderhistory,orderpsorderpage)'=>'Affiche le panneau choisi lors du lancement de SC. (orderproduct,message,orderhistory,orderpsorderpage)',
'default customer properties panel'=>'clients : panneau de propriété par défaut',
'Set customer properties panel displayed when you launch SC. (customerorder,message,customergroup,customeraddress)'=>'Affiche le panneau choisi lors du lancement de SC. (customerorder,message,customergroup,customeraddress)',
'open browser tab'=>'ouvrir dans un onglet navigateur',
'Set to 1 if you wish to open PS windows in a new browser tab instead of SC window. (forced to 1 if SC is run on iPad)'=>'Indiquez 1 si vous souhaitez ouvrir les fenêtres de Prestashop dans de nouveaux onglets plutôt que dans des fenêtres de l\'interface. (Sur iPad, cette option est forcée à 1)',
'Store Commander cannot create the table %s, please contact your hosting support and ask: Can you please confirm that the MySQL user has the necessary permission to execute these commands: SHOW and CREATE TABLE. Upon confirmation, you can restart Store Commander.'=>'Store Commander ne peut pas créer la table %s dans la base de données. Veuillez contacter le support technique de votre hébergeur pour lui demander : Pouvez-vous vérifier que l\'utilisateur MySQL a les permissions nécessaires pour exécuter les commandes : SHOW et CREATE TABLE ? A réception de la confirmation, vous pouvez relancer Store Commander.',
'price'=>'prix',
'prices'=>'prix',
'Status update'=>'Màj état commande',
'Ord from'=>'Com du',
'Inv from'=>'Fact du',
'Action: Dissociate carriers'=>'Action : Dissocier les transporteurs',
'New products default status'=>'Statut par défaut des nouveaux produits',
'Active status used when the product is created by CSV import'=>'Activer le produit lors de sa création par import CSV',
'Export EAN13'=>'Export EAN13',
'When exporting EAN13 for combinations:<br/>Possible values:<br/>0: the exported EAN13 is associated to the combinations.<br/>1: the exported EAN13 is associated to the product if EAN13 is empty for combinations.'=>'Lors d\'un export EAN13 pour les déclinaisons :<br/>Valeurs possibles :<br/>0 : l\'EAN13 exporté est toujours celui des déclinaisons.<br/>1 : l\'EAN13 exporté est celui du produit si l\'EAN13 de la déclinaison est vide.',
'Choose a standard message'=>'Choisissez un message prédéfini',
'Do you want to overwrite your existing message?'=>'Voulez-vous remplacer le message existant ?',
'Products position'=>'Positions des produits',
'Vat number'=>'Numéro TVA',
'Delivery date'=>'Date de livraison',
'Delivery date - Standard'=>'Date de livraison - Standard',
'Delivery date - Limit'=>'Date de livraison - Limite',
'Clear warehouse (keep history)'=>'Vider entrepôt (garder mvt stock)',
'Clear warehouse'=>'Vider entrepôt',
'physical stock'=>'quantité physique',
'available stock'=>'quantité utilisable',
'live stock'=>'quantité réelle',
'display subcategories count'=>'afficher le nombre de sous-catégories',
'Possible values:<br/>0: no count displayed (best display performance)<br/>1: count displayed (best user experience)'=>'Valeurs possibles :<br/>0: pas d\'affichage (meilleures performances)<br/>1: afficher le nombre de sous-catégories (meilleure expérience utilisateur)',
'Tips'=>'Astuces',
'Open Tips'=>'Ouvrir les Astuces',
'Find all tips in Help &gt; Tips'=>'Retrouvez toutes les astuces dans le menu Aide &gt; Astuces',
'Number of decimal to export prices'=>'Nombre de décimales pour l\'export des prix',
'Possible values: 0 to 6'=>'Valeurs possibles : 0 à 6',
'At least one shop needs to be ticked'=>'Au moins une boutique doit être cochée',
'Properties grid: Warehouses'=>'Grilles propriétés : Entrepôts',
'Group rows by product'=>'Grouper les lignes par produits',
'Includes access to \'Advanced Stocks\' and \'Warehouses\' grids, and actions on quantities fields and in the \'Warehouses\' panel'=>'Inclusl\'accès aux grilles \'Stocks avancés\' et \'Entrepôt\', et les actions sur les champs quantités dans le panneau \'Entrepôts\'',
'Would you like to dissociate all products from warehouse'=>'Voulez-vous dissocier tous les produits de l\'entrepôt',
'Quantity in stock at the time of the order'=>'Quantity in stock at the time of the order',
'Action: Dissociate warehouses'=>'Action: Dissociate warehouses',
'You are trying to add quantities to a product with combinations: product ID '=>'Vous essayez d\'ajouter de la quantité à un produit avec des déclinaisons : ID produit ',
'Product grid:'=>'Grille produit :',
'Customer grid:'=>'Grille client :',
'Order grid:'=>'Grille commande :',
'Are you sure you want to dissociate the selected items?'=>'Êtes-vous sûr de vouloir dissocier les éléments sélectionnées ?',
'When \'All shops\' is selected, associate products to categories using the Categories grid in Properties.'=>'Pour affecter des produits à des catégories en vue "Toutes les boutiques", veuillez passer par le panneau de droite.',
'Products positions cannot be set when \'All shops\' is selected'=>'Vous ne pouvez gérer la position des produits en vue "Toutes les boutiques"',
'Supply order ID'=>'ID Cmd. fournisseur',
'Quantity in stock at the time of the order'=>'Qté en stock lors de commande',
'Product grid:'=>'Grille produit :',
'Customer grid:'=>'Grille clients :',
'Order grid:'=>'Grille commandes :'	,
'new combination quantity default'=>'quantité par défaut des nouvelles déclinaisons',
'Combination quantity used when the combination is created in SC.'=>'Quantité qui sera renseignée sur les déclinaisons créées dans SC',
'Possible values:<br/>1: by delivery number<br/>1: by id order.'=>'Valeurs possibles :<br/>1 : par numéro de livraison<br/>2 : par id commande.',
'Delivery slips order in PDF file'=>'Ordre des bons de livraison dans le PDF',
'combination - used'=>'déclinaison - utilisée',
'Total credit notes'=>'Total avoirs',
'License'=>'Licence',
'Create your Store Commander Trial license for 30 days, full features.'=>'Créez votre licence Store Commander Trial avec toutes les fonctionnalités incluses pendant 30 jours.',
'Your Trial license has expired. You need to purchase a new license to boost your ecommerce with Store Commander.'=>'Votre licence Trial a expiré. Vous devez acquérir une nouvelle licence pour booster votre ecommerce avec Store Commander.',
    'Use M4PDF if available'=>'Utiliser M4PDF si disponible',
    'Possible values:<br/>1: use M4PDF<br/>0: do not use M4PDF.'=>'Valeurs possibles :<br/>1: utiliser M4PDF<br/>0: ne pas utiliser M4PDF.',
    'Export Reference'=>'Export Référence',
    'When exporting reference for combinations:<br/>Possible values:<br/>0: the exported reference is associated to the combinations.<br/>1: the exported reference is associated to the product if reference is empty for combinations.'=>'Lors d\'un export référence pour les déclinaisons :<br/>Valeurs possibles :<br/>0 : la référence exportée est toujours celle des déclinaisons.<br/>1 : la référence exportée est celle du produit si la référence de la déclinaison est vide.'
,'Compatibility for module Product Properties Extension'=>'Compatibilité pour module Product Properties Extension'
,'Possible values for min. qty:<br/>0: no decimal (int type)<br/>1: float type in import and interface'=>'Valeurs possibles pour qté. min. :<br/>0: pas de décimale (type int) <br/>1: type float pour import et interface.'
,'priceinctax_with_reduction'=>'prix TTC avec réduction'
,'priceexctax_with_reduction'=>'prix HT avec réduction'
,'Start your 30 days trial period with all SC functionalities'=>'Démarrez vos 30 jours d\'essai avec toutes les fonctionnalités activées'
,'Start a Trial'=>'Démarrer une licence Trial'
    ,'The best catalog management application for Prestashop!'=>'Le meilleur outil de gestion pour Prestashop !'
,'Compare all functionalities to make the right choice for your license'=>'Comparez toutes les fonctionnalités pour bien choisir votre licence'
,'Unleash the power of Store Commander!'=>'Libérez le pouvoir de Store Commander !'
,'30 days trial + 30 days money back guarantee:'=>'30 jours d\'essai + 30 jours satisfait ou remboursé :'
,'You already have a license'=>'Vous possédez déjà une licence'
,'You don\'t have a license'=>'Vous n\'avez pas encore de licence'
,'The trial period cannot be started on localhost but only online!'=>'La version d\'essai ne peut être installée en localhost, uniquement en ligne !'
,'Your Trial period information'=>'Informations sur votre période d\'essai'
,'Actual product price tax excl'=>'Prix produit HT actuel'
,'Actual product price tax incl'=>'Prix produit TTC actuel'
,'Actual product price tax excl after reduction'=>'Prix produit HT actuel avec réduc.'
,'Actual product price tax incl after reduction'=>'Prix produit TTC actuel avec réduc.'
,'allow grids column move'=>'autoriser le déplacement des colonnes'
,'0 to disable. To use when some browser extensions are conflicting with SC interface'=>'Indiquez 0 pour désactiver la possibilité de déplacer les colonnes des grilles. A utiliser si vous avez des conflits entre votre navigateur web et l\'interface SC.'
,'5 days'=>'5 jours'
,'10 days'=>'10 jours'
,'Attachment:'=>'Doc joint :'
,'Reduction tax'=>'Taxe sur la réduction'
,'Group orders by ?'=>'Grouper les commandes par ?'
,'Possible values:<br/>1: by product name<br/>2: by product ID'=>'Valeurs possibles :<br/>1: par nom produit<br/>2: par ID produit'
,'Original wholesale price'=>'Prix d\'achat d\'origine'
,'Automatic description'=>'Description automatique'
,'Possible values:<br/>0: Empty<br/>1: File name + iso code<br/>2: File name'=>'Valeurs possibles :<br/>0: Vide<br/>1: Nom fichier + code iso<br/>2: Nom fichier'
,'Discount vouchers'=>'Bons de réduction'
,'Excl. tax'=>'Taxe non incluse'
,'Incl. tax'=>'Taxe incluse'
,'Discount vouchers (cart rules)'=>'Bons de réduction (règles panier)'
,'Create new cart rule'=>'Créer une nouvelle règle panier'
,'Date from'=>'Date début'
,'Date to'=>'Date fin'
,'Quantity per user'=>'Quantité par client'
,'Filter by shop'=>'Filtre boutique'
,'Create the new cart rule and close this window to refresh the tree'=>'Créez la nouvelle règle panier et fermez cette fenêtre pour mettre à jour l\'arbre des catégories'
,'Min. amount'=>'Montant min.'
,'Edit the discount voucher'=>'Modifier le bon de réduction'
,'Discount voucher:'=>'Bon de réduction :'
,'Export UPC'=>'Export UPC'
,'When exporting UPC for combinations:<br/>Possible values:<br/>0: the exported UPC is associated to the combinations.<br/>1: the exported UPC is associated to the product if UPC is empty for combinations.'=>'Lors d\'un export UPC pour les déclinaisons :<br/>Valeurs possibles :<br/>0 : l\'UPC exporté est toujours celui des déclinaisons.<br/>1 : l\'UPC exporté est celui du produit si l\'UPC de la déclinaison est vide.'
,'Load all fields for import'=>'Charger tous les champs pour l\'import'
,'Apply specific prices by default'=>'Appliquer les prix spécifiques par défaut'
,'Possible values:<br/>0: product excl. tax<br/>1: product incl. tax'=>'Valeurs possibles :<br/>0: Taxe produit excluse<br/>1: Taxe produit incluse'
,'Catalog CSV Import'=>'Import CSV du catalogue'
,'Possible values:<br/>0: debug mode disabled<br/>1: debug mode enabled.'=>'Valeurs possibles :<br/>0: mode debug désactivé<br/>1: mode debug activé'
,'Method to import products'=>'Méthode d\'import des produits'
,'Possible values:<br/>0: standard process<br/>1: use of views.'=>'Valeurs possibles :<br/>0: processus standard<br/>1: utiliser les vues'
,'Disable column move in main grids for Catalog, Order and Customer interfaces'=>'Désactiver le déplacement des colonnes dans les grilles principales de l\'interface Catalogue, Commandes et Clients'
,'Possible values:<br/>0: Enabled colmun move.<br/>1: Disabled colmun move.'=>'Valeurs possibles :<br/>0: Activer le déplacement.<br/>1: Désactiver le déplacement.'
,'Change security key'=>'Changer la clé de sécurité'
,'Changing the security key will change the name of Store Commander\'s security directory.<br/><br/><strong>Beware:</strong><br/>Everyone currently using SC will need to restart Store Commander from the backoffice.  Please let the users know!<br/><br/>If CRON tasks are in place, remember to change the URL path in the tasks configuration files.<br/><br/><a href="http://www.storecommander.com/redir.php?dest=2016021501" target="blank">Get more information</a>'=>'Le changement de clé de sécurité va changer le nom du dossier de sécurité de Store Commander.<br/><br/><strong>Attention :</strong><br/>Toutes les personnes qui utilisent actuellement SC devront relancer l\'application depuis le BackOffice Prestashop. Prévenez les utilisateurs !<br/><br/>Si vous utilisez des tâches CRON, pensez à modifier le chemin d\'accès à vos tâches.<br/><br/><a href="http://www.storecommander.com/redir.php?dest=2016021502" target="blank">Obtenir plus d\'informations</a>'
,'Store Commander is installed in the Admin directory of your Prestashop store. This is the old method that needs updating.<br/><br/>Please follow these instructions to place Store Commander in the up to date folder: <a href="http://www.storecommander.com/redir.php?dest=2016021511" target="_blank">Read more</a>'=>'Store Commander est installé dans le dossier l\'administration de PrestaShop et cette méthode d\'installation est dépassée.<br/><br/>Veuillez suivre ces instructions pour placer Store Commander dans le bon dossier : <a href="http://www.storecommander.com/redir.php?dest=2016021512" target="_blank">En savoir plus</a>'
,'Warning'=>'Attention'
,'Total left to pay'=>'Total restant à payer'
,'Action: Dissociate attachments'=>'Action : Dissocer les documents joints'
,'Categories are identified by'=>'Catégories identifiéés par'
,'Category name'=>'Nom catégorie'
,'Action for new categories'=>'Action pour les nouvelles catégories'
,'Action for existing categories'=>'Action pour les catégories existantes'
,'Modify category'=>'Modifier la catégorie'
,'Categories will be identified by <strong>%s</strong>.'=>'Les catégories seront identifiés par <strong>%s</strong>.',
'Action for new categories: <strong>%s</strong>.'=>'Action pour les nouvelles catégories : <strong>%s</strong>.',
'Action for existing categories: <strong>%s</strong>.'=>'Action pour les catégories existantes : <strong>%s</strong>.'
,'New categories:'=>'Nouvelles catégories :'
,'Modified categories:'=>'Catégories modifiées :'
,'All categories have been imported. The TODO file is deleted.'=>'Toutes les catégories ont été importées. Le fichier TODO a été supprimé.'
,'All categories have been imported.'=>'Toutes les catégories ont été importées.'
,'There are still categories to be imported in the working file. It can mean errors you need to correct or lines which have been ignored on purpose. Once corrections have been made, click again on the import icon to proceed further.'=>'Il reste des catégories à traiter dans le fichier de travail, il peut s\'agir d\'erreurs à corriger ou de lignes qui ont été ignorées à juste titre. Après vos corrections éventuelles, recliquez sur l\'icône Importer pour poursuivre la procédure.'
,'Update categories older than'=>'Mise à jour des catégories plus vieilles de'
,'Force creation of parent categories and create new category'=>'Forcer la création des catégories parentes et créer la nouvelle catégorie'
,'Check if parent categories exist and create new category'=>'Vérifier l\'existence des catégories parentes et créer la nouvelle catégorie'
,'Wrong mapping, mapping should contain the fields: name + parents or path'=>'Mapping incorrect, le mapping doit contenir les champs : Nom + Parents ou Chemin complet'
,'path'=>'chemin complet'
,'Path'=>'Chemin complet'
,'Wrong mapping, mapping should contain the field: path'=>'Mapping incorrect, le mapping doit contenir le champ : Chemin complet'
,'To create the category and its parents, the <strong>Path</strong> field will be used'=>'Pour créer la catégorie et ses parents, le <strong>Chemin complet</strong> sera utilisé.'
,'To create the category and its parents, the <strong>Name + Parents</strong> fields will be used'=>'Pour créer la catégorie et ses parents, les champs <strong>Nom et Parents</strong> seront utilisés.'
,'To create the category in its parents, the <strong>Path</strong> field will be used'=>'Pour créer la catégorie dans les catégories parentes, le <strong>Chemin complet</strong> sera utilisé.'
,'To create the category in its parents, the <strong>Name + Parents</strong> fields will be used'=>'Pour créer la catégorie dans les catégories parentes, les champs <strong>Nom et Parents</strong> seront utilisés.'
,'Action: Dissociate groups'=>'Action: Dissocier les groupes'
,'customer groups'=>'groupes clients'
,'ecotax tax incl.'=>'ecotax TTC'
,'complete path'=>'chemin complet'
,'parents path'=>'chemin parent'
,'Categories - Import CSV'=>'Catégories - Import CSV'
,'Categories - export CSV'=>'Catégories - export CSV'
,'Apply all catalog price rules'=>'Appliquer toutes les règles de prix catalogue'
,'Possible values:<br/>1: Use SpecificPriceRule::applyAllRules() to use catalog price rules everywhere in SC when you move a product in a category or modify a product<br/>0: Disable catalog price rules modifications during products updates with SC'=>'Valeurs possibles :<br/>1: Utiliser SpecificPriceRule::applyAllRules() pour utiliser les règles de prix catalogue partout dans SC lorsque vous déplacez un produit dans une catégorie ou modifiez un produit<br/>0: Désactive les modifications de règles de prix catalogue pendant vos modifications avec SC'
,''=>''
);
// ,''=>''
?>