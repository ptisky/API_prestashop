$(document).ready(function(){

  hasCombination();
  hasDiscount();
  hasFeatures();
  saveConfig();

  $('.simpleimportproduct .show_more').live('click', function(){
    if( $(this).hasClass('active') ){
      $(this).removeClass('active');
      $(this).html(import_show_more);
      $('.simpleimportproduct #fieldset_1_1').attr('style','');
      $('.simpleimportproduct #fieldset_1_1').removeClass('show_all');
    }
    else{
      $(this).addClass('active');
      $(this).html(import_hide);
      $('.simpleimportproduct #fieldset_1_1').addClass('show_all');
      setTimeout(function(){
        $('.simpleimportproduct #fieldset_1_1').css('height', '100%')
      },500);
    }
  });

  $('.panel-heading-gomakoil .panel-step .step_2, .next_button_import').click(function(){
    stepTwoImport();
  });
  $('.panel-heading-gomakoil .panel-step .step_1').click(function(){
    $("body").append('<div class="progres_bar_ex"><div class="loading"><div></div></div></div>');
    location.href = $('#location_href').val();
  });
  $('.simpleimportproduct .format_file').live('change', function(){
    var format_file = $('.format_file').val();
    if(format_file == 'csv'){
      $(this).parent().parent().next().next().show();
    }
    else{
      $(this).parent().parent().next().next().hide();
    }
  });
  $('.simpleimportproduct .import_type_val').live('change', function(){
    var type = $('.import_type_val').val();
    if(type == 'Add/update'){
      $(this).parent().parent().next().show();
    }
    else{
      $(this).parent().parent().next().hide();
    }
  });
  $('.delete_combination').live('click', function(){
    $(this).parents('#fieldset_3_3').remove();
  });
  $('.delete_discount').live('click', function(){
    $(this).parents('#fieldset_5_5').remove();
  });
  $('.delete_featured').live('click', function(){
    $(this).parents('#fieldset_7_7').remove();
  });
  $('input[name="has_combination"]').live('change', function(){
    hasCombination()
  });
  $('input[name="has_discount"]').live('change', function(){
    hasDiscount()
  });
  $('input[name="has_featured"]').live('change', function(){
    hasFeatures()
  });
  $('input[name="save_config"]').live('change', function(){
    saveConfig();
  });

  $('.more_combination').live('click', function(){
    moreCombination($(this).parents('#fieldset_3_3'))
  });
//  $('.more_discount').live('click', function(){
//    moreDiscount()
//  });
  $('.more_featured').live('click', function(){
    moreFeatures($(this).parents('#fieldset_7_7'))
  });


  $('.more_subcategory button').live('click', function(){
    moreSubcategory($(this).parent().attr('category'));
  });
  $('.more_category button').live('click', function(){
    moreCategory();
  });

  $('.delete_one_subcategory').live('click', function(){

    var val_count = $(this).parent().parent().find('.count_cat').html();
    var hidden_count_subcategory = parseInt($('.hidden_count_subcategory_'+val_count).val());
    $('.hidden_count_subcategory_'+val_count).val(hidden_count_subcategory - 1);

    $(this).parent().remove();
  });
  $('.delete_one_category').live('click', function(){
    $(this).parent().remove();
  });

  $('.simpleimportproduct .button_import').live('click', function(){
    importProducts();
  });

  $('.save_all_settings').live('click', function(){
    saveSettings();
  });

  $('.delete_config').live('click', function(){
    removeSettings($(this).attr('settings'));
  });

  if($('#combinations_more').val() == 1 && ($('#key_settings').val())){
    addCombinations();
  }
  if($('#features_more').val() == 1 && ($('#key_settings').val())){
    addFeatures();
  }
  if($('#key_settings').val()){
    addCategories();
  }


});

function addCategories(){
  $.ajax({
    url: '../modules/simpleimportproduct/send.php',
    type: 'post',
    data: 'addCategories=true&key_settings='+$('#key_settings').val(),
    dataType: 'json',
    success: function(json) {
      if (json['page']) {
        $('.content_import_page #fieldset_0 .form-group-categories .col-lg-9').replaceWith(json['page']);
      }
    }
  });
}

function addCombinations(){
  $.ajax({
    url: '../modules/simpleimportproduct/send.php',
    type: 'post',
    data: 'addCombinations=true&key_settings='+$('#key_settings').val(),
    dataType: 'json',
    success: function(json) {
      if (json['page']) {
        $('.content_import_page #fieldset_3_3').last().after(json['page']);
      }
    }
  });
}

function addFeatures(){
  $.ajax({
    url: '../modules/simpleimportproduct/send.php',
    type: 'post',
    data: 'addFeatures=true&key_settings='+$('#key_settings').val(),
    dataType: 'json',
    success: function(json) {
      if (json['page']) {
        $('.content_import_page #fieldset_7_7').last().after(json['page']);
      }
    }
  });
}

function moreCategory(){
  var hidden_count_category = parseInt($('.one_category_block').last().find('.count_cat').html()) + 1;

  $.ajax({
    url: '../modules/simpleimportproduct/send.php',
    type: 'post',
    data: 'moreCategory=true&hidden_count_category='+hidden_count_category,
    dataType: 'json',
    beforeSend: function(){
      $("body").append('<div class="progres_bar_ex"><div class="loading"><div></div></div></div>');
    },
    complete: function(){
      $(".progres_bar_ex").remove();
    },
    success: function(json) {
      $('.alert, .alert-danger, .alert-success').remove();
      if (json['error']) {
        $(document).scrollTop(0);
        $('#bootstrap_products_import').append('<div class="alert alert-danger">' + json['error'] + '</div>');
      }
      else {
        if (json['page']) {
          var block = hidden_count_category-1;
          $('.one_category_block_'+block).after(json['page']);
        }
      }
    }
  });
}

function moreSubcategory(category){
  var hidden_count_subcategory = parseInt($('.hidden_count_subcategory_'+category).val()) + 1;
  $('.hidden_count_subcategory_'+category).val(hidden_count_subcategory);
  $.ajax({
    url: '../modules/simpleimportproduct/send.php',
    type: 'post',
    data: 'moreSubcategory=true&hidden_count_subcategory='+hidden_count_subcategory,
    dataType: 'json',
    beforeSend: function(){
      $("body").append('<div class="progres_bar_ex"><div class="loading"><div></div></div></div>');
    },
    complete: function(){
      $(".progres_bar_ex").remove();
    },
    success: function(json) {
      $('.alert, .alert-danger, .alert-success').remove();
      if (json['error']) {
        $(document).scrollTop(0);
        $('#bootstrap_products_import').append('<div class="alert alert-danger">' + json['error'] + '</div>');
      }
      else {
        if (json['page']) {
          var block = parseInt(hidden_count_subcategory)-1;
          $('.one_category_block_'+category+' .one_subcategory').last().after(json['page']);
        }
      }
    }
  });
}


function moreFeatures(afterBlock){
  $.ajax({
    url: '../modules/simpleimportproduct/send.php',
    type: 'post',
    data: 'moreFeatures=true',
    dataType: 'json',
    beforeSend: function(){
      $("body").append('<div class="progres_bar_ex"><div class="loading"><div></div></div></div>');
    },
    complete: function(){
      $(".progres_bar_ex").remove();
    },
    success: function(json) {
      $('.alert, .alert-danger, .alert-success').remove();
      if (json['error']) {
        $(document).scrollTop(0);
        $('#bootstrap_products_import').append('<div class="alert alert-danger">' + json['error'] + '</div>');
      }
      else {
        if (json['page']) {
          afterBlock.after(json['page']);
        }
      }
    }
  });
}
//function moreDiscount(){
//  $.ajax({
//    url: '../modules/simpleimportproduct/send.php',
//    type: 'post',
//    data: 'moreDiscount=true',
//    dataType: 'json',
//    beforeSend: function(){
//      $("body").append('<div class="progres_bar_ex"><div class="loading"><div></div></div></div>');
//    },
//    complete: function(){
//      $(".progres_bar_ex").remove();
//    },
//    success: function(json) {
//      $('.alert, .alert-danger, .alert-success').remove();
//      if (json['error']) {
//        $(document).scrollTop(0);
//        $('#bootstrap_products_import').append('<div class="alert alert-danger">' + json['error'] + '</div>');
//      }
//      else {
//        if (json['page']) {
//          $('#fieldset_5_5').last().after(json['page']);
//        }
//      }
//    }
//  });
//}
function moreCombination(afterBlock){
  $.ajax({
    url: '../modules/simpleimportproduct/send.php',
    type: 'post',
    data: 'moreCombination=true',
    dataType: 'json',
    beforeSend: function(){
      $("body").append('<div class="progres_bar_ex"><div class="loading"><div></div></div></div>');
    },
    complete: function(){
      $(".progres_bar_ex").remove();
    },
    success: function(json) {
      $('.alert, .alert-danger, .alert-success').remove();
      if (json['error']) {
        $(document).scrollTop(0);
        $('#bootstrap_products_import').append('<div class="alert alert-danger">' + json['error'] + '</div>');
      }
      else {
        if (json['page']) {
          afterBlock.after(json['page']);
        }
      }
    }
  });
}


function saveConfig(){
  var active = $('input[name="save_config"]:checked').val();
  if(active == '1'){
    $('.content_import_page #fieldset_9_9').show();
  }
  else{
    $('.content_import_page #fieldset_9_9').hide();
  }
}
function hasCombination(){
  var active = $('input[name="has_combination"]:checked').val();
  if(active == '1'){
    $('.content_import_page #fieldset_3_3').show();
  }
  else{
    $('.content_import_page #fieldset_3_3').hide();
  }
}
function hasDiscount(){
  var active = $('input[name="has_discount"]:checked').val();
  if(active == '1'){
    $('.content_import_page #fieldset_5_5').show();
  }
  else{
    $('.content_import_page #fieldset_5_5').hide();
  }
}
function hasFeatures(){
  var active = $('input[name="has_featured"]:checked').val();
  if(active == '1'){
    $('.content_import_page #fieldset_7_7').show();
  }
  else{
    $('.content_import_page #fieldset_7_7').hide();
  }
}

function stepTwoImport(){

  var xlsxData = new FormData();
  xlsxData.append('file', $('input[name=file_import]')[0].files[0]);
  xlsxData.append('stepTwo', true);
  xlsxData.append('id_lang', $('.id_lang').val());
  xlsxData.append('id_shop', $('input[name="id_shop"]').val());
  xlsxData.append('id_shop_group', $('input[name="id_shop_group"]').val());
  xlsxData.append('format_file', $('.format_file').val());
  xlsxData.append('delimiter_val', $('.delimiter_val').val());
  xlsxData.append('import_type_val', $('.import_type_val').val());
  xlsxData.append('parser_import_val', $('.parser_import_val').val());

  $.ajax({
    url: '../modules/simpleimportproduct/send.php',
    type: 'post',
    data: xlsxData,
    dataType: 'json',
    processData: false,
    contentType: false,
    beforeSend: function(){
      $("body").append('<div class="progres_bar_ex"><div class="loading"><div></div></div></div>');
    },
    success: function(json) {
      $('.alert, .alert-danger, .alert-success').remove();
      if (json['error']) {
        $('.progres_bar_ex').remove();
        $('.panel-heading-gomakoil').before('<div class="alert alert-danger">' +  json['error'] + '</div>');
        $(document).scrollTop(0);
      }
      else if(json['page']){
        location.href = $('#location_href').val()+'&step=2';
      }
    }
  });
}

function saveSettings(){
  $('input[name="settings_save"]').css('border-color', '#ccc');
  var id_shop = $('input[name="id_shop"]').val();
  var settings_save = $('input[name="settings_save"]').val();
  var key_settings = $('input[name="key_settings"]').val();

  var data = '';
  if(!settings_save){
    $('input[name="settings_save"]').css('border-color', 'red');
    return false;
  }
  $.each($('.simpleimportproduct #fieldset_0 .form-group select').serializeArray(), function( key, value){
    if(value.name !== 'category'){
      data += '&field['+value.name +']=' + value.value;
    }
  });
  $.each($('.simpleimportproduct #fieldset_1_1 .form-group select').serializeArray(), function( key, value){
    if(value.name !== 'category'){
      data += '&field['+value.name +']=' + value.value;
    }
  });
  $.each($('.simpleimportproduct #fieldset_0 .form-group .one_category_block'), function(i){
    $.each($(this).find('.one_subcategory'), function(j){
      data += '&field_category['+i+'][]='+ $(this).find('#category').val();
    });
  });
  if($('input[name="has_combination"]:checked').val() == true){
    $.each($('.simpleimportproduct #fieldset_3_3'), function(i){
      $.each($(this).find('.form-group'), function(j){
        data += '&field_combinations['+i+']['+$(this).find('select').attr('name')+']='+ $(this).find('select').val();
      });
    });
  }
  if($('input[name="has_discount"]:checked').val() == true){
    $.each($('.simpleimportproduct #fieldset_5_5'), function(i){
      $.each($(this).find('.form-group'), function(j){
        data += '&field_discount['+i+']['+$(this).find('select').attr('name')+']='+ $(this).find('select').val();
      });
    });
  }
  if($('input[name="has_featured"]:checked').val() == true){
    $.each($('.simpleimportproduct #fieldset_7_7'), function(i){
      $.each($(this).find('.form-group'), function(j){
        data += '&field_featured['+i+']['+$(this).find('select').attr('name')+']='+ $(this).find('select').val();
      });
    });
  }
  data += '&id_shop='+id_shop;
  data += '&name_save='+settings_save;
  data += '&key_settings='+key_settings;
  data += '&id_shop_group='+$('input[name="id_shop_group"]').val();

  $.ajax({
    url: '../modules/simpleimportproduct/send.php',
    type: 'post',
    data: 'save=true'+data,
    dataType: 'json',
    beforeSend: function(){
      $("body").append('<div class="progres_bar_ex"><div class="loading"><div></div></div></div>');
    },
    success: function(json) {
      $('.alert, .alert-danger, .alert-success').remove();
      if (json['error']) {
        $(document).scrollTop(0);
        $('.panel-heading-gomakoil').before('<div class="alert alert-danger">' +  json['error'] + '</div>');
      }
      else {
        if (json['success']) {
          $('.panel-heading-gomakoil').before('<div class="alert alert-danger">' +  json['success'] + '</div>');
          console.log(json['count_settings']);
          location.href = $('#location_href').val()+'&step=2&save='+json['count_settings'];
        }
      }
    }
  });
}
function removeSettings(key){

  var id_shop = $('input[name="id_shop"]').val();
  $.ajax({
    url: '../modules/simpleimportproduct/send.php',
    type: 'post',
    data: 'remove=true&key='+key+'&id_shop='+id_shop+'&id_shop_group='+$('input[name="id_shop_group"]').val(),
    dataType: 'json',
    beforeSend: function(){
      $("body").append('<div class="progres_bar_ex"><div class="loading"><div></div></div></div>');
    },
    success: function(json) {
      $('.alert, .alert-danger, .alert-success').remove();
      if (json['error']) {
        $(document).scrollTop(0);
        $(".progres_bar_ex").remove();
        $('.panel-heading-gomakoil').before('<div class="alert alert-danger">' +  json['error'] + '</div>');
      }
      else {
        if (json['success']) {
          $('.panel-heading-gomakoil').before('<div class="alert alert-danger">' +  json['success'] + '</div>');
          location.href = $('#location_href').val()+'&step=2';
        }
      }
    }
  });
}

function importProducts(){

  var id_shop = $('input[name="id_shop"]').val();
  var data = '';

  $.each($('.simpleimportproduct #fieldset_0 .form-group select').serializeArray(), function( key, value){
    if(value.name !== 'category'){
      data += '&field['+value.name +']=' + value.value;
    }
  });
  $.each($('.simpleimportproduct #fieldset_1_1 .form-group select').serializeArray(), function( key, value){
    if(value.name !== 'category'){
      data += '&field['+value.name +']=' + value.value;
    }
  });
  $.each($('.simpleimportproduct #fieldset_0 .form-group .one_category_block'), function(i){
    $.each($(this).find('.one_subcategory'), function(j){
      data += '&field_category['+i+'][]='+ $(this).find('#category').val();
    });
  });
  if($('input[name="has_combination"]:checked').val() == true){
    $.each($('.simpleimportproduct #fieldset_3_3'), function(i){
      $.each($(this).find('.form-group'), function(j){
        data += '&field_combinations['+i+']['+$(this).find('select').attr('name')+']='+ $(this).find('select').val();
      });
    });
  }
  if($('input[name="has_discount"]:checked').val() == true){
    $.each($('.simpleimportproduct #fieldset_5_5'), function(i){
      $.each($(this).find('.form-group'), function(j){
        data += '&field_discount['+i+']['+$(this).find('select').attr('name')+']='+ $(this).find('select').val();
      });
    });
  }
  if($('input[name="has_featured"]:checked').val() == true){
    $.each($('.simpleimportproduct #fieldset_7_7'), function(i){
      $.each($(this).find('.form-group'), function(j){
        data += '&field_featured['+i+']['+$(this).find('select').attr('name')+']='+ $(this).find('select').val();
      });
    });
  }
  data += '&id_shop='+id_shop;
  data += '&id_shop_group='+$('input[name="id_shop_group"]').val();

  var refreshIntervalId = setInterval(function(){ returnAddProducts(id_shop); }, 3000);

  $.ajax({
    url: '../modules/simpleimportproduct/send.php',
    type: 'post',
    data: 'import=true'+data,
    dataType: 'json',
    beforeSend: function(){
      $("body").append('<div class="progres_bar_ex"><div class="loading" style="left: 45%;"><div><div class="import_products_true" style="background: none; width: 400px"></div></div></div></div>');
    },
    complete: function(){
      $(".progres_bar_ex").remove();
    },
    success: function(json) {
      $('.alert, .alert-danger, .alert-success').remove();
      if (json['error']) {
	     clearInterval(refreshIntervalId);
        $(document).scrollTop(0);
        $('.panel-heading-gomakoil').before('<div class="alert alert-danger">' + json['error'] + '</div>');
      }
      else {
        if (json['success']) {

          var success = json['success'];

          var error_logs = success.error_logs;

          if(error_logs){
            var url = error_logs;
            url = '<a class="error_logs_import" href="'+url+'">error_logs.csv<a>';
          }
          else{
            var url = '';
          }

		   clearInterval(refreshIntervalId);
          $(document).scrollTop(0);
          $('.panel-heading-gomakoil').before('<div class="alert alert-success">' + success.message + url+ '</div>');
        }
      }
    }
  });
}


function returnAddProducts(id_shop){
  $.ajax({
    url: '../modules/simpleimportproduct/send.php',
    type: 'post',
    data: 'returnCount=true&id_shop='+id_shop+'&id_shop_group='+$('input[name="id_shop_group"]').val(),
    dataType: 'json',
    success: function(json) {
      if (json['count']) {
        $('.import_products_true').html(json['count'])
      }
    }
  });
}


