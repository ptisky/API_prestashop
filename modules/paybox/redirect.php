<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
include(dirname(__FILE__).'/paybox.php');

$paybox = new paybox();
echo $paybox->l('Connexion....');
$cart = new Cart((int)($cookie->id_cart));
$address = new Address((int)($cart->id_address_delivery));
$customer = new Customer((int)($cart->id_customer));
$country = new Country((int)($address->id_country));
$currency_order = new Currency((int)($cart->id_currency));
$currency_module = $paybox->getCurrency((int)($cart->id_currency));

if (!Validate::isLoadedObject($address) OR !Validate::isLoadedObject($customer))
	die($paybox->l('invalid address or customer'));

$paybox->shellPayment($cookie);

?>
