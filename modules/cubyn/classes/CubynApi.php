<?php
/**
 * 2015-2016 Cubyn
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Cubyn <modules@cubyn.com>
 * @copyright 2015-2016 Cubyn
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require _PS_MODULE_DIR_.'/cubyn/endpoint.php';
require _PS_MODULE_DIR_.'/cubyn/classes/CubynParcel.php';
require _PS_MODULE_DIR_.'/cubyn/classes/CubynAddress.php';

class CubynApi
{
    const DISABLED_STATE = 9999;

    protected $parcels;
    protected $context;

    public function __construct()
    {
    }

    public static function getUserAdress()
    {
        $response = self::request('GET', '/users');
        if ($response->error) {
            return $response;
        }
        $user = array_pop($response->body);
        return $user->address;
    }

    public static function getCurrentCollect()
    {
        $response = self::request('GET', '/collects?status=CREATED,CONFIRMED&limit=1');
        if ($response->error) {
            return $response;
        }
        $collect = array_pop($response->body);
        return $collect;
    }

    public static function export($order_id)
    {
        $order   = new Order($order_id);
        $parcel  = new CubynParcel($order->id, self::getCarriers());
        $payload = array(
            'label'            => $parcel->getLabel(),
            'firstName'        => $parcel->getFirstName(),
            'lastName'         => $parcel->getLastName(),
            'organizationName' => $parcel->getOrganizationName(),
            'address'          => $parcel->getAddress()->toArray(),
            'deliveryMode'     => $parcel->getDeliveryMode(),
            'deliverySigned'   => $parcel->getDeliverySigned(),
            'objectCount'      => $parcel->getObjectCount(),
            'phone'            => $parcel->getPhone(),
            'description'      => $parcel->getDescription(),
            'email'            => $parcel->getEmail(),
            'value'            => $parcel->getValue(),
            'orderRef'         => $parcel->getOrderRef(),
        );

        if ($order->id_carrier == Configuration::get('CUBYN_CARRIER_RELAIS')) {
            $relay                     = CubynRelayPoint::getByCart((int) $order->id_cart);
            $payload['relayPickupRef'] = $relay['id_relay_point'];
        }

        $response = self::request('POST', '/parcels', $payload);
        if ($response->error) {
            return $response;
        }
        return true;
    }

    public static function checkApiKey($apikey = null)
    {
        $response = self::request('GET', '/', null, $apikey);
        if ($response->error) {
            return false;
        }
        return isset($response->body->auth);
    }

    public static function getTimeSlots()
    {
        $response = self::request('GET', '/timeslots');
        if ($response->error) {
            return $response;
        }
        return $response->body;
    }

    public static function collect($pickupAt, $messengerType)
    {
        $payload = array(
            'pickupAt'      => $pickupAt,
            'messengerType' => $messengerType,
            'adresse'       => self::getUserAdress(),
        );
        $response = self::request('POST', '/collects', $payload);
        if ($response->error) {
            return $response;
        }
        return true;
    }

    public static function getCarriers()
    {
        return array(
            array(
                'reference' => 'CUBYN_CARRIER_COLISSIMO',
                'name'      => 'Cubyn Colissimo',
                'url'       => 'http://www.colissimo.fr/portail_colissimo/suivre.do?colispart=@',
                'delay'     => '2 à 4 jours',
                'api'       => 'colissimo',
                'signed'    => false,
            ),
            array(
                'reference' => 'CUBYN_CARRIER_COLISSIMOCS',
                'name'      => 'Cubyn Colissimo Contre Signature',
                'url'       => 'http://www.colissimo.fr/portail_colissimo/suivre.do?colispart=@',
                'delay'     => '2 à 4 jours',
                'api'       => 'colissimo',
                'signed'    => true,
            ),
            array(
                'reference' => 'CUBYN_CARRIER_EXPRESS',
                'name'      => 'Cubyn Express',
                'url'       => 'http://www.fr.chronopost.com/web/fr/tracking/suivi_inter.jsp?listeNumeros=@',
                'delay'     => '24 heures',
                'api'       => 'express',
                'signed'    => false,
            ),
            array(
                'reference' => 'CUBYN_CARRIER_RELAIS',
                'name'      => 'Cubyn Relais',
                'url'       => 'https://www.colisprive.com/moncolis/pages/detailColis.aspx?numColis=@',
                'delay'     => '3 à 4 jours',
                'api'       => 'relay',
                'signed'    => false,
            ),
        );
    }

    /**
     * Reads carrier ids from PS Configuration
     * for each Cubyn carrier
     * @return array
     */
    public static function getCarrierIds()
    {
        $ids = array();
        foreach (self::getCarriers() as $carrier) {
            $reference = $carrier['reference'];
            $ids[]     = (int) Configuration::get($reference);
        }
        return $ids;
    }

    private static function request($method, $path, $payload = null, $apiKey = null)
    {
        // concatenate URL
        $req = curl_init(CUBYN_ENDPOINT.$path);

        // POST payload
        $jsonPayload = null;
        if (!is_null($payload)) {
            $jsonPayload = Tools::jsonEncode($payload);
        }

        // HTTP headers
        $headers = array(
            "Content-Type: application/json",
            "X-Client-Name: prestashop",
            "X-Client-Version: ".CUBYN_VERSION,
        );
        $apiKey = $apiKey !== null ? $apiKey : Configuration::get('CUBYN_API_KEY');
        if ($apiKey) {
            $headers[] = "X-Application: $apiKey";
        }
        if ($jsonPayload) {
            $headers[] = "Content-Length: ".Tools::strlen($jsonPayload);
        }
        curl_setopt($req, CURLOPT_HTTPHEADER, $headers);

        // set cURL options
        curl_setopt($req, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($req, CURLOPT_POSTFIELDS, $jsonPayload);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_TIMEOUT, 4);

        $result    = curl_exec($req);
        $status    = (int) curl_getinfo($req, CURLINFO_HTTP_CODE);
        $curlError = curl_error($req);
        curl_close($req);

        if (!$result || $curlError) {
            return (object) array(
                "error"   => true,
                "status"  => $status,
                "message" => $curlError,
                "type"    => "HTTP ERROR",
            );
        }

        if ($result && Tools::strlen($result)) {
            $result = Tools::jsonDecode($result);
        }

        if ($status >= 400) {
            return (object) array(
                "error"   => true,
                "status"  => $status,
                "message" => $result->message,
                "type"    => $result->type,
            );
        }

        return (object) array(
            "error"  => false,
            "status" => $status,
            "body"   => $result,
        );
    }
}
