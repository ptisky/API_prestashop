{**
 * 2015-2016 Cubyn
 * NOTICE OF LICENSE
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 * DISCLAIMER
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * @author    Cubyn <modules@cubyn.com>
 * @copyright 2015-2016 Cubyn
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *}
 {literal}
<script type="text/javascript">
    var cubyn_relai = {/literal}{$cubyn_relai|escape:'htmlall':'UTF-8'}{literal};

    $(function(){
        var relay_point_checked = $('input.delivery_option_radio:checked');
        if (!relay_point_checked || !relay_point_checked.length) return;
        var deliveries = relay_point_checked.val().split(',');

        for (i in deliveries)
        {
            if (deliveries[i] == cubyn_relai)
            {
                relay_point_checked.change();
            }
        }
    });
</script>
{/literal}