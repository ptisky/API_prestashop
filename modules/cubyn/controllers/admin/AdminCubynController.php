<?php
/**
 * 2015-2016 Cubyn
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Cubyn <modules@cubyn.com>
 * @copyright 2015-2016 Cubyn
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require _PS_MODULE_DIR_.'/cubyn/classes/CubynApi.php';
require _PS_MODULE_DIR_.'/cubyn/Model/CubynOrderModel.php';

class AdminCubynController extends ModuleAdminController
{

    protected $statuses_array = array();
    protected $ordercount;
    protected $exportedOrder;
    protected $forceRedirect;

    public function __construct()
    {
        $this->initContext();
        $this->table                = 'cubyn';
        $this->lang                 = false;
        $this->bootstrap            = true;
        $this->meta_title           = $this->l('Export');
        $this->actions              = array('view');
        $this->module               = 'cubyn';
        $this->className            = 'CubynOrderModel';
        $this->forceConfirm         = false;
        $this->bulk_actions['send'] = array(
            'text' => $this->l('Exporter vers Cubyn'),
        );
        $this->context = Context::getContext();
        $statuses      = OrderState::getOrderStates((int) $this->context->language->id);
        $this->_select = 'o.*,osl.`name` AS `osname`';
        $this->_join   = '
        LEFT JOIN '._DB_PREFIX_.'orders o on (o.`id_order` = a.`id_order`)
        LEFT JOIN '._DB_PREFIX_.'order_state os ON (os.`id_order_state` = o.`current_state`)
        LEFT JOIN '._DB_PREFIX_.'order_state_lang osl ON (os.`id_order_state` = osl.`id_order_state` AND osl.`id_lang` = '.(int) $this->context->language->id.')';
        $this->_where = '
        AND a.id_state = "'.(int) Configuration::get('CUBYN_CRT_EXPORT_STATE_NAME_ID').'"
        AND a.exported = "0"
        AND a.shipped = "0"';

        foreach ($statuses as $status) {
            $this->statuses_array[$status['id_order_state']] = $status['name'];
        }

        $this->fields_list = array(
            'id_order'    => array(
                'title' => $this->l('ID'),
                'align' => 'text-center',
            ),
            'reference'   => array(
                'title' => $this->l('Référence'),
                'align' => 'text-center',
            ),
            'id_customer' => array(
                'title'        => $this->l('Customer'),
                'havingFilter' => false,
                'callback'     => 'setCustomerName',
            ),
            'osname'      => array(
                'title' => $this->l('Statut'),
                'align' => 'text-left',
            ),
            'total_paid'  => array(
                'title'    => $this->l('Total'),
                'align'    => 'text-center',
                'callback' => 'setOrderCurrency',
            ),
            'id_carrier'  => array(
                'title'    => $this->l('Transporteur'),
                'align'    => 'center',
                'callback' => 'setCarrierName',
            ),
        );
        parent::__construct();
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addJS('/modules/cubyn/views/js/custom.js', 'all');
    }

    public function setOrderCurrency($echo, $tr)
    {
        $order  = new Order($tr['id_order']);
        $parcel = new CubynParcel($tr['id_order'], CubynApi::getCarriers());
        return Tools::displayPrice($echo, (int) $order->id_currency);
    }

    public function setCarrierName($echo, $tr)
    {
        $carrier = new Carrier($tr['id_carrier'], (int) $this->context->language->id);
        return $carrier->name.' ('.$carrier->delay.')';
    }

    public function setCustomerName($echo, $tr)
    {
        $customer = new Customer($tr['id_customer']);
        if ($customer->firstname || $customer->lastname) {
            return $customer->firstname.' '.$customer->lastname;
        } else {
            return $tr['id_customer'];
        }
    }

    public function displayExportLink($token, $id)
    {
        $tpl = $this->createTemplate('list_action_export.tpl');
        $tpl->assign(array(
            'href'   => "index.php?controller=AdminCubyn&exportOrder".$this->table."=true&id_cubyn=".$id."&token=".Tools::getAdminTokenLite("AdminCubyn"),
            'action' => $this->l('Export'),
        ));
        return $tpl->fetch();
    }

    public function renderList()
    {
        $this->checkKey();
        if (!$this->checkCollect()) {
            return false;
        }
        $this->synchronizeOrders();
        $this->addRowAction('export');
        if (!in_array($this->display, array('collect', 'confirmCollect'))) {
            return parent::renderList();
        }
    }

    public function renderView()
    {
        $id_cubyn   = Tools::getValue('id_cubyn');
        $cubynOrder = new CubynOrderModel($id_cubyn);
        Tools::redirectAdmin("index.php?controller=AdminOrders&id_order=".$cubynOrder->id_order."&vieworder&token=".Tools::getAdminTokenLite("AdminOrders"));
    }

    public function renderCollect()
    {
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Programmer une collecte'),
            ),
            'input'  => array(
                array(
                    'type'     => 'radio',
                    'label'    => $this->l('Choisissez une créneau'),
                    'name'     => 'pickupAt',
                    'required' => true,
                    'class'    => 't',
                    'values'   => $this->getTimeSlots(),
                ),
                array(
                    'type'     => 'radio',
                    'label'    => $this->l('Quelle contenance pour le coursier'),
                    'desc'     => $this->l('2 roues maximun 40x40x40 cm'),
                    'name'     => 'messengerType',
                    'required' => true,
                    'class'    => 't',
                    'values'   => array(
                        array(
                            'id'    => '2-wheeled',
                            'value' => '2-wheeled',
                            'label' => $this->l('2 Roues'),
                        ),
                        array(
                            'id'    => '4-wheeled',
                            'value' => '4-wheeled',
                            'label' => $this->l('4 roues'),
                        ),
                    ),
                ),
            ),
            'submit' => array(
                'name'  => 'confirmcollect',
                'title' => $this->l('Programmer une collecte'),
                'class' => 'btn btn-success',
            ),
        );
        return parent::renderForm();
    }

    public function processBulkSend()
    {
        $cubynOrders  = $this->boxes;
        $exportErrors = array();
        if (!$this->checkCollect()) {
            $this->display = 'list';
            return false;
        }
        if (!is_array($cubynOrders)) {
            $this->errors[] = Tools::displayError($this->l('Vous devez choisir au moins une commande'));
        } else {
            $total = count($cubynOrders);
            foreach ($cubynOrders as $cubynOrder) {
                if (!$this->handleExport($cubynOrder, true)) {
                    $exportErrors[] = $cubynOrder;
                }
            }
            $collectIndication = $this->l("N'oubliez pas de programmer votre collecte sur app.cubyn.com");
            if ((count($cubynOrders) - count($exportErrors)) == 0) {
                $this->errors[] = Tools::displayError($this->l('Aucune commande exportée'));
            } elseif (count($exportErrors) > 0) {
                $str                   = (count($cubynOrders) - count($exportErrors)).' commmandes exportées';
                $this->confirmations[] = $str.' - '.$collectIndication;
            } elseif ($total > 0) {
                $this->confirmations[] = $total.' commmandes exportées - '.$collectIndication;
            }
            $this->display = 'list';
            // $this->display = 'collect';
        }
    }

    public function initContent()
    {
        if ($this->display == 'collect') {
            $this->content .= $this->renderCollect();
        }
        parent::initContent();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitBulksendcubyn')) {
            $this->processBulkSend();
        }
        if (Tools::isSubmit('quickCollect')) {
            $this->display = 'collect';
        }
        if (Tools::isSubmit('confirmcollect')) {
            if ($this->handleCollect()) {
                $this->display = 'confirmCollect';
                $this->context = Context::getContext();
                if (version_compare(_PS_VERSION_, '1.6.0.1', '<')) {
                    $this->setTemplate('ConfirmCollect.tpl');
                } else {
                    $this->setTemplate('ConfirmCollect_bt.tpl');
                }
                $availableTimeSlots = Tools::jsonDecode($this->context->cookie->availableTimeSlots);
                $timeSlot           = $this->getSelectedTimeSlot(Tools::getValue('pickupAt'), $availableTimeSlots);
                $this->content .= $this->createTemplate('ConfirmCollect.tpl')->fetch();
                $this->context->smarty->assign('exportedOrders', CubynOrderModel::exportedOrders());
                $this->context->smarty->assign('timeSlotLabel', $this->generateLabel($timeSlot));
                $this->context->smarty->assign('pickUpAdress', CubynApi::getUserAdress());
                $this->context->smarty->assign('href', 'index.php?controller=AdminCubyn&token='.Tools::getAdminTokenLite('AdminCubyn'));
                parent::renderView();
            } else {
                $this->display = 'collect';
            }
        }
        if (Tools::getValue('exportOrder'.$this->table)) {
            if ($this->handleExport((int) (Tools::getValue('id_cubyn')))) {
                if (!$this->checkCollect()) {
                    $this->display = 'list';
                    return false;
                }
                $this->confirmations[] = $this->l('1 commande exportée');
                // $this->display = 'collect';
                $this->display = 'list';
            } else {
                $this->display = 'list';
            }
        }
    }

    private function handleExport($id_cubyn, $bulk = false)
    {
        // check order was not already exported
        if (CubynOrderModel::alreadyExported($id_cubyn)) {
            $this->errors[] = Tools::displayError($this->l('Cette commande a déjà été exportée'));
            return false;
        }
        // export it
        $cubynOrder = new CubynOrderModel($id_cubyn);
        $result     = CubynApi::export($cubynOrder->id_order);

        // successful
        if ($result === true) {
            $cubynOrder->exported = true;
            $cubynOrder->save();
            return true;
        }

        // error happened
        if ($result['error'] && $result['status'] == 402) {
            $this->errors[] = Tools::displayError($this->l('Fonds insuffisants. Veuillez alimenter votre compte Cubyn.'));
        } else {
            $this->errors[] = Tools::displayError($this->l($result['message']));
        }
        return false;
    }

    private function handleCollect()
    {
        // check values
        $pickupAt      = Tools::getValue('pickupAt');
        $messengerType = Tools::getValue('messengerType');
        if (!$pickupAt) {
            $this->errors[] = $this->l('Vous devez choisir un créneau');
            return false;
        }
        if (!$messengerType) {
            $this->errors[] = $this->l('Vous devez choisir une type de coursier');
            return false;
        }

        // export it
        $result = CubynApi::collect($pickupAt, $messengerType);

        // successful
        if ($result === true) {
            return true;
        }

        // error happened
        if ($result['error'] && $result['status'] == 402) {
            $this->errors[] = Tools::displayError($this->l('Insufficient funds. Please add some credits to your Cubyn account.'));
        } else {
            $this->errors[] = Tools::displayError($this->l($result['message']));
        }
        $this->action  = 'collect';
        $this->display = 'collect';
        return false;
    }

    private function checkKey()
    {
        if (Tools::isEmpty(Configuration::get('CUBYN_API_KEY')) || !CubynApi::checkApiKey()) {
            Tools::redirectAdmin('index.php?controller=AdminModules&apiKey=false&configure=cubyn&token='.Tools::getAdminTokenLite('AdminModules'));
        }
    }

    private function checkCollect()
    {
        $collect = CubynApi::getCurrentCollect();
        // we can edit a current collect if there is
        // none or one still in created mode
        $locked = $collect && $collect->status !== 'CREATED';
        if ($locked) {
            $this->errors[] = Tools::displayError($this->l("Vous avez une collecte en cours. Aucune modification n'est possible pendant le créneau de collecte."));
        }
        return !$locked;
    }

    private function getTimeSlots()
    {
        $availableTimeSlots = CubynApi::getTimeSlots();
        if ($availableTimeSlots['error']) {
            return array();
        }

        $this->context->cookie->availableTimeSlots = Tools::jsonEncode($availableTimeSlots);
        $timeSlots                                 = array();
        foreach ($availableTimeSlots as $key => $timeSlot) {
            $slot = array(
                'id'    => 'timeSlot-'.$key,
                'label' => $this->generateLabel($timeSlot),
                'value' => $timeSlot->date,
            );
            array_push($timeSlots, $slot);
        }
        return $timeSlots;
    }

    private function generateLabel($timeSlot)
    {
        $slot = new DateTime($timeSlot->date);
        $now  = new DateTime();
        $slot->modify('+2 hours');
        if ($timeSlot->asap) {
            $label = $this->l('Dés que possible');
        } elseif ($now->format('Y-m-d') == $slot->format('Y-m-d')) {
            $label = $this->l('Aujourd\'hui');
        } elseif ($now->modify('+1 day')->format('Y-m-d') == $slot->format('Y-m-d')) {
            $label = $this->l('Demain');
        } else {
            setlocale(LC_TIME, 'fr_FR', 'fra');
            $label = $this->l(sprintf('le %1$d %2$s', $slot->format('d'), strftime('%B')));
        }
        $duration = sprintf('+ %d minutes', $timeSlot->duration);
        $starTime = $slot->format('H:i');
        $endTime  = $slot->modify($duration)->format('H:i');
        return $this->l(sprintf('%1$s entre %2$s et %3$s', $label, $starTime, $endTime));
    }

    private function getSelectedTimeSlot($selected, $available)
    {
        foreach ($available as $timeSlot) {
            if (new DateTime($selected) == new DateTime($timeSlot->date)) {
                return $timeSlot;
            }
        }
    }

    private function initContext()
    {
        $this->ajax    = true;
        $this->context = Context::getContext();
    }

    /**
     * This synchronization is crucial to have table cubyn correctly
     * mirror the orders.
     * Scenarios handled correctly so far :
     * — first sync on an order (will create an sync entry into cubyn table)
     * — sync entry created but order's status changed in the meantime
     *     (example : by an external ERP hitting the DB directly)
     */
    private function synchronizeOrders()
    {
        $exportState = (int) Configuration::get('CUBYN_CRT_EXPORT_STATE_NAME_ID');
        $carrierIds  = CubynApi::getCarrierIds();
        $sql         = "SELECT o.* FROM  "._DB_PREFIX_."orders o
            LEFT JOIN "._DB_PREFIX_."cubyn c ON c.id_order = o.id_order
            WHERE o.id_carrier IN (".implode(',', $carrierIds).")
                AND (c.exported IS NULL OR c.exported <> 1)
                AND (o.current_state <> c.id_state OR o.current_state = ".$exportState.")";
        $orders = Db::getInstance()->executeS($sql, true);
        foreach ($orders as $order) {
            $exists = CubynOrderModel::findByOrderId($order['id_order']);
            $order  = new Order($order['id_order']);
            // find or create
            if ($exists) {
                $cubynOrder = new CubynOrderModel($exists['id_cubyn']);
                if ($cubynOrder->exported) {
                    continue;
                }
            } else {
                $cubynOrder = new CubynOrderModel();
            }
            // mirror order item perfectly
            $cubynOrder->id_order   = $order->id;
            $cubynOrder->id_carrier = $order->id_carrier;
            $cubynOrder->id_state   = $order->getCurrentState();
            $cubynOrder->exported   = false;
            $cubynOrder->shipped    = false;
            $cubynOrder->save();
        }
    }
}
