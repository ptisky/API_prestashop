{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{* Assign a value to 'current_step' to display current style *}
{capture name="url_back"}
{if isset($back) && $back}back={$back}{/if}
{/capture}

{if !isset($multi_shipping)}
	{assign var='multi_shipping' value='0'}
{/if}

{if !$opc}
<!-- Steps -->
<div class="steppc">
<ul class="step" id="order_step">
	<li class="{if $current_step=='summary'}step_current{else}{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address' || $current_step=='login'}step_done{else}step_todo{/if}{/if}">
		{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address' || $current_step=='login'}
		<div class="img-pan"></div>
		<a href="{$link->getPageLink('order', true)}">
			
			<span>{l s='Summary'}</span>
		</a>
	
		{else}
			<div class="img-pan"></div>
			<span> {l s='Summary'}</span>
		{/if}
	</li>
	<li class="{if $current_step=='login'}step_current{else}{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address'}step_done{else}step_todo{/if}{/if}">
		{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address'}
		<div class="img-iden"></div> 
		<a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=1&multi-shipping={$multi_shipping}")|escape:'html'}">
			
			<span>{l s='Login'}</span>
		</a>
		{else}
			<div class="img-iden"></div>
			<span> {l s='Login'}</span>
		{/if}
	</li>
	<li class="{if $current_step=='address'}step_current{else}{if $current_step=='payment' || $current_step=='shipping'}step_done{else}step_todo{/if}{/if}">
		{if $current_step=='payment' || $current_step=='shipping'}
		<div class="img-adrress"></div>
		<a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=1&multi-shipping={$multi_shipping}")|escape:'html'}">
			 
			 <span>{l s='Address'}</span>
		</a>
		{else}
			<div class="img-adrress"></div>
			<span> {l s='Address'}</span>
		{/if}
	</li>
	<li class="{if $current_step=='shipping'}step_current{else}{if $current_step=='payment'}step_done{else}step_todo{/if}{/if}">
		{if $current_step=='payment'}
		 <div class="img-liv"></div>
        
		<a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=2&multi-shipping={$multi_shipping}")|escape:'html'}">
			
			 <span>{l s='Shipping'}</span>
		</a>
		{else}
			<div class="img-liv"></div>
			<span> {l s='Shipping'}</span>
		{/if}
	</li>
	<li id="step_end" class="{if $current_step=='payment'}step_current_end{else}step_todo{/if}">
		<div class="img-pai"></div>
		<span> {l s='Payment'}</span>
	</li>
</ul>
</div>
<!-- /Steps -->

<!-- Steps mobile -->
<div class="stepmob">

<ul class="step" id="order_step">
	<li class="{if $current_step=='summary'}step_current{else}{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address' || $current_step=='login'}step_done{else}step_todo{/if}{/if}">
		{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address' || $current_step=='login'}
		
		
		<a href="{$link->getPageLink('order', true)}">
			
		<i class="flaticon-grocery6 {if $current_step=='summary'}step_currenti{else}{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address' || $current_step=='login'}step_donei{else}step_todoi{/if}{/if}"></i> 
		</a>
	
		{else}
			
			<i class="flaticon-grocery6 {if $current_step=='summary'}step_currenti{else}{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address' || $current_step=='login'}step_donei{else}step_todoi{/if}{/if}"></i> 
		
		{/if}
	</li>
	<li class="{if $current_step=='login'}step_current{else}{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address'}step_done{else}step_todo{/if}{/if}">
		{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address'}
		
		<a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=1&multi-shipping={$multi_shipping}")|escape:'html'}">
			
			<i class="flaticon-male12 {if $current_step=='login'}step_currenti{else}{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address'}step_donei{else}step_todoi{/if}{/if}"></i> 
		</a>
		{else}
			<i class="flaticon-male12 {if $current_step=='login'}step_currenti{else}{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address'}step_donei{else}step_todoi{/if}{/if}"></i> 
		{/if}
	</li>
	<li class="{if $current_step=='address'}step_current{else}{if $current_step=='payment' || $current_step=='shipping'}step_done{else}step_todo{/if}{/if}">
		{if $current_step=='payment' || $current_step=='shipping'}
		
		<a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=1&multi-shipping={$multi_shipping}")|escape:'html'}">
			 
			<i class="flaticon-black218 {if $current_step=='address'}step_currenti{else}{if $current_step=='payment' || $current_step=='shipping'}step_donei{else}step_todoi{/if}{/if}"></i> 
		</a>
		{else}
		<i class="flaticon-black218 {if $current_step=='address'}step_currenti{else}{if $current_step=='payment' || $current_step=='shipping'}step_donei{else}step_todoi{/if}{/if}"></i> 
		{/if}
	</li>
	<li class="{if $current_step=='shipping'}step_current{else}{if $current_step=='payment'}step_done{else}step_todo{/if}{/if}">
		{if $current_step=='payment'}
	
        
		<a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=2&multi-shipping={$multi_shipping}")|escape:'html'}">
			
			<i class="flaticon-logistics3 {if $current_step=='shipping'}step_currenti{else}{if $current_step=='payment'}step_donei{else}step_todoi{/if}{/if}"></i> 
		</a>
		{else}
				<i class="flaticon-logistics3 {if $current_step=='shipping'}step_currenti{else}{if $current_step=='payment'}step_donei{else}step_todoi{/if}{/if}"></i>
		{/if}
	</li>
	<li id="step_end" class="{if $current_step=='payment'}step_current_end{else}step_todo{/if}">
	<i class="flaticon-ssl {if $current_step=='payment'}step_current_endi{else}step_todoi{/if}"></i> 
	</li>
</ul>
</div>
{/if}
