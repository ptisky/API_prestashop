<?php
/**
  * Paybox class, paybox.php
  * Accept Payment by credit card with Paybox
  * @category modules
  *
  * @author Magavenue <contact@magavenue.com>
  * @copyright Magavenue
  * @license http://www.opensource.org/licenses/osl-3.0.php Open-source licence 3.0
  * @version 1.5
  * @note  You have first to install the Paybox cgi module
  */
class Paybox extends PaymentModule {
	private	$_html = '';
	private $_postErrors = array();

	public function __construct(){
		$this->name = 'paybox';
		$this->author = 'MagAvenue';
		
        if(_PS_VERSION_ < '1.4')
            $this->tab = 'Payment';
		else
            $this->tab = 'payments_gateways';

		$this->version = '10.5';

		$this->currencies = true;
		$this->currencies_mode = 'radio';

        parent::__construct();

	    /* The parent construct is required for translations */
		$this->page = basename(__FILE__, '.php');
        $this->displayName = $this->l('CB with Paybox');
        $this->description = $this->l('Accepts payments by PayBox (Magavenue)');
		$this->confirmUninstall = $this->l('Are you sure you want to delete your details ?');

		$this->orderPaymentName = $this->l('Credit card');
	}

	public function install(){
		if (!parent::install() 
            OR !Configuration::updateValue('PAYBOX_SITE', '0000000')
			OR !Configuration::updateValue('PAYBOX_RANG', '00')
			OR !Configuration::updateValue('PAYBOX_IDENTIFIANT', '0')
			OR !Configuration::updateValue('PAYBOX_URLHTTP', '0')
			OR !Configuration::updateValue('PAYBOX_TESTMOD', '0')
			OR !Configuration::updateValue('PAYBOX_CHEMIN', '')
			OR !Configuration::updateValue('PAYBOX_3X', '')
			OR !Configuration::updateValue('PAYBOX_DAYS', 28)
			OR !Configuration::updateValue('PAYBOX_MIN', '')
			OR !Configuration::updateValue('PAYBOX_3D', '0')
			OR !$this->registerHook('paymentReturn')
			OR !$this->registerHook('payment'))
			return false;
		return true;
	}

	public function hookPaymentReturn($params)
	{
		if (!$this->active)
			return ;

		return $this->display(__FILE__, 'confirmation.tpl');
	}

	public function uninstall(){
		if (!Configuration::deleteByName('PAYBOX_SITE')
			OR !Configuration::deleteByName('PAYBOX_RANG')
			OR !Configuration::deleteByName('PAYBOX_IDENTIFIANT')
			OR !Configuration::deleteByName('PAYBOX_URLHTTP')
			OR !Configuration::deleteByName('PAYBOX_TESTMOD')
			OR !Configuration::deleteByName('PAYBOX_CHEMIN')
			OR !Configuration::deleteByName('PAYBOX_3X')
			OR !Configuration::deleteByName('PAYBOX_DAYS')
			OR !Configuration::deleteByName('PAYBOX_MIN')
			OR !Configuration::deleteByName('PAYBOX_3D')
			OR !parent::uninstall())
			return false;
		return true;
	}

	public function getContent(){
		$this->_html = '<h2>Paybox</h2>';
		if (isset($_POST['submitPaybox']))
		{
			if (empty($_POST['pbx_site']))
				$this->_postErrors[] = $this->l('Paybox site required');
			if (empty($_POST['pbx_rang']))
				$this->_postErrors[] = $this->l('Paybox rang required');
			if (empty($_POST['pbx_identifiant']))
				$this->_postErrors[] = $this->l('Paybox identifiant required');
			if (empty($_POST['pbx_urlhttp']))
				$_POST['pbx_urlhttp'] = 0;
                        if (empty($_POST['pbx_chemin']))
				$this->_postErrors[] = $this->l('Paybox cgi module path required');
			if (empty($_POST['pbx_testmod']))
				$_POST['pbx_testmod'] = 0;

			if (!sizeof($this->_postErrors))
			{
				Configuration::updateValue('PAYBOX_SITE',$_POST['pbx_site'] ) ;
				Configuration::updateValue('PAYBOX_RANG', $_POST['pbx_rang']);
				Configuration::updateValue('PAYBOX_IDENTIFIANT', $_POST['pbx_identifiant']);
				Configuration::updateValue('PAYBOX_URLHTTP', $_POST['pbx_urlhttp']);
                Configuration::updateValue('PAYBOX_TESTMOD', $_POST['pbx_testmod']);
                Configuration::updateValue('PAYBOX_FORM', $_POST['pbx_form']);
                Configuration::updateValue('PAYBOX_CHEMIN', $_POST['pbx_chemin']);
                Configuration::updateValue('PAYBOX_3X', $_POST['PAYBOX_3X']);
                Configuration::updateValue('PAYBOX_MIN', $_POST['PAYBOX_MIN']);
                Configuration::updateValue('PAYBOX_DAYS', $_POST['PAYBOX_DAYS']);
                Configuration::updateValue('PAYBOX_3D', $_POST['PAYBOX_3D']);
				Configuration::updateValue('PAYBOX_HMAC', $_POST['PAYBOX_HMAC']);
				
				$this->displayConf();
			}
			else
				$this->displayErrors();
		}

		$this->displayPaybox();
		$this->displayFormSettings();
		return $this->_html;
	}

	public function displayConf(){
		$this->_html .= '
		<div class="conf confirm">
			<img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />
			'.$this->l('Settings updated').'
		</div>';
	}

	public function displayErrors(){
		$nbErrors = sizeof($this->_postErrors);
		$this->_html .= '
		<div class="alert error">
			<h3>'.($nbErrors > 1 ? $this->l('There are') : $this->l('There is')).' '.$nbErrors.' '.($nbErrors > 1 ? $this->l('errors') : $this->l('error')).'</h3>
			<ol>';
		foreach ($this->_postErrors AS $error)
			$this->_html .= '<li>'.$error.'</li>';
		$this->_html .= '
			</ol>
		</div>';
	}


	public function displayPaybox(){
		$this->_html .= '
		<img src="../modules/paybox/paiement-securise.png" style="float:left; margin-right:15px;" />
		<b>'.$this->l('This module allows you to accept payments by Paybox.').'</b><br /><br />
		<br /><br /><br />';
	}

	public function displayFormSettings(){
		$conf = Configuration::getMultiple(array('PAYBOX_HMAC','PAYBOX_3D', 'PAYBOX_DAYS', 'PAYBOX_MIN', 'PAYBOX_3X', 'PAYBOX_SITE', 'PAYBOX_RANG', 'PAYBOX_IDENTIFIANT','PAYBOX_URLHTTP', 'PAYBOX_TESTMOD', 'PAYBOX_CHEMIN', 'PAYBOX_FORM'));
		$pbx_site = array_key_exists('pbx_site', $_POST) ? $_POST['pbx_site'] : (array_key_exists('PAYBOX_SITE', $conf) ? $conf['PAYBOX_SITE'] : '');
		$pbx_rang = array_key_exists('pbx_rang', $_POST) ? $_POST['pbx_rang'] : (array_key_exists('PAYBOX_RANG', $conf) ? $conf['PAYBOX_RANG'] : '');
		$pbx_identifiant = array_key_exists('pbx_identifiant', $_POST) ? $_POST['pbx_identifiant'] : (array_key_exists('PAYBOX_IDENTIFIANT', $conf) ? $conf['PAYBOX_IDENTIFIANT'] : '');
		$pbx_urlhttp = array_key_exists('pbx_urlhttp', $_POST) ? $_POST['pbx_urlhttp'] : (array_key_exists('PAYBOX_URLHTTP', $conf) ? $conf['PAYBOX_URLHTTP'] : '');
		$pbx_testmod = array_key_exists('pbx_testmod', $_POST) ? $_POST['pbx_testmod'] : (array_key_exists('PAYBOX_TESTMOD', $conf) ? $conf['PAYBOX_TESTMOD'] : '');
		$pbx_form = array_key_exists('pbx_form', $_POST) ? $_POST['pbx_form'] : (array_key_exists('PAYBOX_FORM', $conf) ? $conf['PAYBOX_FORM'] : '');
        $pbx_chemin = array_key_exists('pbx_chemin', $_POST) ? $_POST['pbx_chemin'] : (array_key_exists('PAYBOX_CHEMIN', $conf) ? $conf['PAYBOX_CHEMIN'] : '');
        $PAYBOX_MIN = array_key_exists('PAYBOX_MIN', $_POST) ? $_POST['PAYBOX_MIN'] : (array_key_exists('PAYBOX_MIN', $conf) ? $conf['PAYBOX_MIN'] : '');
        $PAYBOX_3X = array_key_exists('PAYBOX_3X', $_POST) ? $_POST['PAYBOX_3X'] : (array_key_exists('PAYBOX_3X', $conf) ? $conf['PAYBOX_3X'] : '');
        $PAYBOX_DAYS = array_key_exists('PAYBOX_DAYS', $_POST) ? $_POST['PAYBOX_DAYS'] : (array_key_exists('PAYBOX_DAYS', $conf) ? $conf['PAYBOX_DAYS'] : '');
        $PAYBOX_3D = array_key_exists('PAYBOX_3D', $_POST) ? $_POST['PAYBOX_3D'] : (array_key_exists('PAYBOX_3D', $conf) ? $conf['PAYBOX_3D'] : '');
		$PAYBOX_HMAC = array_key_exists('PAYBOX_HMAC', $_POST) ? $_POST['PAYBOX_HMAC'] : (array_key_exists('PAYBOX_HMAC', $conf) ? $conf['PAYBOX_HMAC'] : '');


		$this->_html .= '
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
		<fieldset>
			<legend><img src="../img/admin/contact.gif" />'.$this->l('Settings').'</legend>
			<label>'.$this->l('Paybox site value').'</label>
			<div class="margin-form"><input type="text" size="33" name="pbx_site" value="'.htmlentities($pbx_site, ENT_COMPAT, 'UTF-8').'" /></div>
			<label>'.$this->l('Paybox rang value').'</label>
			<div class="margin-form"><input type="text" size="33" name="pbx_rang" value="'.htmlentities($pbx_rang, ENT_COMPAT, 'UTF-8').'" /></div>
			<label>'.$this->l('Paybox identifiant value').'</label>
			<div class="margin-form"><input type="text" size="33" name="pbx_identifiant" value="'.htmlentities($pbx_identifiant, ENT_COMPAT, 'UTF-8').'" /></div>
			<label>' . $this -> l('Paybox HMAC') . '</label>
			<div class="margin-form"><input type="text" name="PAYBOX_HMAC" value="' . htmlentities($PAYBOX_HMAC, ENT_COMPAT, 'UTF-8') . '" /></div>			
			<label>'.$this->l('Absolut path value').'</label>
			<div class="margin-form"><input type="text" size="33" name="pbx_chemin" value="'.pSQL($pbx_chemin).'" /></div>
			<label>'.$this->l('Url http mode').'</label>
			<div class="margin-form">
				<input type="radio" name="pbx_urlhttp" value="1" '.($pbx_urlhttp ? 'checked="checked"' : '').' /> '.$this->l('Yes').'
				<input type="radio" name="pbx_urlhttp" value="0" '.(!$pbx_urlhttp ? 'checked="checked"' : '').' /> '.$this->l('No').'
			</div>
                        
            <label>'.$this->l('Test mode').'</label>
			<div class="margin-form">
				<input type="radio" name="pbx_testmod" value="1" '.($pbx_testmod ? 'checked="checked"' : '').' /> '.$this->l('Yes').'
				<input type="radio" name="pbx_testmod" value="0" '.(!$pbx_testmod ? 'checked="checked"' : '').' /> '.$this->l('No').'
			</div>
                        
            <label>'.$this->l('Formulaire').'</label>
			<div class="margin-form">
				<input type="radio" name="pbx_form" value="1" '.($pbx_form ? 'checked="checked"' : '').' /> '.$this->l('Yes').'
				<input type="radio" name="pbx_form" value="0" '.(!$pbx_form ? 'checked="checked"' : '').' /> '.$this->l('No').'
			</div>
			
            <label>'.$this->l('Montant minimum 3D Secure').'</label>
			<div class="margin-form">
				<input type="text" size="33" name="PAYBOX_3D" value="'.pSQL($PAYBOX_3D).'" />
			</div>
			
            <label>'.$this->l('Paiement en 3x').'</label>
			<div class="margin-form">
				<input type="radio" name="PAYBOX_3X" value="1" '.($PAYBOX_3X ? 'checked="checked"' : '').' /> '.$this->l('Yes').'
				<input type="radio" name="PAYBOX_3X" value="0" '.(!$PAYBOX_3X ? 'checked="checked"' : '').' /> '.$this->l('No').'
			</div>
                        
            <label>'.$this->l('Minimum paiement en 3x').'</label>
			<div class="margin-form">
                <input style="width: 150px;" type="text" name="PAYBOX_MIN" value="'.$PAYBOX_MIN.'" />
                <p class="clear">'.$this->l('The minimum amount to activate the multiple payment (format : 10.00)').'</p>
			</div>

            <label>'.$this->l('Nombre de jours entre les paiements').'</label>
			<div class="margin-form">
                <input style="width: 150px;" type="text" name="PAYBOX_DAYS" value="'.$PAYBOX_DAYS.'" />
                <p class="clear">'.$this->l('Number of days between multiple payments').'</p>
			</div>
                        
			<br /><center><input type="submit" name="submitPaybox" value="'.$this->l('Update settings').'" class="button" /></center>
		</fieldset>
		</form><br /><br />
		<fieldset class="width3">
			<legend><img src="../img/admin/warning.gif" />'.$this->l('Informations :').'</legend>
			'.$this->l('Informations').'<br /> Racine : '. getenv("DOCUMENT_ROOT").'<br /><br />
		</fieldset>';
	}

	public function hookPayment($params){
		global $smarty;

		if(intval(Tools::getValue('paybox')))
			$this->shellPayment($params);

        $order_total = $params['cart']->getOrderTotal(true, 3);
        if(Configuration::get('PAYBOX_3X') && $order_total >= floatval(Configuration::get('PAYBOX_MIN')))
                $smarty->assign('paybox3x', 'true');
		else
                $smarty->assign('paybox3x', 'false');
                
		return $this->display(__FILE__, 'paybox.tpl');
    }

    public function shellPayment($cookie){
        $cart = new Cart(intval($cookie->id_cart));
        $customer = new Customer(intval($cart->id_customer));
		$pbx_site = Configuration::get('PAYBOX_SITE');
		$pbx_rang = Configuration::get('PAYBOX_RANG');
		$pbx_identifiant = Configuration::get('PAYBOX_IDENTIFIANT');
		$pbx_urlhttp = Configuration::get('PAYBOX_URLHTTP');
		$link = new Link();

		$threeDSecure = '';//true
		$minimum3DSecure = floatval(Configuration::get('PAYBOX_3D'));

		if($cart->getOrderTotal(true, 3) < $minimum3DSecure)
			$threeDSecure = 'PBX_3DS=N ';//false

		$order_total = number_format($cart->getOrderTotal(true, 3), 2,'', '');
		$id_cart = $cart->id;

        // if((int)$cart->id_currency == 2)
            // $pbx_currency = "840";
        // else
            $pbx_currency = "978";

		$PBX_MODE ='4';

        if(_PS_VERSION_ < '1.4') 	              
			$PBX_LANGUE = ($cart->id_lang ==2?'FRA':'GBR');
		else{
		        $language = new Language((int)$cart->id_lang);       
				$PBX_LANGUE = ($language->iso_code =='fr'?'FRA':'GBR');
		}


        $PBX_SITE =$pbx_site;
		$PBX_RANG =$pbx_rang;
		$PBX_IDENTIFIANT =$pbx_identifiant;
		$PBX_TOTAL =$order_total;
		$PBX_DEVISE =$pbx_currency;
		$PBX_CMD =$id_cart;
                
        //paiement en 3 fois
        if(isset($_POST['paybox3x']) && $_POST['paybox3x']==1){
            $PBX_DATE1 = date('d/m/Y', time() + (Configuration::get('PAYBOX_DAYS') * 24 * 60 * 60));
			$PBX_DATE2 = date('d/m/Y', time() + ((Configuration::get('PAYBOX_DAYS')*2) * 24 * 60 * 60));
			$PBX_2MONT1 = intval($PBX_TOTAL / 3);
			$PBX_2MONT2 = intval($PBX_TOTAL - $PBX_2MONT1 - $PBX_2MONT1);
			$PBX_TOTAL = $PBX_2MONT1.' PBX_2MONT1='.$PBX_2MONT1.' PBX_2MONT2='.$PBX_2MONT2.' PBX_DATE1='.$PBX_DATE1.' PBX_DATE2='.$PBX_DATE2;
        }                
                                
		$PBX_PORTEUR =$customer->email;
                
		if(Configuration::get('PAYBOX_FORM') != 1)
            $PBX_RETOUR = 'pbx_amount:M\;ref:R\;pbx_auth:A\;pbx_trans:T\;pbx_error:E\;pbx_ip:I\;pbx_abonnement:B\;pbx_country:Y\;pbx_sign:K';
        else
            $PBX_RETOUR = 'pbx_amount:M;ref:R;pbx_auth:A;pbx_trans:T;pbx_error:E;pbx_ip:I;pbx_abonnement:B;pbx_country:Y;pbx_sign:K';
		
		$PBX_EFFECTUE = 'http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__.'modules/paybox/validation_manuelle.php';
		$PBX_REFUSE = 'http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__.'modules/paybox/error.php';
        if(_PS_VERSION_ < '1.5') 	              
			$PBX_ANNULE = 'http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__.'order.php?step=3';
		else
			$PBX_ANNULE = $link->getPageLink('order.php', false, null, 'step=3');
		
		$PBX_REPONDRE_A = 'http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__.'modules/paybox/validation_auto.php';
		$MOD = Configuration::get('PAYBOX_CHEMIN');

		$PBX = ' PBX_REPONDRE_A='.$PBX_REPONDRE_A.' '.$threeDSecure.'PBX_MODE='.$PBX_MODE.' PBX_LANGUE='.$PBX_LANGUE.' PBX_SITE='.$PBX_SITE.' PBX_RANG='.$PBX_RANG.' PBX_IDENTIFIANT='.$PBX_IDENTIFIANT.' PBX_TOTAL='.$PBX_TOTAL.' PBX_DEVISE='.$PBX_DEVISE.' PBX_CMD='.$PBX_CMD.' PBX_PORTEUR='.$PBX_PORTEUR.' PBX_RETOUR='.$PBX_RETOUR.' PBX_EFFECTUE='.$PBX_EFFECTUE.' PBX_REFUSE='.$PBX_REFUSE.' PBX_ANNULE='.$PBX_ANNULE;

        if(Configuration::get('PAYBOX_TESTMOD') == 1){
            $PBX_PAYBOX = 'https://preprod-tpeweb.paybox.com/cgi/MYchoix_pagepaiement.cgi';
            $PBX_BACKUP1 = 'https://preprod-tpeweb.paybox.com/cgi/MYchoix_pagepaiement.cgi';
            $PBX .= ' PBX_BACKUP1='.$PBX_BACKUP1.' PBX_PAYBOX='.$PBX_PAYBOX;
        }else{
            $PBX_PAYBOX = 'https://tpeweb.paybox.com/cgi/MYchoix_pagepaiement.cgi';
            $PBX_BACKUP1 = 'https://tpeweb1.paybox.com/cgi/MYchoix_pagepaiement.cgi';
            $PBX .= ' PBX_BACKUP1='.$PBX_BACKUP1.' PBX_PAYBOX='.$PBX_PAYBOX;
        }
		
		if(Configuration::get('PAYBOX_HMAC') != ''){
            $PBX_RETOUR = 'pbx_amount:M;ref:R;pbx_auth:A;pbx_trans:T;pbx_error:E;pbx_sign:K';
			$PBX_TIME = date("c");
			$PBX = 
			 'PBX_SITE=' . $PBX_SITE 
			. '&PBX_RANG=' . $PBX_RANG 
			. '&PBX_IDENTIFIANT=' . $PBX_IDENTIFIANT 
			. '&PBX_TOTAL=' . $PBX_TOTAL 
			. '&PBX_DEVISE=' . $PBX_DEVISE 
			. '&PBX_CMD=' . $PBX_CMD 
			. '&PBX_PORTEUR=' . $PBX_PORTEUR 
			. '&PBX_TIME='.$PBX_TIME
			. '&PBX_LANGUE=' . $PBX_LANGUE 
			. '&PBX_MODE=' . $PBX_MODE 
			. '&PBX_REPONDRE_A=' . $PBX_REPONDRE_A 
			. '&PBX_RETOUR=' . $PBX_RETOUR 
			. '&PBX_EFFECTUE=' . $PBX_EFFECTUE 
			. '&PBX_REFUSE=' . $PBX_REFUSE 
			. '&PBX_ANNULE=' . $PBX_ANNULE
			. '&PBX_HASH=SHA512';
			$binKey = pack("H*", Configuration::get('PAYBOX_HMAC'));
			$hmac = strtoupper(hash_hmac('sha512', $PBX, $binKey));
			echo '<html><body><div style="display:none"><form name=PAYBOX onload="this.submit()" action="' . $PBX_PAYBOX . '" method="post" class="hidden">';
			if (isset($_POST['paybox3x']) && $_POST['paybox3x'] == 1) {
				$PBX_DATE1 = date('d/m/Y', time() + (Configuration::get('PAYBOX_DAYS') * 24 * 60 * 60));
				$PBX_2MONT1 = intval($PBX_TOTAL / 2);
				$PBX_TOTAL = $PBX_2MONT1;
			}

			echo '
				<input type="hidden"  name="PBX_SITE" value="' . $PBX_SITE . '">
				<input type="hidden"  name="PBX_RANG" value="' . $PBX_RANG . '">
				<input type="hidden"  name="PBX_IDENTIFIANT" value="' . $PBX_IDENTIFIANT . '">
				<input type="hidden"  name="PBX_TOTAL" value="' . $PBX_TOTAL . '">
                <input type="hidden"  name="PBX_DEVISE" value="' . $PBX_DEVISE . '">
				<input type="hidden"  name="PBX_CMD" value="' . $PBX_CMD . '">
				<input type="hidden"  name="PBX_PORTEUR" value="' . $PBX_PORTEUR . '">
				<input type="hidden"  name="PBX_TIME" value="'.$PBX_TIME.'">
				<input type="hidden"  name="PBX_LANGUE" value="' . $PBX_LANGUE . '">
				<input type="hidden"  name="PBX_MODE" value="'.$PBX_MODE.'">
				<input type="hidden"  name="PBX_REPONDRE_A" value="' . $PBX_REPONDRE_A . '">
				<input type="hidden"  name="PBX_RETOUR" value="' . $PBX_RETOUR . '">
				<input type="hidden"  name="PBX_EFFECTUE" value="' . $PBX_EFFECTUE . '">
				<input type="hidden"  name="PBX_REFUSE" value="' . $PBX_REFUSE . '">
				<input type="hidden"  name="PBX_ANNULE" value="' . $PBX_ANNULE . '">
                    ';
			if (isset($_POST['paybox3x']) && $_POST['paybox3x'] == 1)
				echo '
                    <input type="hidden"  name="PBX_2MONT1" value="' . $PBX_2MONT1 . '">
                    <input type="hidden"  name="PBX_DATE1" value="' . $PBX_DATE1 . '">
                    ';
			echo '
				<input type="hidden" name="PBX_HASH" value="SHA512">
				<input type="hidden" name="PBX_HMAC" value="'.$hmac.'">

                   
			</form></div></body></html>
                        <script>document.PAYBOX.submit();</script>';
		}else{            
	        if(Configuration::get('PAYBOX_FORM') != 1){
	            echo '<div style="display:none">';
	            echo shell_exec($MOD.$PBX);
	            echo '</div>';
	            echo'<script>document.PAYBOX.submit();</script>';
	        }else{
	            echo '<html><body><div style="display:none"><form name=PAYBOX onload="this.submit()" action="'.$MOD.'" method="post" class="hidden">
					<input type="hidden"  name="PBX_MODE" value="1">
					<input type="hidden"  name="PBX_BACKUP1" value="'.$PBX_BACKUP1.'">
					<input type="hidden"  name="PBX_PAYBOX" value="'.$PBX_PAYBOX.'">
					<input type="hidden"  name="PBX_LANGUE" value="'.$PBX_LANGUE.'">
					<input type="hidden"  name="PBX_SITE" value="'.$PBX_SITE.'">
					<input type="hidden"  name="PBX_RANG" value="'.$PBX_RANG.'">
					<input type="hidden"  name="PBX_IDENTIFIANT" value="'.$PBX_IDENTIFIANT.'">
					<input type="hidden"  name="PBX_DEVISE" value="'.$PBX_DEVISE.'">
					<input type="hidden"  name="PBX_CMD" value="'.$PBX_CMD.'">
					<input type="hidden"  name="PBX_PORTEUR" value="'.$PBX_PORTEUR.'">
					<input type="hidden"  name="PBX_RETOUR" value="'.$PBX_RETOUR.'">
					<input type="hidden"  name="PBX_EFFECTUE" value="'.$PBX_EFFECTUE.'">
					<input type="hidden"  name="PBX_REFUSE" value="'.$PBX_REFUSE.'">
					<input type="hidden"  name="PBX_ANNULE" value="'.$PBX_ANNULE.'">
					<input type="hidden"  name="PBX_REPONDRE_A" value="'.$PBX_REPONDRE_A.'">
	           ';
	               if($threeDSecure != '')
				   	echo '<input type="hidden"  name="PBX_3DS" value="N">';    
				   
	               if(isset($_POST['paybox3x']) && $_POST['paybox3x']==1) 
		               echo'
		                        <input type="hidden"  name="PBX_2MONT1" value="'.$PBX_2MONT1.'">
								<input type="hidden"  name="PBX_2MONT2" value="'.$PBX_2MONT2.'">
		                        <input type="hidden"  name="PBX_TOTAL" value="'.$PBX_2MONT1.'">
		                        <input type="hidden"  name="PBX_DATE1" value="'.$PBX_DATE1.'">
								<input type="hidden"  name="PBX_DATE2" value="'.$PBX_DATE2.'">
		                ';
	               else
	                   echo '<input type="hidden"  name="PBX_TOTAL" value="'.$PBX_TOTAL.'">';
	            echo '
				</form></div></body></html>
	            <script>document.PAYBOX.submit();</script>';
	        }
        }
	}

	public function httpDirect(){
		return Configuration::get('PAYBOX_URLHTTP');
	}
}

?>
