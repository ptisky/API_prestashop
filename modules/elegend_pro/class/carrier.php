<?php
/**
*
*  @package e-legend pro
*  @author    Abouloula Taoufik - E-LEGEND PRO
*  @version    1.0
*  @copyright Copyright (c) Abouloula Taoufik - E-LEGEND PRO
*
*/

class Carriers
{

	
	
	public function getApi()
	{
		return Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'pro_elegend_api where id=1');

	}
	
	public function getMaxId()
	{
		$sql = Db::getInstance()->getValue('SELECT max(`id_carrier`) FROM '._DB_PREFIX_.'carrier');
		return $sql + 1;

	}
	
	public function getMaxPosition()
	{
		$sql = Db::getInstance()->getValue('SELECT max(`position`) FROM '._DB_PREFIX_.'carrier');
        return $sql + 1;
	}
	
	public function getMaxIdRangPrice()
	{
		$sql = Db::getInstance()->getValue('SELECT max(`id_range_price`) FROM '._DB_PREFIX_.'range_price');
        return $sql + 1;
	}
	
	public function setGroupCarrier($id_carrier)
	{
		 $sql = Db::getInstance()->ExecuteS('INSERT IGNORE INTO '._DB_PREFIX_.'carrier_group SELECT '.pSQL($id_carrier).', id_group FROM '._DB_PREFIX_.'group');
         return $sql;
	}
	
	public function setZoneCarrier($id_carrier)
	{
		$sql = Db::getInstance()->ExecuteS('INSERT IGNORE INTO '._DB_PREFIX_.'carrier_zone SELECT '.pSQL($id_carrier).', id_zone FROM '._DB_PREFIX_.'zone WHERE active=1');
         return $sql;
	}
	
	public function setDeliveryCarrier($id_carrier,$id_range_price,$price)
	{
		return Db::getInstance()->ExecuteS('INSERT IGNORE INTO '._DB_PREFIX_.'delivery (id_carrier, id_range_price, price, id_zone) 
		 SELECT '.pSQL($id_carrier).', '.pSQL($id_range_price).', '.pSQL($price).', id_zone FROM '._DB_PREFIX_.'zone WHERE active=1');
         
	}
	
	public function getCarrier()
	{
		$sql = Db::getInstance()->ExecuteS('SELECT id_reference FROM '._DB_PREFIX_.'carrier WHERE external_module_name = "expedition_ell" AND active=1 AND deleted=0');
		return count($sql);

	}
	
	/**
	 * mettreajourManufacturer Insertion manufacturer
	 *
	 * @param string $id_category id de la categorie	 *
	 * @return true
	 */ 
	

   public function _carrier($id_carrier)
	{		
		
		

	try
	{	

		$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);        	
		$opt = array('resource' => 'carriers');
		$opt['id'] = (int)$id_carrier; 
		$xml = $webService->get($opt);
		$c = $xml->children()->children();
		$id_c = (int)$this->getMaxId();
		
		$get_id_carrier = $this->getCarrier();
		if($get_id_carrier == 0){
		// insertion  table carrier 	
         if(!Db::getInstance()->insert('carrier', array(
          'id_carrier' => pSQL((int)$id_c),
		  'id_reference'      => pSQL((int)$id_c),
		  'name'      => pSQL($c->name),
		  'url'      => pSQL($c->url),
		  'active'      => pSQL((int)1),
		  'shipping_handling'      => pSQL((int)1),
		  'need_range'      => pSQL($c->name),
		  'external_module_name'      => pSQL('expedition_ell'),
		  'shipping_method'      => pSQL((int)2),
		  'position'      => (int)$this->getMaxId(),
         ))) die('Error in carrier insert : '.$c->id);


        // insertion  table carrier_lang 
		if(!Db::getInstance()->insert('carrier_lang', array(
          'id_carrier' => pSQL((int)$id_c),
          'id_shop'      => pSQL((int)1),
	      'id_lang'  => pSQL((int)1),
	      'delay'  => pSQL($c->delay->language[0][0]),
         ))) die('Error in carrier_lang insert : '.$c->id);
		 
		
		// insertion table carrier_shop 
		
		 if(!Db::getInstance()->insert('carrier_shop', array(
          'id_carrier' => pSQL((int)$id_c),
	      'id_shop'    => 1,
         ))) die('Error in manufacturer_shop insert : '.$c->id);
		 
		 
		 // insertion table carrier_zone
		 $this->setZoneCarrier($id_c);
		 
		 // insertion table carrier_group
		 $this->setGroupCarrier($id_c);
		 
		 
		 // insertion table carrier_tax_rules_group_shop
		 if(!Db::getInstance()->insert('carrier_tax_rules_group_shop', array(
          'id_carrier' => pSQL((int)$id_c),
	      'id_tax_rules_group'    => 0,
		  'id_shop'    => 1,
         ))) die('Error in carrier_tax_rules_group_shop insert : '.$c->id);
		 
		  $maxIdRangPrice = $this->getMaxIdRangPrice();
		  //$price = 1.8;
		  
		 // insertion table range_price
		 if(!Db::getInstance()->insert('range_price', array(
          'id_range_price' => pSQL((int)$maxIdRangPrice),
	      'id_carrier'    => pSQL((int)$id_c),
		  'delimiter1'    => 0,
		  'delimiter2'    => 8,
         ))) die('Error in range_price insert : '.$c->id);	
		 	 
		 $this->setDeliveryCarrier($id_c,$maxIdRangPrice,1.8);
		 
		 
		 // insertion table carrier_tax_rules_group_shop
		 $nextMaxIdRangPrice = $this->getMaxIdRangPrice();
		 //$price = 0;
		 
		 if(!Db::getInstance()->insert('range_price', array(
          'id_range_price' => $nextMaxIdRangPrice,
	      'id_carrier'    => pSQL((int)$id_c),
		  'delimiter1'    => 8,
		  'delimiter2'    => 999999,
         ))) die('Error in range_price insert : '.$c->id);	
		 
		 $this->setDeliveryCarrier($id_c,$nextMaxIdRangPrice,0);
		 
		}
		 
		
	
			
	}
	catch (PrestaShopWebserviceException $e)
	{
					
			$trace = $e->getTrace();
			if ($trace[0]['args'][0] == 404) echo 'Bad ID';
			else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
			else echo 'Other error'.$e->getMessage().'</b>';
	}			
		
		return true;
	}
	
	
} //Fin class Catalog
?>