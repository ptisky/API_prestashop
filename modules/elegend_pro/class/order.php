<?php
/**
*
*  @package e-legend pro
*  @author    Abouloula Taoufik - E-LEGEND PRO
*  @version    1.0
*  @copyright Copyright (c) Abouloula Taoufik - E-LEGEND PRO
*
*/


class Order
{

	
	
	
	public function getOrders($upc)
	{
		
		
		return Db::getInstance()->ExecuteS('SELECT `id_order`, `reference`, `total_paid`, `id_address_delivery`, `current_state`,  DATE_FORMAT(`invoice_date`, "%d/%m/%Y") AS DatI
									FROM `'._DB_PREFIX_.'orders` 
									WHERE `valid`=1
									AND id_order IN (SELECT id_order FROM '._DB_PREFIX_.'order_detail 
				  	                WHERE  product_id IN (SELECT id_product FROM '._DB_PREFIX_.'product_attribute WHERE upc = '.$upc.'))
									GROUP BY `id_order` DESC
									LIMIT 0,10');
		
		
		
		
	}
	
	public function getTrack($upc)
	{
	
		return Db::getInstance()->ExecuteS('SELECT `id_order`, `reference`, `shipping_number`, `id_address_delivery`,  DATE_FORMAT(`invoice_date`, "%d/%m/%Y") AS DatI
									FROM `'._DB_PREFIX_.'orders` 
									WHERE `valid`=1
									AND `current_state`=110
									AND id_order IN (SELECT id_order FROM '._DB_PREFIX_.'order_detail 
				  	                WHERE  product_id IN (SELECT id_product FROM '._DB_PREFIX_.'product_attribute WHERE upc = '.$upc.'))
									GROUP BY `id_order` DESC
									LIMIT 0,10');
									
		
		
		
	}
	
	
	
} //Fin class Order
?>