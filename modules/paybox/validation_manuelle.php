<?php
/*
 * Module de paiement Paybox par l'�quipe de Magavenue
 * contact@magavenue.com
 * http://www.magavenue.com/blog/
 */
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/paybox.php');

$paybox = new Paybox();
$httpDirect = $paybox->httpDirect();

$erreurPaybox = true;
$signatureValide = false;

$erreurPayment ='';
logTxt("Validation manuelle");
//Verification des adresses ip du serveur paybox (seulement pour http direct)
if($httpDirect) {
	$verification = verification_ip_appelant();
}
$pbx_retour = '';
//recuperation des variables envoyes par Paybox
	$vars = $_GET;
	foreach ($vars as $key => $val) 
	{
		if ($key == 'pbx_sign') 
			$pbx_sign = $val;
		else 
			$pbx_retour .= '&' . $key . '=' . $val;
	}
	$pbx_retour = substr($pbx_retour, 1);
	
	//verification signature : 
	if(PbxVerSign($pbx_sign, $pbx_retour, 'clefpublique.pem')!= 1){
		$signatureValide = false;
		$erreurPayment = 'Erreur lors de la verification de la signature';
	}else {
		$signatureValide = true;
	}
	
	//verification du code derreur
	$erreurRequetePaybox = $vars['pbx_error'];
	switch($erreurRequetePaybox)
	{
		case '00000':
			$erreurPaybox = false;
			break;
		default:
			$erreurPayment ='Code Paybox invalide';
			break;
	}
	$link = new Link();
	if(_PS_VERSION_ >= '1.5')
		Context::getContext()->link = $link;
	
	//si le numero de commande est present, passons au traitement de la commande
	if(isset($vars['ref']))
	{
		$cart = new Cart($vars['ref']);
		
		//si le code derreur est bon et que la signature est bonne alors nous pouvons valider la commande
		if(!$erreurPaybox)
		{
			$total = floatval(number_format($vars['pbx_amount'], 2, '.', ''))/100;	

		        $order = new Order(Order::getOrderByCartId(intval($cart->id)));
                $customer = new Customer((int)$order->id_customer);
				if(_PS_VERSION_ < '1.5')    
               		Tools::redirectLink(__PS_BASE_URI__.'order-confirmation.php?id_cart='.intval($cart->id).'&id_module='.intval($paybox->id).'&id_order='.$order->id.'&key='.$order->secure_key);
				else
					Tools::redirect(__PS_BASE_URI__.'index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$paybox->id.'&id_order='.Order::getOrderByCartId(intval($cart->id)).'&key='.$customer->secure_key);
		} else {	
                    Tools::redirectLink(__PS_BASE_URI__.'history.php');
		}
	}
Tools::redirectLink(__PS_BASE_URI__.'history.php');
function verification_ip_appelant() {
	$unauthorized_server = true;
	//les differentes ips autorises sont en dur pour eviter les modifications par base de donnee
	$authorized_ips = array('195.101.99.76','194.2.122.158','62.39.109.166','194.50.38.6');

	foreach ($authorized_ips as $authorized_ip) {
		if ($_SERVER['REMOTE_ADDR'] == $authorized_ip) {
			return true;
			break;
		}
	}
}

function LoadKey( $keyfile, $pub=true, $pass='' ) {         // chargement de la cl� (publique par d�faut)

    $fp = $filedata = $key = FALSE;                         // initialisation variables
    $fsize =  filesize( $keyfile );                         // taille du fichier
    if( !$fsize ) return FALSE;                             // si erreur on quitte de suite
    $fp = fopen( $keyfile, 'r' );                           // ouverture fichier
    if( !$fp ) return FALSE;                                // si erreur ouverture on quitte
    $filedata = fread( $fp, $fsize );                       // lecture contenu fichier
    fclose( $fp );                                          // fermeture fichier
    if( !$filedata ) return FALSE;                          // si erreur lecture, on quitte
    if( $pub ){
        $key = openssl_pkey_get_public( $filedata );        // recuperation de la cle publique
    } else                                                    // ou recuperation de la cle privee
        $key = openssl_pkey_get_private( array( $filedata, $pass ));
    return $key;                                            // renvoi cle ( ou erreur )
}

function PbxVerSign( $sig, $retour , $keyfile ) {                  // verification signature Paybox
    $key = LoadKey( $keyfile );                             // chargement de la cle
    if( !$key ){
    	return -1;                                  // si erreur chargement cle
    }
	$sig = base64_decode($sig);
	$openSsl=openssl_verify($retour, $sig, $key );
    return $openSsl;            // verification : 1 si valide, 0 si invalide, -1 si erreur
}

function logTxt($log){
  $fp = fopen('log.txt','a+');
  fseek($fp,SEEK_END);
  fputs($fp,"##".date("h:i:s A")."##".$log."\r\n");
  fclose($fp);
}
?>