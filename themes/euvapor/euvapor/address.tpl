{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script type="text/javascript">
// <![CDATA[
var idSelectedCountry = {if isset($smarty.post.id_state)}{$smarty.post.id_state|intval}{else}{if isset($address->id_state)}{$address->id_state|intval}{else}false{/if}{/if};
var countries = new Array();
var countriesNeedIDNumber = new Array();
var countriesNeedZipCode = new Array();
{foreach from=$countries item='country'}
	{if isset($country.states) && $country.contains_states}
		countries[{$country.id_country|intval}] = new Array();
		{foreach from=$country.states item='state' name='states'}
			countries[{$country.id_country|intval}].push({ldelim}'id' : '{$state.id_state}', 'name' : '{$state.name|addslashes}'{rdelim});
		{/foreach}
	{/if}
	{if $country.need_identification_number}
		countriesNeedIDNumber.push({$country.id_country|intval});
	{/if}
	{if isset($country.need_zip_code)}
		countriesNeedZipCode[{$country.id_country|intval}] = {$country.need_zip_code};
	{/if}
{/foreach}
$(function(){ldelim}
	$('.id_state option[value={if isset($smarty.post.id_state)}{$smarty.post.id_state|intval}{else}{if isset($address->id_state)}{$address->id_state|intval}{/if}{/if}]').attr('selected', true);
{rdelim});
{literal}
	$(document).ready(function() {
		/**$('#company').on('input',function(){
			vat_number();
		});
		vat_number();
		function vat_number()
		{
			if ($('#company').val() != '')
				$('#vat_number').show();
			else
				$('#vat_number').hide();
		}**/
		$("select#id_country").find("option#FR").attr("selected", true);
		$('#id_country').change(function () {
        v =  $("#id_country option:selected").val(); 
        t =  $("#id_country option:selected").text();
        var id = $(this).children(":selected").attr("id");
       // alert(id);
        $("input[name$='vat_number']").val(id);
        if(v==8){
        	$('#vat_number').hide();
        }else{$('#vat_number').show();}
 
    })
    .trigger('change');
	});
{/literal}
//]]>
</script>
<div id="bady">
{capture name=path}{l s='Your addresses'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h1>{l s='Your addresses'}</h1>

<h3>
{if isset($id_address) && (isset($smarty.post.alias) || isset($address->alias))}
	{l s='Modify address'} 
	{if isset($smarty.post.alias)}
		"{$smarty.post.alias}"
	{else}
		{if isset($address->alias)}"{$address->alias|escape:'html'}"{/if}
	{/if}
{else}
	{l s='To add a new address, please fill out the form below.'}
{/if}
</h3>

{include file="$tpl_dir./errors.tpl"}

<p class="required"><sup>*</sup> {l s='Required field'}</p>

<form action="{$link->getPageLink('address', true)|escape:'html'}" method="post" class="std" id="add_address">
	
	<fieldset>
		<div class="file-input">
		<h3>{if isset($id_address)}{l s='Your address'}{else}{l s='New address'}{/if}</h3>
	{assign var="stateExist" value=false}
	{assign var="postCodeExist" value=false}

	{foreach from=$ordered_adr_fields item=field_name}
	
		{if $field_name eq 'company'}
		<p class="text">
			<input type="text" id="company" name="company" placeholder="Société" value="{if isset($smarty.post.company)}{$smarty.post.company}{else}{if isset($address->company)}{$address->company|escape:'html'}{/if}{/if}" />
		</p>
		{/if}
		
		{if $field_name eq 'firstname'}
		<p class="required text">
			<input type="text" name="firstname" id="firstname" placeholder="Prénom" value="{if isset($smarty.post.firstname)}{$smarty.post.firstname}{else}{if isset($address->firstname)}{$address->firstname|escape:'html'}{/if}{/if}" />
		</p>
		{/if}
		{if $field_name eq 'lastname'}
		<p class="required text">
			<input type="text" id="lastname" name="lastname" placeholder="Nom" value="{if isset($smarty.post.lastname)}{$smarty.post.lastname}{else}{if isset($address->lastname)}{$address->lastname|escape:'html'}{/if}{/if}" />
		</p>
		{/if}
		{if $field_name eq 'address1'}
		<p class="required text">
		
			<input type="text" id="address1" name="address1" placeholder="Adresse(1)" value="{if isset($smarty.post.address1)}{$smarty.post.address1}{else}{if isset($address->address1)}{$address->address1|escape:'html'}{/if}{/if}" />
		</p>
		{/if}
		{if $field_name eq 'address2'}
		<p class="required text">
			<input type="text" id="address2" name="address2"  placeholder="Adresse(2)"value="{if isset($smarty.post.address2)}{$smarty.post.address2}{else}{if isset($address->address2)}{$address->address2|escape:'html'}{/if}{/if}" />
		</p>
		{/if}
		{if $field_name eq 'postcode'}
		{assign var="postCodeExist" value=true}
		<p class="required postcode text">
			<input type="text" id="postcode" name="postcode"placeholder="Code postal" value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{else}{if isset($address->postcode)}{$address->postcode|escape:'html'}{/if}{/if}" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());" />
		</p>
		{/if}
		{if $field_name eq 'city'}
		<p class="required text">
			<input type="text" name="city" id="city"  placeholder="Ville" value="{if isset($smarty.post.city)}{$smarty.post.city}{else}{if isset($address->city)}{$address->city|escape:'html'}{/if}{/if}" maxlength="64" />
		</p>
		{*
			if customer hasn't update his layout address, country has to be verified
			but it's deprecated
		*}
		{/if}
		{if $field_name eq 'Country:name' || $field_name eq 'country'}
		<p class="required select">
			<select name="id_country" id="id_country">
						
						{foreach from=$countries item=v}
						<option value="{$v.id_country}" id="{$v.iso_code}" {if (isset($smarty.post.id_country) AND $smarty.post.id_country == $v.id_country) OR (!isset($smarty.post.id_country) && $sl_country == $v.id_country)} selected="selected"{/if}>{$v.name}</option>
						{/foreach}
					</select>
		</p>
		{if $vatnumber_ajax_call}
		<script type="text/javascript">
		var ajaxurl = '{$ajaxurl}';
		{literal}
				$(document).ready(function(){
					$('#id_country').change(function() {
						$.ajax({
							type: "GET",
							url: ajaxurl+"vatnumber/ajax.php?id_country="+$('#id_country').val(),
							success: function(isApplicable){
								if(isApplicable == "1")
								{
									$('#vat_area').show();
									$('#vat_number').show();
								}
								else
								{
									$('#vat_area').hide();
								}
							}
						});
					});
				});
		{/literal}
		</script>
		{/if}
		{/if}
		{if $field_name eq 'State:name'}
		{assign var="stateExist" value=true}
		<p class="required id_state select">
			<label for="id_state">{l s='State'} <sup>*</sup></label>
			<select name="id_state" id="id_state">
				<option value="">-</option>
			</select>
		</p>
		{/if}
		{/foreach}
		{if !$postCodeExist}
		<p class="required postcode text hidden">
			<label for="postcode">{l s='Zip / Postal Code'} <sup>*</sup></label>
			<input type="text" id="postcode" name="postcode" value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{else}{if isset($address->postcode)}{$address->postcode|escape:'html'}{/if}{/if}" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());" />
		</p>
		{/if}		
		{if !$stateExist}
		<p class="required id_state select">
			<label for="id_state">{l s='State'} <sup>*</sup></label>
			<select name="id_state" id="id_state">
				<option value="">-</option>
			</select>
		</p>
		{/if}
		
			<div class="account_creation dni">
	
		
		<div id="vat_number" >
							
								<label for="vat_number"></label>
								<p class=" text">
								<input type="text" class="text" name="vat_number" placeholder="{l s='VAT number'}" value="{if isset($smarty.post.vat_number)}{$smarty.post.vat_number}{/if}" />
							</p>
						</div>
	</div>
	
		<p class="textarea">
			<textarea id="other" name="other"  placeholder="Informations supplémentaires" cols="26" rows="3">{if isset($smarty.post.other)}{$smarty.post.other}{else}{if isset($address->other)}{$address->other|escape:'html'}{/if}{/if}</textarea>
		</p>
		{if isset($one_phone_at_least) && $one_phone_at_least}
			<p class="inline-infos required">{l s='You must register at least one phone number.'}</p>
		{/if}
		<p class="text">
			<input type="text" id="phone" name="phone" placeholder="Téléphone domicile"  value="{if isset($smarty.post.phone)}{$smarty.post.phone}{else}{if isset($address->phone)}{$address->phone|escape:'html'}{/if}{/if}" />
		</p>
		<p class="{if isset($one_phone_at_least) && $one_phone_at_least}required {/if}text">
			<input type="text" id="phone_mobile" name="phone_mobile" placeholder="Téléphone portable" value="{if isset($smarty.post.phone_mobile)}{$smarty.post.phone_mobile}{else}{if isset($address->phone_mobile)}{$address->phone_mobile|escape:'html'}{/if}{/if}" />
		</p>
		<p class="required text" id="adress_alias">
			<input type="text" id="alias" name="alias" placeholder="Donnez un titre à cette adresse pour la retrouver plus facilement" value="{if isset($smarty.post.alias)}{$smarty.post.alias}{else if isset($address->alias)}{$address->alias|escape:'html'}{elseif !$select_address}{l s='My address'}{/if}" />
		</p>
	</div>
	</fieldset>

	<p class="submit2">
		{if isset($id_address)}<input type="hidden" name="id_address" value="{$id_address|intval}" />{/if}
		{if isset($back)}<input type="hidden" name="back" value="{$back}" />{/if}
		{if isset($mod)}<input type="hidden" name="mod" value="{$mod}" />{/if}
		{if isset($select_address)}<input type="hidden" name="select_address" value="{$select_address|intval}" />{/if}
		<input type="hidden" name="token" value="{$token}" />		
		<input type="submit" name="submitAddress" id="submitAddress" value="{l s='Save'}" class="button" />
	</p>
</form>
</div>