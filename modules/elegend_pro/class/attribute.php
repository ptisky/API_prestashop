<?php
/**
*
*  @package e-legend pro
*  @author    Abouloula Taoufik - E-LEGEND PRO
*  @version    1.0
*  @copyright Copyright (c) Abouloula Taoufik - E-LEGEND PRO
*
*/

class Attribute
{

	
	
	public function getApi()
	{
		return Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'pro_elegend_api where id=1');

	}
	
	public function getMaxId()
	{
		$sql = Db::getInstance()->getValue('SELECT max(`id_attribute_group`) FROM '._DB_PREFIX_.'attribute_group');
		return $sql + 1;
	}
	
	public function getMaxIdAttribute()
	{
		$sql = Db::getInstance()->getValue('SELECT max(`id_attribute`) FROM '._DB_PREFIX_.'attribute');
		return $sql + 1;
	}
	
	public function getMaxPosition()
	{
		$sql = Db::getInstance()->getValue('SELECT max(`position`) FROM '._DB_PREFIX_.'attribute_group');
		return $sql + 1;
	}
	
	public function getMaxPositionAttribute()
	{
		$sql = Db::getInstance()->getValue('SELECT max(`position`) FROM '._DB_PREFIX_.'attribute');
		return $sql + 1;
	}
	
	public function getNameAttributGroup($name_attribut)
	{
		$sql = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'attribute_group_lang WHERE name LIKE "%'.$name_attribut.'%"');
		return $sql;

	}
	
	
	
	/**
	 * mettreajourManufacturer Insertion manufacturer
	 *
	 * @param string $id_category id de la categorie	 *
	 * @return true
	 */ 
	

   public function _attribute($id_attribute_group)
	{		
		

	    $row = $this->getApi();		
		define('DEBUG', false);
        define('PS_SHOP_PATH', $row['adresse']);
        define('PS_WS_AUTH_KEY', $row['cleapi']);


	try
	{	

		$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);        	
		$opt = array('resource' => 'product_options');
		$opt['id'] = (int)$id_attribute_group; 
		$xml = $webService->get($opt);
		$ag = $xml->children()->children();
		
		$name_attribut = pSQL($ag->name->language[0][0]);
		
		$rec_id_attribut = $this->getNameAttributGroup($name_attribut);
		if(!$rec_id_attribut){		
 	     $id_ag = $this->getMaxId();
         if(!Db::getInstance()->insert('attribute_group', array(
          'id_attribute_group' => (int)$id_ag,
          'is_color_group'      => $ag->is_color_group,
	      'group_type'  => $ag->group_type,
	      'position'  => $this->getMaxPosition(),
         ))) echo('Error in attribute_group insert : '.$ag->id);


        // insertion  table manufacturer_lang 
		if(!Db::getInstance()->insert('attribute_group_lang', array(
          'id_attribute_group' => (int)$id_ag,
          'id_lang'      => 1,
	      'name'  => pSQL($ag->name->language[0][0]),
	      'public_name'  => pSQL($ag->public_name->language[0][0]),
         ))) echo('Error in attribute_group_lang insert : '.$ag->id);
		 
		
		// insertion table manufacturer_shop 
		
		 if(!Db::getInstance()->insert('attribute_group_shop', array(
          'id_attribute_group' => (int)$id_ag,
	      'id_shop'    => 1,
         ))) echo('Error in attribute_group_shop insert : '.$ag->id);
		 
		$id_cat = $ag->associations->product_option_values->product_option_value;
        
         foreach ( $id_cat as $attributs ) {
		
		
		 
	//foreach($ag->associations->product_option_values->product_option_values as $attributs){					
			
		$opta = array('url' => PS_SHOP_PATH . 'api/product_option_values/'.$attributs->id);			
		$xmla = $webService->get($opta);	
		$a = $xmla->children()->children();
		
		$id_a = $this->getMaxIdAttribute();
         if(!Db::getInstance()->insert('attribute', array(
          'id_attribute' => (int)$id_a,
          'id_attribute_group'      => (int)$id_ag,
	      'color'  => $a->group_type,
	      'position'  => $this->getMaxPositionAttribute(),
         ))) echo('Error in attribute insert : '.$a->id);


        // insertion  table manufacturer_lang 
		if(!Db::getInstance()->insert('attribute_lang', array(
          'id_attribute' => (int)$id_a,
          'id_lang'      => 1,
	      'name'  => pSQL($a->name->language[0][0]),
         ))) echo('Error in attribute_lang insert : '.$a->id);
		 
		
		// insertion table manufacturer_shop 
		
		 if(!Db::getInstance()->insert('attribute_shop', array(
          'id_attribute' => (int)$id_a,
	      'id_shop'    => 1,
         ))) echo('Error in attribute_shop insert : '.$a->id);
		
		 }
		
		}
			
	}
	catch (PrestaShopWebserviceException $e)
	{
					
			$trace = $e->getTrace();
			if ($trace[0]['args'][0] == 404) echo 'Bad ID Attribute';
			else if ($trace[0]['args'][0] == 401) echo 'Bad auth key Attribute';
			else echo 'Other error'.$e->getMessage().'</b>';
	}			
		
		return true;
	}
	
	
} //Fin class Catalog
?>