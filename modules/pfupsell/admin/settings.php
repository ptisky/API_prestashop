<?php

$html .= '
	<div class="col-lg-12">
		<div class="panel">				      
			<div class="panel-heading"><i class="icon-cogs"></i> '.$this->l('Up Sell').'</div>
			<div class="form-wrapper">
				<div class="form-group">
				<label class="control-label col-lg-3">'.$this->l('Enable Up Sell?').'</label>
					<div class="col-lg-6">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="show_upsell_info" id="show_upsell_on" value="1" '.($this->upsell_on ?'checked="checked"':'').'/>
							<label for="show_upsell_on">'.$this->l('YES').'</label>
							<input type="radio" name="show_upsell_info" id="show_upsell_off" value="0" '.(!$this->upsell_on ?'checked="checked"':'').'/>
							<label for="show_upsell_off">'.$this->l('NO').'</label>
							<a class="slide-button btn"></a>
						</span>
					</div>
				</div>
				<div class="form-group">
				<label class="control-label col-lg-3">'.$this->l('Enable Left Column?').'</label>
					<div class="col-lg-6">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="left_col_info" id="left_col_on" value="1" '.($this->upsell_left_col ?'checked="checked"':'').'/>
							<label for="left_col_on">'.$this->l('YES').'</label>
							<input type="radio" name="left_col_info" id="left_col_of" value="0" '.(!$this->upsell_left_col ?'checked="checked"':'').'/>
							<label for="left_col_of">'.$this->l('NO').'</label>
							<a class="slide-button btn"></a>
						</span>
					</div>
				</div>
				<div class="form-group">
				<label class="control-label col-lg-3">'.$this->l('Enable Right Column?').'</label>
					<div class="col-lg-6">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="right_col_info" id="right_col_on" value="1" '.($this->upsell_right_col ?'checked="checked"':'').'/>
							<label for="right_col_on">'.$this->l('YES').'</label>
							<input type="radio" name="right_col_info" id="right_col_of" value="0" '.(!$this->upsell_right_col ?'checked="checked"':'').'/>
							<label for="right_col_of">'.$this->l('NO').'</label>
							<a class="slide-button btn"></a>
						</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-3" for="reductionProductFilter">'.$this->l('Search Product').'</label>
					<div class="col-lg-9">
						<div class="col-lg-4">
							<input type="text" id="reductionProductFilter" name="upsell_product_filter" value="" autocomplete="off" class="ac_input">								
						</div>
						<div class="col-lg-4">
							<button type="button" id="add_upsell_item" class="btn btn-default" name="submit_upsell_add_pr">
								<i class="icon-plus-sign-alt"></i> '.$this->l('Add this product').'
							</button>
						</div>
					</div>
					<div class="error col-lg-offset-3 alert alert-warning pack-empty-warning hide" style="margin-top: 32px; margin-left: 27%">'.$this->l('You already have this product').'</div>
				</div>
				<div class="upsell_products_list">';
					$product_ids = unserialize(Configuration::get('PF_UPSELL_PRODUCTS'));
					$id_language = Configuration::get('PS_LANG_DEFAULT');
					$link = new Link();
					$html .= '<div class="form-group listOfPack">
						<div class="col-lg-12">
							<ul id="divPackItems" class="list-unstyled">';
								foreach ($product_ids as $id)
								{
									$product = new Product($id, true, $id_language, Shop::getContextShopID());
									$cover_image = $product->getCover($id);
									$avalibale_image = Image::getImages($id_language, $id);
									if (isset($avalibale_image) && !empty($avalibale_image))
										$img_link = $link->getImageLink($product->link_rewrite, $cover_image['id_image'], 'home_default');
									else
										$img_link = $link->getImageLink($product->link_rewrite, $product->defineProductImage($product->getImages($id_language), $id_language), 'home_default');
								$html .= '
									<li class="product-pack-item media-product-pack" style="width: 188px">
										<img class="media-product-pack-img" src="'.Tools::getProtocol().$img_link.'"/>
										<span class="media-product-pack-title">'.$this->l('Name').': '.(strlen($product->name) >= 14?substr($product->name, 0, 14)."...":$product->name).'</span>
										<span class="media-product-pack-ref">'.$this->l('REF:').' '.$product->reference.'</span>
										<button type="button" class="btn btn-default media-product-pack-action delete_upsell_pr" data-delete="'.$id.'"><i class="icon-trash"></i></button>
									</li>';																	
								}
						$html .= '		
							</ul>
						</div>
					</div>										
				</div>
				<input type="hidden" name="psv" id="psv" value="'.$this->getVersion().'">
				<input type="hidden" name="upsell_basedir" id="basedir" value="'._MODULE_DIR_.$this->name.'">
				<input type="hidden" id="product_language" name="product_language" value="'.$this->context->language->id.'">										
				<div class="panel-footer">
					<button type="submit" class="btn btn-default pull-right" name="submit_upsell">
						<i class="process-icon-save"></i>'.$this->l('Save').'
					</button>
				</div>					
			</div>
		</div>
	</div>
';
	

?>