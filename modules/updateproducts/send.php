<?php
include_once(dirname(__FILE__).'/../../config/config.inc.php');
include_once(dirname(__FILE__).'/../../init.php');

if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || Tools::strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest'){
  header('HTTP/1.0 403 Forbidden');
  echo 'You are forbidden!';  die;
}
$json = array();

try {
  ini_set("max_execution_time","0");
  ini_set('memory_limit', '1024M');


  if( Tools::getValue('removeSettingUpdate') !== false){
    $id = Tools::getValue('id');
    Configuration::deleteByName('GOMAKOIL_FIELDS_CHECKED_UPDATE_'.$id);
    Configuration::deleteByName('GOMAKOIL_LANG_CHECKED_UPDATE_'.$id);
    Configuration::deleteByName('GOMAKOIL_TYPE_FILE_UPDATE_'.$id);
    Configuration::deleteByName('GOMAKOIL_NAME_SETTING_UPDATE_'.$id);
    $settings = array();
    $settings = Tools::unserialize(Configuration::get('GOMAKOIL_ALL_UPDATE_SETTINGS', '' ,Tools::getValue('shopGroupId'), Tools::getValue('id_shop')));
    if(in_array($id, $settings)){
      $key = array_search($id, $settings);
      unset ($settings[$key]);
      $settings =serialize($settings);
      Configuration::updateValue('GOMAKOIL_ALL_UPDATE_SETTINGS', $settings, false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    }
    $json['success'] = true;
  }

  if( Tools::getValue('removeSetting') !== false){
    $id = Tools::getValue('id');
    Configuration::deleteByName('GOMAKOIL_PRODUCTS_CHECKED_'.$id);
    Configuration::deleteByName('GOMAKOIL_MANUFACTURERS_CHECKED_'.$id);
    Configuration::deleteByName('GOMAKOIL_SUPPLIERS_CHECKED_'.$id);
    Configuration::deleteByName('GOMAKOIL_CATEGORIES_CHECKED_'.$id);
    Configuration::deleteByName('GOMAKOIL_FIELDS_CHECKED_'.$id);
    Configuration::deleteByName('GOMAKOIL_LANG_CHECKED_'.$id);
    Configuration::deleteByName('GOMAKOIL_TYPE_FILE_'.$id);
    Configuration::deleteByName('GOMAKOIL_NAME_SETTING_'.$id);
    $settings = array();
    $settings = Tools::unserialize(Configuration::get('GOMAKOIL_ALL_SETTINGS', '' ,Tools::getValue('shopGroupId'), Tools::getValue('id_shop')));
    if(in_array($id, $settings)){
      $key = array_search($id, $settings);
      unset ($settings[$key]);
      $settings =serialize($settings);
      Configuration::updateValue('GOMAKOIL_ALL_SETTINGS', $settings, false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    }
    $json['success'] = true;
  }

  if( Tools::getValue('saveSettingsUpdate') !== false) {
    if( !Tools::getValue('save_setting_update') || Tools::getValue('save_setting_update') == ' ' ){
      throw new Exception(Module::getInstanceByName('updateproducts')->l('Please enter name setting!'));
    }
    $name_config = 'GOMAKOIL_NAME_SETTING_UPDATE_'.Tools::getValue('last_id_update');
    $config = Configuration::get($name_config, '' ,Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    if($config && $config !== Tools::getValue('save_setting_update')){
      $all_setting = Tools::unserialize( Configuration::get('GOMAKOIL_ALL_UPDATE_SETTINGS','',Tools::getValue('shopGroupId'), Tools::getValue('id_shop')));
      if($all_setting){
        $all_setting = max($all_setting);
        $id = $all_setting + 1;
      }
    }
    else{
      $id = Tools::getValue('last_id_update');
    }

    Configuration::updateValue('GOMAKOIL_NAME_SETTING_UPDATE_'.$id, trim(Tools::getValue('save_setting_update')), false, Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    $name_config = 'GOMAKOIL_FIELDS_CHECKED_UPDATE_'.$id;
    Configuration::updateValue($name_config, serialize(Tools::getValue('field_update')), false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    $name_config = 'GOMAKOIL_LANG_CHECKED_UPDATE_'.$id;
    Configuration::updateValue($name_config, Tools::getValue('id_lang_update'), false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    $name_config = 'GOMAKOIL_TYPE_FILE_UPDATE_'.$id;
    Configuration::updateValue($name_config, Tools::getValue('format_file_update'), false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));

    $settings = array();
    $settings = Tools::unserialize(Configuration::get('GOMAKOIL_ALL_UPDATE_SETTINGS', '' ,Tools::getValue('shopGroupId'), Tools::getValue('id_shop')));
    if($settings){
       if(!in_array($id, $settings)){
         $settings[] = $id;
         $settings =serialize($settings);
         Configuration::updateValue('GOMAKOIL_ALL_UPDATE_SETTINGS', $settings, false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
       }
    }
    else{
      $settings[] = $id;
      $settings =serialize($settings);
      Configuration::updateValue('GOMAKOIL_ALL_UPDATE_SETTINGS', $settings, false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));

    }

    $json['id'] = $id;
  }

  if( Tools::getValue('saveSettings') !== false){
    if( !Tools::getValue('save_setting') ){
      throw new Exception(Module::getInstanceByName('updateproducts')->l('Please enter name setting!'));
    }
    $name_config = 'GOMAKOIL_NAME_SETTING_'.Tools::getValue('last_id');
    $config = Configuration::get($name_config, '' ,Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    if($config && $config !== Tools::getValue('save_setting')){
      $all_setting = Tools::unserialize( Configuration::get('GOMAKOIL_ALL_SETTINGS','',Tools::getValue('shopGroupId'), Tools::getValue('id_shop')));
      if($all_setting){
        $all_setting = max($all_setting);
        $id = $all_setting + 1;
      }
    }
    else{
      $id = Tools::getValue('last_id');
    }
    Configuration::updateValue('GOMAKOIL_NAME_SETTING_'.$id, Tools::getValue('save_setting'), false, Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    $config = Configuration::get('GOMAKOIL_PRODUCTS_CHECKED', '' ,Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    $name_config = 'GOMAKOIL_PRODUCTS_CHECKED_'.$id;
    Configuration::updateValue($name_config, $config, false, Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    $config = Configuration::get('GOMAKOIL_MANUFACTURERS_CHECKED', '' ,Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    $name_config = 'GOMAKOIL_MANUFACTURERS_CHECKED_'.$id;
    Configuration::updateValue($name_config, $config, false, Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    $config = Configuration::get('GOMAKOIL_SUPPLIERS_CHECKED', '' ,Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    $name_config = 'GOMAKOIL_SUPPLIERS_CHECKED_'.$id;
    Configuration::updateValue($name_config, $config, false, Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    $name_config = 'GOMAKOIL_CATEGORIES_CHECKED_'.$id;
    Configuration::updateValue($name_config, serialize(Tools::getValue('categories')), false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    $name_config = 'GOMAKOIL_FIELDS_CHECKED_'.$id;
    Configuration::updateValue($name_config, serialize(Tools::getValue('field')), false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    $name_config = 'GOMAKOIL_LANG_CHECKED_'.$id;
    Configuration::updateValue($name_config, Tools::getValue('id_lang'), false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    $name_config = 'GOMAKOIL_TYPE_FILE_'.$id;
    Configuration::updateValue($name_config, Tools::getValue('format_file'), false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    $settings = array();
    $settings = Tools::unserialize(Configuration::get('GOMAKOIL_ALL_SETTINGS', '' ,Tools::getValue('shopGroupId'), Tools::getValue('id_shop')));

    if($settings){
      if(!in_array($id, $settings)){
        $settings[] = $id;
        $settings =serialize($settings);
        Configuration::updateValue('GOMAKOIL_ALL_SETTINGS', $settings, false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
      }
    }
    else{
      $settings[] = $id;
      $settings =serialize($settings);
      Configuration::updateValue('GOMAKOIL_ALL_SETTINGS', $settings, false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));

    }
    $json['id'] = $id;
  }

  if( Tools::getValue('add_product') !== false){
    $name_config = 'GOMAKOIL_PRODUCTS_CHECKED';
    $config = Tools::unserialize(Configuration::get($name_config, '' ,Tools::getValue('shopGroupId'), Tools::getValue('id_shop')));
    if( !$config ){
      $config = array();
    }
    if (!in_array( Tools::getValue('id_product'), $config)){
      array_push($config, Tools::getValue('id_product'));
    }
    else{
      $key = array_search(Tools::getValue('id_product'), $config);
      if ($key !== false)
      {
        unset ($config[$key]);
      }
    }
    $products =serialize($config);
    Configuration::updateValue($name_config, $products, false, Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
  }

  if( Tools::getValue('add_manufacturer') !== false){
    $name_config = 'GOMAKOIL_MANUFACTURERS_CHECKED';
    $config = Tools::unserialize(Configuration::get($name_config, '' ,Tools::getValue('shopGroupId'), Tools::getValue('id_shop')));
    if( !$config ){
      $config = array();
    }
    if (!in_array( Tools::getValue('id_manufacturer'), $config)){
      array_push($config, Tools::getValue('id_manufacturer'));
    }
    else{
      $key = array_search(Tools::getValue('id_manufacturer'), $config);
      if ($key !== false)
      {
        unset($config[$key]);
      }
    }
    $config = serialize($config);
    Configuration::updateValue($name_config, $config, false, Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
  }

  if( Tools::getValue('add_supplier') !== false){
    $name_config = 'GOMAKOIL_SUPPLIERS_CHECKED';
    $config = Tools::unserialize(Configuration::get($name_config, '' ,Tools::getValue('shopGroupId'), Tools::getValue('id_shop')));
    if( !$config ){
      $config = array();
    }
    if (!in_array( Tools::getValue('id_supplier'), $config)){
      array_push($config, Tools::getValue('id_supplier'));
    }
    else{
      $key = array_search(Tools::getValue('id_supplier'), $config);
      if ($key !== false)
      {
        unset($config[$key]);
      }
    }
    $config = serialize($config);
    Configuration::updateValue($name_config, $config, false, Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
  }

  if( Tools::getValue('search_product') !== false){
    $json['products'] = Module::getInstanceByName('updateproducts')->searchProducts(Tools::getValue('search_product'), Tools::getValue('id_shop'), Tools::getValue('id_lang'),Tools::getValue('shopGroupId'));
  }

  if( Tools::getValue('search_manufacturer') !== false){
    $json['manufacturers'] = Module::getInstanceByName('updateproducts')->searchManufacturers(Tools::getValue('search_manufacturer'),Tools::getValue('id_shop'), Tools::getValue('shopGroupId'));
  }

  if( Tools::getValue('search_supplier') !== false){
    $json['suppliers'] = Module::getInstanceByName('updateproducts')->searchSuppliers(Tools::getValue('search_supplier'),Tools::getValue('id_shop'), Tools::getValue('shopGroupId'));
  }

  if( Tools::getValue('show_checked_products') !== false){
    $json['products'] = Module::getInstanceByName('updateproducts')->showCheckedProducts(Tools::getValue('id_shop'), Tools::getValue('id_lang'), Tools::getValue('shopGroupId'));
  }

  if( Tools::getValue('show_checked_manufacturers') !== false){
    $json['manufacturers'] = Module::getInstanceByName('updateproducts')->showCheckedManufacturers(Tools::getValue('id_shop'), Tools::getValue('shopGroupId'));
  }

  if( Tools::getValue('show_checked_suppliers') !== false){
    $json['suppliers'] = Module::getInstanceByName('updateproducts')->showCheckedSuppliers(Tools::getValue('id_shop'), Tools::getValue('shopGroupId'));
  }

  if( Tools::getValue('show_all_products') !== false){
    $json['products'] = Module::getInstanceByName('updateproducts')->showAllProducts(Tools::getValue('id_shop'), Tools::getValue('id_lang'), Tools::getValue('shopGroupId'));
  }

  if( Tools::getValue('show_all_manufacturers') !== false){
    $json['manufacturers'] = Module::getInstanceByName('updateproducts')->showAllManufacturers(Tools::getValue('id_shop'), Tools::getValue('shopGroupId'));
  }

  if( Tools::getValue('show_all_suppliers') !== false){
    $json['suppliers'] = Module::getInstanceByName('updateproducts')->showAllSuppliers(Tools::getValue('id_shop'), Tools::getValue('shopGroupId'));
  }

  if( Tools::getValue('export') !== false){
    Configuration::updateValue('GOMAKOIL_CATEGORIES_CHECKED', '', false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    if( Tools::getValue('categories') ){
      Configuration::updateValue('GOMAKOIL_CATEGORIES_CHECKED', serialize(Tools::getValue('categories')), false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    }
    if( Tools::getValue('field') ){
      Configuration::updateValue('GOMAKOIL_FIELDS_CHECKED', serialize(Tools::getValue('field')), false,  Tools::getValue('shopGroupId'), Tools::getValue('id_shop'));
    }
    include_once('export.php');
    $export = new exportProducts( Tools::getValue('id_shop'), Tools::getValue('id_lang'), Tools::getValue('format_file'), Tools::getValue('shopGroupId') );
    $fileName = $export->export();
    $json['file'] = $fileName;
  }

  if( Tools::getValue('update') !== false){
    if( !Tools::getValue('field_update') ){
      throw new Exception(Module::getInstanceByName('updateproducts')->l('Please select fields for update!'));
    }
    include_once('update.php');
    $export = new updateProductCatalog( Tools::getValue('id_shop'), Tools::getValue('id_lang'), Tools::getValue('format_file'), Tools::getValue('field_update') );
    $res = $export->update();
    $json['success'] = $res;
  }

  echo Tools::jsonEncode($json);
}
catch( Exception $e ){
  $json['error'] = $e->getMessage();
  echo Tools::jsonEncode($json);
}