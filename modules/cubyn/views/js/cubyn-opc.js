/**
 * 2015-2016 Cubyn
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Cubyn <modules@cubyn.com>
 * @copyright 2015-2016 Cubyn
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
$('document').ready(function() {
    checkClick();

    $(document).ajaxComplete(function(event, xhr, settings) {
        
        var response = JSON.parse(xhr.responseText);

        if (response.HOOK_PAYMENT)
        {
            checkClick();
        }

    });
});

function checkClick() {
    $('#HOOK_PAYMENT .payment_module *').click(function() {
        var name = $('input.delivery_option_radio').attr('name');
        var value = $('[name="' + name + '"]:checked').val();
        if (!value) return true;
        var values = value.split(',');
        var found = false;
        var val;

        for (i in values) {
            if (values[i] == cubyn_relai) {
                found = true;
            }
        }

        if (found == false) {
            return true;
        } else {
            if (val = $('[name=cubyn_point_relai]').val()) {
                return true;
            } else {
                alert(selectRelayPointError);
                $('.iframeCubynRelai').click();
                return false;
            }
        }
    });
}

function setCubynRelayPoint(relay_point)
{
    $.ajax({
        type: 'POST',
        dataType: 'json',
        data: {'cubyn_point_relai': relay_point },
    })
    .done(function(data) {
        if (!data.hasError) {
            $('[name=cubyn_point_relai]').val(relay_point);
        }
    });
}