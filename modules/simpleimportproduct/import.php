<?php

class importProducts
{
  private $_context;
  private $_idShop;
  private $_idShopGroup;
  private $_idLang;
  private $_format;
  private $_cover = 'no';
  private $_model;
  private $_delimiter;
  private $_typeImport;
  private $_parser;
  private $ids_images = array();
  private $_importFieldsBase;
  private $_importFieldsCategories;
  private $_importFieldsCombinations;
  private $_importFieldsDiscount;
  private $_importFieldsFeatures;
  private $_importProducts = 0;
  private $_productsForImport = 0;
  private $_PHPExcelFactory;
  private $selected_countries = array();
  private $selected_states = array();
  private $errors = array();



  public function __construct($fields, $id_shop, $idShopGroup){
    Context::getContext()->shop->id = $id_shop;
    Context::getContext()->shop->id_shop_group = $idShopGroup;
    Context::getContext()->shop->save();
    include_once(_PS_MODULE_DIR_ . 'simpleimportproduct/libraries/PHPExcel_1.7.9/Classes/PHPExcel.php');
    include_once(_PS_MODULE_DIR_ . 'simpleimportproduct/libraries/PHPExcel_1.7.9/Classes/PHPExcel/IOFactory.php');
    include_once(_PS_MODULE_DIR_ . 'simpleimportproduct/datamodel.php');
    require_once(dirname(__FILE__).'/simpleimportproduct.php');
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', "0");
    $config_step_one = Tools::unserialize(Configuration::get('GOMAKOIL_CONFIG_IMPORT_PRODUCTS', null, $idShopGroup, $id_shop));
    $this->_simpleimportproduct = new Simpleimportproduct();
    $this->_model = new importProductData();
    $this->_context = Context::getContext();
    $this->_idShop = $id_shop;
    $this->_idShopGroup = $idShopGroup;
    $this->_idLang = $config_step_one['id_lang'];
    $this->_format = $config_step_one['format_file'];
    $this->_delimiter = $config_step_one['delimiter_val'];
    $this->_typeImport = $config_step_one['import_type_val'];
    $this->_parser = $config_step_one['parser_import_val'];
    $this->_importFieldsBase = $fields['base_field'];
    $this->_importFieldsCategories = $fields['field_category'];
    $this->_importFieldsCombinations = $fields['field_combinations'];
    $this->_importFieldsDiscount = $fields['field_discount'];
    $this->_importFieldsFeatures = $fields['field_featured'];
  }

  public function import()
  {
    $this->_clearErrorFile();
    $this->_copyFile();
    $this->_importData();

    if( $this->_importProducts != $this->_productsForImport ){

      $res = array(
        'message'     =>  sprintf(Module::getInstanceByName('simpleimportproduct')->l('Successfully imported %1s products from: %2s'), $this->_importProducts, $this->_productsForImport),
        'error_logs'  => _PS_BASE_URL_.__PS_BASE_URI__.'modules/simpleimportproduct/error/error_logs.csv',
      );

      return $res;
    }

    $rez = array(
      'message'     =>  sprintf(Module::getInstanceByName('simpleimportproduct')->l('Successfully imported %s products!'), $this->_importProducts),
      'error_logs'  => false
    );

    return $rez;
  }

  private function _clearErrorFile()
  {
    $write_fd = fopen('error/error_logs.csv', 'w');

    fwrite($write_fd, 'product_name,error'."\r\n");

    fclose($write_fd);
  }

  private function _copyFile()
  {
    if($this->_format == 'xlsx'){
      $this->_PHPExcelFactory = PHPExcel_IOFactory::load("data/import_products.xls");
    }
    elseif($this->_format == 'csv'){
      $reader = PHPExcel_IOFactory::createReader("CSV");
      $reader->setDelimiter($this->_delimiter);
      $this->_PHPExcelFactory = $reader->load("data/import_products.csv");
    }
  }

  private function _importData()
  {
    foreach ($this->_PHPExcelFactory->getWorksheetIterator() as $worksheet) {
      $highestRow         = $worksheet->getHighestRow(); // e.g. 10
      $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
      $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
      $fileFields = array();

      for ($row = 1; $row <= $highestRow; ++ $row) {
        $product = array();
        for ($col = 0; $col < $highestColumnIndex; ++ $col) {
          $cell = $worksheet->getCellByColumnAndRow($col, $row);
          $val = $cell->getValue();
          if($row == 1){
            if( !$val ){
              continue;
            }
            $fileFields[$col] = $val;
          }
          else{
            if(!isset($fileFields[$col])){
              continue;
            }
            $product[$fileFields[$col]] = $val;
          }
        }
        if( $product ){
          $productData = array();
          foreach( $this->_importFieldsBase as $key => $value){
            if($value !== 'no'){
              $productData['main'][$key] = $product[$value];
            }
          }
          if($this->_importFieldsCategories){
            foreach( $this->_importFieldsCategories as $key => $value){
              $category = array();
              foreach( $value as $k => $v ){
                if($v !== 'no' && $v !== 'undefined'){
                  $category[] = $product[$v];
                }
              }
              $productData['categories'][$key] = $category;
            }
          }

          if($this->_importFieldsCombinations){
            foreach( $this->_importFieldsCombinations as $key => $value){
              $combinations = array();
              foreach( $value as $k => $v ){
                if($v !== 'no' && $k !== 'undefined'){
                  $combinations[$k] = $product[$v];
                }
              }
              $productData['combinations'][$key] = $combinations;
            }
          }
          else{
            $productData['combinations'] = false;
          }

          if($this->_importFieldsDiscount){
            foreach( $this->_importFieldsDiscount as $key => $value){
              $discount = array();
              foreach( $value as $k => $v ){
                if($v !== 'no' && $k !== 'undefined'){
                  $discount[$k] = $product[$v];
                }
              }
              $productData['discount'][$key] = $discount;
            }
          }
          else{
            $productData['discount'] = false;
          }

          if($this->_importFieldsFeatures){
            foreach( $this->_importFieldsFeatures as $key => $value){
              $features = array();
              foreach( $value as $k => $v ){
                if($v !== 'no' && $k !== 'undefined'){
                  $features[$k] = $product[$v];
                }
              }
              $productData['features'][$key] = $features;
            }
          }
          else{
            $productData['features'] = false;
          }

          $this->_productsForImport++;
          if(!$this->_importProduct( $productData )){
            continue;
          }
        }
      }
    }

  }

  private function _importProduct( $productData )
  {
    if($this->_typeImport == 'Add/update'){
      $where = '';

      if(!$productData['main'][$this->_parser]){
        return false;
      }

      if($this->_parser == 'name'){
        $where .= " AND pl.name = '".pSQL($productData['main']['name'])."'";
      }
      elseif($this->_parser == 'reference'){
        $where .= " AND p.reference = '".pSQL($productData['main']['reference'])."'";
      }
      else{
        $where .= " AND pl.link_rewrite = '".pSQL($productData['main']['link_rewrite'])."'";
      }
      $id_product = $this->_model->getProductId($where, $this->_idShop, $this->_idLang);
      return $this->_saveNewProduct($id_product['id_product'], $productData);
    }
    elseif($this->_typeImport == 'Add'){
      if(!$productData['main']['name']){
        return false;
      }
      return $this->_saveNewProduct(false, $productData);
    }
  }

  private function _saveNewProduct($id_product, $productData ){
    $this->ids_images = array();
    $this->_cover = 'no';
    $main = $productData['main'];
    $categories = $productData['categories'];
    $combinations = $productData['combinations'];
    if( $combinations ){
      foreach( $combinations as $key => $comb ){
        if( !$comb['attribute'] ){
          unset($combinations[$key]);
        }
      }
    }
    $discount = $productData['discount'];
    $features = $productData['features'];
    if($this->_typeImport == 'Add/update' && $id_product){
      $object  = new Product($id_product, false, $this->_idLang);

    }
    else{
      $object  = new Product();
    }
    if(isset($main['manufacturer']) && $main['manufacturer']){
      $id_manufacturer = $this->_productManufacturer($main['manufacturer'], $main['name']);
      if(!$id_manufacturer){ return false; }
    }
    $object->name = $this->_createMultiLangField( $main['name'] );

    if(isset( $main['reference']) &&  $main['reference']){
      $object->reference = $main['reference'];
    }
    if(isset( $main['available_for_order']) &&  $main['available_for_order'] !== ''){
      $object->available_for_order = (string)$main['available_for_order'] == '0' ? 0 : 1;
    }
    if(isset( $main['active']) &&  $main['active'] !== ''){
      $object->active = (string)$main['active'] == '0' ? 0 : 1;
    }
    else{
      $object->active = 1;
    }
    if(isset( $main['condition']) &&  $main['condition']){
      $object->condition = $main['condition'];
    }
    if(isset( $main['visibility']) &&  $main['visibility'] !== ''){
      $object->visibility = $main['visibility'];
    }
    if(isset( $main['link_rewrite']) &&  $main['link_rewrite']){
      $object->link_rewrite = $this->_createMultiLangField(  Tools::link_rewrite($main['link_rewrite']) );
    }
    else{
      $object->link_rewrite = $this->_createMultiLangField( Tools::link_rewrite($main['name']) );
    }
    if(isset( $main['meta_description']) &&  $main['meta_description']){
      $object->meta_description = $this->_createMultiLangField( $main['meta_description']);
    }
    if(isset( $main['meta_keywords']) &&  $main['meta_keywords']){
      $object->meta_keywords = $this->_createMultiLangField( $main['meta_keywords']);
    }
    if(isset( $main['meta_title']) &&  $main['meta_title']){
      $object->meta_title = $this->_createMultiLangField( $main['meta_title']);
    }
    if(isset( $main['on_sale']) &&  $main['on_sale'] !== ''){
      $object->on_sale = $main['on_sale'];
    }
    if(isset( $main['min_quantity']) &&  $main['min_quantity'] !== ''){
      $object->minimal_quantity = (int)$main['min_quantity'];
    }
    if(isset( $main['show_price']) &&  $main['show_price'] !== ''){
      $object->show_price = (string)$main['show_price'] == '0' ? 0 : 1;
    }
    if(isset( $main['unit_price']) &&  $main['unit_price']){
      $main['unit_price'] = str_replace(',','.', $main['unit_price']);
      $main['unit_price'] = number_format($main['unit_price'], 4, '.', '');
      $object->unit_price = $main['unit_price'];
    }
    if(isset( $main['price']) &&  $main['price'] !== ''){
      $main['price'] = str_replace(',','.', $main['price']);
      $main['price'] = number_format($main['price'], 4, '.', '');
      $object->price = $main['price'];
    }
    if(isset( $main['wholesale_price']) &&  $main['wholesale_price'] !== ''){
      $main['wholesale_price'] = str_replace(',','.', $main['wholesale_price']);
      $main['wholesale_price'] = number_format($main['wholesale_price'], 4, '.', '');
      $object->wholesale_price = (float)$main['wholesale_price'];
    }
    if(isset( $main['ean13']) &&  $main['ean13']){
      $object->ean13 = $main['ean13'];
    }
    if(isset( $main['ecotax']) &&  $main['ecotax']){
      $object->ecotax = (float)$main['ecotax'];
    }
    if(isset( $main['upc']) &&  $main['upc']){
      $object->upc = $main['upc'];
    }
    if(isset($main['manufacturer']) && $main['manufacturer']){
      $object->id_manufacturer = $id_manufacturer;
    }
    if(isset($main['width']) && $main['width']){
      $object->width = $main['width'];
    }
    if(isset($main['height']) && $main['height']){
      $object->height = $main['height'];
    }
    if(isset($main['depth']) && $main['depth']){
      $object->depth = $main['depth'];
    }
    if(isset($main['weight']) && $main['weight']){
      $object->weight = $main['weight'];
    }
    if(isset($main['short_description']) && $main['short_description']){
      $object->description_short = $this->_createMultiLangField( $main['short_description'] );
    }
    if(isset($main['description']) && $main['description']){
      $object->description = $this->_createMultiLangField( $main['description'] );
    }
    if(isset($main['available_now']) && $main['available_now']){
      $object->available_now = $main['available_now'] ;
    }
    if(isset($main['available_later']) && $main['available_later']){
      $object->available_later = $main['available_later'];
    }

    if(isset( $main['tax']) &&  $main['tax']){
      $name_tax = 'Import module tax ('. $main['tax'] .'%)';
      $tax = new Tax();
      $id_tax = $tax->getTaxIdByName($name_tax);
      if($id_tax){
        $tax_rule_group = new TaxRulesGroupCore();
        $tax_rule_group_id = $tax_rule_group->getIdByName($name_tax);
        $object->id_tax_rules_group = $tax_rule_group_id;
      }
      else{
        if((float)$main['tax']>0 && (float)$main['tax']<100){
          $tax->name = array( $this->_idLang => $name_tax);
          $tax->rate = (float)$main['tax'];
          $tax->active = 1;
          $tax->save();
          $tax_rule_group = new TaxRulesGroup();
          $tax_rule_group->name =  $name_tax;
          $tax_rule_group->active = 1;
          $tax_rule_group->save();
          $tax_rule_group->save();
          $this->_createRule($tax->id, $tax_rule_group->id);
          $object->id_tax_rules_group = $tax_rule_group->id;
        }
      }
    }

    if( ( $error = $object->validateFields(false, true) ) !== true ){
        $this->_createErrorsFile($error,$main['name']);
        return false;
    }

  if( ( $error = $object->validateFieldsLang(false, true) ) !== true ){
      $this->_createErrorsFile($error,$main['name']);
      return false;
    }

    $object->save();
    $productId = $object->id;

    if(isset($categories) && $categories){
      $id_category = $this->_productCategories($categories);
      $object->deleteCategories();
      $id_category = array_unique($id_category);
      Cache::clean('Product::getProductCategories_'.(int)$id_product);
      $object->addToCategories($id_category);
    }
    if(isset($main['default_category']) && $main['default_category']){
      $category_def = $this->_model->getCategoryByName(trim($main['default_category']), $this->_idLang, false);
      if($category_def){
        $object->id_category_default = $category_def;
      }
      else{
        $object->id_category_default = 2;
      }
    }
    if(isset($main['tags']) && $main['tags']){
      Tag::deleteTagsForProduct($productId);
      Tag::addTags($this->_idLang, $productId, $main['tags']);
    }
    if(isset($main['quantity']) && $main['quantity'] !== ''){
      StockAvailable::setQuantity($productId, null, (int)$main['quantity']);
    }

    if(isset($main['supplier']) && $main['supplier']){
      $suppliers = explode(",", $main['supplier']);
      $object->deleteFromSupplier();
      foreach($suppliers as $supplier){
        $rez = $this->_productSuppliers($supplier, $productId);
      }
    }
    if(isset($main['supplier_default']) && $main['supplier_default']){
      $isset_supplier_dafault = $this->_model->getSupplier(trim($main['supplier_default']));
      if($isset_supplier_dafault){
        $object->id_supplier = $isset_supplier_dafault['id_supplier'];
      }
    }

    $img_attr = false;
    if(isset($combinations) && $combinations){
      foreach($combinations as $combination){
        if(isset($combination['images_combination']) && $combination['images_combination']){
          $img_attr = true;
        }
      }
    }

    if((isset($main['images_url']) && $main['images_url']) || $img_attr){

      foreach($object->getImages($this->_idLang) as $img_del){
        if($img_del){
          $image_del = new Image($img_del['id_image']);
          $image_del->delete();
        }
      }

      if(isset($main['images_url']) && $main['images_url']){

        $img_products = explode(",", $main['images_url']);
        if(isset($main['images_alt']) && $main['images_alt']){
          $img_alt = explode(",", $main['images_alt']);
        }
        else{
          $img_alt = false;
        }

        foreach($img_products as $kay => $url_img){
          if(!isset($img_alt[$kay])){
            $img_alt[$kay] = $main['name'];
          }
          $ids_images = $this->ids_images;
          if(!isset($ids_images[$url_img]) || !$ids_images[$url_img]){
            $rez = $this->_productImages($productId, $url_img, $img_alt[$kay], $kay);
          }


        }
      }



      if($img_attr){
        if(isset($combinations) && $combinations){

          foreach($combinations as $k => $combination){
            if(isset($combination['images_combination']) && $combination['images_combination']){
              $img_products = explode(",", $combination['images_combination']);
              foreach($img_products as $kay => $url_img){
                if(!isset($img_alt[$kay])){
                  $img_alt[$kay] = $main['name'];
                }
                $ids_images = $this->ids_images;

                if(!isset($ids_images[$url_img]) || !$ids_images[$url_img]){
                  $rez = $this->_productImages($productId, $url_img, $img_alt[$kay], false);
                }
              }
            }
          }
        }
      }

    }

    if(isset($combinations) && $combinations){

      $object->deleteProductAttributes();
      $rez = $this->_productCombinations($combinations, $productId);

      $attributes = $rez['attributes'];
      $values = $rez['values'];
      $id_images = $rez['id_images'];

      $object->generateMultipleCombinations($values, $attributes);
      $attributesAll = Product::getProductAttributesIds($productId, true);
      foreach ($attributesAll as $key => $attribute){
        StockAvailable::setQuantity($productId, $attribute['id_product_attribute'],  (int)$values[$key]['quantity']);
        $combination = new Combinationcore($attribute['id_product_attribute']);
        if (!empty($id_images[$key])) {
          $combination->setImages($id_images[$key]);
        }
      }
    }
    if($discount){
      $rez = $this->_productDiscount($discount, $productId);
    }
    if($features){
      foreach($features as $featur){
        if(isset($featur['features_name']) && $featur['features_name'] && $featur['features_value'] && isset($featur['features_value'])){
          $rez = $this->_productFeatures($featur);
          $object->addFeatureProductImport($productId, $rez['id_feature'], $rez['id_feature_val']);
        }
      }
    }



    if( ( $error = $object->validateFields(false, true) ) !== true ){
      $this->_createErrorsFile($error,$main['name']);
      return false;
    }

    $object->save();
    $this->_importProducts++;

    Configuration::updateValue('GOMAKOIL_IMPORT_PRODUCTS_COUNT', (int)$this->_importProducts, false, $this->_idShopGroup, $this->_idShop);

    return true;
  }

  private function _createRule($id_tax, $id_tax_rules_group)
  {
    $zip_code = 0;
    $id_rule = (int)0;
    $behavior = (int)0;
    $description = "";

    $countries = Country::getCountries(Context::getContext()->language->id);
    $this->selected_countries = array();
    foreach ($countries as $country) {
      $this->selected_countries[] = (int)$country['id_country'];
    }

    if (empty($this->selected_states) || count($this->selected_states) == 0) {
      $this->selected_states = array(0);
    }
    $tax_rules_group = new TaxRulesGroup((int)$id_tax_rules_group);
    foreach ($this->selected_countries as $id_country) {
      $first = true;
      foreach ($this->selected_states as $id_state) {
        if ($tax_rules_group->hasUniqueTaxRuleForCountry($id_country, $id_state, $id_rule)) {
          $this->errors[] = Tools::displayError('A tax rule already exists for this country/state with tax only behavior.');
          continue;
        }
        $tr = new TaxRule();

        // update or creation?
        if (isset($id_rule) && $first) {
          $tr->id = $id_rule;
          $first = false;
        }

        $tr->id_tax = $id_tax;
        $tax_rules_group = new TaxRulesGroup((int)$id_tax_rules_group);
        $tr->id_tax_rules_group = (int)$tax_rules_group->id;
        $tr->id_country = (int)$id_country;
        $tr->id_state = (int)$id_state;
        list($tr->zipcode_from, $tr->zipcode_to) = $tr->breakDownZipCode($zip_code);

        // Construct Object Country
        $country = new Country((int)$id_country, (int)Context::getContext()->language->id);

        if ($zip_code && $country->need_zip_code) {
          if ($country->zip_code_format) {
            foreach (array($tr->zipcode_from, $tr->zipcode_to) as $zip_code) {
              if ($zip_code) {
                if (!$country->checkZipCode($zip_code)) {
                  $this->errors[] = sprintf(
                    Tools::displayError('The Zip/postal code is invalid. It must be typed as follows: %s for %s.'),
                    str_replace('C', $country->iso_code, str_replace('N', '0', str_replace('L', 'A', $country->zip_code_format))), $country->name
                  );
                }
              }
            }
          }
        }

        $tr->behavior = (int)$behavior;
        $tr->description = $description;
        $this->tax_rule = $tr;

        if (count($this->errors) == 0) {
          $tax_rules_group = $this->updateTaxRulesGroup($tax_rules_group);
//          $tr->id = (int)$tax_rules_group->getIdTaxRuleGroupFromHistorizedId((int)$tr->id);
//          $tr->id_tax_rules_group = (int)$tax_rules_group->id;

          if (!$tr->save()) {
            $this->errors[] = Tools::displayError('An error has occurred: Cannot save the current tax rule.');
          }
        }
      }
    }
  }

  protected function updateTaxRulesGroup($object)
  {
    static $tax_rules_group = null;
    if ($tax_rules_group === null) {
      $object->update();
      $tax_rules_group = $object;
    }

    return $tax_rules_group;
  }

 private function _productImages($productId, $url_img, $img_alt, $kay){

    $url_img = trim($url_img);
    $img_alt = trim($img_alt);

    if(@getimagesize($url_img)){
      $path_parts_prod = pathinfo($url_img);
      $name_img = $path_parts_prod['basename'];
      $newpath_prod =  dirname(__FILE__).'/upload/';
      if (PHP_VERSION_ID < 50300)
        clearstatcache();
      else
        clearstatcache(true, $newpath_prod.$name_img);
      if ( copy($url_img, $newpath_prod.$name_img) ) {
        $image = new Image();
        $image->id_product = $productId;
        $image->legend = @trim($img_alt);
        if( $this->_cover == 'no'){
          $image->cover = 1;
          $image->position = 1;
          $this->_cover = 'yes';
        }

        $image->add();

        $all_img = $this->ids_images;
        $all_img[$url_img] = $image->id;
        $this->ids_images = $all_img;

        $new_path = $image->getPathForCreation();

        if( !ImageManager::resize($newpath_prod.$name_img, $new_path.'.'.$image->image_format, null, null, 'jpg', false) ){
          return false;
        }
        $imagesTypes = ImageType::getImagesTypes('products');
        foreach ($imagesTypes as $imageType)
        {
          ImageManager::resize($newpath_prod.$name_img, $new_path.'-'. Tools::stripslashes($imageType['name']).'.'.$image->image_format, $imageType['width'], $imageType['height'], $image->image_format);
        }

        $image->update();
        if( file_exists($newpath_prod.$name_img) ){
          unlink($newpath_prod.$name_img);
        }
      }
    }
  }

  private function _productSuppliers($supplier, $productId){
    $supplier = trim($supplier);
    $id_shop_list = Shop::getContextListShopID();
    $isset_supplier = $this->_model->getSupplier($supplier);

    if(!$isset_supplier && $supplier){
      $supplier_obj = new Supplier();
      $supplier_obj->name = $supplier;
      $supplier_obj->id_shop_list = $id_shop_list;
      $supplier_obj->active = 1;
      $supplier_obj->save();

      Db::getInstance()->insert('product_supplier', array('id_product' => (int)$productId, 'id_supplier' => (int)$supplier_obj->id));
    }
    else{
      Db::getInstance()->insert('product_supplier', array('id_product' => (int)$productId, 'id_supplier' => (int)$isset_supplier['id_supplier']));
    }
  }

  private function _productFeatures($features){
    $rez = array();
    $id_shop_list = Shop::getContextListShopID();
    $feature_name = trim($features['features_name']);
    $isset_feature = $this->_model->getFeatures($feature_name, $this->_idLang, $this->_idShop);
    if(!$isset_feature){
      $feature= new Feature();
      $feature->name = $this->_createMultiLangField( $feature_name );
      $feature->id_shop_list = $id_shop_list;
      $feature->save();
      $id_feature = $feature->id;
    }
    else{
      $id_feature = $isset_feature;
    }
    $feature_value = trim($features['features_value']);
    $isset_feature_val = $this->_model->getFeaturesValue($feature_value, $id_feature, $this->_idLang);
    if(!$isset_feature_val){
      $feature_val= new FeatureValue();
      $feature_val->id_feature = $id_feature;
      $feature_val->value = $this->_createMultiLangField( $feature_value );
      $feature_val->save();
      $id_feature_val = $feature_val->id;
    }
    else{
      $id_feature_val = $isset_feature_val;
    }

    $rez['id_feature'] = $id_feature;
    $rez['id_feature_val'] = $id_feature_val;

    return $rez;
  }

  private function _productDiscount($discounts, $productId){
    foreach ($discounts as $discount)
    {
      $reduction_type = trim($discount['reduction_type']);
      if($reduction_type == 'amount'){
        $reduction = $discount['reduction'];
      }
      else if($reduction_type == 'percentage'){
        $reduction = $discount['reduction']/100;
      }
      else{
        $reduction = 0;
        $reduction_type = 0;
      }
      $reduction_from = trim($discount['reduction_from']);
      $reduction_to = trim($discount['reduction_to']);
      $price = trim($discount['fixed_price']);
      if(!$price){
        $price = -1;
      }
      else{
        $reduction_type = 'amount';
      }
      $from_quantity = trim($discount['from_quantity']);
      if(!$from_quantity){
        $from_quantity = 1;
      }
      $isst_specific_price = $this->_model->getSpecificPrice($productId, $this->_idShop);

      if ($isst_specific_price){
        $specific_price = new SpecificPrice((int)$isst_specific_price);
      }
      else
      {
        $specific_price = new SpecificPrice();
      }

      if( (float)$price > 0 ){
        $price = str_replace(',','.', $price);
        $price = number_format($price, 4, '.', '');
      }

      $specific_price->id_product = (int)$productId;
      $specific_price->id_specific_price_rule = 0;
      $specific_price->id_shop = $this->_idShop;
      $specific_price->id_currency = 0;
      $specific_price->id_country = 0;
      $specific_price->id_group = 0;
      $specific_price->price = $price;
      $specific_price->id_customer = 0;
      $specific_price->from_quantity = (int)$from_quantity;
      $specific_price->reduction = $reduction;
      $specific_price->reduction_type = $reduction_type;
      $specific_price->from = (isset($reduction_from) && Validate::isDate($reduction_from)) ? $reduction_from : '0000-00-00 00:00:00';
      $specific_price->to = (isset($reduction_to) && Validate::isDate($reduction_to))  ? $reduction_to : '0000-00-00 00:00:00';
      $specific_price->save();
    }
  }

  private function _productCombinations($combinations_all, $productId){
    $attributes = array();
    $values = array();
    $img_attr= array();
    foreach($combinations_all as $k => $combinations){
      $attribut = array();
      $attribut = explode(",", $combinations['attribute']);
      $attribut_val = explode(",",$combinations['value']);
      $id_attributes = array();
      foreach($attribut as $key => $a){
        $val_name  = array();
        $val_name = explode(":", $a);
  	    if( !$val_name[0] || !trim($attribut_val[$key]) ){
          continue;
        }
        $isset_group = $this->_model->getGroupAttribute($val_name[0], $val_name[1], $this->_idLang);
        if(!$isset_group){
          if($val_name[1] !== 'select' && $val_name[1] !== 'radio' && $val_name[1] !== 'color'){
            $type = 'select';
          }
          else{
            $type = $val_name[1];
          }
          $obj = new AttributeGroup();
          $obj->name[$this->_idLang] = $val_name[0];
          $obj->public_name[$this->_idLang] = $val_name[0];
          $obj->group_type = $type;
          if($type == 'color'){
            $obj->is_color_group = 1;
          }
          else{
            $obj->is_color_group = 0;
          }
          $obj->save();
          $id_attribute_group = $obj->id;
        }
        else{
          $id_attribute_group = $isset_group;
        }
        $isset_attribute = $this->_model->getAttribute(trim($attribut_val[$key]), $id_attribute_group, $this->_idLang);
        if(!$isset_attribute){
          $attribute = new Attribute();
          $attribute->id_attribute_group = $id_attribute_group;
          $attribute->name[$this->_idLang] = trim($attribut_val[$key]);
          $attribute->save();
          $id_attributes[] = $attribute->id;
        }
        else{
          $id_attributes[] = $isset_attribute;
        }
      }

	if( $id_attributes ){
		$attributes[] = $id_attributes;

      if($combinations['impact_price']){
        $combinations['impact_price'] = str_replace(',','.', $combinations['impact_price']);
        $combinations['impact_price'] = number_format($combinations['impact_price'], 4, '.', '');
      }

      if($combinations['wholesale_price_combination']){
        $combinations['wholesale_price_combination'] = str_replace(',','.', $combinations['wholesale_price_combination']);
        $combinations['wholesale_price_combination'] = number_format($combinations['wholesale_price_combination'], 4, '.', '');
      }




      if(isset($combinations['images_combination']) && $combinations['images_combination']){
        $img_products = explode(",", $combinations['images_combination']);
        foreach($img_products as $kay => $url_img){
          $ids_images = $this->ids_images;
          if(isset($ids_images[$url_img]) && $ids_images[$url_img]){
            $img_attr[$k][] = $ids_images[$url_img];
          }
        }
      }


	      $values[] = array(
            'id_product' => $productId,
            'price' => (float)$combinations['impact_price'],
            'weight' => (float)$combinations['impact_weight'],
            'ecotax' => (float)$combinations['ecotax_combination'],
            'ean13' => $combinations['ean13_combination'],
            'upc' => $combinations['upc_combination'],
            'wholesale_price' => $combinations['wholesale_price_combination'],
            'min_quantity' => $combinations['min_quantity_combination'],
            'quantity' => $combinations['quantity_combination'],
            'reference' => pSQL($combinations['reference_combination']),
            'default_on' => (int)$combinations['default'],
            'available_date' => '0000-00-00',
	      );
	}
      
    }

    $rez = array();
    $rez['attributes'] = $attributes;
    $rez['values'] = $values;
    $rez['id_images'] = $img_attr;
    return $rez;
  }

  private function _productCategories($categories){
    $id_category = array();
    foreach($categories as $category){
      $count_cat = count($category);
      $parent_cat = 2;
      for($i = 0; $i < $count_cat; $i++){
        $cat = trim($category[$i]);

        if($cat){
          if($i == 0){
            $isset_cat = $this->_model->getCategoryByName($cat, $this->_idLang, $this->_idShop, $parent_cat);
            if(!$isset_cat){
              $obj_cat = new Category(null, null, $this->_idShop);
              $obj_cat->name = $this->_createMultiLangField( $cat );
              $obj_cat->link_rewrite = $this->_createMultiLangField( Tools::link_rewrite( $cat ));
              $obj_cat->id_parent = $parent_cat;
              $obj_cat->save();
              $parent_cat = $obj_cat->id;
            }
            else{
              $parent_cat = $isset_cat;
            }
          }
          else{
            $isset_cat = $this->_model->getCategoryByName($cat, $this->_idLang, $this->_idShop, $parent_cat );
            if(!$isset_cat){
              $obj_cat = new Category(null, null, $this->_idShop);
              $obj_cat->name = $this->_createMultiLangField( $cat );
              $obj_cat->link_rewrite = $this->_createMultiLangField( Tools::link_rewrite( $cat ));
              $obj_cat->id_parent = $parent_cat;
              $obj_cat->save();
              $parent_cat = $obj_cat->id;
            }
            else{
              $parent_cat = $isset_cat;
            }
          }
          $id_category[] = $parent_cat;
        }
      }
    }

    return $id_category;
  }

  private function _productManufacturer($manufacturer, $nameProduct)
  {
    $manufacturer = trim($manufacturer);
    $isset_manufacturer = $this->_model->getManufacturer($manufacturer);
    if(!$isset_manufacturer && $manufacturer){
      $manufacturer_obj = new Manufacturer();
      $manufacturer_obj->name = $manufacturer;
      $manufacturer_obj->active = 1;

      if( ( $error = $manufacturer_obj->validateFields(false, true) ) !== true ){
        $this->_createErrorsFile($error,$nameProduct);
        return false;
      }
      $manufacturer_obj->save();

      return $manufacturer_obj->id;
    }
    else{
      return $isset_manufacturer['id_manufacturer'];
    }
  }

  private function _createErrorsFile($error, $nameProduct)
  {

    $write_fd = fopen('error/error_logs.csv', 'a+');
    if (@$write_fd !== false){
      fwrite($write_fd, $nameProduct . ',' . $error . "\r\n");
    }
    fclose($write_fd);
  }

  private function _createMultiLangField($field)
  {
    $languages = Language::getLanguages(false);
    $res = array();
    foreach ($languages as $lang)
      $res[$lang['id_lang']] = $field;
    return $res;
  }

}
