<?php
/**
*
*  @package e-legend pro
*  @author    Abouloula Taoufik - E-LEGEND PRO
*  @version    1.0
*  @copyright Copyright (c) Abouloula Taoufik - E-LEGEND PRO
*
*/


class State
{

	public function getOrderState()
	{
		$sql = Db::getInstance()->ExecuteS('SELECT id_order_state FROM '._DB_PREFIX_.'order_state  WHERE id_order_state = 100');
		return count($sql);

	}
	
	public function orderState()
	{
		
		$name_state[100] = "(A) 100 - Pret à transmettre";
        $name_state[101] = "(A) 101 - Transmis";	
        $name_state[102] = "(A) 102 - Pris en charge";
        $name_state[105] = "(A) 105 - Colis prêt";
        $name_state[110] = "(A) 110 - Colis envoyé";
        $name_state[120] = "(A) 120 - Problème de livraison";
		$name_state[130] = "(A) 130 - Problème de livraison";
		$name_state[140] = "(A) 140 - Problème de livraison";
		$name_state[150] = "(A) 150 - Problème de livraison";
		
		$getOrderState = $this->getOrderState();
		if($getOrderState == 0){
		$id_order_state = 100; 
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state (id_order_state,invoice,send_email,module_name,color,unremovable,hidden,logable,delivery,shipped,paid,deleted)
				VALUES ($id_order_state, 0, 0, '', '#0009f2', 1, 0, 1, 0, 0, 0, 0)");
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state_lang (id_order_state, id_lang, name, template)
				VALUES ($id_order_state, 1, '".$name_state[$id_order_state]."', '')");
				
				$id_order_state = 101; 
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state (id_order_state,invoice,send_email,module_name,color,unremovable,hidden,logable,delivery,shipped,paid,deleted)
				VALUES ($id_order_state, 0, 0, '', '#0009f2', 1, 0, 1, 0, 0, 0, 0)");
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state_lang (id_order_state, id_lang, name, template)
				VALUES ($id_order_state, 1, '".$name_state[$id_order_state]."', '')");
				
				$id_order_state = 102; 
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state (id_order_state,invoice,send_email,module_name,color,unremovable,hidden,logable,delivery,shipped,paid,deleted)
				VALUES ($id_order_state, 0, 0, '', '#0009f2', 1, 0, 1, 0, 0, 0, 0)");
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state_lang (id_order_state, id_lang, name, template)
				VALUES ($id_order_state, 1, '".$name_state[$id_order_state]."', '')");
				
				
				$id_order_state = 105; 
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state (id_order_state,invoice,send_email,module_name,color,unremovable,hidden,logable,delivery,shipped,paid,deleted)
				VALUES ($id_order_state, 0, 0, '', '#0009f2', 1, 0, 1, 0, 0, 0, 0)");
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state_lang (id_order_state, id_lang, name, template)
				VALUES ($id_order_state, 1, '".$name_state[$id_order_state]."', '')");
				
				
				$id_order_state = 110; 
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state (id_order_state,invoice,send_email,module_name,color,unremovable,hidden,logable,delivery,shipped,paid,deleted)
				VALUES ($id_order_state, 0, 0, '', '#81c200', 1, 0, 1, 0, 0, 0, 0)");
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state_lang (id_order_state, id_lang, name, template)
				VALUES ($id_order_state, 1, '".$name_state[$id_order_state]."', '')");
				
				
				$id_order_state = 120; 
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state (id_order_state,invoice,send_email,module_name,color,unremovable,hidden,logable,delivery,shipped,paid,deleted)
				VALUES ($id_order_state, 0, 0, '', '#ff0000', 1, 0, 1, 0, 0, 0, 0)");
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state_lang (id_order_state, id_lang, name, template)
				VALUES ($id_order_state, 1, '".$name_state[$id_order_state]."', '')");
				
				
				$id_order_state = 130; 
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state (id_order_state,invoice,send_email,module_name,color,unremovable,hidden,logable,delivery,shipped,paid,deleted)
				VALUES ($id_order_state, 0, 0, '', '#ff0000', 1, 0, 1, 0, 0, 0, 0)");
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state_lang (id_order_state, id_lang, name, template)
				VALUES ($id_order_state, 1, '".$name_state[$id_order_state]."', '')");
				
				
				$id_order_state = 140; 
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state (id_order_state,invoice,send_email,module_name,color,unremovable,hidden,logable,delivery,shipped,paid,deleted)
				VALUES ($id_order_state, 0, 0, '', '#ff0000', 1, 0, 1, 0, 0, 0, 0)");
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state_lang (id_order_state, id_lang, name, template)
				VALUES ($id_order_state, 1, '".$name_state[$id_order_state]."', '')");
				
				
				
				$id_order_state = 150; 
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state (id_order_state,invoice,send_email,module_name,color,unremovable,hidden,logable,delivery,shipped,paid,deleted)
				VALUES ($id_order_state, 0, 0, '', '#ff0000', 1, 0, 1, 0, 0, 0, 0)");
			$queryP = Db::getInstance()->ExecuteS("INSERT INTO "._DB_PREFIX_."order_state_lang (id_order_state, id_lang, name, template)
				VALUES ($id_order_state, 1, '".$name_state[$id_order_state]."', '')");
		
		}
		
		
	}
	
	
	
} //Fin class Order
?>