<?php 
	include(dirname(__FILE__).'/../../../config/config.inc.php');
	include(dirname(__FILE__).'/../../../init.php');

	if (Tools::getIsset('action') && Tools::getValue('action') == 'delete')
	{
		$id_product = Tools::getValue('product_id');
		$product_ids = unserialize(Configuration::get('PF_UPSELL_PRODUCTS'));
		unset($product_ids[array_search($id_product, $product_ids)]);
		Configuration::updateValue('PF_UPSELL_PRODUCTS', serialize($product_ids));
	}
?>