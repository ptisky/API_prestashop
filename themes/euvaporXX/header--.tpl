{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="{$lang_iso}"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="{$lang_iso}"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="{$lang_iso}"> <![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="{$lang_iso}"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$lang_iso}">
	<head>
		<title>{$meta_title|escape:'htmlall':'UTF-8'}</title>
{if isset($meta_description) AND $meta_description}
		<meta name="description" content="{$meta_description|escape:html:'UTF-8'}" />
{/if}
{if isset($meta_keywords) AND $meta_keywords}
		<meta name="keywords" content="{$meta_keywords|escape:html:'UTF-8'}" />
{/if}   
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
	<meta http-equiv="content-language" content="{$meta_language}" />
	<meta name="generator" content="PrestaShop" />

	<meta name="robots" content="{if isset($nobots)}no{/if}noindex,{if isset($nofollow) && $nofollow}no{/if}nofollow" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
		
		<script type="text/javascript">
			var baseDir = '{$content_dir|addslashes}';
			var baseUri = '{$base_uri|addslashes}';
			var static_token = '{$static_token|addslashes}';
			var token = '{$token|addslashes}';
			var priceDisplayPrecision = {$priceDisplayPrecision*$currency->decimals};
			var priceDisplayMethod = {$priceDisplay};
			var roundMode = {$roundMode};
			 $.noConflict();
			  jQuery( document ).ready(function($) {

 /**var widthdevice = $(window).width();
 alert(widthdevice);**/
 if (($("#layered_block_left").length == 0)){
   $('#centercol').css('float','none');
   $('#centercol').css('margin','0 auto');
    $('#centercol').css('width','auto');
}
			  	$("#s-menumob").hide();
			  /**	var number_of_items = $('#menumobb').children().size();
			  	var i =1;
$('#menumobb').children().each(function() {
	$('#menumobb li').addClass("chedren"+i);
i++;
});**/

			 /** 	$(".sf-menu li").click(function() {
			  			this.addClass("sfHoverdd");
			  		//alert("test");
			  	});**/
			 $(".btn-navbar").click(function() {

$("#s-menumob").toggle();

});
			// $(".sf-with-ul").removeAttr("href").css("cursor","pointer");
			if ($(window).width() < 640) {
$(".lofadva-block-1").removeAttr( "style" );
$(".lofadva-block-2").removeAttr( "style" );
$(".lofadva-block-3").removeAttr( "style" );
$(".lofadva-block-4").removeAttr( "style" );
}
			});
		</script>
{if isset($css_files)}
	{foreach from=$css_files key=css_uri item=media}
	<link href="{$css_uri}" rel="stylesheet" type="text/css" media="{$media}" />
	{/foreach}

{/if}
{if isset($js_files)}
	{foreach from=$js_files item=js_uri}
	<script type="text/javascript" src="{$js_uri}"></script>
	{/foreach}

{/if}
{literal}
<script type="text/javascript">
$(function() {
$('#new').show();
$('#tops').hide('slow');
$('#promo').hide('slow');
$('.camenu1').click(function() {
$('#new').show('slow');
$('#tops').hide('slow');
$('#promo').hide('slow');
});

$('.camenu2').click(function() {
$('#tops').show('slow');
$('#new').hide('slow');
$('#promo').hide('slow');
});
$('.camenu3').click(function() {
$('#promo').show('slow');
$('#new').hide('slow');
$('#tops').hide('slow');
});
}); </script>
{/literal}
{literal}
<script type="text/javascript">
function Ferme1()
	{
	window.open('about:blank','_parent','');
	window.close();
	}
{literal} 
     var popupStatus = 0;

    <!--
    function loadPopup(){
    //loads popup only if it is disabled
    if(popupStatus==0){
        jQuery(".popup_block").fadeIn("slow");
      jQuery('body').append('<div id="fade"></div>');
        popupStatus = 1;
    }
}
function disablePopup(){
    //disables popup only if it is enabled
    if(popupStatus==1){
       jQuery('#fade , .popup_block').fadeOut(function() {
		jQuery('#fade, a.close').remove();  
		
	});
        popupStatus = 0;
    }
   
}

$(document).ready(function () { // quand la page est chargée	
 var widthdevice = $(window).width();
if(widthdevice<1024)
{
	$('.layered_close').addClass('closed');
	$('#layered_form div div ul').css('display','none')
}


 var x = document.cookie;

var actpop = $('#popa').val();


$('.adddp').click(function(){
 disablePopup();
jQuery.cookie("anewsletter", "1", { expires: 7 });
});

   
   if (jQuery.cookie("anewsletter") != 1) { 
	loadPopup();
	
}
	
	
	jQuery('.adddp1').on('click', function() { 
	 disablePopup();
   jQuery.cookie("anewsletter", "1", { expires: 7 });

		}); 


});
//-->
{/literal} 
</script>{/literal}
<link href='http://fonts.googleapis.com/css?family=Istok+Web:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<script src="{$content_dir}themes/centrale/js/jcarousellite_1.0.1.pack.js" type="text/javascript"></script>
<script src="{$base_dir}themes/centrale/js/jquery.cookie.js" type="text/javascript"></script>
<link href="{$content_dir}themes/centrale/css/responsive.css" rel="stylesheet" type="text/css" media="{$media}" />
		{$HOOK_HEADER}
	</head>
	
	<body>

 
 <input type="hidden" value="popa" id="popa">
	<div id="popup1" class="popup_block" style=" width: 300px; margin-top: -169px; margin-left: -150px;">
	               <div class="atten">Attention</div>
                    <h2>Certains produits contiennent de la nicotine </h2>
                    <div class="supal"></div>
                    <h2>Vous devez être majeur pour continuer</h2>
   <div id="tita">		
	<a class=" adddp1"  href="{$content_dir}" title="{l s='continuer'}"><span></span>{l s='Accueil'}</a>
		<a class=" adddp"  href="http://beta.sitenco.com/central/10-cigarette-electronique"><span></span>{l s='	Cigarette Electronique'}</a>
		<a class=" adddp"  href="http://beta.sitenco.com/central/20-vaporisateur"><span></span>{l s='Vaporisateur'}</a>
	</div>
<a href="#" onclick="Ferme1()" class="close">{l s="Sortir d'ici"}</a>
                    </div>
                  

	{if !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
		<div id="restricted-country">
			<p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country}</span></p>
		</div>
		{/if}
		<div id="page" class="container_9 clearfix">
			<!-- Header -->
			<div id="header" class="">
				<a id="header_logo" href="{$base_dir}" title="{$shop_name|escape:'htmlall':'UTF-8'}">
					<img class="logo" src="{$logo_url}" alt="{$shop_name|escape:'htmlall':'UTF-8'}" {if $logo_image_width}width="{$logo_image_width}"{/if} {if $logo_image_height}height="{$logo_image_height}" {/if}/>
				</a>
				<div id="header_right">
					{$HOOK_TOP}
				</div>
			</div>
			<div id="headermob" class="">

 <a class="btn btn-navbar">
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    </a>

			<div id="header_leftmob">
			<a id="header_logo" href="{$base_dir}" title="{$shop_name|escape:'htmlall':'UTF-8'}">
			<img class="logo1" src="{$img_dir}/logomob.png" alt="{$shop_name|escape:'htmlall':'UTF-8'}" {if $logo_image_width}width="100"{/if} />
			</a></div>   

   
				<div id="ident2">
<div id="comp">

<a class="account" rel="nofollow" href="{$link->getPageLink('my-account', true)}" title="{l s='MON COMPTE'}">
<i class="flaticon1-user581"></i> 
<span>
	{l s='MON COMPTE'}</span></a>
	<a href="{$link->getPageLink('order', true)}" title="{l s='MON PANIER'}" rel="nofollow">{l s='MON PANIER'}</a>
	<i class="flaticon1-grocery6"></i> 
	{if $logged}
	<i class="flaticon1-lock7"></i> 
	<a href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html'}" title="{l s='Log me out'}" class="logout" rel="nofollow">   
      {l s='DECONNEXION'}
		</a>
	{/if}
</div>
</div>
			</div>
			<div id="s-menumob" class="">
					{$HOOK_TOP}
			
			</div>
       {if $page_name == 'index'}
      <div id="sliderd"> {$HOOK_SLIDER}</div>
      {/if}

			<div id="columns" class="grid_9 alpha omega clearfix">
				<!-- Left -->
				<!--div id="left_column" class="column grid_2 alpha">
					{* $HOOK_LEFT_COLUMN *}
				</div->

				<!-- Center -->
				<div id="center_column" class="">

 {if $page_name == 'index'}
				<ul class="ca-menu" >
                   
                      <li id="cercle1" >
                      	<div id="cercle1div">
                        <a href="{$base_dir}guide-va.php" style="text-decoration: none;">
                            <div id="ca-cercle1">&nbsp;</div>
                        </a>   
                        </div>    
                        <div id="ca-text1"><a href="{$base_dir}guide-va.php">{l s="Comment choisir vaporisateur"}</a></div>              
                    </li>
                     <li id="cercle2" >
                     	<div id="cercle2div">
                        <a href="{$base_dir}guide-li.php" style="text-decoration: none;">
                            <div id="ca-cercle2">&nbsp;</div>
                        </a>  
                        </div>   
                        <div id="ca-text2"><a href="{$base_dir}guide-li.php">{l s="Comment choisir son eliquide"}</a></div>              
                    </li>
                    <li id="cercle3" >
                    	<div id="cercle3div">
                        <a href="{$base_dir}guide-ec.php" style="text-decoration: none;">
                            <div id="ca-cercle3">&nbsp;</div>
                        </a>  
                        </div> 
                        <div id="ca-text3"><a href="{$base_dir}guide-ec.php">{l s="Comment choisir sa cigarette électronique"}</a></div>              
                    </li>
                   
    </ul>

<div id="menud">
    <ul class="ca-menu-center">
    	<li id="motif1">&nbsp;</li>
    	<li id="camenu1"><div class="camenu1">Les nouveautés</div></li>
    	<li id="camenu2"><div class="camenu2">Les tops</div></li>
    	<li id="camenu3"><div class="camenu3">Les promos</div></li>
    	<li id="motif2">&nbsp;</li>
    </ul></div>

    <div id="new" >{$HOOK_NEW}</div>
    <div id="tops" >{$HOOK_TOPS}</div>
    <div id="promo">{$HOOK_PROMO}</div>
	{/if}{/if}
