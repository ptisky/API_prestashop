<?php

require_once('PSWebServiceLibrary.php');

$url = 'http://127.0.0.1/modules/prestashop2/';
$webService = new PrestaShopWebservice($url, 'KGU4N3YTI3AYIYJ59FCBCEVDPZ2PRG3N', true);

$opt['resource'] = 'products';
$opt['display'] = 'full';
$xml = $webService->get($opt);
$productNodes = $xml->products->children();
$products = array();
foreach ($productNodes as $product) {
  $nameLanguage = $product->xpath('name/language[@id=1]');
  $name = (string) $nameLanguage[0];
  $idImage = (string) $product->id_default_image;
  $image = '/img/p/';
  for ($i = 0; $i < strlen($idImage); $i++) {
    $image .= $idImage[$i] . '/';
  }
  $image .= $idImage . '.jpg';
  $id = (int) $product->id;
  $descriptionLanguage = $product->xpath('description/language[@id=1]');
  $description = (string) $descriptionLanguage[0];
  $path = '/index.php?controller=product&id_product=' . $product->id;
  $products[] = array('name' => $name, 'image' => $image, 'id' => $id, 'description' => $description, 'path' => $path);
}
?>