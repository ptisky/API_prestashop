<?php

class productsUpdateModel
{
  private $_context;

  public function __construct(){
    include_once(dirname(__FILE__).'/../../config/config.inc.php');
    include_once(dirname(__FILE__).'/../../init.php');
    $this->_context = Context::getContext();
  }

  public function searchProduct( $id_shop = false, $id_lang  = false, $search = false )
  {
    if($id_shop === false){
      $id_shop =  $this->_context->shop->id ;
    }
    if($id_lang === false){
      $id_lang =  $this->_context->language->id ;
    }
    $where = "";
    if( $search ){
      $where = " AND (pl.name LIKE '%".pSQL($search)."%' OR p.id_product LIKE '%".pSQL($search)."%')";
    }
    $sql = '
			SELECT p.id_product, pl.name
      FROM ' . _DB_PREFIX_ . 'product_lang as pl
      LEFT JOIN ' . _DB_PREFIX_ . 'product as p
      ON p.id_product = pl.id_product
      WHERE pl.id_lang = ' . (int)$id_lang . '
      AND pl.id_shop = ' . (int)$id_shop . '
      ' . $where . '
      ORDER BY pl.name
      LIMIT 0,50
			';
    return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
  }

  public function searchManufacturer( $search = false )
  {
    $where = "";
    if( $search ){
      $where = " AND (m.name LIKE '%".pSQL($search)."%' OR m.id_manufacturer LIKE '%".pSQL($search)."%')";
    }
    $sql = '
			SELECT m.id_manufacturer, m.name
      FROM ' . _DB_PREFIX_ . 'manufacturer as m
      WHERE 1
      ' . $where . '
      ORDER BY m.name
      LIMIT 0,50
			';
    return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
  }

  public function searchSupplier( $search = false )
  {
    $where = "";
    if( $search ){
      $where = " AND (p.name LIKE '%".pSQL($search)."%' OR p.id_supplier LIKE '%".pSQL($search)."%')";
    }
    $sql = '
			SELECT p.id_supplier, p.name
      FROM ' . _DB_PREFIX_ . 'supplier as p
      WHERE 1
      ' . $where . '
      ORDER BY p.name
      LIMIT 0,50
			';
    return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
  }

  public function showCheckedProducts( $id_shop = false, $id_lang  = false, $products_check = false )
  {
    if($id_shop === false){
      $id_shop = $this->_context->shop->id ;
    }
    if($id_lang === false){
      $id_lang = $this->_context->language->id ;
    }
    $where = "";
    $limit = "  LIMIT 300 ";
    if( $products_check !== false ){
      if( !$products_check ){
        return array();
      }
      $products_check = implode(",", $products_check);
      $where = " AND p.id_product  IN ($products_check) ";
      $limit = "";
    }
    $sql = '
			SELECT p.id_product, pl.name
      FROM ' . _DB_PREFIX_ . 'product_lang as pl
      LEFT JOIN ' . _DB_PREFIX_ . 'product as p
      ON p.id_product = pl.id_product
      WHERE pl.id_lang = ' . (int)$id_lang . '
      AND pl.id_shop = ' . (int)$id_shop . '
      ' . $where . '
      ORDER BY pl.name
      ' . $limit . '
			';

    return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
  }

  public function showCheckedManufacturers( $items_check = false )
  {
    $where = "";
    $limit = "  LIMIT 300 ";
    if( $items_check !== false ){
      if( !$items_check ){
        return array();
      }
      $items_check = implode(",", $items_check);
      $where = " AND m.id_manufacturer  IN ($items_check) ";
      $limit = "";
    }
    $sql = '
			SELECT m.id_manufacturer, m.name
      FROM ' . _DB_PREFIX_ . 'manufacturer as m
      WHERE 1
      ' . $where . '
      ORDER BY m.name
      ' . $limit . '
			';

    return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
  }

  public function showCheckedSuppliers( $items_check = false )
  {
    $where = "";
    $limit = "  LIMIT 300 ";
    if( $items_check !== false ){
      if( !$items_check ){
        return array();
      }
      $items_check = implode(",", $items_check);
      $where = " AND s.id_supplier  IN ($items_check) ";
      $limit = "";
    }
    $sql = '
			SELECT s.id_supplier, s.name
      FROM ' . _DB_PREFIX_ . 'supplier as s
      WHERE 1
      ' . $where . '
      ORDER BY s.name
      ' . $limit . '
			';

    return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
  }

  public function getProductSuppliersID(  $productId = false ){

    $sql = '
			SELECT GROUP_CONCAT(DISTINCT ps.id_supplier SEPARATOR ";") as suppliers_ids,
			GROUP_CONCAT(DISTINCT s.name SEPARATOR ";") as suppliers_name
      FROM ' . _DB_PREFIX_ . 'product_supplier as ps
      INNER JOIN ' . _DB_PREFIX_ . 'supplier as s
       ON ps.id_supplier = s.id_supplier
      WHERE  ps.id_product = '.(int)$productId.'
      AND ps.id_product_attribute = 0
			';

    return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
    
  }


  public function getExportIds( $idShop, $shopGroupId )
  {

    $products_check = Tools::unserialize(Configuration::get('GOMAKOIL_PRODUCTS_CHECKED','',$shopGroupId, $idShop));
    $selected_manufacturers = Tools::unserialize(Configuration::get('GOMAKOIL_MANUFACTURERS_CHECKED','',$shopGroupId, $idShop));
    $selected_suppliers = Tools::unserialize(Configuration::get('GOMAKOIL_SUPPLIERS_CHECKED','',$shopGroupId, $idShop));
    $selected_categories = Tools::unserialize(Configuration::get('GOMAKOIL_CATEGORIES_CHECKED','',$shopGroupId, $idShop));
    $where = "";
    $justProducts = true;



    if( $selected_manufacturers ){
      $justProducts = false;
      $selected_manufacturers = implode(",", $selected_manufacturers);
      $where .= " AND p.id_manufacturer IN ($selected_manufacturers) ";
    }

    if( $selected_suppliers ){
      $justProducts = false;
      $selected_suppliers = implode(",", $selected_suppliers);
      $where .= " AND s.id_supplier IN ($selected_suppliers) ";
    }

    if( $selected_categories ){
      $justProducts = false;
      $selected_categories = implode(",", $selected_categories);
      $where .= " AND cp.id_category IN ($selected_categories) ";
    }


    if( $products_check ){
      $products_check = implode(",", $products_check);
      $justProducts = $justProducts ? 'AND' : 'OR';
      $where .= " $justProducts p.id_product IN ($products_check) ";
    }

    $sql = "
      SELECT DISTINCT p.id_product
       FROM " . _DB_PREFIX_ . "product as p
       INNER JOIN " . _DB_PREFIX_ . "product_shop as ps
       ON p.id_product = ps.id_product
       LEFT JOIN " . _DB_PREFIX_ . "category_product as cp
       ON p.id_product = cp.id_product
       LEFT JOIN " . _DB_PREFIX_ . "product_supplier as s
       ON p.id_product = s.id_product
       WHERE ps.id_shop = " . (int)$idShop . "
       $where
    ";

    return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
  }
}