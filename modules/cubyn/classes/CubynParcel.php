<?php
/**
 * 2015-2016 Cubyn
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Cubyn <modules@cubyn.com>
 * @copyright 2015-2016 Cubyn
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class CubynParcel
{

    /**
     * @var CubynAddress
     */
    protected $address;
    protected $firstName;
    protected $lastName;

    /**
     * @return CubynAddress
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return null|string
     */
    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    /**
     * @return string
     */
    public function getDeliveryMode()
    {
        return $this->deliveryMode;
    }

    /**
     * @return string
     */
    public function getDeliverySigned()
    {
        return $this->deliverySigned;
    }

    /**
     * @return int
     */
    public function getObjectCount()
    {
        return $this->objectCount;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * @return string
     */
    public function getOrderRef()
    {
        return $this->orderRef;
    }

    protected $organizationName;
    protected $deliveryMode;
    protected $deliverySigned;
    protected $objectCount;
    protected $phone;
    protected $label;
    protected $description;
    protected $email;
    protected $value;
    protected $insurance;
    protected $orderRef;

    /**
     * @var CustomerCore
     */
    private $_customer;
    /**
     * @var OrderCore
     */
    private $_order;

    public function __construct($id_order, $carriers)
    {
        $this->initContext();
        $this->lang             = $this->context->language->id;
        $this->_order           = new Order($id_order);
        $this->address          = new CubynAddress($this->_order->id_customer, $this->_order->id_address_delivery);
        $this->firstName        = $this->address->getCustomerFirstName();
        $this->lastName         = $this->address->getCustomerLastName();
        $this->organizationName = $this->address->getCustomerCompany();
        $this->email            = $this->address->getCustomerEmail();
        $this->phone            = $this->address->getDeliveryPhone();

        // delivery options
        $options              = $this->getDeliveryOptions($carriers);
        $this->deliveryMode   = $options['api'];
        $this->deliverySigned = $options['signed'];

        // other meta info
        $this->objectCount = $this->getOrderProductCount();
        $this->label       = $this->getOrderLabel();
        $this->description = $this->getOrderDescription();
        $this->value       = $this->getOrderValue();
        $this->orderRef    = $this->getOrderReference();
        return $this;
    }

    private function initContext()
    {
        $this->ajax    = true;
        $this->context = Context::getContext();
    }

    private function getOrderLabel()
    {
        $label = '';
        foreach ($this->_order->getProducts() as $productArray) {
            $product = new Product($productArray['product_id'], false, $this->lang);
            $label .= $product->name;
            $label.-' - ';
        }
        return $label;
    }

    private function getOrderDescription()
    {
        $description = array();
        foreach ($this->_order->getProducts() as $productArray) {
            $product = array(
                "id"        => $productArray['product_id'],
                "reference" => $productArray['reference'],
                "count"     => $productArray['product_quantity'],
                "name"      => $productArray['product_name'],
                "value"     => $productArray['product_price_wt'],
                "width"     => $productArray['width'],
                "height"    => $productArray['height'],
                "depth"     => $productArray['depth'],
                "weight"    => $productArray['weight'],
            );
            array_push($description, $product);
        }
        return Tools::jsonEncode($description);
    }

    private function getDeliveryOptions($carriers)
    {
        $id = $this->_order->id_carrier;
        foreach ($carriers as $carrier) {
            $reference = $carrier['reference'];
            if ($id == Configuration::get($reference)) {
                return $carrier;
            }
        }
        // in case the carrier got deleted by the user
        // in the meantime
        return array(
            'api'    => 'colissimo',
            'signed' => false,
        );
    }

    private function getOrderProductCount()
    {
        $products = $this->_order->getProducts();
        $count    = 0;
        foreach ($products as $productArray) {
            $count += $productArray['product_quantity'];
        }
        return $count;
    }

    private function getOrderValue()
    {
        return $this->_order->total_paid;
    }

    private function getOrderReference()
    {
        return $this->_order->reference;
    }
}
