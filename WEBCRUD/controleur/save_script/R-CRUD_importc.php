<?php

try{
$bdd_customers = new PDO('mysql:host=localhost;dbname=prestashop;charset=utf8', 'root', '');
}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}
	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	   //																																			  ///////
	  //-----------------------------------------------------------// PARTIE PRESTASHOP //-----------------------------------------------------------///////
	 //																																				///////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				//connection de l'api au webservice
				include("connection.php");

	//on recupere le fichier xml
	$get_xml_customers = simplexml_load_file("customers.xml");
	// $ordersxml = simplexml_load_file("orders.xml");
	
	//d�but de la boucle pour envoyer les customers
	foreach ($get_xml_customers->children()->children() as $ressources_customers)
	{
		try {
				$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); //connection
				$xml_customers = $webService->get(array('url' => PS_SHOP_PATH.'/api/customers?schema=synopsis'));//on recupere un shemas blanc xml

				// $ressources = $customersxml->customers->children();//on recupere l'enfant 'customer' 

				//donn�es obligatoires dans mon xml
				$lastname_customers  					         	= $ressources_customers->lastname;
				$firstname_customers			  			      	= $ressources_customers->firstname;
				$email_customers     						        = $ressources_customers->email;

				//autres donn�es sans l'id dans mon xml
				$id_default_group_customers  			     		= $ressources_customers->id_default_group;
				$id_lang_customers 						        	= $ressources_customers->id_lang;
				$newsletter_date_add_customers   			  		= $ressources_customers->newsletter_date_add;
				$ip_registration_newsletter_customers  				= $ressources_customers->ip_registration_newsletter;
				$last_passwd_gen_customers 		    		 		= $ressources_customers->last_passwd_gen;
				$secure_key_customers     	     	   				= $ressources_customers->secure_key;
				$deleted_customers  			      	   			= $ressources_customers->deleted;
				$passwd_customers 					           		= $ressources_customers->passwd;
				$id_gender_customers     		     	 		   	= $ressources_customers->id_gender;
				$birthday_customers  				      	 		= $ressources_customers->birthday;
				$newsletter_customers 			     	 			= $ressources_customers->newsletter;
				$optin_customers     					  	    	= $ressources_customers->optin;
				$website_customers  					 	     	= $ressources_customers->website;
				$company_customers 						        	= $ressources_customers->company;
				$siret_customers     						        = $ressources_customers->siret;
				$ape_customers  							        = $ressources_customers->ape;
				$outstanding_allow_amount_customers	  		 		= $ressources_customers->outstanding_allow_amount;
				$show_public_prices_customers    		  			= $ressources_customers->show_public_prices;
				$id_risk_customers  						        = $ressources_customers->id_risk;
				$max_payment_days_customers 		   			    = $ressources_customers->max_payment_days;
				$active_customers     			       		 		= $ressources_customers->active;
				$note_customers 						        	= $ressources_customers->note;
				$is_guest_customers 						        = $ressources_customers->is_guest;
				$id_shop_customers     				       	    	= $ressources_customers->id_shop;
				$id_shop_group_customers     			   			= $ressources_customers->id_shop_group;
				$date_add_customers     					   	    = $ressources_customers->date_add;
				$date_upd_customers     					   	    = $ressources_customers->date_upd;
				$id_customers										= $ressources_customers->id;
				$group_customers									= $ressources_customers->associations->groups->group->id;	    
				// var_dump($email);

				//on associe les donn�es de mon xml au xml blank cr�er au debut
				$xml_customers->customer->lastname 					            	= $lastname_customers;
				$xml_customers->customer->firstname  					            = $firstname_customers;
				$xml_customers->customer->email 						            = $email_customers;
				$xml_customers->customer->id_default_group 			        		= $id_default_group_customers;
				$xml_customers->customer->id_lang  					            	= $id_lang_customers;
				$xml_customers->customer->newsletter_date_add 		      			= $newsletter_date_add_customers;
				$xml_customers->customer->ip_registration_newsletter 	  			= $ip_registration_newsletter_customers;
				$xml_customers->customer->last_passwd_gen  			        		= $last_passwd_gen_customers;
				$xml_customers->customer->secure_key 					          	= $secure_key_customers;
				$xml_customers->customer->deleted 					            	= $deleted_customers;
				$xml_customers->customer->passwd  					            	= $passwd_customers;
				$xml_customers->customer->id_gender 					          	= $id_gender_customers;
				$xml_customers->customer->birthday 				            		= $birthday_customers;
				$xml_customers->customer->newsletter  				       			= $newsletter_customers;
				$xml_customers->customer->optin 						            = $optin_customers;
				$xml_customers->customer->website 					            	= $website_customers;
				$xml_customers->customer->company  				            		= $company_customers;
				$xml_customers->customer->siret 					            	= $siret_customers;
				$xml_customers->customer->ape 					              		= $ape_customers;
				$xml_customers->customer->outstanding_allow_amount     				= $outstanding_allow_amount_customers;
				$xml_customers->customer->show_public_prices 	       				= $show_public_prices_customers;
				$xml_customers->customer->id_risk 			            			= $id_risk_customers;
				$xml_customers->customer->max_payment_days 	         				= $max_payment_days_customers;
				$xml_customers->customer->active 					             	= $active_customers;
				$xml_customers->customer->note 					               		= $note_customers;
				$xml_customers->customer->is_guest  			          			= $is_guest_customers;
				$xml_customers->customer->id_shop 				             		= $id_shop_customers;
				$xml_customers->customer->id_shop_group 			        		= $id_shop_group_customers;
				$xml_customers->customer->date_add  				        	   	= $date_add_customers;
				$xml_customers->customer->date_upd 				            		= $date_upd_customers;
				$xml_customers->customer->associations->groups->group->id			= $group_customers;
				
				$opt_customers = array('resource' => 'customers');
				$opt_customers['postXml'] = $xml_customers->asXML();

				// var_dump($opt);
				
				//envoie le xml a prestashop
				$xml_customers = $webService->add($opt_customers);

				$monid_customers = $xml_customers->customer->id;//r�cupere l'id ins�r� dans prestashop par auto increment
				$bdd_customers->query('UPDATE `prestashop`.`ps_customer` SET `id_customer` = '.$id_customers.' WHERE `ps_customer`.`id_customer` = '.$monid_customers.'');//envoie la nouvelle id				
			}	
			catch (PrestaShopWebserviceException $e)
			{
			  // Here we are dealing with errors
			  $trace = $e->getTrace();
			  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
			  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
			  else echo 'Other error<br />'.$e->getMessage();
			}
	}
	echo 'client ok','<br>';
	?>