<?php
/**
*
*  @package e-legend pro
*  @author    Abouloula Taoufik - E-LEGEND PRO
*  @version    1.0
*  @copyright Copyright (c) Abouloula Taoufik - E-LEGEND PRO
*
*/

class Combination
{

	
	
	public function getApi()
	{
		return Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'pro_elegend_api where id=1');

	}

	
	public function getMaxIdProductAttribut()
	{
		$sql = Db::getInstance()->getValue('SELECT max(`id_product_attribute`) FROM '._DB_PREFIX_.'product_attribute');
		return $sql + 1;

	}

	public function getAttribut($name_attribut)
	{
		$sql = Db::getInstance()->getValue('SELECT id_attribute FROM '._DB_PREFIX_.'attribute_lang WHERE name LIKE "%'.$name_attribut.'%"');
		return $sql;

	}
	
	
	public function getGroupAttribut($id_attribut)
	{
		$sql = Db::getInstance()->getValue('SELECT id_attribute_group FROM '._DB_PREFIX_.'attribute WHERE id_attribute = '.$id_attribut.'');
		return $sql;

	}
	
	public function getQuantity($quantity)
	{	
	
	//Db::getInstance()->getValue('SELECT id_product_attribute FROM '._DB_PREFIX_.'product_attribute WHERE id_product_attribute = '.$id_product_attribute.'');  
		return 	$quantity;			
	}
	
	public function setQuantity($id_product,$id_product_attribute,$id_shop,$quantite)
	{	
	
	 $sql = Db::getInstance()->ExecuteS('INSERT IGNORE INTO '._DB_PREFIX_.'stock_available
						(id_product, id_product_attribute, id_shop, id_shop_group, quantity, depends_on_stock, out_of_stock)
						VALUES ('.$id_product.', '.$id_product_attribute.', '.$id_shop.',  0, '.$quantite.', 0, 0 )');  
						
		if($sql)
		 Db::getInstance()->ExecuteS('INSERT IGNORE INTO '._DB_PREFIX_.'stock_available
						(id_product, id_product_attribute, id_shop, id_shop_group, quantity, depends_on_stock, out_of_stock)
						VALUES ('.$id_product.', 0, '.$id_shop.',  0, 0, 0, 0 )');
						
	}
	
	public function calculQuantity($id_product)
	{	
	
	 $sumQuantiy = Db::getInstance()->getValue('SELECT sum(quantity) FROM '._DB_PREFIX_.'stock_available WHERE id_product='.$id_product.'');
	
	  Db::getInstance()->ExecuteS('UPDATE '._DB_PREFIX_.'stock_available SET quantity = '.$sumQuantiy.'
                               WHERE id_product_attribute=0
							   AND id_product='.$id_product.''); 
						
	}
	
	public function getParamEl($name)
	{
		return Db::getInstance()->getValue('SELECT `value` FROM `'._DB_PREFIX_.'pro_elegend_param` WHERE name="'.pSQL($name).'"');
	}
	
	
	
	
	/**
	 * mettreajourCatalog Insertion catalogue
	 *
	 * @param string $id_category id de la categorie	 *
	 * @return true
	 */ 
	
	

   public function _combinations($id_product_att,$id_product)
	{		
		
		$row = $this->getApi();		
		define('DEBUG', false);
        define('PS_SHOP_PATH', $row['adresse']);
        define('PS_WS_AUTH_KEY', $row['cleapi']);


	try
	{	

		$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);       	
		$optcom = array('resource' => 'combinations');
		$optcom['id'] = (int)$id_product_att; 
		$xmlcom = $webService->get($optcom);	
		$com = $xmlcom->children()->children();
		
		$opt_stock = array('url' => PS_SHOP_PATH . 'api/stock_availables/&filter[id_product_attribute]='.(int)$id_product_att);		
		$xml_stock = $webService->get($opt_stock);	
		$stock = $xml_stock->children()->children();
		
	
	    // insertion  sous categorie
		
		if ($com->default_on > 0) {
			$data['default_on'] = 1;
			$dataShop['default_on'] = 1;
						
		}else{
			$data['default_on'] === NULL;
			$dataShop['default_on'] === NULL;
		}
		
		// insertion  table product_attribute 
		$idProduct = $id_product;
		$id_product_attribute =	$this->getMaxIdProductAttribut();
		foreach ($stock as $resource)
		       {				  

			   $xml_o = $webService->get(array('url' => PS_SHOP_PATH.'api/stock_availables/'.$resource->attributes()));
			   $resources_o =  $xml_o->children()->children();	
			   $el_qty = $resources_o->quantity;
			   }
			   $el_stock = $this->getParamEl('EL_STOCK');
		if($el_qty > $el_stock){ 
		$quantity = $el_stock;
		}else{
		$quantity = $el_qty;  
		}
		
		
		$price_tmp = $com->price;		
		$price = ($price_tmp)+($price_tmp*20/100);
		$price = number_format(round($price, 2),2, '.', '');
		
		
		$data['id_product_attribute'] = $id_product_attribute;
		$data['id_product'] = $idProduct;
		$data['reference'] = $com->reference;
		$data['supplier_reference'] = $com->supplier_reference;
        $data['ean13'] = $com->ean13;
        $data['upc'] = $com->upc;	
		$data['wholesale_price'] = $wholesale_price;	
		$data['price'] = $price;	
		$data['quantity'] = $quantity;		
		$data['weight'] = $com->weight;
        $data['minimal_quantity'] = '1';

		
		$dataShop['id_product'] = $idProduct;
		$dataShop['id_product_attribute'] = $id_product_attribute;
        $dataShop['wholesale_price'] = $wholesale_price;	
		$dataShop['price'] = $price;	
		$dataShop['id_shop'] = '1';	
		$dataShop['minimal_quantity'] = '1';	
		$dataShop['weight'] = $com->weight;

	

        if(!DB::getInstance()->insert('product_attribute', $data)) echo ('Error in product_attribute insert : '.$id_product_attribute);
       // if(!DB::getInstance()->insert('product_attribute_shop', $datal)) die('Error in category lang insert : '.$id);
        if(!DB::getInstance()->insert('product_attribute_shop', $dataShop)) echo ('Error in product_attribute_shop insert : '.$id_product_attribute);		

         $attributs = $com->associations->product_option_values->product_option_value;

        if (count($attributs)) {
         foreach ( $attributs as $value ) {
		
		//$id_attribut = $com->associations->product_option_values->product_option_values;
		$optatt = array('url' => PS_SHOP_PATH . 'api/product_option_values/'.$value->id);			
		$xmlatt = $webService->get($optatt);	
		$att = $xmlatt->children()->children();
		
		
		
		$name_attribut = pSQL($att->name->language[0][0]);
		$rec_id_attribut = $this->getAttribut($name_attribut);

			//$rec_id_group_attribut = $this->getGroupAttribut($rec_id_attribut);
		  if(!Db::getInstance()->ExecuteS('INSERT INTO '._DB_PREFIX_.'product_attribute_combination (id_attribute, id_product_attribute) 
		  VALUES ('.$rec_id_attribut.', '.$id_product_attribute.')')) echo ('Error in product_attribute_combination insert : '.$id_product_attribute);		

		;
		
	}
		}
		
		$this->setQuantity($idProduct,$id_product_attribute,1,$quantity);

		
		
	}
	catch (PrestaShopWebserviceException $e)
	{
					
			$trace = $e->getTrace();
			if ($trace[0]['args'][0] == 404) echo 'Bad ID Combination';
			else if ($trace[0]['args'][0] == 401) echo 'Bad auth key Combination';
			else echo 'Other error'.$e->getMessage().'</b>';
	}			
		
		return true;
	}
	
	
} //Fin class Catalog
?>